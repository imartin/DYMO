
#ifndef PROGRAMA_V4_CONSTANT_DEFINITION_H
#define PROGRAMA_V4_CONSTANT_DEFINITION_H



#define EXIT_RREP_RECEIVED 1000
#define EXIT_RREQ_WAIT_TIME_FINISH 1001
#define EXIT_RREP_ACK_RECEIVED 1020
#define EXIT_RREP_ACK_SENT_FINISH 1021

/*------Monitors-------*/
#define MESSAGE_QUEUE_EXTERNAL_INTERNAL 0
#define MESSAGE_QUEUE_ACK_to_EXTERNAL 1
#define NUMBER_OF_MESSAGE_QUEUES 2

#define TIMER_ON 1
#define TIMER_OFF 2
#define TIMER_OUT 3

#define MIN 0



#define MAXUNREACHABLELIST 20
#define MAX_THREAD_EXTERNAL 10

#define ALIVE 1
#define INACTIVE 0
#define IPV4 4 //numero de octetos de la direccion IPv4
#define UDP_PORT 269
#define QUEUE_LENGHT_PACKET 1

#define IPBITS (sizeof(u_int32_t) * 8)

/*----------TIMERS-------Pag.62*/
#define ACTIVE_INTERVAL 5
#define MAX_IDLETIME 200
#define MAX_BLACKLIST_TIME 10 /*OJO SON 200 , para pruebas se pone 20s*/
#define MAX_SEQNUM_LIFETIME 10
#define RERR_TIMEOUT 3

#define T_McMsg_ENTRY_TIME 12
#define RREQ_WAIT_TIME 2
#define RREP_ACK_SENT_TIMEOUT 1
#define RREQ_HOLDDOWN_TIME 10
/*------------------------------*/

/*----------Protocol Constants----------Pag.64*/
#define DISCOVERY_ATTEMPTS_MAX 3
#define RREP_RETRIES 2
#define MAX_METRIC_HopCount 255
#define MAX_HOPCOUNT 20
#define EXPONENTIAL_BACKOFF 2
/*---------------------------------*/
/*---------Message Type Allocation------Pag.66*/
#define RREQ 224
#define RREP 225
#define RERR 226
#define RREP_ACK 227
/*-------------------------------------*/

/*-----Message TLV Types--------------- Pag.66*/
#define ACK_REQ 128
/*------------------------------------------------*/

/*-----IPAddress Block TLV Type Allocation----Pag.67*/
#define PATH_METRIC 129
#define SEQ_NUM 130
#define ADDRESS_TYPE 131
/*------------------------------------------------*/

/*-----ADDRESS_TYPE TLV Values--------------Pag.67*/
#define ORIGPREFIX 0
#define TARGPREFIX 1
#define UNREACHABLE 2
#define PKTSOURCE 3
#define UNSPECIFIED 225
/*-------------------------------------------------*/
#define ICMP_TYPE3_CODE0 0
#define ICMP_TYPE3_CODE1 1


//Posibles estados para Neighbor.sate
//Posibles estados para localroute.State
enum f_gman { ZERO, ONE }; //Posibles estados para los flags de Pgman packet

#define MAXBUFFER  // tama\Uffffffffel buffer de recepcion



#define INFINITY_TIME 10000000000000000



#define SIZE_ARRAY_TLV_RREP_ACK  1
#define SIZE_ARRAY_TLV_RREQ  5
#define SIZE_ARRAY_TLV_RREP  4

#define VERSION 0




#define MAX_LIST_ADDRESS_BLOCK 20


#define UNASSIGNED 0    //MetricType  tipos de metrica usados en AODVv2
#define HOP_COUNT 1


#define HEAD 0  //tipos de array para direcciones de address block
#define TAIL 1
#define MID 2
#define PREFIX 3

#define MICRO 1000000

#endif //PROGRAMA_V4_CONSTANT_DEFINITION_H
