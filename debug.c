#include "main.h"

#ifdef _DEBUG
#include <stdio.h>
#include <stdarg.h>
#endif

#include "debug.h"

void printd(const char * msg, ...) {
#ifdef _DEBUG
    va_list argptr;
    va_start(argptr, msg);
    vfprintf(stderr, msg, argptr);
    va_end(argptr);
#endif
}