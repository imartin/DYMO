//
// Created by root on 1/04/18.
//

#include "Neighbor.h"
#include <stdio.h>
#include <string.h>
#include "../global_structs.h"

void add_Neighbor_Position(T_Neighbor *Neighbor,int Position){

    Neighbor->Position = Position;
}
void add_Neighbor_IPaddress(T_Neighbor *Neighbor,uint32_t IPAddress){

    Neighbor->IPAddress = IPAddress;
}
void add_Neighbor_State(T_Neighbor *Neighbor,enum n_state State){

    Neighbor->State = State;
}
void add_Neighbor_Timeout(T_Neighbor *Neighbor,long long int Timeout ){

    Neighbor->Timeout = Timeout;
}
void add_Neighbor_Interface(T_Neighbor *Neighbor, char Interface[256]){

    strcpy(Neighbor->Interface,Interface);
}

int get_Neighbor_Position(T_Neighbor *Neighbor){

    return Neighbor->Position;
}
uint32_t get_Neighbor_IPAddress(T_Neighbor *Neighbor){

    return Neighbor->IPAddress;
}
enum n_state get_Neighbor_State(T_Neighbor *Neighbor){

    return Neighbor->State;
}
long long int get_Neighbor_Timeout(T_Neighbor *Neighbor){

    return Neighbor->Timeout;
}
char *get_Neighbor_Interface(T_Neighbor *Neighbor){

    return Neighbor->Interface;
}


void print_Neighbor(T_Neighbor * Neighbor) {

    printf("|IPAddress   |%s\n", Int32_To_String(get_Neighbor_IPAddress(Neighbor)));
    printf("|State     |%s\n", Int_To_StringN(get_Neighbor_State(Neighbor)));
    printf("|TimeOut:  |%lld\n",get_Neighbor_Timeout(Neighbor));
    printf("|Interface |%s\n",get_Neighbor_Interface(Neighbor));
    printf("|Position  |%d\n",get_Neighbor_Position(Neighbor));
    printf("|----------|\n");
}