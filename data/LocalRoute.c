//
// Created by root on 1/04/18.
//

#include "LocalRoute.h"
#include <stdio.h>
#include <string.h>
#include "../global_structs.h"

uint32_t get_LocalRoute_Address(T_LocalRoute *LocalRoute){

    return LocalRoute->Address;
}
uint8_t get_LocalRoute_Prefixlength(T_LocalRoute *LocalRoute){

    return LocalRoute->PrefixLength;
}
uint16_t get_LocalRoute_Seqnum(T_LocalRoute *LocalRoute){

    return LocalRoute->SeqNum;
}
uint32_t get_LocalRoute_Nexthop(T_LocalRoute *LocalRoute){

    return LocalRoute->NextHop;
}
char * get_LocalRoute_NextHopInterface(T_LocalRoute *LocalRoute){

    return LocalRoute->NextHopInterface;
}
long long int   get_LocalRoute_Lastused(T_LocalRoute *LocalRoute){

    return LocalRoute->LastUsed;
}
long long int   get_LocalRoute_Lastseqnumupdate(T_LocalRoute *LocalRoute){

    return LocalRoute->LastSeqNumUpdate;
}
uint8_t  get_LocalRoute_Metrictype(T_LocalRoute *LocalRoute){

    return LocalRoute->MetricType;
}
uint8_t  get_LocalRoute_Metric(T_LocalRoute *LocalRoute){

    return LocalRoute->Metric;
}
enum l_state get_LocalRoute_State(T_LocalRoute *LocalRoute){

    return LocalRoute->State;
}
int get_LocalRoute_Position(T_LocalRoute *LocalRoute){

    return LocalRoute->Position;
}


void add_LocalRoute_Address(T_LocalRoute *LocalRoute,uint32_t address ){

    LocalRoute->Address = address;
}
void add_LocalRoute_Prefixlength(T_LocalRoute *LocalRoute,uint8_t prefixlength){

    LocalRoute->PrefixLength = prefixlength;
}
void add_LocalRoute_Seqnum(T_LocalRoute *LocalRoute,uint16_t seqnum){

    LocalRoute->SeqNum = seqnum;
}
void add_LocalRoute_Nexthop(T_LocalRoute *LocalRoute,uint32_t nexthop){

    LocalRoute->NextHop = nexthop;
}
void add_LocalRoute_NextHopInterface(T_LocalRoute *LocalRoute,char NextHopInterface[256]){

    strcpy(LocalRoute->NextHopInterface,NextHopInterface);
}
void add_LocalRoute_Lastused(T_LocalRoute *LocalRoute,long long int lastused){

    LocalRoute->LastUsed = lastused;
}
void add_LocalRoute_Lastseqnumupdate(T_LocalRoute *LocalRoute,long long int lastseqnumupdate){

    LocalRoute->LastSeqNumUpdate = lastseqnumupdate;
}
void add_LocalRoute_Metrictype(T_LocalRoute *LocalRoute,uint8_t metrictype){

    LocalRoute->MetricType = metrictype;
}
void add_LocalRoute_Metric(T_LocalRoute *LocalRoute,uint8_t metric){

    LocalRoute->Metric = metric;
}
void add_LocalRoute_State(T_LocalRoute *LocalRoute, enum l_state state){

    LocalRoute->State = state;
}
void add_LocalRoute_Position(T_LocalRoute *LocalRoute, int position){

    LocalRoute->Position = position;
}

T_Timer *get_LocalRoute_TimerMaxSeqNumLifeTime (T_LocalRoute *LocalRoute){

    return LocalRoute->timer_MAX_SEQNUM_LIFETIME;
}
void add_LocalRoute_TimerMaxSeqNumLifeTime (T_LocalRoute *LocalRoute , T_Timer *Timer){

    LocalRoute->timer_MAX_SEQNUM_LIFETIME = Timer;
}
T_Timer *get_LocalRoute_TimerActiveInterval (T_LocalRoute *LocalRoute){

    return LocalRoute->timer_ACTIVE_INTERVAL;
}
void add_LocalRoute_TimerActiveInterval(T_LocalRoute *LocalRoute , T_Timer *Timer){

    LocalRoute->timer_ACTIVE_INTERVAL = Timer;
}
void print_LocalRoute(T_LocalRoute *LocalRoute){


    printf("|IPAddress          |%s\n", Int32_To_String(get_LocalRoute_Address(LocalRoute)));
    printf("|PrefixLength        |%u\n",get_LocalRoute_Prefixlength(LocalRoute));
    printf("|SeqNum           |%u\n",get_LocalRoute_Seqnum(LocalRoute));
    printf("|Nexthop          |%s\n", Int32_To_String(get_LocalRoute_Nexthop(LocalRoute)));
    printf("|NextHopInterface |%s\n",get_LocalRoute_NextHopInterface(LocalRoute));
    printf("|LastUsed         |%lld\n",get_LocalRoute_Lastused(LocalRoute));
    printf("|LastSeqNumUpdate |%lld\n",get_LocalRoute_Lastseqnumupdate(LocalRoute));
    printf("|Metrictype       |%u\n",get_LocalRoute_Metrictype(LocalRoute));
    printf("|Metric           |%u\n",get_LocalRoute_Metric(LocalRoute));
    printf("|State            |%s\n", Int_To_StringL(get_LocalRoute_State(LocalRoute)));
    printf("|Position         |%d\n",get_LocalRoute_Position(LocalRoute));
    printf(" ------------------\n");
}