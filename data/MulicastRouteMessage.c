
#include "MulicastRouteMessage.h"
#include <string.h>
#include <stdio.h>
#include "../global_structs.h"


void add_McMsg_OrigPrefix(T_McMsg *McMsg,uint32_t OrigPrefix){

    McMsg->OrigPrefix = OrigPrefix;
}
void add_McMsg_OrigPrefixLen(T_McMsg *McMsg,uint8_t OrigPrefixLen){

    McMsg->OrigPrefixLen = OrigPrefixLen;
}
void add_McMsg_TargPrefix(T_McMsg *McMsg,uint32_t TargPrefix){

    McMsg->TargPrefix = TargPrefix;
}
void add_McMsg_OrigSeqNum(T_McMsg *McMsg,uint16_t OrigSeqNum){

    McMsg->OrigSeqNum = OrigSeqNum;
}
void add_McMsg_TargSeqNum(T_McMsg *McMsg,uint16_t TargSeqNum){

    McMsg->TargSeqNum = TargSeqNum;
}
void add_McMsg_MetricType(T_McMsg *McMsg,uint8_t MetricType){

    McMsg->MetricType = MetricType;
}
void add_McMsg_Metric(T_McMsg *McMsg,uint8_t Metric){

    McMsg->Metric = Metric;
}
void add_McMsg_TimeStamp(T_McMsg *McMsg,long long int Timestamp){

    McMsg->TimeStamp = Timestamp;
}
void add_McMsg_RemovalTime(T_McMsg *McMsg, long long int RemovalTime){

    McMsg->RemovalTime = RemovalTime;
}
void add_McMsg_Interface(T_McMsg *McMsg,char Interface[256]){

    strcpy(McMsg->Interface,Interface);
}
void add_McMsg_Position(T_McMsg *McMsg, int Position){

    McMsg->Position = Position;
}

uint32_t get_McMsg_OrigPrefix(T_McMsg *McMsg){

    return McMsg->OrigPrefix;
}
uint8_t get_McMsg_OrigPrefixLen(T_McMsg *McMsg){

    return McMsg->OrigPrefixLen;
}
uint32_t get_McMsg_TargPrefix(T_McMsg *McMsg){

    return McMsg->TargPrefix;
}
uint16_t get_McMsg_OrigSeqNum(T_McMsg *McMsg){

    return McMsg->OrigSeqNum;
}
uint16_t get_McMsg_TargSeqNum(T_McMsg *McMsg){

    return McMsg->TargSeqNum;
}
uint8_t get_McMsg_MetricType(T_McMsg *McMsg){

    return McMsg->MetricType;
}
uint8_t get_McMsg_Metric(T_McMsg *McMsg){

    return McMsg->Metric;
}
long long int get_McMsg_TimeStamp(T_McMsg *McMsg){

    return McMsg->TimeStamp;
}
long long int get_McMsg_RemoveTime(T_McMsg *McMsg){

    return McMsg->RemovalTime;
}
char * get_McMsg_Interface(T_McMsg *McMsg){

    return McMsg->Interface;
}
int get_McMsg_Position(T_McMsg *McMsg){

    return McMsg->Position;
}

void print_McMsg(T_McMsg *McMsg){


    printf("|OrigPrefix    |%s\n", Int32_To_String(get_McMsg_OrigPrefix(McMsg)));
    printf("|OrigPrefixLen |%u\n",get_McMsg_OrigPrefixLen(McMsg));
    printf("|TargPrefix    |%s\n", Int32_To_String(get_McMsg_TargPrefix(McMsg)));
    printf("|OrigSeqNum    |%u\n",get_McMsg_OrigSeqNum(McMsg));
    printf("|TargSeqNum    |%u\n",get_McMsg_TargSeqNum(McMsg));
    printf("|MetricType    |%u\n",get_McMsg_MetricType(McMsg));
    printf("|Metric        |%u\n",get_McMsg_Metric(McMsg));
    printf("|TimeStamp     |%lld\n",get_McMsg_TimeStamp(McMsg));
    printf("|RemovalTime    |%lld\n",get_McMsg_RemoveTime(McMsg));
    printf("|Interface     |%s\n",get_McMsg_Interface(McMsg));
    printf("|--------------|\n");
}