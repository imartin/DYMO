
#include "RouteError.h"
#include <stdio.h>
#include "../global_structs.h"

void add_RerrMsg_UnreachableAddress(T_RerrMsg * RerrMsg,uint32_t UnreachableAddress){

    RerrMsg->UnreachableAddress = UnreachableAddress;
}
void add_RerrMsg_PktSource(T_RerrMsg * RerrMsg,uint32_t PktSource){

    RerrMsg->PktSource = PktSource;
}
void add_RerrMsg_TimeOut(T_RerrMsg * RerrMsg,time_t TimeOut){

    RerrMsg->TimeOut = TimeOut;
}
void add_RerrMsg_Position(T_RerrMsg * RerrMsg,int Position){

    RerrMsg->Position = Position;
}


uint32_t get_RerrMsg_UnreachableAddress(T_RerrMsg * RerrMsg){

    return RerrMsg->UnreachableAddress;
}
uint32_t get_RerrMsg_PktSource(T_RerrMsg *RerrMsg){

    return RerrMsg->PktSource;
}
long long int get_RerrMsg_TimeOut(T_RerrMsg * RerrMsg){

    return RerrMsg->TimeOut;
}
int get_RerrMsg_Position(T_RerrMsg * RerrMsg){

    return RerrMsg->Position;
}

void print_RerrMsg(T_RerrMsg *RerrMsg){

    printf("|UnreachableAddress |%s\n", Int32_To_String(get_RerrMsg_UnreachableAddress(RerrMsg)));
    printf("|PktSource          |%s\n", Int32_To_String(get_RerrMsg_PktSource(RerrMsg)));
    printf("|TimeOut            |%lld\n",get_RerrMsg_TimeOut(RerrMsg));
    printf("|Position           |%d\n",get_RerrMsg_Position(RerrMsg));
    printf("|-------------------|\n");

}