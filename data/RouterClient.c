
#include "RouterClient.h"
#include <stdio.h>
#include "../global_structs.h"

void add_RouterClient_Address(T_RouterClient * RouterClient,uint32_t Address){

    RouterClient->IPAddress = Address;
}
void add_RouterClient_PrefixLength(T_RouterClient * RouterClient,uint8_t PrefixLen){

    RouterClient->PrefixLength = PrefixLen;
}
void add_RouterClient_Cost(T_RouterClient * RouterClient,uint8_t Cost){

    RouterClient->Cost = Cost;
}
void add_RouterClient_Position(T_RouterClient * RouterClient,int Position){

    RouterClient->Position = Position;
}


uint32_t get_RouterClient_Address(T_RouterClient * RouterClient){

    return RouterClient->IPAddress;
}
uint8_t get_RouterClient_PrefixLen(T_RouterClient *RouterClient){

    return RouterClient->PrefixLength;
}
uint8_t get_RouterClient_Cost(T_RouterClient * RouterClient){

    return RouterClient->Cost;
}
int get_RouterClient_Position(T_RouterClient * RouterClient){

    return RouterClient->Position;
}

void print_RouterClient(T_RouterClient *RouterClient){

    printf("|IPAddress   |%s        |\n", Int32_To_String(get_RouterClient_Address(RouterClient)));
    printf("|PrefixLength |%u              |\n", get_RouterClient_PrefixLen(RouterClient));
    printf("|Cost      |%u               |\n",get_RouterClient_Cost(RouterClient));
    printf("|---------------------------|\n");
}
