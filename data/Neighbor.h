#ifndef PROGRAMA_V4_Neighbor_H
#define PROGRAMA_V4_Neighbor_H

#include <stdint.h>
#include "../constant_definition.h"
enum n_state {HEARD, CONFIRMED, BLACKLISTED};
typedef struct{

    uint32_t IPAddress;
    enum n_state State;
    long long int Timeout;
    char Interface[256];
    int Position;
}T_Neighbor;

void add_Neighbor_Interface(T_Neighbor *Neighbor, char Interface[256]);
void add_Neighbor_Position(T_Neighbor *Neighbor, int Position);
void add_Neighbor_IPaddress(T_Neighbor *Neighbor,uint32_t IPAddress);
void add_Neighbor_State(T_Neighbor *Neighbor,enum n_state State);
void add_Neighbor_Timeout(T_Neighbor *Neighbor,long long int Timeout);
long long int get_Neighbor_Timeout(T_Neighbor *Neighbor);
char *get_Neighbor_Interface(T_Neighbor *Neighbor);
int get_Neighbor_Position(T_Neighbor *Neighbor);
uint32_t get_Neighbor_IPAddress(T_Neighbor *Neighbor);
enum n_state get_Neighbor_State(T_Neighbor *Neighbor);
void print_Neighbor(T_Neighbor *Neighbor);
#endif //PROGRAMA_V4_Neighbor_H