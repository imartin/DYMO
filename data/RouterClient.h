
#ifndef PROGRAMA_V4_ROUTER_CLIENT_H
#define PROGRAMA_V4_ROUTER_CLIENT_H

#include <stdint.h>

typedef struct{

    uint32_t IPAddress;
    uint8_t PrefixLength;
    uint8_t Cost;
    int Position;
}T_RouterClient;

void add_RouterClient_Address(T_RouterClient * RouterClient,uint32_t Address);
void add_RouterClient_PrefixLength(T_RouterClient * RouterClient, uint8_t PrefixLen);
void add_RouterClient_Cost(T_RouterClient * RouterClient,uint8_t Cost);
void add_RouterClient_Position(T_RouterClient * RouterClient,int Position);
uint32_t get_RouterClient_Address(T_RouterClient * RouterClient);
uint8_t get_RouterClient_PrefixLen(T_RouterClient *RouterClient);
uint8_t get_RouterClient_Cost(T_RouterClient * RouterClient);
int get_RouterClient_Position(T_RouterClient * RouterClient);

void print_RouterClient(T_RouterClient *RouterClient);
#endif //PROGRAMA_V4_ROUTER_CLIENT_H
