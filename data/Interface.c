//
// Created by root on 8/04/18.
//
#include "Interface.h"
#include <string.h>
#include <stdio.h>

void add_Interface_InterfaceId(T_Interface *Interface, char *InterfaceId){

    strcpy(Interface->InterfaceId,InterfaceId);
}
char *get_Interface_InterfaceId(T_Interface *Interface){

    return Interface->InterfaceId;
}
void add_Interface_position(T_Interface *Interface, int Position){

    Interface->Position = Position;
}
void print_Interface(T_Interface *Interface){

    printf("|Interface.Id| %s\n", get_Interface_InterfaceId(Interface));
    printf(" ------------");
}