//
// Created by root on 8/04/18.
//

#ifndef PROGRAMA_V4_INTERFACE_H
#define PROGRAMA_V4_INTERFACE_H

#include <stdint.h>

typedef struct{

    char InterfaceId[256];
    int Position;
}T_Interface;


void add_Interface_InterfaceId(T_Interface *Interface, char *InterfaceId);
char *get_Interface_InterfaceId(T_Interface *Interface);
void add_Interface_position(T_Interface *Interface, int Position);
void print_Interface(T_Interface *Interface);
#endif //PROGRAMA_V4_INTERFACE_H
