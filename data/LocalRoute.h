#ifndef PROGRAMA_V4_LocalRoute_H
#define PROGRAMA_V4_LocalRoute_H

#include <stdint.h>
#include "../monitors_pthreads_v3/timer.h"

enum l_state {UNCONFIRMED, IDLE, ACTIVE, INVALID};

typedef struct{

    uint32_t Address;
    uint8_t PrefixLength;
    uint16_t SeqNum;
    uint32_t NextHop;
    char NextHopInterface[256];
    long long int LastUsed;
    long long int LastSeqNumUpdate;
    uint8_t MetricType;
    uint8_t Metric;
    enum l_state State;
    int Position;
    T_Timer *timer_MAX_SEQNUM_LIFETIME;
    T_Timer *timer_ACTIVE_INTERVAL;
}T_LocalRoute;

T_Timer *get_LocalRoute_TimerMaxSeqNumLifeTime (T_LocalRoute *LocalRoute);
void add_LocalRoute_TimerMaxSeqNumLifeTime (T_LocalRoute *LocalRoute , T_Timer *Timer);
T_Timer *get_LocalRoute_TimerActiveInterval (T_LocalRoute *LocalRoute);
void add_LocalRoute_TimerActiveInterval(T_LocalRoute *LocalRoute , T_Timer *Timer);
uint32_t get_LocalRoute_Address(T_LocalRoute *LocalRoute);
uint8_t get_LocalRoute_Prefixlength(T_LocalRoute *LocalRoute);
uint16_t get_LocalRoute_Seqnum(T_LocalRoute *LocalRoute);
uint32_t get_LocalRoute_Nexthop(T_LocalRoute *LocalRoute);
char * get_LocalRoute_NextHopInterface(T_LocalRoute *LocalRoute);
long long int get_LocalRoute_Lastused(T_LocalRoute *LocalRoute);
long long int get_LocalRoute_Lastseqnumupdate(T_LocalRoute *LocalRoute);
uint8_t get_LocalRoute_Metrictype(T_LocalRoute *LocalRoute);
uint8_t get_LocalRoute_Metric(T_LocalRoute *LocalRoute);
enum l_state get_LocalRoute_State(T_LocalRoute *LocalRoute);
int get_LocalRoute_Position(T_LocalRoute *LocalRoute);


void add_LocalRoute_Address(T_LocalRoute *LocalRoute,uint32_t address );
void add_LocalRoute_Prefixlength(T_LocalRoute *LocalRoute,uint8_t prefixlength);
void add_LocalRoute_Seqnum(T_LocalRoute *LocalRoute,uint16_t seqnum);
void add_LocalRoute_Nexthop(T_LocalRoute *LocalRoute,uint32_t nexthop);
void add_LocalRoute_NextHopInterface(T_LocalRoute *LocalRoute,char NextHopInterface[256]);
void add_LocalRoute_Lastused(T_LocalRoute *LocalRoute,long long int lastused);
void add_LocalRoute_Lastseqnumupdate(T_LocalRoute *LocalRoute,long long int lastseqnumupdate);
void add_LocalRoute_Metrictype(T_LocalRoute *LocalRoute,uint8_t metrictype);
void add_LocalRoute_Metric(T_LocalRoute *LocalRoute,uint8_t metric);
void add_LocalRoute_State(T_LocalRoute *LocalRoute, enum l_state state);
void add_LocalRoute_Position(T_LocalRoute *LocalRoute, int position);

void print_LocalRoute(T_LocalRoute *LocalRoute);
#endif //PROGRAMA_V4_LocalRoute_H
