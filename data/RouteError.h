
#ifndef ROUTEERROR_H
#define ROUTEERROR_H

#include <stdint.h>
#include <time.h>

typedef struct{

    long long int TimeOut;
    uint32_t UnreachableAddress;
    uint32_t PktSource;
    int Position;
}T_RerrMsg;

void add_RerrMsg_UnreachableAddress(T_RerrMsg *RerrMsg,uint32_t UnreachableAddress);
void add_RerrMsg_TimeOut(T_RerrMsg *RerrMsg, time_t TimeOut);
void add_RerrMsg_PktSource(T_RerrMsg *RerrMsg,uint32_t PktSource);
void add_RerrMsg_Position(T_RerrMsg *RerrMsg,int Position);
uint32_t get_RerrMsg_UnreachableAddress(T_RerrMsg *RerrMsg);
uint32_t get_RerrMsg_PktSource(T_RerrMsg *RerrMsg);
long long int get_RerrMsg_TimeOut(T_RerrMsg *RerrMsg);
int get_RerrMsg_Position(T_RerrMsg *RerrMsg);
void print_RerrMsg(T_RerrMsg *RerrMsg);



#endif
