
#ifndef PROGRAMA_V4_MULICAST_ROUTE_MESSAGE_H
#define PROGRAMA_V4_MULICAST_ROUTE_MESSAGE_H

#include <stdint.h>

typedef struct{

    uint32_t OrigPrefix;
    uint8_t OrigPrefixLen;
    uint32_t TargPrefix;
    uint16_t OrigSeqNum;
    uint16_t TargSeqNum;
    uint8_t MetricType;
    uint8_t Metric;
    long long int TimeStamp;
    long long int RemovalTime;
    char Interface[256];
    int Position;
}T_McMsg;

void add_McMsg_OrigPrefix(T_McMsg *McMsg,uint32_t OrigPrefix);
void add_McMsg_OrigPrefixLen(T_McMsg *McMsg,uint8_t OrigPrefixLen);
void add_McMsg_TargPrefix(T_McMsg *McMsg,uint32_t TargPrefix);
void add_McMsg_OrigSeqNum(T_McMsg *McMsg,uint16_t OrigSeqNum);
void add_McMsg_TargSeqNum(T_McMsg *McMsg,uint16_t TargSeqNum);
void add_McMsg_MetricType(T_McMsg *McMsg,uint8_t MetricType);
void add_McMsg_Metric(T_McMsg *McMsg,uint8_t Metric);
void add_McMsg_TimeStamp(T_McMsg *McMsg,long long int Timestamp);
void add_McMsg_RemovalTime(T_McMsg *McMsg, long long int RemovalTime);
void add_McMsg_Interface(T_McMsg *McMsg,char Interface[256]);
void add_McMsg_Position(T_McMsg *McMsg, int Position);


uint32_t get_McMsg_OrigPrefix(T_McMsg *McMsg);
uint8_t get_McMsg_OrigPrefixLen(T_McMsg *McMsg);
uint32_t get_McMsg_TargPrefix(T_McMsg *McMsg);
uint16_t get_McMsg_OrigSeqNum(T_McMsg *McMsg);
uint16_t get_McMsg_TargSeqNum(T_McMsg *McMsg);
uint8_t get_McMsg_MetricType(T_McMsg *McMsg);
uint8_t get_McMsg_Metric(T_McMsg *McMsg);
long long int get_McMsg_TimeStamp(T_McMsg *McMsg);
long long int get_McMsg_RemoveTime(T_McMsg *McMsg);
char * get_McMsg_Interface(T_McMsg *McMsg);
int get_McMsg_Position(T_McMsg *McMsg);
void print_McMsg(T_McMsg *McMsg);

#endif //PROGRAMA_V4_MULICAST_ROUTE_MESSAGE_H
