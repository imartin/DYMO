//
// Created by root on 22/03/18.
//

#include "Network_Interface_Status.h"
#include <asm/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include "../global_structs.h"

#define LINK_UP 1
#define LINK_DOWN (-1)
#define LINK_UNDEFINED 0

int check_if_up(int sockint, int * current_dev_status, T_DataPoint *DataPoint);


void *callback_thread_NetworkStatus(void *arg){

    T_DataPoint *DataPoint=(T_DataPoint *) arg;
    fd_set rfds, wfds;
    struct timeval tv;
    int retval;
    int nl_socket= DataPoint->NlSocket;
    int current_dev_status = LINK_UNDEFINED;

    while (1) {

        FD_ZERO (&rfds);
        FD_CLR (nl_socket, &rfds);
        FD_SET (nl_socket, &rfds);

        tv.tv_sec = 10;
        tv.tv_usec = 0;

        retval = select (FD_SETSIZE, &rfds, NULL, NULL, &tv);
        if (retval == (-1))
            printf ("Error select() \n");
        else if (retval)  {
            //printf ("Event recieved >> ");
            check_if_up (nl_socket, &current_dev_status, DataPoint);
        }else{
            //printf ("## Select TimedOut ## \n");
        }
    }
    return 0;
}
int check_if_up(int sockint, int * current_dev_status, T_DataPoint *DataPoint){

    int status;
    int ret = 0;
    char buf[4096];
    struct iovec iov = { buf, sizeof buf };
    struct sockaddr_nl snl;
    struct msghdr msg = { (void *) &snl, sizeof snl, &iov, 1, NULL, 0, 0 };
    struct nlmsghdr *h;
    struct ifinfomsg *ifi;

    status = recvmsg (sockint, &msg, 0);

    if (status < 0){
        /* Socket non-blocking so bail out once we have read everything */
        if (errno == EWOULDBLOCK || errno == EAGAIN) {
            return ret;
        }
        /* Anything else is an error */
        printf ("read_netlink: Error recvmsg: %d\n", status);
        perror ("read_netlink: Error: ");
        return status;
    }

    // We need to handle more than one message per 'recvmsg'
    for (h = (struct nlmsghdr *) buf; NLMSG_OK (h, (unsigned int) status); h = NLMSG_NEXT (h, status)) {

        //Finish reading
        if (h->nlmsg_type == NLMSG_DONE) {
            return ret;
        }
        // Message is some kind of error
        if (h->nlmsg_type == NLMSG_ERROR) {
            printf("read_netlink: Message is an error - decode TBD\n");
            return -1;        // Error
        }

        if (h->nlmsg_type == RTM_NEWLINK) {

            ifi = NLMSG_DATA (h);
            int link_up = ifi->ifi_flags & IFF_UP;
            if ((link_up && *current_dev_status != LINK_UP) || (!link_up && *current_dev_status != LINK_DOWN)) {

                if (link_up) {

                    *current_dev_status = LINK_UP;
                } else {

                    ini_DataPoint(DataPoint);
                    printf("NETWORK STATUS : EMPTY TABLES\n");
                    *current_dev_status = LINK_DOWN;
                }
                printf("NETLINK::%s\n", link_up ? "Up" : "Down");
            }
        }
    }
    return ret;
}