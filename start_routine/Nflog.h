#ifndef NFLOG_H
#define NFLOG_H

#include <libnetfilter_log/libnetfilter_log.h>


void *callback_thread_LogIcmp(void *arg);
void print_pkt(struct nflog_data *nfa);
void print_ip_header(uint8_t *buffer);
void print_icmp_packet( char* Buffer , int size);
void PrintData ( char* data , int size);

uint32_t get_invalid_route(struct nflog_data *nfa);

#endif