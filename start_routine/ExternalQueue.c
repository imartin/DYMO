#include "ExternalQueue.h"
#include <stdio.h>
#include <pthread.h>
#include <netinet/in.h>
#include <linux/netfilter.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <containers/RREP_container.h>
#include <global_structs.h>
#include "../operative_system/operative_system_functions.h"
#include "../monitors_pthreads_v3/thread_queue_manager.h"
#include "../event_definition.h"
#include "../global_structs.h"
#include "../RFC5444/gman_packet_parse.h"
#include "../processes/RERR_processes.h"
#include "../processes/RREQ_processes.h"
#include "../processes/RREP_processes.h"

int callback_external(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data);

int external_process(struct nfq_data *nfa, T_DataPoint *DataPoint, int *veredict_flag_ex);

int exit_external_process(int decision, int *veredict_flag_ex, int id_packet, T_PointParsed *pointers_parse);

void *callback_thread_External(void *arg) {

    T_DataPoint *DataPoint = (T_DataPoint *) arg;
    struct nfq_handle *h;
    struct nfq_q_handle *qh;
    char buf[4096] __attribute__ ((aligned));
    int fd;
    int rv;
    h = nfq_open();
    if (!h) {

        fprintf(stderr, "cannot open nfq_open()\n");
        return NULL;
    }
    //unbinding previous procfs
    if (nfq_unbind_pf(h, AF_INET) < 0) {

        fprintf(stderr, "error during nfq_unbind_pf()\n");
        return NULL;
    }
    //binding the netlink procfs
    if (nfq_bind_pf(h, AF_INET) < 0) {

        fprintf(stderr, "error during nfq_bind_pf()\n");
        return NULL;
    }
    //connet the thread for specific socket

    qh = nfq_create_queue(h, 0, &callback_external, DataPoint);
    if (!qh) {

        fprintf(stderr, "error during nfq_create_queue()\n");
        return NULL;
    }
    //set the queue for copy mode
    if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {

        fprintf(stderr, "can't set packet_copy mode\n");
        return NULL;
    }
    //getting the file descriptor
    fd = nfq_fd(h);
    while ((rv = (int) recv(fd, buf, sizeof(buf), 0)) && rv >= 0) {

        nfq_handle_packet(h, buf, rv);
    }
    nfq_destroy_queue(qh);
    nfq_close(h);
    pthread_exit(0);
}

int callback_external(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data) {

    T_DataPoint *DataPoint = (T_DataPoint *) data;
    int veredict_flag_ex;
    int id = external_process(nfa, DataPoint, &veredict_flag_ex);

    return nfq_set_verdict(qh, (u_int32_t) id, veredict_flag_ex == 0 ? NF_ACCEPT : NF_DROP , 0, NULL);
}

int external_process(struct nfq_data *nfa, T_DataPoint *DataPoint, int *veredict_flag_ex) {

    int prev=0, err, id;
    int id_packet;
    uint32_t ipsource;
    uint32_t ipdest;
    T_DataPacket DataPacket;
    T_RREQ rreq;
    T_RREP rrep;
    T_RERR rerr;
    T_ADVRTE advrte_container, advrte;
    T_PointParsed pointers_parse;
    T_LocalRouteSet invalid_routes;
    T_RerrPoint RerrPoint;

    get_DataPacket(nfa, &DataPacket);
    id_packet = get_DataPacket_IdPacket(&DataPacket);
    ipsource = get_DataPacket_sourceIP(&DataPacket);
    ipdest = get_DataPacket_destIP(&DataPacket);
    pointer_parsed_NULL(&pointers_parse);

    if (DataPacket.PayLoad[0] != 0) {

        printf("EXTERNAL: MESSAGE INVALID\n");
        return exit_external_process(ONE, veredict_flag_ex, id_packet, &pointers_parse);
    }
    if (DataPacket.PayLoad[1] == RREQ) {

        printf("EXTERNAL: RREQ RECIVED: %s\n", Int32_To_String(ipsource));
        prev = RREQ_ReceptionProcess(DataPoint,&advrte,&DataPacket,&rreq,&pointers_parse);
        if (prev == GENERATE_RREP) {

            err = send_RREP_Packet(&rreq, DataPoint);
            if ( err == FAIL_OPERATION) {

                printf("EXTERNAL: send_RREP_Packet FAIL\n");
                return exit_external_process(ONE, veredict_flag_ex, id_packet, &pointers_parse);
            }
            else if( err == Neighbor_UPDATE_BLACKLISTED){

                printf ("EXTERNAL: RETRY SEND RREP\n");
                err = send_RREP_Packet(&rreq, DataPoint);
                if(err != CORRECT_OPERATION){

                    printf("EXTERNAL : %s\n",Int_To_StringEvent(err));
                    return exit_external_process(ONE, veredict_flag_ex, id_packet, &pointers_parse);
                }

            }else{

                printf("EXTERNAL: SEND RREP\n");
                return exit_external_process(ONE, veredict_flag_ex, id_packet, &pointers_parse);
            }
        }
        if (prev == IGNORE_MESSAGE) {

            printf("EXTERNAL: RREQ DROP: %s\n", Int32_To_String(ipsource));
        }
        if (prev == FAIL_OPERATION) {

            printf("EXTERNAL: rcv_RREQ_Packet FAIL\n");
        }
        if (prev == FORWARDING_RREQ) {

            printf(forwar_RREQ_Packet(&rreq, DataPoint) != CORRECT_OPERATION ? "EXTERNAL:forwar_RREQ_Packet FAIL\n" : "EXTERNAL: RREQ FORWARDING\n");
        }
    }
    if (DataPacket.PayLoad[1] == RREP) {

        printf("EXTERNAL: RREP RECIVED:%s \n", Int32_To_String(ipsource));
        prev = RREP_ReceptionProcess(DataPoint,&DataPacket,&rrep,&advrte,&pointers_parse);
        if(prev == ROUTE_DISCOVERY_FINISH) {

            ThreadMessage *message = create_message(0,EXIT_RREP_RECEIVED,NULL);
            put_message(message,MESSAGE_QUEUE_EXTERNAL_INTERNAL);
        }else if(prev == FORWARDING_RREP) {

            err = forwarding_rrep_packet(&rrep, DataPoint);
            if(err == CORRECT_OPERATION) {

                printf("EXTERNAL : FORWARDING RREP\n");
            }
            else if(err == GENERATE_RERR){

                printf("External : GENERATE RERR\n");
                add_DataPoint_AddressList(DataPoint,get_RREP_AddressList(&rrep));
                add_DataPoint_OrigPrefixRREP(DataPoint,Int8_To_32(rrep.AddressList.OrigPrefix));
                err = RERR_GenerationProcess(DataPoint,&DataPacket,&RerrPoint,2);
                if( err != CORRECT_OPERATION){

                    printf("External : RERR_GenerationProcess FAIL\n");
                }
            }else{
                printf("EXTERNAL : %s\n",Int_To_StringEvent(err));
            }

        }else if(prev == IGNORE_MESSAGE) {

            printf("EXTERNAL: RREP DROP:%s\n", Int32_To_String(ipsource));
        }else if(prev == GENERATE_RERR){

            null_RERR(&RerrPoint);
            add_DataPoint_AddressList(DataPoint,get_RREP_AddressList(&rrep));
            err = RERR_GenerationProcess(DataPoint,&DataPacket,&RerrPoint,2);
            if(err != CORRECT_OPERATION){

                printf("EXTERNAL : RERR_GenerationProcess FAIL\n");
            }
            free_RERR(&RerrPoint);
            null_RERR(&RerrPoint);
        }
    }
    if (DataPacket.PayLoad[1] == RERR) {

        printf("EXTERNAL: RERR RECIVED: %s\n", Int32_To_String(ipsource));
        err = rcv_RERR_Packet(&rerr, DataPoint, &DataPacket, &invalid_routes, &pointers_parse);
        if (err == REGENERATE_RERR) {

            err = RERR_ReGenerationProcess(DataPoint, &rerr, &invalid_routes);
            if (err != CORRECT_OPERATION) {

                printf("EXTERNAL: RERR_ReGenerationProcess FAIL : %s\n", Int_To_StringEvent(err));
            }
        }
        if (err == IGNORE_MESSAGE) {

            printf("EXTERNAL : IGNORE RERR from %s\n", Int32_To_String(ipsource));
        }
    }
    id = exit_external_process(ONE, veredict_flag_ex, id_packet, &pointers_parse);
    return id;
}

int exit_external_process(int decision, int *veredict_flag_ex, int id_packet, T_PointParsed *pointers_parse) {

    pointer_parsed_FREE(pointers_parse);
    pointer_parsed_NULL(pointers_parse);
    *veredict_flag_ex = decision;
    return id_packet;
}
	      
	  
	    
	    
	
