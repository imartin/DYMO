#include "ACK_activity.h"
#include <stdio.h>
#include <pthread.h>
#include <netinet/in.h>
#include <linux/netfilter.h>
#include <unistd.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <global_structs.h>
#include "../RFC5444/gman_packet_parse.h"
#include "../monitors_pthreads_v3/thread_queue_manager.h"
#include "../operative_system/operative_system_functions.h"
#include "../event_definition.h"
#include "../global_structs.h"

#include "../processes/RREP_ACK_processes.h"

int callback_ack(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data);
void *callback_thread_LogAck(void *arg) {
    T_DataPoint *DataPoint = (T_DataPoint *) arg;

    struct nfq_handle *h;
    struct nfq_q_handle *qh;
    char buf[4096] __attribute__ ((aligned));

    int fd;
    int rv;


    h = nfq_open();
    if (!h) {
        fprintf(stderr, "cannot open nfq_open()\n");
        return NULL;
    }
    //unbinding previous procfs
    if (nfq_unbind_pf(h, AF_INET) < 0) {
        fprintf(stderr, "error during nfq_unbind_pf()\n");
        return NULL;
    }
    //binding the netlink procfs
    if (nfq_bind_pf(h, AF_INET) < 0) {
        fprintf(stderr, "error during nfq_bind_pf()\n");
        return NULL;
    }
    //connet the thread for specific socket

    qh = nfq_create_queue(h, 4, &callback_ack, DataPoint);
    if (!qh) {
        fprintf(stderr, "error during nfq_create_queue()\n");
        return NULL;
    }
    //set the queue for copy mode
    if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
        fprintf(stderr, "can't set packet_copy mode\n");
        return NULL;
    }

    //getting the file descriptor
    fd = nfq_fd(h);
    while ((rv = (int) recv(fd, buf, sizeof(buf), 0)) && rv >= 0) {
        nfq_handle_packet(h, buf, rv);
    }
    nfq_destroy_queue(qh);
    nfq_close(h);
    pthread_exit(0);
}
int callback_ack(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *nfa, void *data) {


    int err;
    T_DataPacket DataPacket;
    T_PointParsed pointers_parse;
    T_DataPoint *DataPoint = (T_DataPoint *) data;

    get_DataPacket(nfa, &DataPacket);
    pointer_parsed_NULL(&pointers_parse);
    if (DataPacket.PayLoad[0] != 0) {

        printf("ACK_ACTIVITY: MESSAGE INVALID\n");
        return nfq_set_verdict(qh, (u_int32_t) DataPacket.idPacket, NF_DROP, 0, NULL);
    }
    if (DataPacket.PayLoad[1] == RREP_ACK) {
        //printf_DataPacket(&DataPacket);
        err = rrep_ack_reception_process(&DataPacket,&pointers_parse);
        if (err == RREP_ACK_RESPONSE) {

            if ( send_RREP_Ack (DataPacket.sourceIP , DataPacket.IntDev) == CORRECT_OPERATION ) {
                printf ("ACK_ACTIVITY: send_RREP_Ack\n");
            }
            else {
                printf ("ACK_ACTIVITY: send_RREP_ack FAIL\n");
            }
        }
        if (err == FAIL_OPERATION) {

            printf("ACK_ACTIVITY:: rrep_ack_reception_process FAIL:%s\n", Int32_To_String(DataPacket.sourceIP));
        }
    }
    pointer_parsed_FREE (&pointers_parse);
    return nfq_set_verdict(qh, (u_int32_t) DataPacket.idPacket, NF_DROP, 0, NULL);
}