#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <libnetfilter_log/libnetfilter_log.h>
#include "Nflog_activity.h"
#include "../Timers/timers.h"
#include "../event_definition.h"
#include "../global_structs.h"
#include "../operative_system/operative_system_functions.h"

int callback_nflog_activity(struct nflog_g_handle *gh, struct nfgenmsg *nfmsg,struct nflog_data *nfa, void *data);



void *callback_thread_LogPacket(void *arg){
    T_DataPoint *DataPoint=(T_DataPoint *) arg;
    struct nflog_handle *h;
    struct nflog_g_handle *qh;
    int fd;
    ssize_t rv;
    char buf[4096];
    h = nflog_open();
    if (!h){
        fprintf(stderr, "error during nflog_open()\n");
        exit(1);
    }
    if (nflog_unbind_pf(h, AF_INET) < 0)
        {
            fprintf(stderr, "error nflog_unbind_pf()\n");
            exit(1);
        }
    if (nflog_bind_pf(h, AF_INET) < 0)
        {
            fprintf(stderr, "error during nflog_bind_pf()\n");
            exit(1);
        }
    qh = nflog_bind_group(h,3);
    if (!qh)
        {
            fprintf(stderr, "no handle for grup 3\n");
            exit(1);
        }
    if (nflog_set_mode(qh, NFULNL_COPY_PACKET, 0xffff) < 0)
        {
            fprintf(stderr, "can't set packet copy mode\n");
            exit(1);
        }
    fd = nflog_fd(h);
    nflog_callback_register(qh, &callback_nflog_activity, DataPoint);

    while ((rv = recv(fd, buf, sizeof(buf), 0)) && rv >= 0)
        {
            nflog_handle_packet(h, buf, (int) rv);
        }

    nflog_unbind_group(qh);

    #ifdef INSANE
            /* norally, applications SHOULD NOT issue this command,
             * since it detaches other programs/sockets from AF_INET, too ! */
            printf("unbinding from AF_INET\n");
            nflog_unbind_pf(h, AF_INET);
    #endif

    printf("closing Nflog Activity\n");
    nflog_close(h);

    return EXIT_SUCCESS;
}
int callback_nflog_activity(struct nflog_g_handle *gh, struct nfgenmsg *nfmsg,struct nflog_data *nfa, void *data) {

    T_DataPoint *DataPoint = ( T_DataPoint * ) data;
    int err;
    T_DataPacket DataPacket;
    T_LocalRoute LocalRoute;
    pthread_t ACTIVE_INTERVAL_id;
    err = get_DataLog2 (nfa , &DataPacket);
    if ( err != CORRECT_OPERATION ) {

        printf ("get_DataLog2: FAIL  %s\n" , Int_To_StringEvent (err));
        return 0;
    }
    for(int i = 0 ; i < get_LocalRouteSet_Overall (get_DataPoint_LocalRouteSet (DataPoint)) ; i ++) {

        err = get_LocalRouteSet_LocalRoute_Index (get_DataPoint_LocalRouteSet (DataPoint) , &LocalRoute , i);
        if ( err != CORRECT_OPERATION ) {

            printf ("send_RREP_Packet: get_LocalRouteSet_LocalRoute_Index FAIL \n");
            return 0;
        }
        if ( get_LocalRoute_Address (&LocalRoute) == get_DataPacket_destIP (&DataPacket)) {

            add_LocalRoute_Lastused (&LocalRoute , get_CTime_ms ());
            update_LocalRouteSet_LocalRoute (get_DataPoint_LocalRouteSet (DataPoint) , &LocalRoute);
            /*if ( get_status (get_LocalRoute_TimerActiveInterval (&LocalRoute)) == TIMEOUT_UNDEFINED ) {

                add_DataPoint_LocalRoute (DataPoint , &LocalRoute);
                err = pthread_create (&ACTIVE_INTERVAL_id , NULL , timer_ACTIVE_INTERVAL , ( void * ) DataPoint);
                if ( err != 0 ) {

                    printf ("send_RREP_Packet : pthread_create: [%s]\n" , strerror (err));
                    return FAIL_OPERATION;
                }

            }
            if ( get_status (get_LocalRoute_TimerActiveInterval (&LocalRoute)) == TIMEOUT_ONGOING ||
                 get_status (get_LocalRoute_TimerActiveInterval (&LocalRoute)) == TIMEOUT_RESTARTED ) {

                restart_timer (get_LocalRoute_TimerActiveInterval (&LocalRoute) ,
                               get_Timer_Timeout (get_LocalRoute_TimerActiveInterval (&LocalRoute)));
                printf ("Timer Active Interval ReStart\n");
            }*/
        }
    }
    return 0;
}