
#include "consola.h"
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include "../data_set/RouteErrorSet.h"
#include "../data_set/LocalRouteSet.h"
#include "../data_set/NeighborSet.h"
#include "../data_set/MulticastRouteMessageSet.h"
#include "../global_structs.h"

void *callback_thread_Consola(void *arg){
	char command[1024] = {0};
	snprintf(command,sizeof(command)-1,"clear");
	system(command);
	T_DataPoint *DataPoint=(T_DataPoint *) arg;
	int menu = 9;
	while (menu != 0){

		printf("Press 1 to print Local Route Set\n");
		printf("Press 2 to print Neighbor Set\n");
		printf("Press 3 to print Multicasr Route Set\n");
		printf("Press 4 to print Route Error Set\n");
		printf("Press 5 to exit\n");
		printf("Enter your choice:\n");
		scanf("%d", &menu);
		switch (menu){

			case 1:{
					system(command);
					print_LocalRouteSet(get_DataPoint_LocalRouteSet(DataPoint));
					break;
				}
			case 2:{
					system(command);
					print_NeighborSet(get_DataPoint_NeighborSet(DataPoint));
					break;
				}
			case 3:{
					system(command);
					print_McMsgSet(get_DataPoint_McMsgSet(DataPoint));
					break;
				}
			case 4:{
					system(command);
					print_RerrMsgSet(get_DataPoint_RerrMsgSet(DataPoint));
					break;
				}
			case 6:{
					pthread_exit(0);
					break;
				}
			default:{
					printf("Wrong Input\n");
				}
		}
	}
	return 0;
}