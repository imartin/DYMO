#include "InternalQueue.h"
#include <stdio.h>
#include <pthread.h>
#include <netinet/in.h>
#include <linux/netfilter.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <curses.h>
#include <monitors_pthreads_v3/thread_queue_manager.h>
#include "../event_definition.h"
#include "../global_structs.h"
#include "../monitors_pthreads_v3/thread_message.h"
#include "../Timers/timers.h"
#include "../operative_system/operative_system_functions.h"
#include "../processes/RREQ_processes.h"



int internal_process (struct nfq_data *nfa , T_DataPoint *DataPoint , int *veredict_flag);

int exit_internal_process (int decision , int *veredict_flag , int id_packet);

int callback_internal (struct nfq_q_handle *qhin , struct nfgenmsg *nfmsg , struct nfq_data *nfa , void *data);

void *callback_thread_Internal(void *arg) {

    T_DataPoint *DataPoint = ( T_DataPoint * ) arg;
    struct nfq_handle *h;
    struct nfq_q_handle *qhin;
    char bufinternal[4096] __attribute__ ((aligned));
    int fd;
    int rv;
    h = nfq_open ();
    if ( ! h ) {
        fprintf (stderr , "cannot open nfq_open()\n");
        return NULL;
    }
    //unbinding previous procfs
    if ( nfq_unbind_pf (h , AF_INET) < 0 ) {
        fprintf (stderr , "error during nfq_unbind_pf()\n");
        return NULL;
    }

    //binding the netlink procfs
    if ( nfq_bind_pf (h , AF_INET) < 0 ) {
        fprintf (stderr , "error during nfq_bind_pf()\n");
        return NULL;
    }
    //connet the thread for specific socket
    qhin = nfq_create_queue (h , 1 , &callback_internal , DataPoint);
    if ( ! qhin ) {
        fprintf (stderr , "error during nfq_create_queue()\n");
        return NULL;
    }
    if ( nfq_set_queue_maxlen (qhin , QUEUE_LENGHT_PACKET) < 0 ) {
        fprintf (stderr , "nfq_set_queue_maxlen FAIL\n");
        return NULL;
    }
    //set the queue for copy mode
    if ( nfq_set_mode (qhin , NFQNL_COPY_PACKET , 0xffff) < 0 ) {
        fprintf (stderr , "can't set packet_copy mode\n");
        return NULL;
    }
    //getting the file descriptor
    fd = nfq_fd (h);

    while (( rv = ( int ) recv (fd , bufinternal , sizeof (bufinternal) , 0)) && rv >= 0 ) {
        nfq_handle_packet (h , bufinternal , rv);
    }
    nfq_destroy_queue (qhin);
    nfq_close (h);
    pthread_exit (0);
}

int callback_internal (struct nfq_q_handle *qhin , struct nfgenmsg *nfmsg , struct nfq_data *nfa , void *data) {
    T_DataPoint *DataPoint = ( T_DataPoint * ) data;
    int veredict_flag;
    int id = internal_process (nfa , DataPoint , &veredict_flag);

    if ( veredict_flag == 0 ) {
        //printf("ID PACKET ACCEPT : %d",id);
        return nfq_set_verdict (qhin , ( u_int32_t ) id , NF_ACCEPT , 0 , NULL);
    }
    else if ( veredict_flag == 1 ) {
        //printf("ID PACKET DROP : %d",id);
        return nfq_set_verdict (qhin , ( u_int32_t ) id , NF_DROP , 0 , NULL);
    }
    return 0;
}

int internal_process (struct nfq_data *nfa , T_DataPoint *DataPoint , int *veredict_flag) {

    int err;
    int exit = 0;
    int id_packet;
    uint32_t ipsource;
    uint32_t ipdest;
    T_DataPacket DataPacket;
    pthread_t thread_retry_id;
    T_RouterClient RouterClient;
    get_DataPacket (nfa , &DataPacket);
    id_packet = get_DataPacket_IdPacket(&DataPacket);
    ipsource = get_DataPacket_sourceIP(&DataPacket);
    ipdest = get_DataPacket_destIP(&DataPacket);
    //printf_DataPacket(&DataPacket);
    err = get_RouterClientSet_RouterClient_Index(get_DataPointers_RouterClientSet(DataPoint), &RouterClient, 0);
    if(err !=CORRECT_OPERATION){

        printf("RREQ_ReceptionProcess : get_RouterClientSet_RouterClient_Index FAIL\n");
        return exit_internal_process (ONE , veredict_flag ,id_packet);

    }
    if (get_RouterClient_Address(&RouterClient) != ipsource) {
        printf ("INTERNAL:ROUTER CLIENT NOT REGISTER\n");
        return exit_internal_process (ONE , veredict_flag , id_packet);
    }


    printf ("INTERNAL: ROUTER CLIENT PACKET%s\n" , Int32_To_String(ipsource));
    err = check_RIB_Route(ipdest);
    if ( err == TRUE ) {

        return exit_internal_process (ZERO , veredict_flag , id_packet);
    }
    printf ("INTERNAL: NO ROUTE %s\n" , Int32_To_String(ipdest));
    err = send_RREQ_Packet(&RouterClient, &DataPacket, DataPoint);
    if ( err == FAIL_OPERATION ) {

        printf ("INTERNAL: send_RREQ_Packet: FAIL\n");
        return exit_internal_process (ONE , veredict_flag ,id_packet);
    }
    printf ("INTERNAL: RREQ SEND\n");
    DataPoint->RouterClient = &RouterClient;
    DataPoint->DataPacket = &DataPacket;
    DataPoint->RREQ_GEN_WAIT_TIME = RREQ_WAIT_TIME;
    err = pthread_create (&thread_retry_id , NULL , timer_RREQ_WAIT_TIME , ( void * ) DataPoint);
    if ( err != 0 ) {

        printf ("INTERNAL: pthread_create FAIL :[%s]\n" , strerror (err));
        return exit_internal_process (ONE , veredict_flag , id_packet);
    }
    while(!exit) {

        ThreadMessage * message = get_message(MESSAGE_QUEUE_EXTERNAL_INTERNAL);
        if(message->message_code == EXIT_RREP_RECEIVED){

            exit = message->message_code;
        }
        if(message->message_code == EXIT_RREQ_WAIT_TIME_FINISH){

            exit = message->message_code;
        }
    }
    if ( exit == EXIT_RREP_RECEIVED) {

        err = pthread_cancel (thread_retry_id);
        if ( err != 0 ) {

            printf ("INTERNAL: pthread_cancel: FAIL\n");
        }
        printf ("INTERNAL: ROUTE_DISCOVERY COMPLETED\n");
        return exit_internal_process (ONE , veredict_flag , id_packet);
    }
    if ( exit == EXIT_RREQ_WAIT_TIME_FINISH ) {

        printf ("INTERNAL: ROUTE DISCOVERY FAIL\n");
        return exit_internal_process (ONE , veredict_flag , id_packet);
    }
    return exit_internal_process (ONE , veredict_flag , id_packet);
}

int exit_internal_process (int decision , int *veredict_flag , int id_packet) {

    *veredict_flag = decision;
    return id_packet;
}