//
// Created by Israel Martin on 2/14/18.
//

#ifndef MONITORS_PTHREADS_THREAD_MESSAGE_H
#define MONITORS_PTHREADS_THREAD_MESSAGE_H

#define CODE_UNDEFINED -1

typedef struct {
    int exit_status;
    int message_code;
    void * data;
} ThreadMessage;

ThreadMessage * create_message(int status, int code, void * data);
ThreadMessage * create_message_from(ThreadMessage * message);
int getExitStatus(ThreadMessage *message);
int getMessageCode(ThreadMessage * message);
void * getData(ThreadMessage * message);

#endif //MONITORS_PTHREADS_THREAD_MESSAGE_H
