#include "monitor.h"
#include <stdlib.h> 

Monitor * create_monitor(int numCV) {
    Monitor * monitor = (Monitor *) malloc(sizeof(Monitor));
    monitor->cv = (pthread_cond_t *) malloc(sizeof(pthread_cond_t) * numCV);
}

void destroy_monitor(Monitor * monitor) {
    if (monitor != NULL) {
        free(monitor->cv);
        free(monitor);
    }
}

pthread_mutex_t * get_monitor_lock(Monitor * monitor) {
    return &monitor->lock;
}

pthread_cond_t * get_monitor_condition(Monitor * monitor, int numCV) {
    return &(monitor->cv[numCV]);
}

void lock_monitor(Monitor * monitor) {
    if (monitor != NULL) pthread_mutex_lock(get_monitor_lock(monitor));
}

void unlock_monitor(Monitor * monitor) {
    if (monitor != NULL) pthread_mutex_unlock(get_monitor_lock(monitor));
}