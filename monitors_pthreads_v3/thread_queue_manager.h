//
// Created by imartin on 2/14/18.
//

#ifndef MONITORS_PTHREADS_THREAD_QUEUE_MANAGER_H
#define MONITORS_PTHREADS_THREAD_QUEUE_MANAGER_H

#include "queue.h"
#include "monitor.h"
#include "thread_message.h"

typedef struct {
    Queue * messages;
    Monitor * monitor;
} MessageQueue;

void init_thread_queue_manager(int totalCV);  // Inits the manager
void destroy_thread_queue_manager();   // Finishes the manager

void put_message(ThreadMessage * message, int numCV);   // Put a new message in the queue
ThreadMessage * get_message(int numCV);   // Gets a message from the queue

#endif //MONITORS_PTHREADS_THREAD_QUEUE_MANAGER_H
