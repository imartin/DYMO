#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "timer.h"
#include "debug.h"

#define CV_TIMER 0
#define CV_APP 1
#define CV_TOTAL 2
struct timespec get_Timer_Timeout(T_Timer * Timer){

    return Timer->timeout;
}

T_Timer * create_timer(struct timespec timeout, TimerFunction onExitFunction, void * onExitFunctionArgs) {

    T_Timer * timer = (T_Timer *) malloc(sizeof(T_Timer));
    if (timer == NULL) {
        return NULL;
    }
    timer->monitor = create_monitor(CV_TOTAL);
    if (timer->monitor == NULL) {

        destroy_timer(timer);
        return NULL;
    }
    timer->timeout = timeout;
    timer->status = TIMEOUT_UNDEFINED;
    timer->onExitFunction = onExitFunction;
    timer->onExitFunctionArgs = onExitFunctionArgs;
    return timer;
}

void destroy_timer(T_Timer * timer) {

    if (timer == NULL){
        return;
    }
    if (timer->monitor != NULL){

        destroy_monitor(timer->monitor);
    }

    free(timer);
}

void * __run_timer(void * arg) {

    T_Timer * timer = (T_Timer *) arg;
    lock_monitor(timer->monitor);
    timer->status = TIMEOUT_ONGOING;
    struct timespec time_to = {time(NULL) + timer->timeout.tv_sec, timer->timeout.tv_nsec};
    while (timer->status == TIMEOUT_ONGOING) {
        pthread_cond_timedwait(get_monitor_condition(timer->monitor, CV_TIMER), get_monitor_lock(timer->monitor),&time_to);
        switch (timer->status) {

            case TIMEOUT_RESTARTED:

                timer->status = TIMEOUT_ONGOING;
                time_to.tv_sec = time(NULL) + timer->timeout.tv_sec;
                time_to.tv_nsec = timer->timeout.tv_nsec;
                break;
            case TIMEOUT_ONGOING:
                timer->status = TIMEOUT_EXPIRED;
                break;
        }

    }
    // Run on exit function
    if (timer->onExitFunction != NULL)
        timer->onExitFunction(timer->status, timer->onExitFunctionArgs);

    // Signal all the threads waiting for timer to end (due to regular expiration, abort, etc.)
    pthread_cond_broadcast(get_monitor_condition(timer->monitor, CV_APP));
    unlock_monitor(timer->monitor);

    return NULL;
}

void start_timer(T_Timer * timer) {

    if (timer == NULL) return;
    timer->status = TIMEOUT_ONGOING;
    int err = pthread_create(&timer->tid, NULL, &__run_timer, (void*) timer);
    if (err != 0) {

        printd("\ncan't create thread :[%s]", strerror(err));
    }
}

void change_status_and_notify(T_Timer * timer, int status) {

    if (timer == NULL){

        return;
    }
    lock_monitor(timer->monitor);
    timer->status = status;
    pthread_cond_signal(get_monitor_condition(timer->monitor, CV_TIMER));
    unlock_monitor(timer->monitor);
}

void restart_timer(T_Timer * timer,  struct timespec timeout) {

    timer->timeout = timeout;
    if (is_ongoing(timer)){

        change_status_and_notify(timer, TIMEOUT_RESTARTED);
    }else {

        start_timer(timer);
    }
}

void abort_timer(T_Timer * timer) {

    change_status_and_notify(timer, TIMEOUT_ABORTED);
}

void wait_for_timeout(T_Timer * timer){

    if (timer == NULL) return;

    lock_monitor(timer->monitor);
    while(timer->status == TIMEOUT_ONGOING || timer->status == TIMEOUT_RESTARTED) {


        pthread_cond_wait(get_monitor_condition(timer->monitor, CV_APP), get_monitor_lock(timer->monitor));
    }

    unlock_monitor(timer->monitor);
}

int get_status(T_Timer * timer) {

    if ( timer != NULL) {

        return timer->status;
    }
    else {

        return TIMEOUT_UNDEFINED;
    }
}

bool is_expired(T_Timer * timer) {

    return timer != NULL && timer->status == TIMEOUT_EXPIRED;
}

bool is_aborted(T_Timer * timer) {

    return timer != NULL && timer->status == TIMEOUT_ABORTED;
}

bool is_ongoing(T_Timer * timer){

    return timer != NULL && timer->status == TIMEOUT_ONGOING;
}

