#ifndef MONITORS_PTHREADS_MONITOR_H__
#define MONITORS_PTHREADS_MONITOR_H__

#include <pthread.h>

typedef struct {
    pthread_cond_t * cv;
    pthread_mutex_t lock;
} Monitor;

Monitor * create_monitor(int numCV);
void lock_monitor(Monitor * monitor);
void unlock_monitor(Monitor * monitor);
void destroy_monitor(Monitor * monitor);
pthread_mutex_t * get_monitor_lock(Monitor * monitor);
pthread_cond_t * get_monitor_condition(Monitor * monitor, int numCV);

#endif 