#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <zconf.h>
#include "timer.h"
#include "debug.h"
#include "main.h"
#include "message_codes.h"  // Exit codes used by timer messages
#include "thread_queue_manager.h"  // Shared queue of messages (feed by timers)
#include "thread_message.h"

#define MESSAGE_QUEUE_EXTERNAL_INTERNAL 0
#define NUMBER_OF_MESSAGE_QUEUES 1


char * create_string(char * str) {

    char * result = (char*) malloc(sizeof(char) * strlen(str));
    if (result != NULL){

        strcpy(result, str);
    }
    return result;
}

void on_timer_exit(int status, void * args) {

    char * str = args;
    printf("Thread \"%s\" ended with status %d\n", str);
    free(str);
}

/*
 * This is the onExitFunction the child is going to run. Testing restart timers.
 */
void * external_run_restart_timers(void *arg) {

    // Timeout value for the timer
    struct timespec timeout = {

            .tv_sec = 10,
            .tv_nsec = 0
    };

    // Print debug info
    printd("(External) Timeout: %u - %d\n", timeout.tv_sec, timeout.tv_nsec);

    // Create the timer
    Timer * timer = create_timer(timeout, on_timer_exit, create_string("timer 1"));

    // Start the timer (i.e. run it)
    start_timer(timer);

    // Wait 1 second before restarting the timer
    sleep(1);

    // New timer duration (if applies)
    timeout.tv_sec = 4;

    // Print debug info
    printd("(External) Timeout restarted: %u - %d\n", timeout.tv_sec, timeout.tv_nsec);

    // Set new arguments to the timer
    timer->onExitFunctionArgs = create_string("timer restarted");

    // Restart the current timer
    restart_timer(timer, timeout);

    return NULL;
}

/*
 * This is the onExitFunction the child is going to run. Testing two timers
 */
void* external_run_two_timers(void *arg) {

    // Timeout value for the timer
    struct timespec timeout = {
            .tv_sec = 4,
            .tv_nsec = 0
    };

    // Print debug info
    printd("(External) Timeout: %u - %d\n", timeout.tv_sec, timeout.tv_nsec);

    // Create the timer
    Timer * timer = create_timer(timeout, on_timer_exit, create_string("timer 1"));

    // Start the timer (i.e. run it)
    start_timer(timer);

    // Second timer duration
    timeout.tv_sec = 10;

    // Print debug info
    printd("(External) Timeout restarted: %u - %d\n", timeout.tv_sec, timeout.tv_nsec);

    // Create the second timer
    timer = create_timer(timeout, on_timer_exit, create_string("timer 2"));

    // Start the second timer
    start_timer(timer);

    return NULL;
}

void* internal_run(void *arg) {
    int exit = 0;

    while(!exit) {
        // Getting a message (produced by timers)
        ThreadMessage * message = get_message(MESSAGE_QUEUE_EXTERNAL_INTERNAL);
        // Print debug information
        printd("(Internal) Message received at %d with code %d and exit status %d\n", time(NULL), getMessageCode(message), getExitStatus(message));
        // Exit after receiving EXIT_CODE_2. Just to make the thread finish
        exit = message->message_code == EXIT_CODE_2;
    }
}


int main(void)
{
    pthread_t external_tid;
    pthread_t internal_tid;
    int child_result;
    int err;

    // Init the queue manager (where the messages between external-generated timers and internal thread are stored)
    init_thread_queue_manager(NUMBER_OF_MESSAGE_QUEUES);

    // Debug message
    printd("Init queue manager\n");

    // Create the external thread
    err = pthread_create(&external_tid, NULL, &external_run_restart_timers, NULL);
    if (err != 0) {
        printd("can't create the external thread :[%s]\n", strerror(err));
        exit(-1);
    }

    // Create the internal thread
    err = pthread_create(&internal_tid, NULL, &internal_run, NULL);
    if (err != 0) {
        printd("can't create the internal thread :[%s]\n", strerror(err));
        exit(-1);
    }


    // Print debug info
    printd("Thread created successfully\n");

    // Wait until child thread finishes
    // pthread_join(external_tid, (void **)&child_result);
    pthread_join(internal_tid, (void **)&child_result);

    // Debug message
    printd("Destroy queue manager\n");

    // Destroy the queue manager
    destroy_thread_queue_manager();

    return 0;
}
