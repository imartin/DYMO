#ifndef MONITORS_PTHREADS_TIMER_H__
#define MONITORS_PTHREADS_TIMER_H__

#include <pthread.h>
#include <time.h>
#include <stdbool.h>
#include "monitor.h"

#define TIMEOUT_UNDEFINED 0
#define TIMEOUT_ONGOING 1
#define TIMEOUT_EXPIRED 2
#define TIMEOUT_ABORTED 3
#define TIMEOUT_RESTARTED 4

typedef void (*TimerFunction)(int exit_status, void * args);

typedef struct {

    pthread_t tid;
    Monitor * monitor;
    struct timespec timeout;
    int status;
    TimerFunction onExitFunction;
    void * onExitFunctionArgs;
} T_Timer;


T_Timer * create_timer(struct timespec timeout, TimerFunction onExitFunction, void * onExitFunctionArgs);
void destroy_timer(T_Timer * timer);
void start_timer(T_Timer * timer);
void restart_timer(T_Timer * timer, struct timespec timeout);
void abort_timer(T_Timer * timer);
void wait_for_timeout(T_Timer * timer);
int get_status(T_Timer * timer);
bool is_expired(T_Timer * timer);
bool is_aborted(T_Timer * timer);
bool is_ongoing(T_Timer * timer);
struct timespec get_Timer_Timeout(T_Timer * Timer);

#endif 