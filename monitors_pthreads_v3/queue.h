//
// Created by imartin on 2/14/18.
//

#ifndef MONITORS_PTHREADS_QUEUE_H
#define MONITORS_PTHREADS_QUEUE_H

/* Node of the Queue */
typedef struct Node_t {
    void * data;
    struct Node_t * next;
} NODE;

/* HEAD of the Queue */
typedef struct Queue {
    NODE *head;
    NODE *tail;
    unsigned long size;
} Queue;

Queue * create_queue();
void destroy_queue(Queue * queue);
void queue_push(Queue * queue, void * data);
void * queue_pop(Queue * queue);
void * queue_head(Queue * queue);
unsigned long queue_size(Queue * queue);
int queue_is_empty(Queue * queue);

#endif //MONITORS_PTHREADS_QUEUE_H
