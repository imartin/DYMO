//
// Created by Israel Martin on 2/14/18.
//

#include <stdlib.h>
#include "thread_queue_manager.h"

static MessageQueue * messageQueue = NULL;

void init_thread_queue_manager(int totalCV) {
    messageQueue = (MessageQueue *) malloc(sizeof(MessageQueue));
    messageQueue->messages = create_queue();
    messageQueue->monitor = create_monitor(totalCV);
}

void destroy_thread_queue_manager() {
    if (messageQueue != NULL) {
        destroy_queue(messageQueue->messages);
        destroy_monitor(messageQueue->monitor);
        free(messageQueue);
    }
    messageQueue = NULL;
}

void put_message(ThreadMessage * message, int numCV) {
    if (message == NULL) return;

    lock_monitor(messageQueue->monitor);
    if (queue_is_empty(messageQueue->messages))
        pthread_cond_broadcast(get_monitor_condition(messageQueue->monitor, numCV));

    queue_push(messageQueue->messages, message);

    unlock_monitor(messageQueue->monitor);
}

ThreadMessage * get_message(int numCV) {
    lock_monitor(messageQueue->monitor);

    while (queue_is_empty(messageQueue->messages)) {
        pthread_cond_wait(get_monitor_condition(messageQueue->monitor, numCV), get_monitor_lock(messageQueue->monitor));
    }

    ThreadMessage * message = queue_pop(messageQueue->messages);
    unlock_monitor(messageQueue->monitor);

    return message;
}