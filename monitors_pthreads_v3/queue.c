//
// Created by imartin on 2/14/18.
//

#include <stdlib.h>
#include "queue.h"

Queue * create_queue() {
    Queue * result = (Queue *) malloc(sizeof(Queue));

    if (result != NULL) {
        result->size = 0;
        result->head = (NODE *) malloc(sizeof(NODE));;
        result->tail = result->head;
    }

    return result;
}

void destroy_queue(Queue * queue) {
    if (queue == NULL) return;

    for ( int i = ( int ) queue_size(queue) ; i > 0 ; i--) {
        void * data = queue_pop(queue);
        if (data != NULL) free(data);
    }

    free(queue->head);
    free(queue);
}

void queue_push(Queue * queue, void * data) {
    NODE * node = (NODE *) malloc(sizeof(NODE));

    if (node == NULL) return;

    // Set node data
    node->data = data;
    node->next = NULL;


    // Setup links
    queue->tail->next = node;
    queue->tail = node;

    // Increase size
    queue->size++;
}

void * queue_pop(Queue * queue) {
    if (queue_is_empty(queue)) return NULL;

    // Get the node
    NODE * node = queue->head->next;

    // Get the data stored if the first node
    void * data = node->data;

    // Free the node
    free(queue->head);

    // Unlink the node
    queue->head = node;

    // Update the queue size
    queue->size--;

    return data;
}

void * queue_head(Queue * queue){
    return (!queue_is_empty(queue) ? queue->head->next->data: NULL);
}

void * queue_tail(Queue * queue){
    return (!queue_is_empty(queue) ? queue->tail->data: NULL);
}

unsigned long queue_size(Queue * queue) {
    return (queue != NULL ? queue->size : 0);
}

int queue_is_empty(Queue * queue) {
    return queue_size(queue) == 0;
}