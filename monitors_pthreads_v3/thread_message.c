//
// Created by Israel Martin on 2/14/18.
//

#include <stddef.h>
#include <stdlib.h>
#include "thread_message.h"
#include "timer.h"

ThreadMessage * create_message(int status, int code, void * data) {

    ThreadMessage * result = (ThreadMessage *) malloc(sizeof(ThreadMessage));
    if (result != NULL) {
        result->exit_status = status;
        result->message_code = code;
        result->data = data;
    }

    return result;
}

ThreadMessage * create_message_from(ThreadMessage * message) {
    if (message == NULL) return NULL;

    return create_message(message->exit_status, message->message_code, message->data);
}

int getExitStatus(ThreadMessage *message) {
    return (message != NULL ? message->exit_status : TIMEOUT_UNDEFINED);
}

int getMessageCode(ThreadMessage * message) {
    return (message != NULL ? message->message_code : CODE_UNDEFINED);
}

void * getData(ThreadMessage * message) {
    return (message != NULL ? message->data : NULL);
}
