#include <stdio.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <zconf.h>
#include <pthread.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <libnfnetlink/libnfnetlink.h>
#include <curses.h>
#include <data/Interface.h>
#include <sys/ioctl.h>
#include "data_set/InterfaceSet.h"


#include "operative_system/operative_system_functions.h"
#include "monitors_pthreads_v3/thread_queue_manager.h"
#include "event_definition.h"
#include "start_routine/ACK_activity.h"
#include "start_routine/Network_Interface_Status.h"
#include "start_routine/InternalQueue.h"
#include "start_routine/ExternalQueue.h"
#include "start_routine/Nflog.h"
#include "start_routine/consola.h"
#include "start_routine/Nflog_activity.h"

#include "debug.h"


int main (int argc , char *argv[]) {

    char ip2[256] = "10.0.3.2";
    printf("IP2 :  %u\n",string2int(ip2));
    char ip3[256] = "10.0.3.3";
    printf("IP3 :  %u\n",string2int(ip3));
    char ip4[256] = "10.0.3.4";

    printf("IP4 :  %u\n",string2int(ip4));
    char ip5[256] = "10.0.3.5";
    printf("IP5 :  %u\n",string2int(ip5));
    char ip6[256] = "10.0.3.6";
    printf("IP6 :  %u\n",string2int(ip6));


/*-Declaration of the variables-*/
    int err;
    int nl_socket;
    struct sockaddr_nl addr;
    T_Interface Interface;
    T_DataPoint DataPoint;
    pthread_mutex_t lock;
    pthread_t thread_LogAck_ID;
    pthread_t thread_Internal_id;
    pthread_t thread_External_id;
    pthread_t thread_LogIcmp_id;
    pthread_t thread_Consola_id;
    pthread_t thread_LogPacket_id;
    pthread_t thread_NetworkStatus_id;

    T_LocalRouteSet LocalRouteSet;
    T_McMsgSet McMsgSet;
    T_InterfaceSet InterfaceSet;
    T_RerrMsgSet RerrMsgSet;
    T_MetricSet MetricSet;
    T_NeighborSet NeighborSet;
    T_RouterClientSet RouterClientSet;
    T_SeqNumSet SeqNumSet;
    T_RouterClient RouterClient;


/*-Initialization of the variables-*/
    init_thread_queue_manager (NUMBER_OF_MESSAGE_QUEUES);

    ini_RerrMsgSet(&RerrMsgSet);
    ini_McMsgSet(&McMsgSet);
    ini_SeqNumSet(&SeqNumSet);
    ini_RouterClientSet(&RouterClientSet);
    ini_NeighborSet(&NeighborSet);
    ini_MetricSet(&MetricSet);
    ini_LocalRouteSet(&LocalRouteSet);


    DataPoint.point_RerrMsgSet = &RerrMsgSet;
    DataPoint.point_SeqNumSet = &SeqNumSet;
    DataPoint.point_RouterClientSet = &RouterClientSet;
    DataPoint.point_NeighborSet = &NeighborSet;
    DataPoint.point_MetricSet = &MetricSet;
    DataPoint.point_LocalRouteSet = &LocalRouteSet;
    DataPoint.point_InterfaceSet = &InterfaceSet;
    DataPoint.point_McMsgSet = &McMsgSet;
    DataPoint.Mutex = &lock;
    pthread_mutex_init (DataPoint.Mutex , NULL);

/*-Socket Netlink is created and assigned Address-*/
    nl_socket = socket (AF_NETLINK , SOCK_RAW , NETLINK_ROUTE);
    if ( nl_socket < 0 ) {

        printd ("Socket Open Error!\n");
        exit (1);
    }
    memset (( void * ) &addr , 0 , sizeof (addr));
    addr.nl_family = AF_NETLINK;
    addr.nl_pid    = ( __u32 ) getpid ();
    addr.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV6_IFADDR;
    if ( bind (nl_socket , ( struct sockaddr * ) &addr , sizeof (addr)) < 0 ) {

        perror ("bind");
        exit (1);
    }
    DataPoint.NlSocket = nl_socket;
    struct nlif_handle *nlif_handle;

/*-Manager to manage queries on the name of the interfaces-*/
    nlif_handle = nlif_open ();
    nlif_query (nlif_handle);
    if ( nlif_handle == NULL) {

        perror ("nlif_open");
        fflush (stderr);
        exit (0);
    }
    nlif_query (nlif_handle);

/*-Automatic configuration of the program development mode for the InterfaceSet tables, RouterClientSet - */
    err = ini_InterfaceSet(&InterfaceSet,TRUE);
    if(err != CORRECT_OPERATION){

        printf("ini_InterfaceSet : FAIL\n");
        exit(0);
    }

/*-Automatic configuration of the program development mode for the RouterClientSet - */
    err = configuracion_automatica_RouterClientSet (&InterfaceSet , &RouterClientSet);
    if(err != CORRECT_OPERATION){

        printd("configuracion_automatica_RouterClientSet : %s\n", Int_To_StringEvent(err));
        exit (0);
    }
    err = get_RouterClientSet_RouterClient_Index(&RouterClientSet, &RouterClient, 0);
    if(err != CORRECT_OPERATION){

        printd("get_RouterClientSet_RouterClient_Index : %s\n", Int_To_StringEvent(err));
        exit (0);
    }

/*-Inserting the rules for Netfilter-*/
    insert_NetFilterRules(get_RouterClient_Address(&RouterClient), &DataPoint);

/*-Get interface AODV-*/
    err = get_InterfaceSet_Interface_Index(&InterfaceSet,&Interface,0);
    if(err != CORRECT_OPERATION){

        printf("get_InterfaceSet_Interface_Index FAIL\n");
        exit(0);
    }
    add_DataPoint_InterfaceId(&DataPoint,Interface.InterfaceId);

/*-Get IP address of interface AODV-*/
    struct ifreq ifr;
    size_t if_name_len = strlen(Interface.InterfaceId);
    if (if_name_len < sizeof(ifr.ifr_name)) {

        memcpy(ifr.ifr_name,Interface.InterfaceId,if_name_len);
        ifr.ifr_name[if_name_len] = 0;
    } else {

        printf("interface name is too long\n");
    }
    int fd = socket(AF_INET,SOCK_DGRAM,0);
    if (fd == (-1)) {

        printf("%s",strerror(errno));
    }

    if (ioctl(fd,SIOCGIFADDR,&ifr) == (-1)) {

        close(fd);
        printf("IOCTL error :%d\n",errno);
    }

    struct sockaddr_in* ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
    add_DataPoint_InterfaceIp(&DataPoint,ipaddr->sin_addr.s_addr);

/*-Get netmask of interface AODV-*/
    if (ioctl(fd,SIOCGIFNETMASK,&ifr) == (-1)) {

        close(fd);
        printf("IOCTL error :%d\n",errno);
    }
    struct sockaddr_in* macaddr = (struct sockaddr_in*)&ifr.ifr_netmask;
    close(fd);
    add_DataPoint_InterfaceMasc(&DataPoint,macaddr->sin_addr.s_addr);

/*-Inserting Network address of Interface.id in the RIB -*/
    add_RIB_Net(&DataPoint);

/*-Starts a  thread's  for the different processes-*/
    err = pthread_create(&thread_LogAck_ID, NULL, callback_thread_LogAck, (void *) &DataPoint);
    if (err != 0) {

        printd("MAIN: No se pudo generar hilo FAIL :[%s]\n", strerror(err));
    }
    err = pthread_create(&thread_NetworkStatus_id, NULL, callback_thread_NetworkStatus, (void *) &DataPoint);
    if (err != 0) {

        printd("MAIN: No se pudo generar hilo FAIL :[%s]\n", strerror(err));
    }
    err = pthread_create(&thread_Internal_id, NULL, callback_thread_Internal, (void *) &DataPoint);
    if (err != 0) {

        printd("MAIN: No se pudo generar hilo FAIL :[%s]\n", strerror(err));
    }
    err = pthread_create(&thread_External_id, NULL, callback_thread_External, (void *) &DataPoint);
    if (err != 0) {

        printd("MAIN: No se pudo generar hilo FAIL [%s]\n", strerror(err));
    }
    err = pthread_create(&thread_LogIcmp_id, NULL, callback_thread_LogIcmp, (void *) &DataPoint);
    if (err != 0) {

        printd("MAIN: No se pudo generar hilo FAIL [%s]\n", strerror(err));
    }
    err = pthread_create(&thread_Consola_id, NULL, callback_thread_Consola, (void *) &DataPoint);
    if (err != 0) {

        printd("MAIN: No se pudo generar hilo FAIL [%s]\n", strerror(err));
    }
    err = pthread_create(&thread_LogPacket_id, NULL, callback_thread_LogPacket, (void *) &DataPoint);
    if (err != 0) {

        printd("MAIN: No se pudo generar hilo FAIL [%s]\n", strerror(err));
    }

    pthread_join (thread_NetworkStatus_id , NULL);
    pthread_join (thread_Internal_id , NULL);
    pthread_join (thread_External_id , NULL);
    pthread_join (thread_LogIcmp_id , NULL);
    pthread_join (thread_Consola_id , NULL);
    pthread_join (thread_LogPacket_id , NULL);
    pthread_mutex_destroy (DataPoint.Mutex);

    destroy_thread_queue_manager();

    exit (0);
}