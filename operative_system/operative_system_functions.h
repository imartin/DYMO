#ifndef PROGRAMA_V4_OPERATIVE_SYSTEM_FUNCTIONS_H
#define PROGRAMA_V4_OPERATIVE_SYSTEM_FUNCTIONS_H


#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <libnetfilter_log/libnetfilter_log.h>
#include "../global_structs.h"



void REMOVE_rule_CHAIN_AODV_IPTABLES(uint32_t address);
void del_ARP_Entry(uint32_t *address);
void ADD_entry_ARP(uint32_t address,T_DataPacket *DataPacket);
int get_DataLog2(struct nflog_data *nfa, T_DataPacket *DataPacket);
void printf_data_log_2(T_DataPacket *DataPacket);
void printf_data_log(T_DataPacket *DataPacket);
void printf_DataPacket(T_DataPacket *DataPacket);
struct nlif_handle *fetch_interface_table(void);
int check_RIB_Route(uint32_t);
uint32_t get_BroadcastAddress(char *);
void get_DataPacket(struct nfq_data *nfa,T_DataPacket *DataPacket);
int get_InDev_Name(struct nfq_data *nfq_pkt, char *indevname);
void add_RIB_Route(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint);
void add_RIB_Host(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint);
void del_RIB_Route(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint);
void del_RIB_Host(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint);
void add_RIB_Net(T_DataPoint *DataPoint);
void del_RIB_Net(T_DataPoint *DataPoint);
int send_PacketUdpUnicast(uint8_t *buffer, size_t length, uint32_t ipaddress, char interface[]);
int send_PacketUdpBroadcast(uint8_t *buffer, int length, uint32_t ipdst);
int get_Interface_Index(uint32_t index, T_DataPacket *DataPacket, int IO);
int get_DataLog(struct nflog_data *nfa, T_DataPacket *DataPacket);





#endif //PROGRAMA_V4_OPERATIVE_SYSTEM_FUNCTIONS_H
