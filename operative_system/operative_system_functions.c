#include "operative_system_functions.h"
#include <curses.h>
#include <stdlib.h>
#include <linux/ip.h>
#include <linux/icmp.h>
#include <net/route.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <unistd.h>
#include <net/if.h>
#include <arpa/inet.h>
#include "../event_definition.h"



int check_RIB_Route(uint32_t ip_destination){

    char buffer[256];
    FILE *fp;
    fp= fopen("/proc/net/route","r");
    char iface[100];
    uint32_t destination;
    uint32_t gateway;
    int flags;
    int refcnt;
    int use;
    int metric;
    uint32_t masc;
    int mtu;
    int window;
    int irtt;

    if(fp==NULL){

        printf("CHECK ROUTE: Error abriendo RIB\n");
        return -1;
    }
    else{
        fgets(buffer, 256 , fp);
        while (fgets(buffer, 256, fp) != NULL){

            sscanf(buffer,"%s	%x	%x	%d	%d	%d	%d	%x	%d	%d	%d\n", iface,&destination,&gateway,&flags,&refcnt,&use,&metric,&masc,&mtu,&window,&irtt);
            if((destination != 0)&&(masc != 0)){

                if(destination == ip_destination){

                    fclose(fp);
                    return TRUE;
                }
            }
        }
    }
    fclose(fp);
    //printf("CHECK ROUTE: NO existe ruta\n");
    return FALSE;
}
void add_RIB_Net(T_DataPoint *DataPoint){

    uint32_t  rt_dst = get_NetAddress(get_DataPoint_InterfaceIp(DataPoint),Mask_To_Prefix(get_DataPoint_InterfaceMasc(DataPoint)));

    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    struct sockaddr_in *addr;
    struct rtentry route;
    memset(&route, 0, sizeof(route));

    printf("IP address interface: %s\n",Int32_To_String(rt_dst));
    addr = (struct sockaddr_in*)&route.rt_dst;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = rt_dst;

    printf("Mac address: %s\n",Int32_To_String(get_DataPoint_InterfaceMasc(DataPoint)));
    addr = (struct sockaddr_in*)&route.rt_genmask;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = get_DataPoint_InterfaceMasc(DataPoint);

    route.rt_dev = get_DataPoint_InterfaceId(DataPoint);
    route.rt_flags = RTF_UP ;
    if(ioctl(fd, SIOCADDRT, &route)== (-1)){

        printf("add_RIB_NET:  %d\n",errno);
        return;
    }

}
void add_RIB_Route(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint){

    char command[1024] = {0};
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    struct sockaddr_in *addr;
    struct rtentry route;
    memset(&route, 0, sizeof(route));

    addr = (struct sockaddr_in*)&route.rt_dst;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = (LocalRoute->Address);

    addr = (struct sockaddr_in*)&route.rt_gateway;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = (LocalRoute->NextHop);

    addr = (struct sockaddr_in*)&route.rt_genmask;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = 0xFFFFFFFF;
    route.rt_flags = RTF_UP | RTF_GATEWAY;
    route.rt_metric = LocalRoute->Metric;
    route.rt_dev = LocalRoute->NextHopInterface;

    if(ioctl(fd, SIOCADDRT, &route)==-1){

        printf("add_RIB_Route:  %d\n",errno);
        return;
    }
    snprintf(command,sizeof(command)-1,"iptables -I aodv -d %s -j ACCEPT", Int32_To_String(LocalRoute->Address));
    system(command);

}
void add_RIB_Host(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint){

    int fd;
    char command[1024] = {0};
    fd = socket(AF_INET, SOCK_DGRAM, 0);

    struct rtentry route;
    memset(&route, 0, sizeof(route));

    struct sockaddr_in *addr = (struct sockaddr_in*)&route.rt_dst;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = LocalRoute->Address;

    route.rt_flags = RTF_UP | RTF_HOST;
    route.rt_dev = LocalRoute->NextHopInterface;

    if(ioctl(fd, SIOCADDRT, &route) == (-1) ){

        printf("add_RIB_Host : %d\n",errno);
        return;
    }
    snprintf(command,sizeof(command)-1,"iptables -I aodv -d %s -j ACCEPT", Int32_To_String(LocalRoute->Address));
    system(command);


}
void del_RIB_Net(T_DataPoint *DataPoint){

    uint32_t  rt_dst = get_NetAddress(get_DataPoint_InterfaceIp(DataPoint),Mask_To_Prefix(get_DataPoint_InterfaceMasc(DataPoint)));

    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    struct sockaddr_in *addr;
    struct rtentry route;
    memset(&route, 0, sizeof(route));

    addr = (struct sockaddr_in*)&route.rt_dst;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = rt_dst;

    addr = (struct sockaddr_in*)&route.rt_genmask;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr =  get_DataPoint_InterfaceMasc(DataPoint);

    route.rt_dev = get_DataPoint_InterfaceId(DataPoint);

    route.rt_flags = RTF_UP ;
    if(ioctl(fd, SIOCDELRT, &route) == (-1)){

        printf("del_RIB_Route:  %d\n",errno);
        return;
    }

}
void del_RIB_Host(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint){

    int fd,err;
    struct rtentry route;
    char command[1024] = {0};

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    memset(&route, 0, sizeof(route));

    struct sockaddr_in *addr = (struct sockaddr_in*)&route.rt_dst;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = LocalRoute->Address;

    route.rt_dev = LocalRoute->NextHopInterface;

    route.rt_flags = RTF_UP | RTF_HOST;

    if(ioctl(fd, SIOCDELRT, &route) == (-1)){

        printf("del_RIB_Host:  %d\n",errno);
        return;
    }
    snprintf(command,sizeof(command)-1,"iptables -D aodv -d %s -j ACCEPT", Int32_To_String(LocalRoute->Address));
    system(command);

}
void del_RIB_Route(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint){

    int fd;
    struct sockaddr_in *addr;
    struct rtentry route;
    char command[1024] = {0};

    fd = socket(AF_INET, SOCK_DGRAM, 0);

    memset(&route, 0, sizeof(route));

    addr = (struct sockaddr_in*)&route.rt_dst;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = LocalRoute->Address;

    addr = (struct sockaddr_in*)&route.rt_gateway;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = LocalRoute->NextHop;

    addr = (struct sockaddr_in*)&route.rt_genmask;
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = 0xFFFFFFFF;

    route.rt_flags = RTF_UP | RTF_GATEWAY;
    route.rt_metric = LocalRoute->Metric;
    route.rt_dev = LocalRoute->NextHopInterface;

    if (ioctl(fd, SIOCDELRT, &route) == (-1)) {

        printf("del_RIB_Route :  fail %d\n", errno);
        return;
    }

    snprintf(command,sizeof(command)-1,"iptables -D aodv -d %s -j ACCEPT", Int32_To_String(LocalRoute->Address));
    system(command);

}

int send_PacketUdpUnicast(uint8_t *buffer, size_t length, uint32_t ipaddress, char *interface){

    int fd;
    struct sockaddr_in addr,srcaddr;
    if((fd = socket(AF_INET,SOCK_DGRAM,0))< 0){
        perror("socket");
        return FAIL_OPERATION;
    }
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr=ipaddress;
    addr.sin_port = htons(UDP_PORT);
    memset(&srcaddr, 0, sizeof(srcaddr));
    srcaddr.sin_family = AF_INET;
    srcaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    srcaddr.sin_port = htons(UDP_PORT);
    if(bind(fd, (struct sockaddr *) &srcaddr, sizeof(srcaddr)) < 0){

        perror("bind");
        return FAIL_OPERATION;
    }
    struct ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    snprintf(ifr.ifr_name, sizeof(ifr.ifr_name),"%s",interface);
    if(setsockopt(fd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr)) < 0){

        perror("bind to Interface");
        return FAIL_OPERATION;
    }
    if(sendto(fd, buffer,length, 0, (struct sockaddr *) &addr,sizeof(addr)) < 0){

        perror("sendto");
        return FAIL_OPERATION;
    }
    close(fd);
    return CORRECT_OPERATION;
}
int send_PacketUdpBroadcast(uint8_t *buffer, int length, uint32_t ipdst) {

    int rest, fd;
    struct sockaddr_in addr, srcaddr;
    if((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {

        perror("socket");
        rest = FAIL_OPERATION;
    }else{

        int broadcastEnable = 1;
        setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = ipdst;
        addr.sin_port = htons(UDP_PORT);
        memset(&srcaddr, 0, sizeof(srcaddr));
        srcaddr.sin_family = AF_INET;
        srcaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        srcaddr.sin_port = htons(UDP_PORT);
        if (bind(fd, (struct sockaddr *) &srcaddr, sizeof(srcaddr)) < 0) {

            perror("bind");
            rest = FAIL_OPERATION;
        }else{

            rest = sendto(fd, buffer, (size_t) length, 0, (struct sockaddr *) &addr, sizeof(addr)) < 0 ? FAIL_OPERATION : CORRECT_OPERATION;
        }
    }
    close(fd);
    return rest;
}

void del_ARP_Entry(uint32_t *address){

    char command[1024] = {0};
    snprintf(command,sizeof(command)-1,"arp -d %s", Int32_To_String(*address));
    system(command);
}
void ADD_entry_ARP(uint32_t address,T_DataPacket *DataPacket){

    char command[1024] = {0};
    snprintf(command,
             sizeof(command)-1,
             "arp -s %s %02x:%02x:%02x:%02x:%02x:%02x",
             Int32_To_String(address),
             DataPacket->HwAddress[0],
             DataPacket->HwAddress[1],
             DataPacket->HwAddress[2],
             DataPacket->HwAddress[3],
             DataPacket->HwAddress[4],
             DataPacket->HwAddress[5]);
    printf("COMMAND: %s\n",command);
    system(command);
}


uint32_t get_BroadcastAddress(char *interface){

    int fd;
    struct ifreq ifr;
    struct in_addr broadaddr;
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name,interface,IFNAMSIZ-1);
    ioctl(fd, SIOCGIFBRDADDR, &ifr);
    broadaddr = ((struct sockaddr_in *)&ifr.ifr_broadaddr)->sin_addr;
    return broadaddr.s_addr;
}
int get_Interface_Index(uint32_t index, T_DataPacket *DataPacket, int OI){

    int socketfd, result;
    struct ifreq ifr;
    if (index < 1 || !DataPacket){

        return errno = EINVAL;
    }
    socketfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (socketfd == -1){

        return errno;
    }
    ifr.ifr_ifindex = index;
    if (ioctl(socketfd, SIOCGIFNAME, &ifr) == -1){
        do{
            result = close(socketfd);
        }
        while (result == -1 && errno == EINTR);
        return errno = ENOENT;
    }
    if(OI==1){

        strcpy(DataPacket->IntDev, ifr.ifr_name);
    }
    if(OI==2){

        strcpy(DataPacket->OutDev, ifr.ifr_name);
    }
    return CORRECT_OPERATION;
}
int get_DataLog(struct nflog_data *nfa, T_DataPacket *DataPacket){

    int iphdrlen;
    char *payload;
    struct sockaddr_in source, dest;
    uint32_t index_device;
    int err;
    nflog_get_payload(nfa, &payload);
    struct iphdr *iph = (struct iphdr *)payload;
    memset(&source, 0, sizeof(source));
    DataPacket->sourceIP = iph->saddr;
    memset(&dest, 0, sizeof(dest));
    DataPacket->destIP = iph->daddr;
    iphdrlen = iph->ihl*4;
    struct icmphdr *icmph = (struct icmphdr *)(payload + iphdrlen);
    DataPacket->codeICMP = icmph->code;
    iph = (struct iphdr *)(payload + iphdrlen +8);
    DataPacket->soUIP = le32toh(iph->saddr);
    DataPacket->deUIP = le32toh(iph->daddr);

    index_device = nflog_get_indev(nfa);
    if(index_device > 0 ){
        err = get_Interface_Index(index_device, DataPacket, 1);
        if(err != CORRECT_OPERATION){
            return err;
        }
    }
    index_device = nflog_get_outdev(nfa);
    if(index_device > 0){

        err = get_Interface_Index(index_device, DataPacket, 2);
        if(err != CORRECT_OPERATION){

            return err;
        }
    }
    return CORRECT_OPERATION;
}
int get_DataLog2(struct nflog_data *nfa, T_DataPacket *DataPacket){

    int err;
    char *payload;
    uint32_t index_device;
    struct sockaddr_in source, dest;
    nflog_get_payload(nfa, &payload);
    struct iphdr *iph = (struct iphdr *)payload;
    memset(&source, 0, sizeof(source));
    DataPacket->sourceIP = iph->saddr;
    memset(&dest, 0, sizeof(dest));
    DataPacket->destIP = iph->daddr;
    index_device = nflog_get_outdev(nfa);
    if(index_device > 0){

        err = get_Interface_Index(index_device, DataPacket, 2);
        if(err != CORRECT_OPERATION){

            return err;
        }
    }
    return  CORRECT_OPERATION;
}
void get_DataPacket(struct nfq_data *nfa,T_DataPacket *DataPacket){

    char indevname[256];
    short  iphdrlen;
    uint8_t *array_packet;
    int sp;
    struct sockaddr_in source, dest;
    struct nfqnl_msg_packet_hdr *ph;
    struct nfqnl_msg_packet_hw *hwph;
    hwph = nfq_get_packet_hw(nfa);
    if (hwph){

        for (int i = 0; i < 6; i++) {

            DataPacket->HwAddress[i]= hwph->hw_addr[i];
        }
    }
    ph = nfq_get_msg_packet_hdr(nfa);
    DataPacket->idPacket = ntohl(ph->packet_id);
    DataPacket->size_payload=nfq_get_payload(nfa,&array_packet);
    sp = nfq_get_payload(nfa,&array_packet);
    get_InDev_Name(nfa, indevname);
    strcpy(DataPacket->IntDev,indevname);
    fflush(stdout);
    struct iphdr *iph = (struct iphdr *)array_packet;
    iphdrlen = (short) (iph->ihl * 4);
    struct icmphdr *icmph = (struct icmphdr *)(array_packet + iphdrlen);
    DataPacket->codeICMP = icmph->code;
    memset(&source, 0, sizeof(source));
    DataPacket->sourceIP = iph->saddr;
    memset(&dest, 0, sizeof(dest));
    DataPacket->destIP = iph->daddr;
    array_packet += 28;
    DataPacket->PayLoad=array_packet;
}


int get_InDev_Name(struct nfq_data *nfq_pkt, char *indevname){

/* pointer to a nlif Interface resolving handle */
    struct nlif_handle *nlif_h;
    int ret = 0;
    nlif_h = fetch_interface_table();
    if(nlif_h == NULL){

        return(0);
    }
    if( nfq_get_indev_name(nlif_h,nfq_pkt,indevname) > 0 ){

        ret = 1;
    }
    fflush(stdout);
    nlif_close(nlif_h);
    return(ret);
}
int get_OutDev_Name(struct nfq_data *nfq_pkt, char *outdevname){

/* pointer to a nlif Interface resolving handle */
    struct nlif_handle *nlif_h;
    int ret=0;
    nlif_h=fetch_interface_table();
    if(nlif_h==NULL){

        return(0);
    }
    if( nfq_get_outdev_name(nlif_h,nfq_pkt,outdevname) > 0 ){

        ret = 1;
    }
    fflush(stdout);
    nlif_close(nlif_h);
    return(ret);
}
struct nlif_handle *fetch_interface_table(void){

    struct nlif_handle *h;
    h = nlif_open();
    if (h == NULL){

        perror("nlif_open");
        fflush(stderr);
        return(NULL);
    }
    nlif_query(h);
    return(h);
}



void printf_data_log_2(T_DataPacket *DataPacket){
    printf("---------------------\n");
    printf("      DATA LOG_2    \n");
    printf("---------------------\n");
    printf(" |-IP source: %s\n", Int32_To_String(DataPacket->sourceIP));
    printf(" |-IP destiny: %s\n", Int32_To_String(DataPacket->destIP));
    printf(" |-Interface output: %s\n",DataPacket->OutDev);

    printf("---------------------\n");
}
void printf_data_log(T_DataPacket *DataPacket){
    printf("---------------------\n");
    printf("      DATA LOG    \n");
    printf("---------------------\n");
    printf(" |-ICMP CODE: %d \n",DataPacket->codeICMP);
    printf(" |-IP source: %s\n", Int32_To_String(DataPacket->sourceIP));
    printf(" |-IP destiny: %s\n", Int32_To_String(DataPacket->destIP));
    printf(" |-IP source unreachable: %s\n", Int32_To_String(DataPacket->soUIP));
    printf(" |-IP destiny unreachable: %s\n", Int32_To_String(DataPacket->deUIP));
    printf("---------------------\n");

}