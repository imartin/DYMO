#include "RERR_container.h"
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <global_structs.h>
#include "../event_definition.h"

T_PrefixLenList *get_RerrPoint_PrefixLenList(T_RerrPoint *RerrPoint){

	return RerrPoint->PrefixLenList;
}
T_SeqNumList *get_RerrPoint_SeqNumList(T_RerrPoint *RerrPoint){

	return RerrPoint->SeqNumList;
}
T_MetricTypeList *get_RerrPoint_MetricTypeList(T_RerrPoint *RerrPoint){

	return RerrPoint->MetricTypeList;
}
T_CIDR *get_RerrPoint_PktSource(T_RerrPoint *RerrPoint){

	return RerrPoint->PktSource;
}

T_CIDR *get_RERR_PktSource(T_RERR *rerr){

	return rerr->PktSource;
}
void ini_AddressListRERR(T_AddressListRERR *AddressListRERR){

		AddressListRERR->overall = 0;
	}
void ini_PrefixLenList(T_PrefixLenList *PrefixLenList){

		PrefixLenList->overall = 0;
	}
void ini_SeqNumList(T_SeqNumList *SeqNumList){

		SeqNumList->overall = 0;
	}
void ini_MetricTypeList(T_MetricTypeList *MetricTypeList){

		MetricTypeList->overall = 0;
	}
void add_RERR_PktSource(T_RERR *rerr,T_CIDR *PktSource){

	rerr->PktSource = PktSource;
}
void add_PktSource_AddressPrefix(T_CIDR *PktSource,uint32_t Address,uint8_t PrefixLen){

	*PktSource = get_CIDR(Address, PrefixLen);
}

int add_AddressListRERR_Address(T_CIDR cidr,T_AddressListRERR *AddressListRERR){

	int rest;
	if(AddressListRERR->overall < MAX_ADDRESSLIST){

		AddressListRERR->Addresses[AddressListRERR->overall] = cidr;
		AddressListRERR->overall++;
		rest= ADDED_ITEM;
	}else{

		rest= FULL_ARRAY;
	}
	return rest;
}
int add_PrefixLenList_PrefixLen(uint8_t PrefixLen, T_PrefixLenList *PrefixLenList){

	int rest;
	if(PrefixLenList->overall < MAX_ADDRESSLIST){

		PrefixLenList->PrefixLengths[PrefixLenList->overall] = PrefixLen;
		PrefixLenList->overall++;
		rest= ADDED_ITEM;
	}else{

		rest= FULL_ARRAY;
	}
	return rest;
}
int add_SeqNumList_SeqNum(uint16_t SeqNum, T_SeqNumList *SeqNumList){

	int rest;
	if(SeqNumList->overall < MAX_ADDRESSLIST){

		SeqNumList->SeqNumbers[SeqNumList->overall] = SeqNum;
		SeqNumList->overall++;
		rest= ADDED_ITEM;
	}else{

		rest= FULL_ARRAY;
	}
	return rest;
}
int add_MetricTypeList_MetricTye(uint8_t MetricType, T_MetricTypeList *MetricTypeList){

	int rest;
	if(MetricTypeList->overall < MAX_ADDRESSLIST){

		MetricTypeList->MetricTypes[MetricTypeList->overall]=MetricType;
		MetricTypeList->overall++;
		rest= ADDED_ITEM;
	}else{

		rest= FULL_ARRAY;
	}
	return rest;
}
T_RERR create_RERR(T_CIDR *PktSource, T_AddressListRERR AddressListRERR, T_PrefixLenList *PrefixLenList,
                   T_SeqNumList *SeqNumList, T_MetricTypeList *MetricTypeList){

	T_RERR rerr_container;
	rerr_container.PktSource = PktSource;
	rerr_container.AddressListRERR = AddressListRERR;
	rerr_container.PrefixLenList = PrefixLenList;
	rerr_container.MetricTypeList = MetricTypeList;
	rerr_container.SeqNumList = SeqNumList;
	return rerr_container;
}
int get_RERR_AdressList_Overall(T_RERR * rerr){

	return rerr->AddressListRERR.overall;
}
int get_RERR_PrefixLengthList_Overall(T_RERR * rerr){

	if(rerr->PrefixLenList != NULL){

		return rerr->PrefixLenList->overall;
	}
}
int get_RERR_SeqnumList_Overall(T_RERR * rerr){


	if(rerr->SeqNumList != NULL) {

		return rerr->SeqNumList->overall;
	}
}
int get_RERR_MetricTypeList_Overall(T_RERR * rerr){

	if(rerr->SeqNumList != NULL) {

		return rerr->MetricTypeList->overall;
	}
}
uint32_t get_RERR_AdressList_Index(T_RERR * rerr,int index){

return Int8_To_32(rerr->AddressListRERR.Addresses[index]);

}
uint8_t get_RERR_PrefixLengthList_Index(T_RERR * rerr,int index) {

	return  rerr->AddressListRERR.Addresses[index].PrefixLen;

}
uint16_t get_RERR_SeqnumList_Index(T_RERR * rerr,int index){

	return  rerr->SeqNumList->SeqNumbers[index];

}
uint32_t get_RERR_PktSource_Address(T_RERR * rerr){

    return Int8_To_32(*rerr->PktSource);
}
uint8_t get_RERR_MetricTypeList_Index(T_RERR * rerr,int index){

	return  rerr->MetricTypeList->MetricTypes[index];

}

void print_RERR(T_RERR *rerr){

	printf("START : RERR CONTAINER:\n\n");
	if(rerr->PktSource!=NULL){

		printf("PKTSOURCE: ");
		for(int i = 0 ;i < 4; i++){

			printf(i != 3 ? "%u." : "%u", rerr->PktSource->Address[i]);
		}
		printf(" /%"PRIu8"\n\n",rerr->PktSource->PrefixLen);
	}
	printf("ADDRESLIST:\n");
	for(int i = 0; i < get_RERR_AdressList_Overall(rerr); i++){
		printf("Address:%u\n",Int8_To_32 (rerr->AddressListRERR.Addresses[i]));
	}
	if(rerr->MetricTypeList!=NULL){

		printf("METRICTYPELIST: \n");
		for(int i = 0; i < get_RERR_MetricTypeList_Overall(rerr); i++){

			printf("%u \n",rerr->MetricTypeList->MetricTypes[i]);
		}
	}
	if(rerr->SeqNumList!=NULL){

		printf("SEQNUMLIST: \n");
		for(int i = 0 ; i < get_RERR_SeqnumList_Overall(rerr);i++){

			printf("%"PRIu16"\n\n",rerr->SeqNumList->SeqNumbers[i]);
		}
		printf("FINISH : RERR CONTAINER\n");
	}
}

void free_RERR(T_RerrPoint *RerrPoint){

	free(RerrPoint->PrefixLenList);
	free(RerrPoint->SeqNumList);
	free(RerrPoint->MetricTypeList);
	free(RerrPoint->PktSource);
}
void null_RERR(T_RerrPoint *RerrPoint){

	RerrPoint->PrefixLenList = NULL;
	RerrPoint->SeqNumList = NULL;
	RerrPoint->MetricTypeList = NULL;
	RerrPoint->PktSource = NULL;
}


