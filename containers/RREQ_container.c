#include "RREQ_container.h"
#include <curses.h>
#include <inttypes.h>


uint8_t get_RREQ_MsgHopLimit(T_RREQ *rreq){

    return rreq->MsgHopLimit;
}
uint32_t get_RREQ_OrigPrefix(T_RREQ * rreq){

	return Int8_To_32(rreq->AddressList.OrigPrefix);
}
uint8_t get_RREQ_OrigPrefixLen(T_RREQ *rreq){

	return rreq->AddressList.OrigPrefix.PrefixLen;
}
uint32_t get_RREQ_TargPrefix(T_RREQ * rreq){

	return Int8_To_32(rreq->AddressList.TargPrefix);
}
uint8_t get_RREQ_TargPrefixLen(T_RREQ *rreq){

	return rreq->AddressList.TargPrefix.PrefixLen;
}
uint16_t get_RREQ_OrigSeqNum(T_RREQ *rreq){

    return rreq->OrigSeqNum;
}
uint16_t get_RREQ_TargSeqNum(T_RREQ *rreq){

    return rreq->TargSeqNum;
}
uint8_t get_RREQ_MetricType(T_RREQ *rreq){

    return rreq->MetricType;
}
uint8_t get_RREQ_OrigMetric(T_RREQ *rreq){

    return rreq->OrigMetric;
}

void add_RREQ_MsgHopLimit(T_RREQ *rreq, uint8_t MsgHopLimit){

	rreq->MsgHopLimit = MsgHopLimit;
}
void add_RREQ_OrigPrefix(T_RREQ * rreq, uint32_t Address, uint8_t OrigPrefixLen){

	T_CIDR  orig = get_CIDR(Address, OrigPrefixLen);
	rreq->AddressList.OrigPrefix = orig;
}
void add_RREQ_TargPrefix(T_RREQ * rreq, uint32_t Address, uint8_t TargPrefixLen){

	T_CIDR  targ = get_CIDR(Address, TargPrefixLen);
	rreq->AddressList.TargPrefix = targ;
}
void add_RREQ_OrigSeqNum(T_RREQ *rreq, uint16_t OrigSeqNum){

	rreq->OrigSeqNum = OrigSeqNum;
}
void add_RREQ_TargSeqNum(T_RREQ *rreq,uint16_t TargSeqNum){

	rreq->TargSeqNum = TargSeqNum;
}
void add_RREQ_MetricType(T_RREQ *rreq,uint8_t MetricType){

    rreq->MetricType = MetricType;
}
void add_RREQ_OrigMetric(T_RREQ *rreq,uint8_t OrigMetric){

    rreq->OrigMetric = OrigMetric;
}
int check_RREQ(T_RREQ *rreq){

    if(get_RREQ_MsgHopLimit(rreq) < 0 ||
       get_RREQ_MsgHopLimit(rreq) > MAX_HOPCOUNT ||
       get_RREQ_OrigMetric(rreq) <= 0 ||
       get_RREQ_OrigSeqNum(rreq) <= 0 ){

        return FALSE;
    }
    for(int i= 0; i < IPV4; i++){

        if(rreq->AddressList.OrigPrefix.Address[i] > 255 || rreq->AddressList.TargPrefix.Address[i] > 255){

            return FALSE;
        }
    }
    return TRUE;
}

void print_RREQ(T_RREQ  *rreq){

    printf("START : RREQ CONTAINER:\n");
    printf("- MSG-HOP-LIMIT: %"PRIu8"\n", get_RREQ_MsgHopLimit(rreq));
    printf("- ADDRESSLIST:\n");
    printf("   OrigPrefix: ");
    for(int i = 0;i < 4; i++){

        printf(i != 3 ? "%u." : "%u", rreq->AddressList.OrigPrefix.Address[i]);
    }
    printf(" /%"PRIu8"\n",rreq->AddressList.OrigPrefix.PrefixLen);
    printf("   TargPrefix: ");
    for(int i = 0;i < 4; i++ ) {

        printf(i != 3 ? "%u." : "%u", rreq->AddressList.TargPrefix.Address[i]);
    }
    printf(" /%"PRIu8"\n",rreq->AddressList.TargPrefix.PrefixLen);
    printf("- OrigSeqNum: %"PRIu8"\n",get_RREQ_OrigSeqNum(rreq));
    printf("- TargSeqNum: %"PRIu8"\n",get_RREQ_TargSeqNum(rreq));
    printf("- MetricType: %"PRIu8"\n",get_RREQ_MetricType(rreq));
    printf("- OrigMetric: %"PRIu8"\n",get_RREQ_OrigMetric(rreq));
    printf("FINISH: RREQ CONTAINER\n");
}




