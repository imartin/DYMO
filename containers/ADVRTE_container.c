#include "ADVRTE_container.h"
#include <stdio.h>
#include "../global_structs.h"




T_ADVRTE create_ADVRTE(uint32_t Address,
					   uint8_t PrefixLen,
					   uint16_t SeqNum,
					   uint32_t NextHop,
					   uint8_t MetricType,
					   uint8_t Metric,
					   uint8_t Cost)
	{
		T_ADVRTE advrte;
		advrte.Address=Address;
		advrte.PrefixLen=PrefixLen;
		advrte.SeqNum=SeqNum;
		advrte.NextHop=NextHop;
		advrte.MetricType=MetricType;
		advrte.Metric=Metric;
		advrte.Cost=Cost;
		return advrte;
	}

uint32_t get_ADVRTE_Address(T_ADVRTE* AdvRte)
	{
		return AdvRte->Address;
	}
uint8_t  get_ADVRTE_Prefixlength(T_ADVRTE* AdvRte)
	{
		return AdvRte->PrefixLen;
	}
uint16_t get_ADVRTE_Seqnum(T_ADVRTE* AdvRte)
	{
		return AdvRte->SeqNum;
	}
uint32_t get_ADVRTE_Nexthop(T_ADVRTE* AdvRte)
	{
		return AdvRte->NextHop;
	}
uint8_t  get_ADVRTE_Metrictype(T_ADVRTE* AdvRte)
	{
		return AdvRte->MetricType;
	}
uint8_t  get_ADVRTE_Metric(T_ADVRTE* AdvRte)
	{
		return AdvRte->Metric;
	}
uint8_t  get_ADVRTE_Cost(T_ADVRTE* AdvRte)
	{
		return AdvRte->Cost;
	}

void print_ADVRTE(T_ADVRTE *AdvRte)
	{
		printf("----AdvRte CONTAINER------\n");
		printf("IPAddress: %s\n", Int32_To_String(get_ADVRTE_Address(AdvRte)));
		printf("prefixlegnt:  %u\n",get_ADVRTE_Prefixlength(AdvRte));
		printf("SeqNum: %u\n",get_ADVRTE_Seqnum(AdvRte));
		printf("NextHop: %s\n", Int32_To_String(get_ADVRTE_Nexthop(AdvRte)));
		printf("MetricType: %u\n",get_ADVRTE_Metrictype(AdvRte));
		printf("Metric: %u\n",get_ADVRTE_Metric(AdvRte) );
		printf("Cost: %u\n",get_ADVRTE_Cost(AdvRte));
		printf("---------------------------\n\n");

	}
