
#ifndef RREQ_CONTAINER_H
#define RREQ_CONTAINER_H

#include <stdint.h>
#include "../global_structs.h"

typedef struct{

	uint8_t MsgHopLimit;
	T_AddressList AddressList;
	uint8_t *PrefixLenList;
	uint16_t OrigSeqNum;
	uint16_t TargSeqNum;
	uint8_t MetricType;
	uint8_t OrigMetric;
}T_RREQ;

int check_RREQ(T_RREQ *rreq);

uint8_t  get_RREQ_MsgHopLimit (T_RREQ *rreq);
uint32_t get_RREQ_OrigPrefix(T_RREQ * rreq);
uint8_t  get_RREQ_OrigPrefixLen(T_RREQ *rreq);
uint32_t get_RREQ_TargPrefix(T_RREQ * rreq);
uint8_t  get_RREQ_TargPrefixLen(T_RREQ *rreq);
uint16_t get_RREQ_OrigSeqNum(T_RREQ *rreq);
uint16_t get_RREQ_TargSeqNum(T_RREQ *rreq);
uint8_t  get_RREQ_MetricType(T_RREQ *rreq);
uint8_t  get_RREQ_OrigMetric(T_RREQ *rreq);

void add_RREQ_MsgHopLimit(T_RREQ *rreq, uint8_t MsgHopLimit);
void add_RREQ_TargPrefix(T_RREQ * rreq, uint32_t Address, uint8_t TargPrefixLen);
void add_RREQ_OrigPrefix(T_RREQ * rreq, uint32_t Address, uint8_t OrigPrefixLen);
void add_RREQ_OrigSeqNum(T_RREQ *rreq, uint16_t OrigSeqNum);
void add_RREQ_TargSeqNum(T_RREQ *rreq,uint16_t TargSeqNum);
void add_RREQ_MetricType(T_RREQ *rreq,uint8_t MetricType);
void add_RREQ_OrigMetric(T_RREQ *rreq,uint8_t OrigMetric);

void print_RREQ(T_RREQ *rreq);
#endif