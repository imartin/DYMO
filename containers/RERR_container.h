#ifndef RERR_CONTAINER_H
#define RERR_CONTAINER_H

#include <stdint.h>
#include "../constant_definition.h"
#include "../global_structs.h"

#define MAX_ADDRESSLIST 10
#define MAX_PREFIXLENGHTLIST 10
#define MAX_SEQNUMLIST 10
#define MAX_METRICTYPELIST 10

typedef struct{

	T_CIDR Addresses[MAX_ADDRESSLIST];
	int overall;
}T_AddressListRERR;

typedef struct{

	uint8_t PrefixLengths[MAX_PREFIXLENGHTLIST];
	int overall;
}T_PrefixLenList;

typedef struct{

	uint16_t SeqNumbers[MAX_SEQNUMLIST];
	int overall;
}T_SeqNumList;

typedef struct{

	uint8_t MetricTypes[MAX_METRICTYPELIST];
	int overall;
}T_MetricTypeList;

typedef struct{

	T_CIDR *PktSource;
	T_AddressListRERR AddressListRERR;
	T_PrefixLenList *PrefixLenList;
	T_SeqNumList *SeqNumList;
	T_MetricTypeList *MetricTypeList;
}T_RERR;

typedef struct{

	T_PrefixLenList *PrefixLenList;
	T_SeqNumList *SeqNumList;
	T_MetricTypeList *MetricTypeList;
	T_CIDR *PktSource;
}T_RerrPoint;

T_PrefixLenList *get_RerrPoint_PrefixLenList(T_RerrPoint *RerrPoint);
T_SeqNumList *get_RerrPoint_SeqNumList(T_RerrPoint *RerrPoint);
T_MetricTypeList *get_RerrPoint_MetricTypeList(T_RerrPoint *RerrPoint);
T_CIDR *get_RerrPoint_PktSource(T_RerrPoint *RerrPoint);

void free_RERR(T_RerrPoint *RerrPoint);
void null_RERR(T_RerrPoint *RerrPoint);
void ini_AddressListRERR(T_AddressListRERR *AddressListRERR);
void ini_PrefixLenList(T_PrefixLenList *PrefixLenList);
void ini_SeqNumList(T_SeqNumList *SeqNumList);
void ini_MetricTypeList(T_MetricTypeList *MetricTypeList);
int add_AddressListRERR_Address(T_CIDR cidr, T_AddressListRERR *AddressListRERR);
int add_PrefixLenList_PrefixLen(uint8_t PrefixLen, T_PrefixLenList *PrefixLenList);
int add_SeqNumList_SeqNum(uint16_t SeqNum, T_SeqNumList *SeqNumList);
int add_MetricTypeList_MetricTye(uint8_t MetricType, T_MetricTypeList *MetricTypeList);
T_CIDR *get_RERR_PktSource(T_RERR *rerr);
void add_RERR_PktSource(T_RERR * rerr, T_CIDR *PktSource);
void add_PktSource_AddressPrefix(T_CIDR *PktSource,uint32_t Address, uint8_t PrefixLen);
uint32_t get_RERR_PktSource_Address(T_RERR * rerr);
T_RERR create_RERR(T_CIDR *PktSource, T_AddressListRERR AddressListRERR, T_PrefixLenList *PrefixLenList,
                   T_SeqNumList *SeqNumList, T_MetricTypeList *MetricTypeList);
int get_RERR_AdressList_Overall(T_RERR * rerr);
int get_RERR_PrefixLengthList_Overall(T_RERR * rerr);
int get_RERR_SeqnumList_Overall(T_RERR * rerr);
int get_RERR_MetricTypeList_Overall(T_RERR * rerr);
uint32_t get_RERR_AdressList_Index(T_RERR *rerr,int index);
uint8_t get_RERR_PrefixLengthList_Index(T_RERR *rerr,int index);
uint16_t get_RERR_SeqnumList_Index(T_RERR *rerr,int index);
uint8_t get_RERR_MetricTypeList_Index(T_RERR *rerr,int index);

void print_RERR(T_RERR *rerr);
#endif