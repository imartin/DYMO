#ifndef ADVRTE_CONTAINER_H
#define ADVRTE_CONTAINER_H

#include <stdint.h>
/**
 * @brief  Structure that describes a route advertised in an incoming route message.
 */
typedef struct 
{
	uint32_t Address;///< Unsigned integer type with width of exactly 32 bits which encodes the IPv4 address RteMsg.OrigPrefix (in RREQ) or RteMsg.TargPrefix(in RREP).
	uint8_t  PrefixLen;///< Unsigned integer type with width of exactly 8 bits which encodes RteMsg.OrigPrefixLen (in RREQ) or RteMsg.TargPrefixLen (in RREP).
	uint16_t SeqNum; ///< Unsigned integer type with width of exactly 16 bits which encodes RteMsg.OrigSeqNum (in RREQ) or RteMsg.TargSeqNum (in RREP).
	uint32_t NextHop;///< Unsigned integer type with width of exactly 32 bits which encodes RteMsg.IPSourceAddress (an address of the sending interface of the router from which the RteMsg was received)
	uint8_t  MetricType;///< Unsigned integer type with width of exactly 8 bits which encodes RteMsg.MetricType
	uint8_t  Metric;///< Unsigned integer type with width of exactly 8 bits which encodes RteMsg.Metric
	uint8_t  Cost;///< Unsigned integer type with width of exactly 8 bits which encodes Cost(R) using the cost function associated with the  routes metric type. For cost metrics, Cost(R) =dvRte.Metric + Cost(L)

}T_ADVRTE;

/**
 * @brief This metod create a T_ADVRTE from initial values
 * @param Address
 * @param PrefixLen
 * @param SeqNum
 * @param NextHop
 * @param MetricType
 * @param Metric
 * @param Cost
 * @return T_ADVRTE structure
 */
T_ADVRTE create_ADVRTE(uint32_t Address, uint8_t PrefixLen, uint16_t SeqNum, uint32_t NextHop, uint8_t MetricType,uint8_t Metric, uint8_t Cost);
/**
 *
 * @param AdvRte
 * @return Address of T_ADVRTE
 */
uint32_t get_ADVRTE_Address(T_ADVRTE *AdvRte);
uint8_t  get_ADVRTE_Prefixlength(T_ADVRTE *AdvRte);
uint16_t get_ADVRTE_Seqnum(T_ADVRTE *AdvRte);
uint32_t get_ADVRTE_Nexthop(T_ADVRTE *AdvRte);
uint8_t  get_ADVRTE_Metrictype(T_ADVRTE *AdvRte);
uint8_t  get_ADVRTE_Metric(T_ADVRTE *AdvRte);
uint8_t  get_ADVRTE_Cost(T_ADVRTE *AdvRte);
void print_ADVRTE(T_ADVRTE *AdvRte);

#endif