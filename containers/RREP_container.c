#include "RREP_container.h"
#include <curses.h>
#include <inttypes.h>
#include <global_structs.h>


int check_RREP(T_RREP *rrep){

	if(get_RREP_MsgHopLimit(rrep) < 0 || get_RREP_MsgHopLimit(rrep) > MAX_HOPCOUNT || get_RREP_TargMetric(rrep) <= 0 || get_RREP_TargSeqNum(rrep) <= 0 ){

		return FALSE;
	}
	for(int i = 0; i < IPV4; i++){

		if(rrep->AddressList.OrigPrefix.Address[i] > 255 || rrep->AddressList.TargPrefix.Address[i] > 255){

			return FALSE;
		}
	}
	return TRUE;
}

uint8_t get_RREP_MsgHopLimit(T_RREP *rrep){

	return rrep->MsgHopLimit;
}
uint32_t get_RREP_OrigPrefix(T_RREP * rrep){

	return Int8_To_32(rrep->AddressList.OrigPrefix);
}
uint8_t get_RREP_OrigPrefixLen(T_RREP *rrep){

	return rrep->AddressList.OrigPrefix.PrefixLen;
}
uint32_t get_RREP_TargPrefix(T_RREP * rrep){

	return Int8_To_32(rrep->AddressList.TargPrefix);
}
uint8_t get_RREP_TargPrefixLen(T_RREP *rrep){

	return rrep->AddressList.TargPrefix.PrefixLen;
}
uint16_t get_RREP_TargSeqNum(T_RREP *rrep){

	return rrep->TargSeqNum;
}
uint8_t get_RREP_MetricType(T_RREP *rrep){

	return rrep->MetricType;
}
uint8_t get_RREP_TargMetric(T_RREP *rrep){

	return rrep->TargMetric;
}
void add_RREP_MsgHopLimit(T_RREP *rrep, uint8_t MsgHopLimit){

	rrep->MsgHopLimit = MsgHopLimit;
}
void add_RREP_TargPrefix(T_RREP * rrep, T_CIDR *TargPrefix){

	rrep->AddressList.TargPrefix = *TargPrefix;
}
void add_RREP_Origprefix(T_RREP * rrep, T_CIDR *OrigPrefix){

	rrep->AddressList.OrigPrefix = *OrigPrefix;
}
void add_RREP_TargSeqNum(T_RREP *rrep,uint16_t TargSeqNum){

	rrep->TargSeqNum = TargSeqNum;
}
void add_RREP_MetricType(T_RREP *rrep,uint8_t MetricType){

	rrep->MetricType = MetricType;
}
void add_RREP_TargMetric(T_RREP *rrep,uint8_t TargMetric){

	rrep->TargMetric = TargMetric;
}
T_AddressList *get_RREP_AddressList(T_RREP *rrep){

	return &rrep->AddressList;
}
void print_RREP(T_RREP *rrep){

	int cont;
	printf("---------------------------------------------------\n");
	printf("START : rrep CONTAINER:\n\n");
	printf("- MSG-HOP-LIMIT: %"PRIu8"\n", get_RREP_MsgHopLimit(rrep));
	printf("- ADDRESSLIST:\n\n");
	printf("   OrigPrefix: ");
	for(cont=0;cont<4;cont++){

		printf(cont != 3 ? "%u." : "%u", rrep->AddressList.OrigPrefix.Address[cont]);
	}
	printf(" /%"PRIu8"\n",rrep->AddressList.OrigPrefix.PrefixLen);
	printf("   TargPrefix: ");
	for(cont=0;cont<4;cont++){

		printf(cont != 3 ? "%u." : "%u", rrep->AddressList.TargPrefix.Address[cont]);
	}
	printf(" /%"PRIu8"\n\n",rrep->AddressList.TargPrefix.PrefixLen);

	if(rrep->PrefixLengthList!=NULL){

		printf("- PrefixLenList: %"PRIu8"\n",*rrep->PrefixLengthList);
	}
	printf("- TargSeqNum: %"PRIu8"\n",get_RREP_TargSeqNum(rrep));
	printf("- MetricType: %"PRIu8"\n",get_RREP_MetricType(rrep));
	printf("- TargMetric: %"PRIu8"\n\n",get_RREP_TargMetric(rrep));
	printf("FINISH: rrep CONTAINER\n\n");
	printf("---------------------------------------------------\n");

}




