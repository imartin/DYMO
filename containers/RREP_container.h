#ifndef RREP_CONTAINER_H
#define RREP_CONTAINER_H


#include <stdint.h>
#include "../global_structs.h"

typedef struct{

	uint8_t MsgHopLimit;
	T_AddressList AddressList;
	uint8_t *PrefixLengthList;
	uint16_t TargSeqNum;
	uint8_t MetricType;
	uint8_t TargMetric;
}T_RREP;

uint8_t  get_RREP_MsgHopLimit(T_RREP *rrep);
uint32_t get_RREP_OrigPrefix(T_RREP * rrep);
uint8_t  get_RREP_OrigPrefixLen(T_RREP *rrep);
uint32_t get_RREP_TargPrefix(T_RREP * rrep);
uint8_t  get_RREP_TargPrefixLen(T_RREP *rrep);
uint16_t get_RREP_TargSeqNum(T_RREP *rrep);
uint8_t  get_RREP_MetricType(T_RREP *rrep);
uint8_t  get_RREP_TargMetric(T_RREP *rrep);
T_AddressList *get_RREP_AddressList(T_RREP *rrep);

void add_RREP_MsgHopLimit(T_RREP *rrep, uint8_t MsgHopLimit);
void add_RREP_TargPrefix(T_RREP * rrep, T_CIDR *TargPrefix);
void add_RREP_Origprefix(T_RREP * rrep, T_CIDR *OrigPrefix);
void add_RREP_TargSeqNum(T_RREP *rrep,uint16_t TargSeqNum);
void add_RREP_MetricType(T_RREP *rrep,uint8_t MetricType);
void add_RREP_TargMetric(T_RREP *rrep,uint8_t TargMetric);

int check_RREP(T_RREP *rrep);

void print_RREP(T_RREP  *rrep);

#endif