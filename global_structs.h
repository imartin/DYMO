#ifndef GLOBAL_STRUCTS_H
#define GLOBAL_STRUCTS_H

#include <stdint.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include "data_set/LocalRouteSet.h"
#include "data_set/MulticastRouteMessageSet.h"
#include "data_set/NeighborSet.h"
#include "data_set/InterfaceSet.h"
#include "data_set/RouterClientSet.h"
#include "data_set/RouteErrorSet.h"
#include "data_set/MetricSet.h"
#include "data_set/SequenceNumberSet.h"



typedef struct{

    uint8_t Address[4];
    uint8_t PrefixLen;
}T_CIDR;
typedef struct{
    T_CIDR OrigPrefix;
    T_CIDR TargPrefix;
}T_AddressList;

typedef struct{

    u_int8_t HwAddress[6];
    int idPacket;
    int size_payload;
    uint32_t sourceIP;
    uint32_t destIP;
    char IntDev[256];
    char OutDev[256];
    uint8_t *PayLoad;
    uint32_t soUIP;
    uint32_t deUIP;
    u_int8_t codeICMP;


}T_DataPacket;

int get_DataPacket_IdPacket(T_DataPacket *DataPacket);
uint32_t get_DataPacket_sourceIP(T_DataPacket *DataPacket);
uint32_t get_DataPacket_destIP(T_DataPacket *DataPacket);
char *get_DataPacket_IntDev(T_DataPacket *DataPacket);
char *get_DataPacket_OutDev(T_DataPacket *DataPacket);
uint8_t *get_DataPacket_Payload(T_DataPacket *DataPacket);
uint32_t get_DataPacket_deUIP(T_DataPacket *DataPacket);
uint32_t get_DataPacket_soUIP(T_DataPacket *DataPacket);
int get_DataPacket_codeICMP(T_DataPacket *DataPacket);




typedef struct{

    T_RerrMsgSet *point_RerrMsgSet;
    T_NeighborSet  *point_NeighborSet;
    T_InterfaceSet *point_InterfaceSet;
    T_RouterClientSet *point_RouterClientSet;
    T_MetricSet *point_MetricSet;
    T_SeqNumSet *point_SeqNumSet;
    T_LocalRouteSet *point_LocalRouteSet;
    T_McMsgSet *point_McMsgSet;

    pthread_mutex_t *Mutex;
    uint8_t RREQ_GEN_WAIT_TIME;
    uint32_t black_Address;
    int NlSocket;
    T_RouterClient *RouterClient;
    T_DataPacket *DataPacket;
    T_RerrMsg RerrMsg;
    T_AddressList *AddressList;
    char  InterfaceId[256];
    uint32_t InterfaceIp;
    uint32_t InterfaceMasc;
    uint32_t OrigPrefixRREP;
    T_LocalRoute LocalRoute;

}T_DataPoint;

T_LocalRoute get_DataPoint_LocalRoute(T_DataPoint * DataPoint);
void add_DataPoint_LocalRoute(T_DataPoint *DataPoint, T_LocalRoute *LocalRoute);
uint32_t get_DataPoint_OrigPrefixRREP(T_DataPoint * DataPoint);
void add_DataPoint_OrigPrefixRREP(T_DataPoint * DataPoint, uint32_t OrigPrefixRREP );
T_AddressList *get_DataPoint_AddressList(T_DataPoint *DataPoint);
void add_DataPoint_AddressList(T_DataPoint *DataPoint,T_AddressList *AddressList);
T_CIDR get_DataPoint_AddressList_OrigPrefix(T_DataPoint *DataPoint);
T_CIDR get_DataPoint_AddressList_TargPrefix(T_DataPoint *DataPoint);

T_RerrMsgSet *get_DataPoint_RerrMsgSet(T_DataPoint *DataPoint);
T_InterfaceSet *get_DataPoint_InterfaceSet(T_DataPoint *DataPoint);
T_McMsgSet *get_DataPoint_McMsgSet(T_DataPoint *DataPoint);
T_LocalRouteSet *get_DataPoint_LocalRouteSet(T_DataPoint *DataPoint);
T_SeqNumSet *get_DataPoint_SeqNumSet(T_DataPoint *DataPoint);
T_NeighborSet *get_DataPoint_NeighborSet(T_DataPoint *DataPoint);
T_MetricSet *get_DataPoint_MetricSet(T_DataPoint *DataPoint);
uint32_t get_DataPoint_NeighborBlacklist(T_DataPoint *DataPoint);
T_RerrMsg get_DataPoint_RerrMsg(T_DataPoint *DataPoint);
void add_DataPoint_RerrMsg(T_DataPoint *DataPoint,T_RerrMsg *RerrMsg);
T_RouterClientSet *get_DataPointers_RouterClientSet(T_DataPoint *DataPoint);
char  *get_DataPoint_InterfaceId(T_DataPoint *DataPoint);
uint32_t get_DataPoint_InterfaceIp(T_DataPoint *DataPoint);
uint32_t get_DataPoint_InterfaceMasc(T_DataPoint *DataPoint);
void add_DataPoint_InterfaceId(T_DataPoint *DataPoint,char InterfaceId[256]);
void add_DataPoint_InterfaceIp(T_DataPoint *DataPoint,uint32_t InterfaceIp);
void add_DataPoint_InterfaceMasc(T_DataPoint *DataPoint, uint32_t InterfaceMasc);


uint32_t get_CIDR_Adress(T_CIDR *cidr);
void ini_DataPoint(T_DataPoint *DataPoint);
void insert_NetFilterRules(uint32_t Address, T_DataPoint *DataPoint);
void print_MacAddress(uint8_t *cMacAddr);
int Mask_To_Prefix(u_int32_t mask);
uint32_t Prefix_To_Mask(int prefix);

uint32_t string2int(char *Address);
int check_WirelessInterface(const char *IfName, char *Protocol);
long long int get_CTime_ms();
char *Int32_To_String(uint32_t Address);
T_CIDR get_CIDR(uint32_t Address, uint8_t Prefix);

char *Int_To_StringN(int nState);
char *Int_To_StringL(int lState);
uint16_t Int8_To_16(uint8_t oData, uint8_t uData);
uint8_t *Int16_To_8(uint16_t uData);
uint32_t Int8_To_32(T_CIDR Cidr);
char *Int_To_StringEvent(int Event);
int valid_Address(T_CIDR);
in_addr_t get_NetAddress(in_addr_t Addr, int Prefix);
uint32_t Int_To_32(int prefix);
#endif
