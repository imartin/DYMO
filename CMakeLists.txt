cmake_minimum_required(VERSION 3.9)
project(Programa_v4)

set(CMAKE_CXX_STANDARD 11)

include_directories(.)
include_directories(containers)
include_directories(data_set)
include_directories(linux)
include_directories(processes)
include_directories(RFC5444)
include_directories(monitors_pthreads_v3)

add_executable(
        Programa_v4
        main.c
        global_structs.c
        global_structs.h
        containers/ADVRTE_container.c
        containers/RERR_container.c
        containers/RREP_container.c
        containers/RREQ_container.c
        data_set/LocalRouteSet.c
        data_set/InterfaceSet.c
        data_set/MetricSet.c
        data_set/MulticastRouteMessageSet.c
        data_set/NeighborSet.c
        data_set/RouteErrorSet.c
        data_set/RouterClientSet.c
        data_set/SequenceNumberSet.c
        processes/applying_route_updates_process.c
        processes/evaluating_route_information_process.c
        processes/initialitation_process.c
        processes/RERR_processes.c
        processes/RREP_ACK_processes.c
        processes/RREP_processes.c
        processes/RREQ_processes.c
        processes/suppressing_redundant_messages_process.c
        RFC5444/gman_packet.c
        RFC5444/gman_packet_parse.c
        RFC5444/gman_packet_serialized.c
        RFC5444/parse_container.c
        operative_system/operative_system_functions.c
        start_routine/consola.c
        start_routine/ExternalQueue.c
        start_routine/InternalQueue.c
        start_routine/Nflog.c
        start_routine/Nflog_activity.c
        start_routine/Network_Interface_Status.c
        monitors_pthreads_v3/thread_message.c
        monitors_pthreads_v3/thread_queue_manager.c
        monitors_pthreads_v3/queue.c
        monitors_pthreads_v3/monitor.c
        monitors_pthreads_v3/timer.c
        monitors_pthreads_v3/message_codes.h
        debug.c
        constant_definition.h
        event_definition.h
        start_routine/ACK_activity.c
        start_routine/ACK_activity.h
        Timers/timers.c Timers/timers.h
        data/Neighbor.c
        data/Neighbor.h
        data/LocalRoute.c
        data/LocalRoute.h
        data/RouterClient.c
        data/RouterClient.h
        data/MulicastRouteMessage.c
        data/MulicastRouteMessage.h data/RouteError.c data/RouteError.h processes/Suppressing_Redundant_RouteError_Messages_Process.c processes/Suppressing_Redundant_RouteError_Messages_Process.h data/Interface.c data/Interface.h)

target_link_libraries(Programa_v4 -pthread -lnfnetlink -lnetfilter_queue -lnetfilter_log)