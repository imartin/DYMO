#!/bin/sh

/sbin/iptables -F
/sbin/iptables -F -t mangle

/sbin/iptables -I INPUT -p UDP -s 10.0.3.20 -d 10.0.3.255 --sport 269 --dport 269 -j DROP
/sbin/iptables -A INPUT -p UDP --sport 269 --dport 269 -j NFQUEUE --queue-num 0



/sbin/iptables -t mangle -N state
/sbin/iptables -t mangle -A POSTROUTING -j state
/sbin/iptables -t mangle -A state -s 127.0.0.1/8  -j RETURN
/sbin/iptables -t mangle -A state -p udp --sport 269 -j RETURN
/sbin/iptables -t mangle -A state -p udp --dport 269 -j RETURN
/sbin/iptables -t mangle -A state -j NFLOG --nflog-group 11 --nflog-prefix foo

/sbin/iptables -N aodv
/sbin/iptables -A aodv -j RETURN
/sbin/iptables -I OUTPUT 1 -s 127.0.0.1/8 -j ACCEPT
/sbin/iptables -I OUTPUT 2 -d 224.0.0.0/4 -j DROP
/sbin/iptables -I OUTPUT 3 -p UDP --sport 269 --dport 269 -j ACCEPT
/sbin/iptables -I OUTPUT 4 -p icmp --icmp-type destination-unreachable -j NFLOG --nflog-group 10 --nflog-prefix foo
/sbin/iptables -I OUTPUT 5 -d 10.0.3.20 -s 10.0.3.20 -j ACCEPT
/sbin/iptables -t filter -I OUTPUT 6 -j aodv
/sbin/iptables -I OUTPUT 7 -j NFQUEUE --queue-num 1

/sbin/ip -s -s neigh flush all





echo 1 > /proc/sys/net/ipv4/ip_forward

sh -c 'echo 1 > /proc/sys/net/ipv6/conf/lo/disable_ipv6'

ip route flush table main

echo 0 > /proc/sys/net/ipv4/conf/eth0/send_redirects
echo 0 > /proc/sys/net/ipv4/conf/all/send_redirects
echo 0 > /proc/sys/net/ipv4/conf/all/secure_redirects
exit
