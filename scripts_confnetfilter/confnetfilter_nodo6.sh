#!/bin/sh

ip route flush table main

/sbin/iptables -F
/sbin/iptables -F -t mangle

/sbin/iptables -I INPUT -p UDP -s 10.0.3.6 -d 10.0.3.255 --sport 269 --dport 269 -j DROP
/sbin/iptables -A INPUT -p UDP --sport 269 --dport 269 -j NFQUEUE --queue-num 0

/sbin/iptables -t mangle -I PREROUTING 1 -m mac --mac-source 08:00:00:00:00:03 -j DROP
/sbin/iptables -t mangle -I PREROUTING 2 -m mac --mac-source 08:00:00:00:00:04 -j DROP
/sbin/iptables -t mangle -I PREROUTING 3 -p UDP --sport 269 --dport 269 -m length --length 28:37 -j NFQUEUE --queue-num 4


/sbin/iptables -t mangle -N state
/sbin/iptables -t mangle -A POSTROUTING -j state
/sbin/iptables -t mangle -A state -s 127.0.0.1/8  -j RETURN
/sbin/iptables -t mangle -A state -p udp --sport 269 -j RETURN
/sbin/iptables -t mangle -A state -p udp --dport 269 -j RETURN
/sbin/iptables -t mangle -A state -j NFLOG --nflog-group 3 --nflog-prefix PACKET

/sbin/iptables -N aodv
/sbin/iptables -A aodv -j RETURN
/sbin/iptables -I OUTPUT 1 -s 127.0.0.1/8 -j ACCEPT
/sbin/iptables -I OUTPUT 2 -d 224.0.0.0/4 -j DROP
/sbin/iptables -I OUTPUT 3 -p UDP --sport 269 --dport 269 -j ACCEPT
/sbin/iptables -I OUTPUT 4 -p icmp --icmp-type destination-unreachable -j NFLOG --nflog-group 2 --nflog-prefix ICMP_TYPE3
/sbin/iptables -I OUTPUT 5 -d 10.0.3.6 -s 10.0.3.6 -j ACCEPT
/sbin/iptables -t filter -I OUTPUT 6 -j aodv
/sbin/iptables -I OUTPUT 7 -j NFQUEUE --queue-num 1

/sbin/ip -s -s neigh flush all

/sbin/arptables -F

/sbin/arptables -A INPUT --source-mac 08:00:00:00:00:03 -j DROP
/sbin/arptables -A INPUT --source-mac 08:00:00:00:00:04 -j DROP

echo 1 > /proc/sys/net/ipv4/ip_forward

sh -c 'echo 1 > /proc/sys/net/ipv6/conf/lo/disable_ipv6'

echo 0 > /proc/sys/net/ipv4/conf/enp0s3/send_redirects
echo 0 > /proc/sys/net/ipv4/conf/all/send_redirects
echo 0 > /proc/sys/net/ipv4/conf/all/secure_redirects
exit
