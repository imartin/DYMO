//
// Created by carlos on 29/03/18.
//

#ifndef PROGRAMA_V4_TIMERS_H
#define PROGRAMA_V4_TIMERS_H
/**
 *
 * @param arg
 * @return
 */
void *timer_RREP_ACK_SENT_TIMEOUT(void *arg);
/**
 *
 * @param arg
 * @return
 */
void *timer_MAX_BLACKLIST_TIME(void *arg);
/**
 *
 * @param arg
 * @return
 */
void *timer_RERR_TIMEOUT(void *arg);
/**
 *
 * @param arg
 * @return
 */
void *timer_RREQ_WAIT_TIME (void *arg);

void *timer_MAX_SEQNUM_LIFETIME(void *arg);
void *timer_ACTIVE_INTERVAL(void *arg);

#endif //PROGRAMA_V4_TIMERS_H
