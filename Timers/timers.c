
#include "timers.h"
#include <zconf.h>
#include <stdio.h>
#include "../containers/RREQ_container.h"
#include "../processes/RREQ_processes.h"

#include "../event_definition.h"
#include "../monitors_pthreads_v3/thread_queue_manager.h"



void *timer_ACTIVE_INTERVAL(void *arg){

    T_DataPoint *DataPoint = (T_DataPoint *) arg;
    T_LocalRoute LocalRoute = get_DataPoint_LocalRoute (DataPoint);
    struct timespec Timeout = {

            .tv_sec = ACTIVE_INTERVAL,
            .tv_nsec = 0
    };
    T_Timer * Timer = create_timer(Timeout, NULL,NULL);

    add_LocalRoute_TimerActiveInterval (&LocalRoute,Timer);

    add_LocalRoute_State(&LocalRoute,ACTIVE);
    printf ("Route to %s is %s\n",Int32_To_String(get_LocalRoute_Address(&LocalRoute)),Int_To_StringL(get_LocalRoute_State (&LocalRoute)));
    /*The LocalRoute of the LocalRouteSet is updated*/
    update_LocalRouteSet_LocalRoute (get_DataPoint_LocalRouteSet (DataPoint),&LocalRoute);
    printf("Timer Active Interval Start\n");

    start_timer(Timer);


    /*Waiting for the timer to end*/
    wait_for_timeout(Timer);


    if(get_status (Timer) == TIMEOUT_ABORTED) {


        printf ("timer_ActiveInterval ABORTED\n");
        pthread_exit (0);
    }
    else if(get_status (Timer) == TIMEOUT_EXPIRED){

        printf ("timer_ActiveInterval EXPIRED\n");

        add_LocalRoute_State(&LocalRoute,IDLE);
        printf ("Route to %s is %s\n",Int32_To_String(get_LocalRoute_Address(&LocalRoute)),Int_To_StringL(get_LocalRoute_State (&LocalRoute)));
        update_LocalRouteSet_LocalRoute (get_DataPoint_LocalRouteSet (DataPoint),&LocalRoute);

        pthread_exit(0);
    }
    else{
        printf ("timer_ActiveInterval FAIL\n");
        pthread_exit(0);
    }
}
void *timer_MAX_SEQNUM_LIFETIME(void *arg){

    T_DataPoint *DataPoint = (T_DataPoint *) arg;
    T_LocalRoute LocalRoute = get_DataPoint_LocalRoute (DataPoint);
    struct timespec Timeout = {

            .tv_sec = MAX_SEQNUM_LIFETIME,
            .tv_nsec = 0
    };
    T_Timer * Timer = create_timer(Timeout, NULL,NULL);   /*Assign the timer_MAX_SEQNUM_LIFETIME to its corresponding LocalRoute*/
    add_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute , Timer);

    /*The LocalRoute of the LocalRouteSet is updated*/
    update_LocalRouteSet_LocalRoute (get_DataPoint_LocalRouteSet (DataPoint),&LocalRoute);

    /*Start the timer*/
    start_timer(Timer);
    printf("timer_MAX_SEQNUM_LIFETIME for LocalRoute.Address = %s Start\n",Int32_To_String (get_LocalRoute_Address (&LocalRoute)));

    /*Waiting for the timer to end*/
    wait_for_timeout(Timer);

    if(get_status (Timer) == TIMEOUT_ABORTED) {

        printf ("timer_MAX_SEQNUM_LIFETIME ABORTED\n");
        pthread_exit (0);
    }
    else if(get_status (Timer) == TIMEOUT_EXPIRED){

        /*The LocalRoute is removed from the LocalRouteSet*/
        del_LocalRouteSet_LocalRoute_Position (get_DataPoint_LocalRouteSet (DataPoint),&LocalRoute);
        printf ("timer_MAX_SEQNUM_LIFETIME EXPIRED\n");
        pthread_exit(0);
    }
    else{
        printf ("timer_MAX_SEQNUM_LIFETIME FAIL\n");
        pthread_exit(0);
    }
}
void *timer_RREQ_WAIT_TIME (void *arg){

    int err;
    T_DataPoint *DataPoint = (T_DataPoint *) arg;
    printf("INTERNAL: WAIT RREP: %ds\n",DataPoint->RREQ_GEN_WAIT_TIME);
    /**/
    sleep(DataPoint->RREQ_GEN_WAIT_TIME);
    for(int i = 0; i< DISCOVERY_ATTEMPTS_MAX; i++){

        err = send_RREQ_Packet(DataPoint->RouterClient,DataPoint->DataPacket,DataPoint);
        if(err != CORRECT_OPERATION){

            printf("timer_RREQ_WAIT_TIME send_RREQ_Packet: FAIL");
            pthread_exit(0);
        }else{

            printf ("Send RREQ\n");
        }
        DataPoint->RREQ_GEN_WAIT_TIME = ( uint8_t ) ((DataPoint->RREQ_GEN_WAIT_TIME) * EXPONENTIAL_BACKOFF );
        printf("INTERNAL: WAIT RREP: %ds\n",DataPoint->RREQ_GEN_WAIT_TIME);
        sleep(DataPoint->RREQ_GEN_WAIT_TIME);
    }
    ThreadMessage *message = create_message(0,EXIT_RREQ_WAIT_TIME_FINISH,NULL);
    put_message(message,MESSAGE_QUEUE_EXTERNAL_INTERNAL);
    pthread_exit(0);
}


void *timer_RREP_ACK_SENT_TIMEOUT(void *arg) {

    printf ("TIMER GEN ACK ON\n");
    sleep (RREP_ACK_SENT_TIMEOUT);
    printf ("TIMER GEN ACK OFF\n");

    ThreadMessage *message = create_message (0 , EXIT_RREP_ACK_SENT_FINISH , NULL);
    put_message (message , MESSAGE_QUEUE_ACK_to_EXTERNAL);
    pthread_exit (0);
}
void *timer_MAX_BLACKLIST_TIME(void *arg){

    int err;
    T_Neighbor Neighbor;
    T_DataPoint *DataPoint=(T_DataPoint *) arg;

    printf("TIMER BLACKLIST INIT\n");
    sleep(MAX_BLACKLIST_TIME);
    printf("TIMER BLACKLIST END\n");

    err = get_NeighborSet_Neighbor_Address(get_DataPoint_NeighborSet(DataPoint),&Neighbor,get_DataPoint_NeighborBlacklist(DataPoint));
    if(err == NOTFOUND_ITEM){

        printf("timer_MAX_BLACKLIST_TIME : get_NeighborSet_Neighbor_Address :  FAIL NOTFOUND ITEM\n");
        pthread_exit(0);
    }
    add_Neighbor_State(&Neighbor,HEARD);
    update_NeighborSet_Neighbor(get_DataPoint_NeighborSet(DataPoint),&Neighbor);
    pthread_exit(0);
}

void *timer_RERR_TIMEOUT(void *arg) {

    int err;
    T_DataPoint *DataPoint=(T_DataPoint *) arg;
    T_RerrMsg RerrMsg;

    printf("RERR_TIMEOUT ON\n");
    sleep(RERR_TIMEOUT);
    printf("RERR_TIMEOUT OFF\n");

    RerrMsg = get_DataPoint_RerrMsg(DataPoint);
    err = del_RerrMsgSet_RerrMsg(get_DataPoint_RerrMsgSet(DataPoint),&RerrMsg);
    if( err != CORRECT_OPERATION){

        printf("timer_RERR_TIMEOUT : del_RerrMsgSet_RerrMsg FAIL\n");
    }
    pthread_exit(0);
}
