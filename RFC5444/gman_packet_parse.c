#include "gman_packet_parse.h"


void pointer_parsed_NULL(T_PointParsed *pointers)
	{
		pointers->pointer_head=NULL;
		pointers->pointer_tail=NULL;
		pointers->pointer_mid=NULL;
		pointers->pointer_prefix_length=NULL;			
		pointers->pointer_tlv_SEQ_NUM=NULL;
		pointers->pointer_tlv_PATH_METRIC=NULL;
		pointers->pointer_list_network_prefix=NULL;
		pointers->pointer_seqnumlist=NULL;
		pointers->pointer_metrictypelist=NULL;
		pointers->pointer_tlvs=NULL;
	}
void pointer_parsed_FREE(T_PointParsed *pointers)
	{
		free(pointers->pointer_head);
		free(pointers->pointer_tail);
		free(pointers->pointer_mid);					
		free(pointers->pointer_prefix_length);
		free(pointers->pointer_tlv_SEQ_NUM);
		free(pointers->pointer_tlv_PATH_METRIC);
		free(pointers->pointer_list_network_prefix);
		free(pointers->pointer_seqnumlist);
		free(pointers->pointer_metrictypelist);
		free(pointers->pointer_tlvs);
	}
T_PgmanPacket get_parse_array(uint8_t *buffer,T_PointParsed *pointers)
	{
		T_PgmanPacket packet;
		int i=0;
		int *index = &i;
		//printf("INDEX 1 %d: %d\n",cont4++ , *index  );
		packet.pkt_header=get_parse_pkt_header(buffer,index);	
		packet.message.msg_header=get_parse_msg_header(buffer,index);
		packet.message.message_tlv_block=get_parse_tlv_block(buffer,index,pointers);
        if(packet.message.msg_header.msg_type==227)
            {
                return packet;
            }
		packet.message.address_block=get_parse_address_block(buffer,index,pointers);	
		packet.message.address_tlv_block=get_parse_tlv_block(buffer,index,pointers);
		return packet;
	}
Pgman_pkt_header get_parse_pkt_header(uint8_t *buffer,int  *index)
	{
		Pgman_pkt_header pkt_header;
		pkt_header.version=          buffer[*index] >> 4;
     	pkt_header.pkt_flags.bit0=   buffer[*index] >> 3;
  	 	pkt_header.pkt_flags.bit1=   buffer[*index] >> 2;	  
		pkt_header.pkt_flags.bit2=   buffer[*index] >> 1;
		pkt_header.pkt_flags.bit3=   buffer[*index];			
		pkt_header.pkt_header_length=compute_pkt_header_length();
		
		return pkt_header;
	}
Pgman_msg_header get_parse_msg_header(uint8_t *buffer,int *index)
	{
		Pgman_msg_header msg_header;
		struct flags4 msg_flags;
		int cont=0;
		int cont1;
		int cont2;
		*index=*index+1;
		//printf("INDEX 2 %d: %d\n",cont4++ , *index  );
		msg_header.msg_type =(buffer[*index]);
		*index=*index+1;
		//printf("INDEX 3 %d: %d\n",cont4++ , *index  );	
		msg_header.msg_flags.bit0=    buffer[*index] >> 7;
  	 	msg_header.msg_flags.bit1=    buffer[*index] >> 6;	  
		msg_header.msg_flags.bit2=    buffer[*index] >> 5;
		msg_header.msg_flags.bit3=    buffer[*index] >> 4;
		msg_header.msg_addr_length=   buffer[*index]; 
		*index=*index+1;cont1=*index;
		//printf("INDEX 4 %d: %d\n",cont4++ , *index  );		
		*index=*index+1;cont2=*index;
		//printf("INDEX 5  %d: %d\n",cont4++ , *index  );		
		msg_header.msg_size = Int8_To_16(buffer[cont1], buffer[cont2]);
		msg_flags=msg_header.msg_flags;		
		if(msg_flags.bit0==ONE) //mhasorig
			{
				*index=*index+1;
				//printf("INDEX  6 %d: %d\n",cont4++ , *index  );				
				msg_header.msg_orig_address= buffer[*index];
			}
		if(msg_flags.bit1==ONE) //mhashoplimit
			{
				*index=*index+1;
				//printf("INDEX  7 %d: %d\n",cont4++ , *index  );				
				msg_header.msg_hop_limit= buffer[*index];
			}
		if(msg_flags.bit2==ONE) //mhashopcount
			{
				*index=*index+1;
				//printf("INDEX  8 %d: %d\n",cont4++ , *index  );			
				msg_header.msg_hop_count=buffer[*index];
			}
		if(msg_flags.bit3==ONE) //mhasseqnum
			{
				
				*index=*index+1;cont1=*index;
				//printf("INDEX 9 %d: %d\n",cont4++ , *index  );				
				*index=*index+1;cont2=*index;
				//printf("INDEX 10 %d: %d\n",cont4++ , *index  );				
				msg_header.msg_seq_num= Int8_To_16(buffer[cont1], buffer[cont]);
			}
		msg_header.msg_header_length= (uint8_t) compute_msg_header_length(msg_header.msg_type);
		return msg_header;

	}
Pgman_address_block get_parse_address_block(uint8_t *buffer,int *index,T_PointParsed *pointers)
	{
		uint8_t length = 0;
		uint8_t mid_length=0;
		Pgman_address_block address_block;
		address_block.head_length=0;
		address_block.tail_length=0;

		*index=*index+1;
		//printf("INDEX 11 %d: %d\n",cont4++ , *index  );		
		address_block.num_addr=	buffer[*index];
		*index=*index+1;
		//printf("INDEX 12 %d: %d\n",cont4++ , *index  );		
		address_block.addr_flags.bit0=  buffer[*index] >> 7;
		address_block.addr_flags.bit1=  buffer[*index] >> 6;
		address_block.addr_flags.bit2=  buffer[*index] >> 5;	  
		address_block.addr_flags.bit3=  buffer[*index] >> 4;
		address_block.addr_flags.bit4=  buffer[*index] >> 3;
		address_block.addr_flags.bit5=  buffer[*index] >> 2;
		address_block.addr_flags.bit6=  buffer[*index] >> 1;
		address_block.addr_flags.bit7=  buffer[*index];

		struct flags addr_flags=address_block.addr_flags;

		if(addr_flags.bit0==ONE)
			{
				*index=*index+1;
				//printf("INDEX 13 %d: %d\n",cont4++ , *index  );				
				address_block.head_length=buffer[*index];
				address_block.head=get_parse_head(buffer,address_block.head_length,index,HEAD,pointers);
			}
		else
			{
				address_block.head_length=0;
			}		
		if(addr_flags.bit1==ONE && addr_flags.bit2==ZERO)
			{
				*index=*index+1;
				//printf("INDEX 14 %d: %d\n",cont4++ , *index  );				
				address_block.tail_length=buffer[*index];	 
				address_block.tail=get_parse_tail(buffer,address_block.tail_length,index,TAIL,pointers);
			}
		else if(addr_flags.bit1 ==ZERO && addr_flags.bit2 == ONE)
			{
				*index=*index+1;
				//printf("INDEX 15 %d: %d\n",cont4++ , *index  );				
				address_block.tail_length=buffer[*index];
			}
		else
			{
				address_block.tail_length=0;
			}		
		 mid_length = (address_block.num_addr)*get_mid_length(address_block.head_length,address_block.tail_length);
		if(mid_length>0)
			{
				address_block.mid = get_parse_mid(buffer,mid_length,index,MID,pointers);
			}
		if(addr_flags.bit3==ONE && addr_flags.bit4==ZERO)
			{
				length=1;
			}
		if(addr_flags.bit3==ZERO && addr_flags.bit4==ONE)
			{							
				length=address_block.num_addr;
			}		
		address_block.prefix_length=get_parse_prefix(buffer,length,index,PREFIX,pointers);
		address_block.address_block_length=compute_address_block_length(address_block);
		return address_block;
	}
uint8_t* get_parse_tail(uint8_t *buffer,uint8_t length,int *index, int type,T_PointParsed  *pointer)
{

	int cont;
	uint8_t *tail = (uint8_t *) malloc(length*sizeof(uint8_t));
	if(tail==NULL)
	{
		printf("get_parse_HMTP: unable to allocate memory for tail  %d \n", type);
		free(tail);
		return NULL;
	}
	pointer->pointer_tail =tail;
	for(cont=0;cont<length;cont++)
	{
		*index=*index+1;
		//printf("INDEX 16 %d: %d\n",cont4++ , *index  );
		tail[cont]=buffer[*index];
	}
	return tail;
}
uint8_t* get_parse_mid(uint8_t *buffer,uint8_t length,int *index, int type,T_PointParsed  *pointer)
{

	int cont;
	uint8_t *mid = (uint8_t *) malloc(length*sizeof(uint8_t));
	if(mid==NULL)
	{
		printf("get_parse_HMTP: unable to allocate memory for tail  %d \n", type);
		free(mid);
		return NULL;
	}
	pointer->pointer_mid = mid;
	for(cont=0;cont<length;cont++)
	{
		*index=*index+1;
		//printf("INDEX 16 %d: %d\n",cont4++ , *index  );
		mid[cont]=buffer[*index];
	}
	return mid;
}
uint8_t* get_parse_head(uint8_t *buffer,uint8_t length,int *index, int type,T_PointParsed  *pointer)
{

	int cont;
	uint8_t *head = (uint8_t *) malloc(length*sizeof(uint8_t));
	if(head==NULL)
	{
		printf("get_parse_HMTP: unable to allocate memory for type array  %d \n", type);
		return NULL;
	}
	pointer->pointer_head=head;
	for(cont=0;cont<length;cont++)
	{
		*index=*index+1;
		//printf("INDEX 16 %d: %d\n",cont4++ , *index  );
		head[cont]=buffer[*index];
	}
	return head;
}
uint8_t* get_parse_prefix(uint8_t *buffer,uint8_t length,int *index, int type,T_PointParsed  *pointer)
	{

		int cont;
		uint8_t *prefix = (uint8_t *) malloc(length*sizeof(uint8_t));
		if(prefix==NULL)
			{
				printf("get_parse_HMTP: unable to allocate memory for type array  %d \n", type);
				free(prefix);
				return NULL;
			}
		pointer->pointer_prefix_length=prefix;
		for(cont=0;cont<length;cont++)
			{
				*index=*index+1;
				//printf("INDEX 16 %d: %d\n",cont4++ , *index  );
				prefix[cont]=buffer[*index];
			}
		return prefix;
	}
Pgman_tlv_block get_parse_tlv_block(uint8_t *buffer,int *index,T_PointParsed *pointers)
	{		
		Pgman_tlv_block tlv_block;

		uint16_t index_tlvs=0;
		uint16_t tlvs_length;
		int size_array=0;		
		int cont1,cont2,cont3;
		*index=*index+1;cont1=*index;
		//printf("INDEX 17 %d: %d\n",cont4++ , *index  );
		*index=*index+1;cont2=*index;
		//printf("INDEX 18 %d: %d\n",cont4++ , *index  );
		tlv_block.tlvs_length= Int8_To_16(buffer[cont1], buffer[cont2]);
		cont3=0;		
		if(tlv_block.tlvs_length > 0)
			{
                Pgman_tlv *tlvs= (Pgman_tlv *) malloc(sizeof(Pgman_tlv));
                if(tlvs==NULL)
                {
                    printf("get_parse_tlvs_RREQ: unable to allocate memory for  tlvs array\n");
                    free(tlvs);
                }
                pointers->pointer_tlvs=tlvs;
				tlvs[cont3]=get_parse_tlv(buffer,index,pointers);				
				tlvs_length=compute_tlvs_length(tlvs[cont3].tlv_flags,tlvs[cont3].length);
				//printf("TVLS_LEGNT: %u\n",tlvs_length);
				index_tlvs=index_tlvs +tlvs_length;
				//printf("INDEX TLVS: %u\n",index_tlvs);
				size_array=1;
				cont3++;				
				while(index_tlvs<tlv_block.tlvs_length)
				   {			
                       size_array++;
                       tlvs=realloc (tlvs,(size_array)*sizeof(Pgman_tlv));
                       if(tlvs==NULL)
							{
								printf("get_parse_tlvs_RREQ: unable to allocate memory for  tlvs array\n");
                                free(tlvs);
							}
                       pointers->pointer_tlvs=tlvs;
                       tlvs[cont3]=get_parse_tlv(buffer,index,pointers);
                       tlvs_length=compute_tlvs_length(tlvs[cont3].tlv_flags,tlvs[cont3].length);
                       //printf("TVLS_LEGNT: %u\n",tlvs_length);
                       index_tlvs=index_tlvs +tlvs_length;
                       //printf("INDEX TLVS: %u\n",index_tlvs);
                       cont3++;
                   }
                tlv_block.tlvs=tlvs;
                tlv_block.overall= (uint8_t) cont3;
            }



		return tlv_block;
	}
Pgman_tlv get_parse_tlv(uint8_t *buffer,int *index,T_PointParsed *pointers)
	{
		Pgman_tlv tlv;
		uint8_t number_values=0;
		tlv.index_start=0;
		tlv.index_stop=0;
		
		*index=*index+1;
		
		tlv.tlv_type=buffer[*index];
		*index=*index+1;
		//printf("INDEX 12 %d: %d\n",cont4++ , *index  );		
		tlv.tlv_flags.bit0=  buffer[*index] >> 7;
		tlv.tlv_flags.bit1=  buffer[*index] >> 6;
		tlv.tlv_flags.bit2=  buffer[*index] >> 5;	  
		tlv.tlv_flags.bit3=  buffer[*index] >> 4;
		tlv.tlv_flags.bit4=  buffer[*index] >> 3;
		tlv.tlv_flags.bit5=  buffer[*index] >> 2;
		tlv.tlv_flags.bit6=  buffer[*index] >> 1;
		tlv.tlv_flags.bit7=  buffer[*index];
		//printf("BIT : %u\n",tlv.tlv_flags.bit5 );
		if(tlv.tlv_flags.bit0 == ONE)
			{
				*index=*index+1; //tlv_type_ext
				//printf("INDEX 20 %d: %d\n",cont4++ , *index  );
				tlv.tlv_type_ext=buffer[*index];
			}
		if(tlv.tlv_flags.bit0 == ZERO)
			{
				tlv.tlv_type_ext=0;
			}
		if(tlv.tlv_flags.bit1 == ONE  && tlv.tlv_flags.bit2 == ZERO)
			{
				*index=*index+1;//tlv_index_start
				//printf("INDEX 21%d: %d\n",cont4++ , *index  );
				tlv.index_start=buffer[*index];
				tlv.index_stop=tlv.index_start;						
			}
		if(tlv.tlv_flags.bit1 == ZERO && tlv.tlv_flags.bit2 == ONE)
			{
				*index=*index+1;//tlv_index_start
				//printf("INDEX 22 %d: %d\n",cont4++ , *index  );
				tlv.index_start=buffer[*index];
				*index=*index+1;
				//printf("INDEX 23 %d: %d\n",cont4++ , *index  );
				tlv.index_stop=buffer[*index];
			}
		if(tlv.tlv_flags.bit3 == ONE  && tlv.tlv_flags.bit4 == ZERO)
			{
				*index=*index+1;
				//printf("INDEX 24 %d: %d\n",cont4++ , *index  );
				tlv.length=buffer[*index];
			}
		if(tlv.tlv_flags.bit5==ONE)
			{
				number_values= (uint8_t) (tlv.index_stop - tlv.index_start + 1);
			}
		else
			{
				number_values=1;
			}
		if(tlv.tlv_type==SEQ_NUM)
			{
				tlv.value_16bit=get_parse_tlv_SEQ_NUM(buffer,index,number_values,pointers);
			}		
		if(tlv.tlv_type==PATH_METRIC)
			{
				tlv.value_8bit=get_parse_tlv_PATH_METRIC(buffer,index,number_values,pointers);
			}		
		if(tlv.tlv_type==ADDRESS_TYPE)
			{
				tlv.value=get_parse_tlv_ADDRESS_TYPE(buffer,index);
			}		
		if(tlv.tlv_type==ACK_REQ)
			{
				tlv.length=0;
			}		
		return tlv;
	}
uint16_t* get_parse_tlv_SEQ_NUM(uint8_t *buffer,int *index,uint8_t number_values,T_PointParsed *pointers)
	{
		int cont,cont1,cont2;
		uint16_t *seq_num = (uint16_t *) malloc(number_values*sizeof(uint16_t));
		if(seq_num==NULL)
			{
				printf("get_parse_tlv value 16bit: unable to allocate memory for type array\n");
				free(seq_num);
				return NULL;
			}
		pointers->pointer_tlv_SEQ_NUM=seq_num;
		for(cont=0;cont<number_values;cont++)
			{						
				*index=*index+1;cont1=*index;
				//printf("INDEX 26 %d: %d\n",cont4++ , *index  );
				*index=*index+1;cont2=*index;
				//printf("INDEX 27 %d: %d\n",cont4++ , *index  );
				seq_num[cont]= Int8_To_16(buffer[cont1], buffer[cont2]);
			}				
		return seq_num;
	}
uint8_t* get_parse_tlv_PATH_METRIC(uint8_t *buffer,int *index,uint8_t number_values,T_PointParsed *pointers)
	{
		int cont;
		uint8_t * path_metric = (uint8_t *) malloc(number_values*sizeof(uint8_t));
		if(path_metric==NULL)
			{
				printf("get_parse_tlv value 8bit: unable to allocate memory for type array\n");
				free(path_metric);
				return NULL;
			}
		pointers->pointer_tlv_PATH_METRIC=path_metric;
		for(cont=0;cont<number_values;cont++)
			{
				*index=*index+1;
				//printf("INDEX 28 %d: %d\n",cont4++ , *index  );
				path_metric[cont]=buffer[*index];
			}
		return path_metric;
	}
uint8_t	get_parse_tlv_ADDRESS_TYPE(uint8_t *buffer,int *index)
	{
		uint8_t value;
		*index=*index+1;
		//printf("INDEX 29 %d: %d\n",cont4++ , *index  );
		value=buffer[*index];

		return value;
	}