#ifndef GMAN_PACKET_SERIALIZED_H
#define GMAN_PACKET_SERIALIZED_H

#include <stdint.h> /* int8_t*/
#include <stddef.h>//NULL
#include "gman_packet.h"

uint8_t *get_Packet_Array(T_PgmanPacket packet, uint8_t *size, T_PointSerialized *pointers);
uint8_t serialized_byte2(struct flags4 msg_flags,uint8_t msg_addr_length);
uint8_t  get_array_pkt_header(Pgman_pkt_header pkt_header);
uint8_t get_array_8flags(struct flags  tlv_flags);
void print_packet(uint8_t *array_packet , int lenght_array);


#endif


