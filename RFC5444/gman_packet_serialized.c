
#include "gman_packet_serialized.h"



uint8_t* get_Packet_Array(T_PgmanPacket packet, uint8_t *size, T_PointSerialized *pointers)
   {

      int cont,cont1,cont3;
      struct flags4 msg_flags=packet.message.msg_header.msg_flags;
      uint8_t   packet_size = (uint8_t) (packet.message.msg_header.msg_size + packet.pkt_header.pkt_header_length);
      *size=packet_size;
      uint8_t msg_type=packet.message.msg_header.msg_type;
      uint8_t *array_packet = (uint8_t *) malloc(packet_size*sizeof(uint8_t));
      if(array_packet==NULL)
         {
            printf("get_Packet_Array: unable to allocate memory for  packet array\n");
            return NULL;
         }
      pointers->pointer_array_packet=array_packet;
      cont=0;
      
  
      array_packet[cont++]=get_array_pkt_header(packet.pkt_header);
         
      array_packet[cont++]= msg_type;
      array_packet[cont++]=serialized_byte2(packet.message.msg_header.msg_flags,
                                            (uint8_t) packet.message.msg_header.msg_addr_length);
     
      uint8_t * valor= Int16_To_8(packet.message.msg_header.msg_size);


      array_packet[cont++]= valor[0];
      array_packet[cont++]= valor[1];
      
      if(msg_flags.bit1==ONE)
         {
            array_packet[cont++]= packet.message.msg_header.msg_hop_limit;
         }

      array_packet[cont++]= (uint8_t) ((uint8_t) 0x00FF & (packet.message.message_tlv_block.tlvs_length >> 8));
      array_packet[cont++]= (uint8_t) ((uint8_t) 0x00FF & packet.message.message_tlv_block.tlvs_length);

      if(packet.message.message_tlv_block.tlvs_length== 2)
         {
            struct flags tlv_flags=packet.message.message_tlv_block.tlvs[0].tlv_flags;
 
            array_packet[cont++]=packet.message.message_tlv_block.tlvs[0].tlv_type;
            array_packet[cont++]=get_array_8flags(tlv_flags);
            
            if(tlv_flags.bit0==ONE)
               {
                  array_packet[cont++]=packet.message.message_tlv_block.tlvs[0].tlv_type_ext;
               }
            if(tlv_flags.bit1==ONE && tlv_flags.bit2==ZERO)
               {
                  array_packet[cont++]=packet.message.message_tlv_block.tlvs[0].index_start;
               }
            if(tlv_flags.bit1==ZERO && tlv_flags.bit2==ONE)
               {
                  array_packet[cont++]=packet.message.message_tlv_block.tlvs[0].index_stop;
               }
            if(tlv_flags.bit3==ONE && tlv_flags.bit4==ZERO)
               {
                  array_packet[cont++]=packet.message.message_tlv_block.tlvs[0].length;
               }
            if(tlv_flags.bit3==ZERO && tlv_flags.bit4==ONE)
               {
                  array_packet[cont++]=packet.message.message_tlv_block.tlvs[0].value;
               }
         }     
      
      if(msg_type!=RREP_ACK)
         {
            Pgman_address_block address_block = packet.message.address_block;
            Pgman_tlv_block tlv_block= packet.message.address_tlv_block;
            struct flags  addr_flags= address_block.addr_flags;
            uint8_t mid_length=get_mid_length(address_block.head_length,address_block.tail_length);                  

            array_packet[cont++]=address_block.num_addr;
            array_packet[cont++]=get_array_8flags(addr_flags);
            if(addr_flags.bit0==ONE)
               {
                        array_packet[cont++]=address_block.head_length;
                        for(cont1=0;cont1<address_block.head_length;cont1++)
                           {
                              array_packet[cont++]=address_block.head[cont1];
                           }
               }
            if(addr_flags.bit1==ONE && addr_flags.bit2==ZERO)
               {
                  array_packet[cont++]=address_block.tail_length;
                  for(cont1=0;cont1<address_block.tail_length;cont1++)
                     {
                        array_packet[cont++]=address_block.tail[cont1];
                     }
               }
            if(addr_flags.bit1==ZERO && addr_flags.bit2==ONE)
               {
                  array_packet[cont++]=address_block.tail_length;
               }
            if(mid_length>0)
               {
                  for (cont1=0;cont1<(address_block.num_addr)*(mid_length);cont1++)
                     {
                        array_packet[cont++]=address_block.mid[cont1];
                     }
               }
            if(addr_flags.bit3==ONE && addr_flags.bit4== ZERO)
               {
                  array_packet[cont++]=address_block.prefix_length[0];
               }
            if(addr_flags.bit3==ZERO && addr_flags.bit4==ONE)
               {
                  for(cont1=0;cont1<address_block.num_addr;cont1++)
                     {
                        array_packet[cont++]=address_block.prefix_length[cont1];
                     }
               }
            array_packet[cont++]= (uint8_t) ((uint8_t) 0x00FF & (tlv_block.tlvs_length >> 8));
            array_packet[cont++]= (uint8_t) ((uint8_t) 0x00FF & tlv_block.tlvs_length);
            for(cont3=0;cont3<tlv_block.overall;cont3++)
               {
                  Pgman_tlv tlv=tlv_block.tlvs[cont3];
                  array_packet[cont++]=tlv.tlv_type;
                  array_packet[cont++]=get_array_8flags(tlv.tlv_flags);
                  if(tlv.tlv_flags.bit0==ONE)
                           {
                              array_packet[cont++]= tlv.tlv_type_ext;
                           }              
                  if(tlv.tlv_type==SEQ_NUM)
                     {
                        if(tlv.tlv_flags.bit1==ONE && tlv.tlv_flags.bit2==ZERO)
                           {
                              array_packet[cont++]=tlv.index_start;
                              array_packet[cont++]=tlv.length;
                              array_packet[cont++]= (uint8_t) ((uint8_t) 0x00FF & (tlv.value_16bit[0] >> 8));
                              array_packet[cont++]= (uint8_t) ((uint8_t) 0x00FF & tlv.value_16bit[0]);
                           }
                        if(tlv.tlv_flags.bit1==ZERO && tlv.tlv_flags.bit2==ONE)
                           {
                              array_packet[cont++]=tlv.index_start;
                              array_packet[cont++]=tlv.index_stop; 
                              array_packet[cont++]=tlv.length;                                    
                              for(cont1=0;cont1<(tlv.length)/2;cont1++)
                                 {
                                    array_packet[cont++]= (uint8_t) ((uint8_t) 0x00FF & (tlv.value_16bit[cont1] >> 8));
                                    array_packet[cont++]= (uint8_t) ((uint8_t) 0x00FF & tlv.value_16bit[cont1]);
                                 }                                                       
                           }
                     }
                  if(tlv.tlv_type==PATH_METRIC)
                     {
                        if(tlv.tlv_flags.bit1==ONE && tlv.tlv_flags.bit2==ZERO)
                           {
                              array_packet[cont++]=tlv.index_start;
                              array_packet[cont++]=tlv.length;
                              array_packet[cont++]=tlv.value_8bit[0];
                           }
                        if(tlv.tlv_flags.bit1==ZERO && tlv.tlv_flags.bit2==ONE)
                           {  
                              array_packet[cont++]=tlv.index_start;
                              array_packet[cont++]=tlv.index_stop;             
                              array_packet[cont++]=tlv.length;
                              for(cont1=0;cont1< tlv.length;cont1++)
                                 {
                                    array_packet[cont++]=tlv.value_8bit[cont1];
                                 }                                        
                           }        
                     }
                  if(tlv.tlv_type==ADDRESS_TYPE)
                     {
                        if(tlv.tlv_flags.bit1==ONE && tlv.tlv_flags.bit2==ZERO)
                           {
                              array_packet[cont++]=tlv.index_start;
                           }
                        if(tlv.tlv_flags.bit1==ZERO && tlv.tlv_flags.bit2==ONE)
                           {  
                              array_packet[cont++]=tlv.index_start;
                              array_packet[cont++]=tlv.index_stop; 
                           }
                        array_packet[cont++]=tlv.length;
                        array_packet[cont++]=tlv.value;
                     }   
               }                             
         }
     
     return array_packet;
   }
uint8_t get_array_pkt_header(Pgman_pkt_header pkt_header)
   {
      uint8_t tmp_array=0;
       
      tmp_array <<=4;
      tmp_array |=pkt_header.version;
      tmp_array <<= 1;      
      tmp_array |= pkt_header.pkt_flags.bit0;
      tmp_array <<= 1;
      tmp_array |= pkt_header.pkt_flags.bit1;
      tmp_array <<= 1;
      tmp_array |= pkt_header.pkt_flags.bit2;
      tmp_array <<= 1;
      tmp_array |= pkt_header.pkt_flags.bit3;
          
      return tmp_array;
   }
uint8_t get_array_8flags(struct flags  tlv_flags)
   {
      uint8_t tmp_array=0;
      tmp_array  <<= 1;
      tmp_array  |= tlv_flags.bit0;
      tmp_array  <<= 1;
      tmp_array  |= tlv_flags.bit1;
      tmp_array  <<= 1;
      tmp_array  |= tlv_flags.bit2;
      tmp_array  <<= 1;
      tmp_array  |= tlv_flags.bit3;
      tmp_array  <<= 1;
      tmp_array  |= tlv_flags.bit4;
      tmp_array  <<= 1;
      tmp_array  |= tlv_flags.bit5;
      tmp_array  <<= 1;
      tmp_array  |= tlv_flags.bit6;
      tmp_array  <<= 1;
      tmp_array  |= tlv_flags.bit7;

      return tmp_array;
   }
uint8_t serialized_byte2(struct flags4 msg_flags,uint8_t msg_addr_length)
   {
      uint8_t byte2=0;
      byte2 <<= 1;
      byte2 |= msg_flags.bit0;
      byte2 <<= 1;
      byte2 |= msg_flags.bit1;
      byte2 <<= 1;
      byte2 |= msg_flags.bit2;
      byte2 <<= 1;
      byte2 |= msg_flags.bit3;
      byte2 <<=4;
      byte2|=msg_addr_length;

      return byte2;
   }
void print_packet(uint8_t *array_packet , int lenght_array)
   {
      int cont1,cont2;
      cont2=0;
      for(cont1=0;cont1<lenght_array;cont1++)
         {
            if(array_packet[cont1]>9 && array_packet[cont1]<100)
            {
               
               printf("| %u ", array_packet[cont1]);
            }
            if(array_packet[cont1]>99)
            {
               
               printf("| %u", array_packet[cont1]);
            }
            if(array_packet[cont1]<10)
            {
               
               printf("|  %u ", array_packet[cont1]);
            }
            cont2++;
            if(cont2==4)
               {
                 printf("|\n");
                 cont2=0; 
               }              
            
         }
         printf("\n");
   }









