

#include <stdint.h>
#include <global_structs.h>
#include "parse_container.h"

T_CIDR *get_parse_container_addresslist(Pgman_address_block address_block,T_PointParsed *pointers);
T_AddressListRERR get_parse_container_rerr_UNREACHABLE(T_CIDR *network_prefix,Pgman_tlv_block tlv_block);
T_CIDR* get_parse_container_rerr_PKSOURCE(T_CIDR *network_prefix,Pgman_tlv_block tlv_block);
void get_parse_rerr_tlvs(Pgman_tlv_block tlv_block,T_SeqNumList *seqnumlist,T_MetricTypeList *metrictypelist);
T_MetricTypeList *get_parse_rerr_METRICTYPELIST(Pgman_tlv_block tlv_block,T_PointParsed *pointers);
T_SeqNumList  *get_parse_rerr_SEQNUMLIST(Pgman_tlv_block tlv_block,T_PointParsed *pointers);

uint8_t get_parse_container(T_PgmanPacket packet){
		return packet.message.msg_header.msg_type;
	}
T_RREQ get_parse_container_rreq(T_PgmanPacket packet,T_PointParsed *pointers){
		T_RREQ rreq;
		 		
		rreq.MsgHopLimit = packet.message.msg_header.msg_hop_limit;
		T_CIDR *network_prefix = get_parse_container_addresslist(packet.message.address_block,pointers);
		rreq.AddressList.OrigPrefix = network_prefix[0];
		rreq.AddressList.TargPrefix = network_prefix[1];
		rreq.OrigSeqNum = packet.message.address_tlv_block.tlvs[1].value_16bit[0];;
		rreq.TargSeqNum = packet.message.address_tlv_block.tlvs[3].value_16bit[0];
		rreq.MetricType = packet.message.address_tlv_block.tlvs[2].tlv_type_ext;
		rreq.OrigMetric = packet.message.address_tlv_block.tlvs[2].value_8bit[0];
		return rreq;
	}
T_RREP get_parse_container_rrep(T_PgmanPacket packet,T_PointParsed *pointers){
		T_RREP rrep;
			
		rrep.MsgHopLimit = packet.message.msg_header.msg_hop_limit;
		T_CIDR *network_prefix = get_parse_container_addresslist(packet.message.address_block,pointers);
		rrep.AddressList.OrigPrefix = network_prefix[0];
		rrep.AddressList.TargPrefix = network_prefix[1];
		rrep.MsgHopLimit = packet.message.msg_header.msg_hop_limit;
		rrep.TargSeqNum = packet.message.address_tlv_block.tlvs[2].value_16bit[0];
		rrep.MetricType = packet.message.address_tlv_block.tlvs[3].tlv_type_ext;
		rrep.TargMetric = packet.message.address_tlv_block.tlvs[3].value_8bit[0];
		return rrep;
	}
T_RERR get_parse_container_rerr(T_PgmanPacket packet,T_PointParsed *pointers){
		T_RERR rerr_container;
		
		T_CIDR * network_prefix = get_parse_container_addresslist(packet.message.address_block,pointers);
		rerr_container.AddressListRERR = get_parse_container_rerr_UNREACHABLE(network_prefix,packet.message.address_tlv_block);
		rerr_container.PktSource = get_parse_container_rerr_PKSOURCE(network_prefix,packet.message.address_tlv_block);
    	rerr_container.SeqNumList = get_parse_rerr_SEQNUMLIST(packet.message.address_tlv_block,pointers);
		rerr_container.MetricTypeList = get_parse_rerr_METRICTYPELIST(packet.message.address_tlv_block,pointers);
		return  rerr_container;
	}
uint8_t get_parse_container_rrep_ack(T_PgmanPacket packet){
		uint8_t acknowledgement;
		if(packet.message.msg_header.msg_size==8)
			{
				acknowledgement=1;
			}
		else acknowledgement=0;
		return acknowledgement;
	}
T_CIDR *get_parse_container_rerr_PKSOURCE(T_CIDR *network_prefix,Pgman_tlv_block tlv_block){
		T_CIDR *pktsource=NULL;
		int cont;	
		for(cont=0;cont<tlv_block.overall;cont++)
			{			
				if(tlv_block.tlvs[cont].tlv_type==ADDRESS_TYPE && tlv_block.tlvs[cont].value==PKTSOURCE)
					{
						
						pktsource=&network_prefix[tlv_block.tlvs[cont].index_start];			
					}				
			}
		return pktsource;
	}
T_AddressListRERR get_parse_container_rerr_UNREACHABLE(T_CIDR *network_prefix,Pgman_tlv_block tlv_block){
		int cont,cont1,cont2;
		T_AddressListRERR addresslist_rerr={0};
		for(cont=0;cont<tlv_block.overall;cont++)
			{

				if(tlv_block.tlvs[cont].tlv_type==ADDRESS_TYPE && tlv_block.tlvs[cont].value==UNREACHABLE)
					{
						
						uint8_t number_values= (uint8_t) (tlv_block.tlvs[cont].index_stop - tlv_block.tlvs[cont].index_start + 1);
						cont2=0;
						for(cont1=tlv_block.tlvs[cont].index_start;cont1<number_values;cont1++)
							{
								addresslist_rerr.Addresses[cont2]= network_prefix[cont1];
									cont2++;
							}
						addresslist_rerr.overall= (uint8_t) cont2;
					}				
			}
		return addresslist_rerr;
	}
T_CIDR *get_parse_container_addresslist(Pgman_address_block address_block,T_PointParsed *pointers){
		int cont,cont1,cont2;
		uint8_t mid_length=0;
		uint8_t	 num_addr= address_block.num_addr;
		struct flags addr_flags= address_block.addr_flags;
		
		T_CIDR *list_network_prefix = (T_CIDR *) malloc(num_addr*sizeof(T_CIDR));
		pointers->pointer_list_network_prefix=list_network_prefix;
		if(list_network_prefix==NULL)
			{
				printf("get_parse_container_addresslist: unable to allocate memory for  network_prefix array\n");
			}
		
		else
			{
				if(addr_flags.bit0==ONE)
					{
						for(cont=0;cont<address_block.head_length;cont++)
							{
								for(cont1=0;cont1<num_addr;cont1++)
									{
										list_network_prefix[cont1].Address[cont]=address_block.head[cont];
									}
							}
					}
				mid_length=get_mid_length(address_block.head_length,address_block.tail_length);
				if(mid_length>0)
					{	
						cont2=0;
						for(cont1=0;cont1<num_addr;cont1++)					
							{
								for(cont=address_block.head_length;cont<(address_block.head_length+mid_length);cont++)
									{
										list_network_prefix[cont1].Address[cont]=address_block.mid[cont2];
										cont2++;
									}									
							}
					}	
				if(addr_flags.bit1 == ONE  && addr_flags.bit2==ZERO)
					{
						cont2=0;						
						for(cont=(4-address_block.tail_length);cont<4;cont++)
							{
								for(cont1=0;cont1<num_addr;cont1++)
									{
										list_network_prefix[cont1].Address[cont]=address_block.tail[cont2];
										if(address_block.tail_length>1)
											{
												cont2++;
											}
									}
							}						
					}
				if(addr_flags.bit1 == ZERO && addr_flags.bit2==ONE)
					{
						for(cont=(4-address_block.tail_length);cont<4;cont++)
							{
								for(cont1=0;cont1<num_addr;cont1++)
									{
										list_network_prefix[cont1].Address[cont]=0;
									}
							}
					}
				if(addr_flags.bit3 == ONE  && addr_flags.bit4==ZERO)
					{
						for(cont1=0;cont1<num_addr;cont1++)
							{
								list_network_prefix[cont1].PrefixLen=address_block.prefix_length[0];
							}
					}
				if(addr_flags.bit3 == ZERO && addr_flags.bit4==ONE)
					{
						for(cont1=0;cont1<num_addr;cont1++)
							{
								list_network_prefix[cont1].PrefixLen=address_block.prefix_length[cont1];
							}
					}
			}
		return list_network_prefix;
	}
T_SeqNumList  *get_parse_rerr_SEQNUMLIST(Pgman_tlv_block tlv_block,T_PointParsed *pointers){
		T_SeqNumList *seqnumlist = (T_SeqNumList *) malloc(sizeof(T_SeqNumList));
		pointers->pointer_seqnumlist=seqnumlist;
		seqnumlist->overall=0;
		int cont,cont1;
		uint8_t number_values;
		
		for(cont=0;cont<tlv_block.overall;cont++)
			{
				if(tlv_block.tlvs[cont].tlv_type==SEQ_NUM)
					{
						

						number_values= (uint8_t) (tlv_block.tlvs[cont].index_stop - tlv_block.tlvs[cont].index_start + 1);
						for(cont1=0;cont1<number_values;cont1++)
							{
								seqnumlist->SeqNumbers[cont1]=tlv_block.tlvs[cont].value_16bit[cont1];
								seqnumlist->overall++;
							}					
					}							
			}
		return seqnumlist;
	}
T_MetricTypeList *get_parse_rerr_METRICTYPELIST(Pgman_tlv_block tlv_block,T_PointParsed *pointers){
		int cont,cont1;
		uint8_t number_values=0;
		T_MetricTypeList *metrictypelist = (T_MetricTypeList *) malloc(sizeof(T_MetricTypeList));
		pointers->pointer_metrictypelist=metrictypelist;
		metrictypelist->overall=0;
		
		for(cont=0;cont<tlv_block.overall;cont++)
			{
				if(tlv_block.tlvs[cont].tlv_type==PATH_METRIC)
					{
				
						number_values= (uint8_t) (tlv_block.tlvs[cont].index_stop - tlv_block.tlvs[cont].index_start + 1);
			
						for(cont1=0;cont1<number_values;cont1++)
							{
								metrictypelist->MetricTypes[cont1]=tlv_block.tlvs[cont].value_8bit[cont1];
								metrictypelist->overall++;
							}					
					}
			}
		return metrictypelist;
	}
