#ifndef GMAN_PACKET_H
#define GMAN_PACKET_H

#include <stdio.h>
#include <stdint.h> /* int8_t*/
#include <stddef.h>
#include <stdlib.h>
#include <inttypes.h> //PRIu8,PRIu16 etc
#include "../constant_definition.h"
#include "../containers/RERR_container.h"
#include "../containers/RREQ_container.h"
#include "../containers/RREP_container.h"

/**
	 * @brief      { struct_description }
	 */
	struct flags  {
	    
	    unsigned  bit0:1; //1 bit de longitud
	    unsigned  bit1:1;    
	    unsigned  bit2:1;    
	    unsigned  bit3:1;   
	    unsigned  bit4:1;    
	    unsigned  bit5:1;    
	    unsigned  bit6:1;
	    unsigned  bit7:1;
	};
	/**
	 * @brief      { struct_description }
	 */
	struct flags4 {
	    
	    unsigned  bit0:1; //1 bit de longitud
	    unsigned  bit1:1;    
	    unsigned  bit2:1;    
	    unsigned  bit3:1;
	};
	/**
	 * { item_description }
	 */
	typedef struct{
		uint8_t	 		tlv_type;
		struct flags    tlv_flags;
		uint8_t	  		tlv_type_ext;
		uint8_t  		index_start;
		uint8_t	  		index_stop;
		uint8_t  		length;
		uint8_t         value;  // para los tipo de ADDRESS TYPe
		uint8_t   		*value_8bit; //Array  para la metrica
		uint16_t		*value_16bit;//Array para los numeros de secuencia
	}Pgman_tlv;	
	/**
	 * { item_description }
	 */
	typedef struct{
		uint8_t	     num_addr;				
		struct flags addr_flags;
		uint8_t	     head_length;
		uint8_t      *head;
		uint8_t	     tail_length;
		uint8_t      *tail;
		uint8_t	     *mid;			
		uint8_t	     *prefix_length;
		uint8_t      address_block_length;//no lo defiene el protocolo 
	}Pgman_address_block;
	/**
	 * { item_description }
	 */
	typedef struct{
		uint16_t  tlvs_length;
		Pgman_tlv  *tlvs;
		uint8_t overall;
	}Pgman_tlv_block;
	/**
	 * { item_description }
	 */
	typedef struct{
		uint8_t   	   msg_type;
		struct flags4  msg_flags;
		unsigned   	   msg_addr_length:4;
		uint16_t  	   msg_size;				
	    uint8_t        msg_orig_address;// Solo se implementa en el parse, AODVv2 no lo usa.
		uint8_t	       msg_hop_limit;			
	    uint8_t        msg_hop_count;	// Solo se implementa en el parse, AODVv2 no lo usa. 
	    uint16_t       msg_seq_num;     // Solo se implementa en el parse, AODVv2 no lo usa. 
	    uint8_t        msg_header_length; //no lo defiene el protocolo
	}Pgman_msg_header;
	/**
	 * { item_description }
	 */
	typedef struct{
		Pgman_msg_header       msg_header;
		Pgman_tlv_block        message_tlv_block;		
		Pgman_address_block    address_block;
		Pgman_tlv_block        address_tlv_block;
	}Pgman_message;
	/**
	 * { item_description }
	 */
	typedef struct{
		unsigned   	    version:4;
		struct flags4   pkt_flags;
	  //uint16_t        pkt_seq_num;       
	  //Pgman_tlv_block pkt_tlv_block;         
	  	uint8_t         pkt_header_length;	//no lo defiene el protocolo
	}Pgman_pkt_header;
	/**
	 * { item_description }
	 */
	typedef struct{
		Pgman_pkt_header pkt_header;
		Pgman_message message;
		uint16_t packet_length;	//no lo defiene el protocolo
	}T_PgmanPacket;

	/**
	 * { item_description }
	 */
	typedef struct //Estructura para IPAddress block
	{
		T_CIDR cidr[MAX_LIST_ADDRESS_BLOCK];
		uint8_t msg_type;
		uint8_t overall;	
	}Tadress_block_list;
	/**
	 * { item_description }
	 */
	typedef struct 
	{
		uint8_t   		*pointer_value_8bit;
		uint16_t		*pointer_value_16bit;
		uint8_t     	*pointer_head;
		uint8_t    		*pointer_tail;
		uint8_t	     	*pointer_mid;			
		uint8_t	     	*pointer_prefix_length;
		Pgman_tlv 		*pointer_tlvs;
		uint8_t 		*pointer_array_packet;
		
		
	}T_PointSerialized;

	/*------PACKET------------------------*/
	/**
	 * @brief      { function_description }
	 *
	 * @param      pointers  The pointers
	 */
	void nullPointSerialized(T_PointSerialized *pointers);
	/**
	 * @brief      { function_description }
	 *
	 * @param      pointers  The pointers
	 */
	void freePointSerialized(T_PointSerialized *pointers);
	/**
	 * @brief      Gets the packet rrep.
	 *
	 * @param[in]  rrep  The rrep container
	 * @param      pointers        The pointers
	 *
	 * @return     The packet rrep.
	 */
	T_PgmanPacket get_RREP_Packet(T_RREP rrep, T_PointSerialized *pointers);
	/**
	 * @brief      Gets the packet rreq.
	 *
	 * @param[in]  rreq  The rreq container
	 * @param      pointers        The pointers
	 *
	 * @return     The packet rreq.
	 */
	T_PgmanPacket get_RREQ_Packet(T_RREQ rreq, T_PointSerialized *pointers);
	/**
	 * @brief      Gets the packet rerr.
	 *
	 * @param[in]  rerr_container  The rerr container
	 * @param      pointers        The pointers
	 *
	 * @return     The packet rerr.
	 */
	T_PgmanPacket get_RERR_Packet(T_RERR rerr_container, T_PointSerialized *pointers);
	/**
	 * @brief      Gets the packet rrep acknowledge.
	 *
	 * @param[in]  acknowledgement  The acknowledgement
	 * @param      pointers         The pointers
	 *
	 * @return     The packet rrep acknowledge.
	 */
	T_PgmanPacket get_RREPACK_Packet(uint8_t acknowledgement, T_PointSerialized *pointers);
	/**
	 * @brief      { function_description }
	 *
	 * @param[in]  packet  The packet
	 */
	void printf_packet(T_PgmanPacket packet);
	/*------PKT_HEADER--------------------*/
	/**
	 * @brief      Gets the gman packet header.
	 *
	 * @return     The gman packet header.
	 */
	Pgman_pkt_header get_gman_pkt_header();
	/**
	 * @brief      Calculates the packet flags.
	 *
	 * @return     The packet flags.
	 */
	struct flags4 compute_pkt_flags();
	/**
	 * @brief      Gets the packet header version.
	 *
	 * @return     The packet header version.
	 */
	unsigned get_pkt_header_version();
	/**
	 * @brief      Calculates the packet header length.
	 *
	 * @return     The packet header length.
	 */
	uint8_t compute_pkt_header_length();
	/**
	 * @brief      { function_description }
	 *
	 * @param[in]  pkt_header  The packet header
	 */
	void printf_pkt_header(Pgman_pkt_header pkt_header);
	
	
	/*------MESSAGE-----------------------*/
	/**
	 * @brief      Gets the message rreq.
	 *
	 * @param[in]  rreq  The rreq container
	 * @param      pointers        The pointers
	 *
	 * @return     The message rreq.
	 */
	Pgman_message get_message_rreq(T_RREQ rreq,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the message rrep.
	 *
	 * @param[in]  rrep  The rrep container
	 * @param      pointers        The pointers
	 *
	 * @return     The message rrep.
	 */
	Pgman_message get_message_rrep(T_RREP rrep,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the message rerr.
	 *
	 * @param[in]  rerr_container  The rerr container
	 * @param      pointers        The pointers
	 *
	 * @return     The message rerr.
	 */
	Pgman_message get_message_rerr(T_RERR rerr_container,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the message rrep acknowledge.
	 *
	 * @param[in]  acknowledgement  The acknowledgement
	 * @param      pointers         The pointers
	 *
	 * @return     The message rrep acknowledge.
	 */
	Pgman_message get_message_rrep_ack(uint8_t acknowledgement,T_PointSerialized *pointers);


    /*------MSG HEADER--------------------*/
    /**
     * @brief      Gets the message header.
     *
     * @param[in]  aodv_msg_type  The aodv message type
     * @param[in]  msg_hop_limit  The message hop limit
     *
     * @return     The message header.
     */
	Pgman_msg_header get_msg_header(uint8_t aodv_msg_type, uint8_t msg_hop_limit);
	/**
	 * @brief      Calculates the message flags.
	 *
	 * @param[in]  aodv_msg_type  The aodv message type
	 *
	 * @return     The message flags.
	 */
	struct flags4 compute_msg_flags(uint8_t aodv_msg_type);
	/**
	 * @brief      Gets the message size.
	 *
	 * @param[in]  message  The message
	 *
	 * @return     The message size.
	 */
	uint16_t get_msg_size(Pgman_message message);
	/**
	 * @brief      Gets the message hop limit.
	 *
	 * @param[in]  msg_hop_limit  The message hop limit
	 *
	 * @return     The message hop limit.
	 */
	uint8_t get_msg_hop_limit(uint8_t msg_hop_limit);
	/**
	 * @brief      Calculates the message header length.
	 *
	 * @param[in]  aodv_msg_type  The aodv message type
	 *
	 * @return     The message header length.
	 */
	uint16_t compute_msg_header_length(uint8_t aodv_msg_type);
	/**
	 * @brief      { function_description }
	 *
	 * @param[in]  msg_header  The message header
	 */
	void printf_msg_header(Pgman_msg_header msg_header);
	
	/*------IPAddress Block--------------------------*/
	/**
	 * @brief      Gets the address block.
	 *
	 * @param[in]  list      The list
	 * @param      pointers  The pointers
	 *
	 * @return     The address block.
	 */
	Pgman_address_block get_address_block(Tadress_block_list list,T_PointSerialized *pointers);
	/**
	 * @brief      Calculates the address flags.
	 *
	 * @param[in]  list         The list
	 * @param[in]  head_length  The head length
	 * @param[in]  tail_length  The tail length
	 *
	 * @return     The address flags.
	 */
	struct flags compute_addr_flags(Tadress_block_list list,uint8_t head_length, uint8_t tail_length);//Devuelve  un uintt8_t que representa addr_flags
	/**
	 * @brief      Gets the head length.
	 *
	 * @param[in]  list  The list
	 *
	 * @return     The head length.
	 */
	uint8_t get_head_length(Tadress_block_list list);
	/**
	 * @brief      Gets the tail length.
	 *
	 * @param[in]  list         The list
	 * @param[in]  head_length  The head length
	 *
	 * @return     The tail length.
	 */
	uint8_t get_tail_length(Tadress_block_list list,uint8_t head_length);
	/**
	 * @brief      Gets the middle length.
	 *
	 * @param[in]  head_length  The head length
	 * @param[in]  tail_length  The tail length
	 *
	 * @return     The middle length.
	 */
	uint8_t get_mid_length(uint8_t head_length, uint8_t tail_length);
	/**
	 * @brief      Calculates the ahaszerotail.
	 *
	 * @param[in]  list         The list
	 * @param[in]  tail_length  The tail length
	 *
	 * @return     The ahaszerotail.
	 */
	unsigned compute_ahaszerotail(Tadress_block_list list, uint8_t tail_length);
	/**
	 * @brief      Calculates the address block length.
	 *
	 * @param[in]  address_block  The address block
	 *
	 * @return     The address block length.
	 */
	uint8_t compute_address_block_length(Pgman_address_block address_block);
	/**
	 * @brief      Gets the tail.
	 *
	 * @param[in]  list         The list
	 * @param[in]  tail_length  The tail length
	 * @param      pointers     The pointers
	 *
	 * @return     The tail.
	 */
	uint8_t* get_tail(Tadress_block_list list, uint8_t tail_length,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the head.
	 *
	 * @param[in]  list         The list
	 * @param[in]  head_length  The head length
	 * @param      pointers     The pointers
	 *
	 * @return     The head.
	 */
	uint8_t* get_head(Tadress_block_list list, uint8_t head_length,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the middle.
	 *
	 * @param[in]  list         The list
	 * @param[in]  head_length  The head length
	 * @param[in]  mid_length   The middle length
	 * @param      pointers     The pointers
	 *
	 * @return     The middle.
	 */
	uint8_t* get_mid(Tadress_block_list list,uint8_t head_length ,uint8_t mid_length,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the prefix length.
	 *
	 * @param[in]  list        The list
	 * @param[in]  addr_flags  The address flags
	 * @param      pointers    The pointers
	 *
	 * @return     The prefix length.
	 */
	uint8_t* get_prefix_length(Tadress_block_list list,struct flags addr_flags,T_PointSerialized *pointers);
	/**
	 * @brief      { function_description }
	 *
	 * @param[in]  address_block  The address block
	 */
	void printf_address_block(Pgman_address_block address_block);
	/**
	 * @brief      { function_description }
	 *
	 * @param[in]  origprefix  The origprefix
	 * @param[in]  targprefix  The targprefix
	 * @param      list        The list
	 * @param[in]  msg_type    The message type
	 */
	void data_preparation_one(T_CIDR origprefix, T_CIDR targprefix,Tadress_block_list *list, uint8_t msg_type);
	/**
	 * @brief      { function_description }
	 *
	 * @param[in]  addresslist_rerr  The addresslist rerr
	 * @param      pktsource         The pktsource
	 * @param      list              The list
	 * @param[in]  msg_type          The message type
	 */
	void data_preparation_two(T_AddressListRERR addresslist_rerr,T_CIDR *pktsource,Tadress_block_list *list);
	


	/*------- TLV-BLOCK --------------------------*/
	/**
	 * @brief      Gets the tlv block rreq.
	 *
	 * @param[in]  rreq  The rreq container
	 * @param      pointers        The pointers
	 *
	 * @return     The tlv block rreq.
	 */
	Pgman_tlv_block  get_tlv_block_rreq(T_RREQ rreq,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the tlv block rrep.
	 *
	 * @param[in]  rrep  The rrep container
	 * @param      pointers        The pointers
	 *
	 * @return     The tlv block rrep.
	 */

	Pgman_tlv_block  get_tlv_block_rrep(T_RREP rrep,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the tlv block rerr.
	 *
	 * @param[in]  rerr_container  The rerr container
	 * @param      pointers        The pointers
	 *
	 * @return     The tlv block rerr.
	 */
	Pgman_tlv_block  get_tlv_block_rerr(T_RERR rerr_container,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the tlv block rrep acknowledge.
	 *
	 * @param[in]  acknowledgement  The acknowledgement
	 * @param      pointers         The pointers
	 *
	 * @return     The tlv block rrep acknowledge.
	 */
	Pgman_tlv_block  get_tlv_block_rrep_ack(uint8_t acknowledgement,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the tlvs rreq.
	 *
	 * @param[in]  size            The size
	 * @param[in]  rreq  The rreq container
	 * @param      pointers        The pointers
	 *
	 * @return     The tlvs rreq.
	 */
	Pgman_tlv* get_tlvs_rreq(int size,T_RREQ rreq,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the tlvs rrep.
	 *
	 * @param[in]  size            The size
	 * @param[in]  rrep  The rrep container
	 * @param      pointers        The pointers
	 *
	 * @return     The tlvs rrep.
	 */
	Pgman_tlv* get_tlvs_rrep(int size,T_RREP rrep,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the tlvs rerr.
	 *
	 * @param[in]  size            The size
	 * @param[in]  rerr_container  The rerr container
	 * @param[in]  A               { parameter_description }
	 * @param[in]  B               { parameter_description }
	 * @param      pointers        The pointers
	 *
	 * @return     The tlvs rerr.
	 */
	Pgman_tlv* get_tlvs_rerr(int size,T_RERR rerr_container,int A,int B,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the tlvs rrep acknowledge.
	 *
	 * @param      pointers  The pointers
	 *
	 * @return     The tlvs rrep acknowledge.
	 */
	Pgman_tlv* get_tlvs_rrep_ack(T_PointSerialized *pointers);
	/**
	 * @brief      Calculates the tlvs length.
	 *
	 * @param[in]  flag        The flag
	 * @param[in]  tlv_length  The tlv length
	 *
	 * @return     The tlvs length.
	 */
	uint16_t compute_tlvs_length(struct flags flag , uint8_t tlv_length);
	

	/*-------TLV----------------------------------*/
	/**
	 * @brief      Gets the tlv address type.
	 *
	 * @param[in]  value         The value
	 * @param[in]  index_start   The index start
	 * @param[in]  index_stop    The index stop
	 * @param[in]  multielement  The multielement
	 *
	 * @return     The tlv address type.
	 */
	Pgman_tlv get_tlv_ADDRESS_TYPE(uint8_t value,uint8_t index_start, uint8_t index_stop,uint8_t multielement);
	/**
	 * @brief      Gets the tlv seq num.
	 *
	 * @param      seqnumlist   The seqnumlist
	 * @param      value        The value
	 * @param[in]  index_start  The index start
	 * @param[in]  index_stop   The index stop
	 * @param      pointers     The pointers
	 *
	 * @return     The tlv seq num.
	 */
	Pgman_tlv get_tlv_SEQ_NUM(T_SeqNumList *seqnumlist,uint16_t *value,uint8_t index_start,uint8_t index_stop,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the tlv path metric.
	 *
	 * @param      MetricTypeList  The metrictypelist
	 * @param      value           The value
	 * @param      MetricType     The metric type
	 * @param[in]  index_start     The index start
	 * @param[in]  index_stop      The index stop
	 * @param      pointers        The pointers
	 *
	 * @return     The tlv path metric.
	 */
	Pgman_tlv get_tlv_PATH_METRIC(T_MetricTypeList *MetricTypeList,
								  uint8_t 		  *value,
								  const uint8_t         *MetricType,
								  uint8_t 		  index_start,
								  uint8_t         index_stop,
								  T_PointSerialized *pointers);
	/**
	 * @brief      Gets the tlv ack req.
	 *
	 * @return     The tlv ack req.
	 */
	Pgman_tlv get_tlv_ACK_REQ();
	/**
	 * @brief      { function_description }
	 *
	 * @param[in]  tlv   The tlv
	 */
	void printf_tlv(Pgman_tlv tlv);
	/**
	 * @brief      Calculates the tlv flags.
	 *
	 * @param[in]  tlv_type      The tlv type
	 * @param[in]  multielement  The multielement
	 *
	 * @return     The tlv flags.
	 */
	struct flags compute_tlv_flags(uint8_t tlv_type, uint8_t multielement);
	/**
	 * @brief      Gets the value 16 bit.
	 *
	 * @param[in]  size        The size
	 * @param      seqnumlist  The seqnumlist
	 * @param      value       The value
	 * @param      pointers    The pointers
	 *
	 * @return     The value 16 bit.
	 */
	uint16_t *get_value_16bit(int size,T_SeqNumList *seqnumlist, const uint16_t *value,T_PointSerialized *pointers);
	/**
	 * @brief      Gets the value 8 bit.
	 *
	 * @param[in]  size            The size
	 * @param      metrictypelist  The metrictypelist
	 * @param      value           The value
	 * @param      pointers        The pointers
	 *
	 * @return     The value 8 bit.
	 */
	uint8_t *get_value_8bit(int size,T_MetricTypeList *metrictypelist, const uint8_t *value,T_PointSerialized *pointers);

#endif



	




