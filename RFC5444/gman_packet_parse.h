#ifndef GMAN_PACKET_PARSE_H
#define GMAN_PACKET_PARSE_H

#include <stdint.h> /* int8_t*/
#include <stddef.h>//NULL
#include "gman_packet.h"


typedef struct
{
	uint8_t *pointer_head;

	uint8_t *pointer_tail;
	uint8_t *pointer_mid;
	uint8_t *pointer_prefix_length;	
	uint16_t *pointer_tlv_SEQ_NUM;
	uint8_t *pointer_tlv_PATH_METRIC;
	T_CIDR *pointer_list_network_prefix;
	T_SeqNumList *pointer_seqnumlist;
	T_MetricTypeList *pointer_metrictypelist;
	Pgman_tlv *pointer_tlvs;
	
}T_PointParsed;

void pointer_parsed_NULL(T_PointParsed *pointers);
void pointer_parsed_FREE(T_PointParsed *pointers);


T_PgmanPacket get_parse_array(uint8_t * buffer,T_PointParsed *pointers);
Pgman_pkt_header get_parse_pkt_header(uint8_t *buffer,int  *index);
Pgman_msg_header get_parse_msg_header(uint8_t *buffer,int  *index);

Pgman_address_block get_parse_address_block(uint8_t *buffer,int *index,T_PointParsed *pointers);
uint8_t* get_parse_mid(uint8_t *buffer,uint8_t length,int *index, int type,T_PointParsed  *pointer);
uint8_t* get_parse_tail(uint8_t *buffer,uint8_t length,int *index, int type,T_PointParsed  *pointer);
uint8_t* get_parse_prefix(uint8_t *buffer,uint8_t length,int *index, int type,T_PointParsed  *pointer);
uint8_t* get_parse_head(uint8_t *buffer,uint8_t length,int *index, int type,T_PointParsed  *pointer);

Pgman_tlv_block get_parse_tlv_block(uint8_t *buffer,int *index,T_PointParsed *pointers);
Pgman_tlv get_parse_tlv(uint8_t *buffer,int *index,T_PointParsed *pointers);
uint16_t* get_parse_tlv_SEQ_NUM(uint8_t *buffer,int *index,uint8_t number_values,T_PointParsed *pointers);
uint8_t*  get_parse_tlv_PATH_METRIC(uint8_t *buffer,int *index,uint8_t number_values,T_PointParsed *pointers);
uint8_t	get_parse_tlv_ADDRESS_TYPE(uint8_t *buffer,int *index);
#endif