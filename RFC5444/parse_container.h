#ifndef PARSE_CONTAINER_H
#define PARSE_CONTAINER_H


#include <stdint.h> /* int8_t*/
#include <stddef.h>//NULL
#include "containers/RREQ_container.h"
#include "containers/RREP_container.h"
#include "containers/RERR_container.h"
#include "gman_packet.h"
#include "gman_packet_parse.h"


uint8_t get_parse_container(T_PgmanPacket packet);
T_RREQ get_parse_container_rreq(T_PgmanPacket packet,T_PointParsed *pointers);
T_RREP get_parse_container_rrep(T_PgmanPacket packet,T_PointParsed *pointers);
T_RERR get_parse_container_rerr(T_PgmanPacket packet,T_PointParsed *pointers);
uint8_t get_parse_container_rrep_ack(T_PgmanPacket packet);



#endif
