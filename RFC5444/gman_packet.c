
#include <global_structs.h>
#include "gman_packet.h"

void nullPointSerialized(T_PointSerialized *pointers) {

    pointers->pointer_value_8bit = NULL;
    pointers->pointer_value_16bit = NULL;
    pointers->pointer_head = NULL;
    pointers->pointer_tail = NULL;
    pointers->pointer_mid = NULL;
    pointers->pointer_prefix_length = NULL;
    pointers->pointer_tlvs = NULL;
    pointers->pointer_array_packet = NULL;
}

void freePointSerialized(T_PointSerialized *pointers) {

    free (pointers->pointer_value_8bit);
    free (pointers->pointer_value_16bit);
    free (pointers->pointer_head);
    free (pointers->pointer_tail);
    free (pointers->pointer_mid);
    free (pointers->pointer_prefix_length);
    free (pointers->pointer_tlvs);
    free (pointers->pointer_array_packet);
}

/*----------PACKET---------------------------*/
T_PgmanPacket get_RREP_Packet(T_RREP rrep, T_PointSerialized *pointers) {

    T_PgmanPacket packet;
    packet.pkt_header = get_gman_pkt_header ();
    packet.message = get_message_rrep (rrep , pointers);
    return packet;
}

T_PgmanPacket get_RREQ_Packet(T_RREQ rreq, T_PointSerialized *pointers) {

    T_PgmanPacket packet;
    packet.pkt_header = get_gman_pkt_header ();
    packet.message = get_message_rreq (rreq , pointers);
    return packet;
}

T_PgmanPacket get_RERR_Packet(T_RERR rerr_container, T_PointSerialized *pointers) {

    T_PgmanPacket packet;
    packet.pkt_header = get_gman_pkt_header ();
    packet.message = get_message_rerr (rerr_container , pointers);
    return packet;
}

T_PgmanPacket get_RREPACK_Packet(uint8_t acknowledgement, T_PointSerialized *pointers) {

    T_PgmanPacket packet;
    packet.pkt_header = get_gman_pkt_header ();
    packet.message = get_message_rrep_ack (acknowledgement , pointers);
    return packet;
}

Pgman_pkt_header get_gman_pkt_header () {

    Pgman_pkt_header gman_pkt_header;
    gman_pkt_header.version = get_pkt_header_version ();
    gman_pkt_header.pkt_flags = compute_pkt_flags ();
    gman_pkt_header.pkt_header_length = compute_pkt_header_length ();
    return gman_pkt_header;
}

struct flags4 compute_pkt_flags () {

    unsigned phasseqnum , lphastlv;
    struct flags4 pkt_flags;
    phasseqnum = ZERO;
    lphastlv = ZERO;
    pkt_flags.bit0 = phasseqnum;
    pkt_flags.bit1 = lphastlv;
    pkt_flags.bit2 = ZERO;
    pkt_flags.bit3 = ZERO;
    return pkt_flags;
}

unsigned get_pkt_header_version () {

    return VERSION;
}

uint8_t compute_pkt_header_length () {

    return 1;
}

/*----------MESSAGE--------------------------*/
Pgman_message get_message_rreq (T_RREQ rreq , T_PointSerialized *pointers) {

    Pgman_message message;
    Tadress_block_list list;
    message.msg_header = get_msg_header (RREQ , rreq.MsgHopLimit);
    message.message_tlv_block.tlvs_length = 0;
    message.message_tlv_block.tlvs = NULL;
    data_preparation_one (rreq.AddressList.OrigPrefix , rreq.AddressList.TargPrefix , &list , RREQ);
    message.address_block = get_address_block (list , pointers);
    message.address_tlv_block = get_tlv_block_rreq (rreq , pointers);
    message.msg_header.msg_size = get_msg_size (message);
    return message;
}

Pgman_message get_message_rrep (T_RREP rrep , T_PointSerialized *pointers) {

    Pgman_message message;
    Tadress_block_list list;
    message.msg_header = get_msg_header (RREP , rrep.MsgHopLimit);
    message.message_tlv_block.tlvs_length = 0;
    message.message_tlv_block.tlvs = NULL;
    data_preparation_one (rrep.AddressList.OrigPrefix , rrep.AddressList.TargPrefix , &list , RREP);
    message.address_block = get_address_block (list , pointers);
    message.address_tlv_block = get_tlv_block_rrep (rrep , pointers);
    message.msg_header.msg_size = get_msg_size (message);
    return message;
}

Pgman_message get_message_rerr (T_RERR rerr_container , T_PointSerialized *pointers) {

    Pgman_message message;
    Tadress_block_list list;
    message.msg_header = get_msg_header (RERR , MAX_HOPCOUNT);//MsgHopLimit= max_hop_count por defecto
    message.message_tlv_block.tlvs_length = 0;
    message.message_tlv_block.tlvs = NULL;
    data_preparation_two (rerr_container.AddressListRERR , rerr_container.PktSource , &list);
    message.address_block = get_address_block (list , pointers);
    message.address_tlv_block = get_tlv_block_rerr (rerr_container , pointers);
    message.msg_header.msg_size = get_msg_size (message);
    return message;
}

Pgman_message get_message_rrep_ack (uint8_t acknowledgement , T_PointSerialized *pointers) {

    Pgman_message message;
    message.msg_header = get_msg_header (RREP_ACK , MAX_HOPCOUNT);//MsgHopLimit= max_hop_count por defecto
    message.message_tlv_block = get_tlv_block_rrep_ack (acknowledgement , pointers);
    message.msg_header.msg_size = get_msg_size (message);
    return message;
}

/*----------MSG HEADER-----------------------*/
Pgman_msg_header get_msg_header (uint8_t aodv_msg_type , uint8_t msg_hop_limit) {

    Pgman_msg_header gman_msg_header;
    gman_msg_header.msg_type = aodv_msg_type;
    gman_msg_header.msg_flags = compute_msg_flags (aodv_msg_type);
    gman_msg_header.msg_addr_length = IPV4 - 1;
    gman_msg_header.msg_hop_limit = get_msg_hop_limit (msg_hop_limit);
    gman_msg_header.msg_header_length = ( uint8_t ) compute_msg_header_length (aodv_msg_type);
    return gman_msg_header;
}

struct flags4 compute_msg_flags (uint8_t aodv_msg_type) {

    struct flags4 msg_flags;
    unsigned mhasorig = 0 , mhashoplimit = 0 , mhashopcount = 0 , mhasseqnum = 0;
    if ( aodv_msg_type == RREQ ) {

        mhasorig = ZERO;
        mhashoplimit = ONE;    //Solo se incluye el campo <MsgHopLimit> en la <msg-header>
        mhashopcount = ZERO;
        mhasseqnum = ZERO;
    }
    if ( aodv_msg_type == RREP ) {

        mhasorig = ZERO;
        mhashoplimit = ONE;    //Solo se incluye el campo <MsgHopLimit> en la <msg-header>
        mhashopcount = ZERO;
        mhasseqnum = ZERO;
    }
    if ( aodv_msg_type == RERR ) {

        mhasorig = ZERO;
        mhashoplimit = ZERO;
        mhashopcount = ZERO;
        mhasseqnum = ZERO;
    }
    if ( aodv_msg_type == RREP_ACK ) {

        mhasorig = ZERO;
        mhashoplimit = ZERO;
        mhashopcount = ZERO;
        mhasseqnum = ZERO;
    }
    msg_flags.bit0 = mhasorig;
    msg_flags.bit1 = mhashoplimit;
    msg_flags.bit2 = mhashopcount;
    msg_flags.bit3 = mhasseqnum;
    return msg_flags;

}

uint16_t get_msg_size (Pgman_message message) {

    uint16_t msg_size = 0;
    uint8_t type;
    type = message.msg_header.msg_type;
    if ( type == RREP || type == RREQ ) {

        msg_size = ( uint16_t ) ( 5 +   // msg_header es 5 para RREQ,RREP,RERR;
                                  2 +   //  message_tlv_block.tlv_length
                                  message.address_block.address_block_length +
                                  2 +   //  address_block_tlv_block.tlv_length
                                  message.address_tlv_block.tlvs_length );
    }
    if ( type == RERR ) {

        msg_size = ( uint16_t ) ( 4 +   // msg_header es  para RERR;
                                  2 +   //  message_tlv_block.tlv_length
                                  message.address_block.address_block_length + +
                                          2 +
                                  message.address_tlv_block.tlvs_length );
    }
    if ( type == RREP_ACK ) {

        if ( message.message_tlv_block.tlvs_length == 0 ) {

            msg_size = 4 + 2;
        }
        if ( message.message_tlv_block.tlvs_length == 2 ) {

            msg_size = 5 + 3;
        }
    }
    return msg_size;
}

uint8_t get_msg_hop_limit (uint8_t msg_hop_limit) {

    return msg_hop_limit;
}

uint16_t compute_msg_header_length (uint8_t aodv_msg_type) {

    uint16_t msg_header_length = 0;
    if ( aodv_msg_type == RREQ || aodv_msg_type == RREP ) {

        msg_header_length = 5;
    }
    if ( aodv_msg_type == RREP_ACK ||aodv_msg_type == RERR ) {

        msg_header_length = 4;
    }
    return msg_header_length;

}

/*----------IPAddress Block--------------------*/
Pgman_address_block get_address_block (Tadress_block_list list , T_PointSerialized *pointers) {

    Pgman_address_block gman_address_block;
    uint8_t head_length;
    uint8_t tail_length;
    uint8_t mid_length;
    uint8_t *head;
    uint8_t *tail;
    uint8_t *mid;
    uint8_t *prefix_length;
    struct flags addr_flags;
    head_length = get_head_length (list);
    tail_length = get_tail_length (list , head_length);
    mid_length = get_mid_length (head_length , tail_length);
    addr_flags = compute_addr_flags (list , head_length , tail_length);
    if ( addr_flags.bit0 == ONE ) {

        head = get_head (list , head_length , pointers);
        if ( head != NULL) {

            gman_address_block.head = head;
        }
    }
    if (( addr_flags.bit1 == ONE ) && ( addr_flags.bit2 == ZERO )) {

        tail = get_tail (list , tail_length , pointers);
        if ( tail != NULL) {

            gman_address_block.tail = tail;
        }
    }
    if ( mid_length > 0 ) {

        mid = get_mid (list , head_length , mid_length , pointers);
        if ( mid != NULL) {

            gman_address_block.mid = mid;
        }
    }
    prefix_length = get_prefix_length (list , addr_flags , pointers);
    if ( prefix_length != NULL) {

        gman_address_block.prefix_length = prefix_length;
    }

    gman_address_block.num_addr = list.overall;
    gman_address_block.addr_flags = addr_flags;
    gman_address_block.tail_length = tail_length;
    gman_address_block.head_length = head_length;
    gman_address_block.address_block_length = compute_address_block_length (gman_address_block);
    return gman_address_block;
}
uint8_t get_head_length (Tadress_block_list list) {

    uint8_t head_length;
    if ( list.overall == 1 ) {

        head_length = 0;
    }else {

        head_length = 0;
        for ( int i = 0 ; i < 3 ; i ++ ) {

            for( int a = 0 ; a < list.overall ; a ++ ) {

                if(list.cidr[0].Address[i] != list.cidr[a].Address[i]) {

                    return head_length;
                }
            }
            head_length ++;
        }
    }
    return head_length;
}

uint8_t get_tail_length (Tadress_block_list list , uint8_t head_length) {

    uint8_t tail_length;
    if ( list.overall == 1 ) {

        tail_length = 0;
    }else {

        tail_length = 0;
        for ( int i = 3 ; i >= head_length ; i -- ) {

            for ( int a = 0 ; a < list.overall ; a ++ ) {

                if ( list.cidr[0].Address[i] !=
                     list.cidr[a].Address[i] ) {

                    return tail_length;
                }
            }
            tail_length ++;
        }
    }
    return tail_length;
}
uint8_t get_mid_length (uint8_t head_length , uint8_t tail_length) {

    return ( uint8_t ) ( IPV4 - head_length - tail_length );
}
unsigned compute_ahaszerotail (Tadress_block_list list , uint8_t tail_length) {

    int a = 0;
    unsigned ahaszerotail;
    for (int i  = 3 ; i >= IPV4 - tail_length ; i -- ) {

        if ( list.cidr[ 0 ].Address[ i ] == 0 ) {

            a ++;
        }
    }
    if ( a == tail_length ) {

        ahaszerotail = ONE;
    } else {

        ahaszerotail = ZERO;
    }
    return ahaszerotail;
}
struct flags compute_addr_flags (Tadress_block_list list , uint8_t head_length , uint8_t tail_length) {

    struct flags flag;
    unsigned ahashead , ahasfulltail , ahaszerotail , ahassingleprelen , ahasmultiprelen;
    int cont , different;
    ahashead = head_length > 0 ? ONE : ZERO;
    if ( tail_length > 0 ) {

        ahaszerotail = compute_ahaszerotail (list , tail_length);
        ahasfulltail = ahaszerotail == ONE ? ZERO : ONE;
    }else {

        ahasfulltail = ZERO;
        ahaszerotail = ZERO;
    }
    if ( list.overall == 1 ) {

        ahassingleprelen = ONE;
        ahasmultiprelen = ZERO;
    }else {

        different = 0;
        for ( cont = 1 ; cont < list.overall ; cont ++ ) {

            if ( list.cidr[ cont ].PrefixLen != list.cidr[ 0 ].PrefixLen ) {

                different = 1;
            }
        }
        if ( different == 1 ) {

            ahassingleprelen = ZERO;
            ahasmultiprelen = ONE;
        }else {

            ahassingleprelen = ONE;
            ahasmultiprelen = ZERO;
        }
    }
    flag.bit0 = ahashead;
    flag.bit1 = ahasfulltail;
    flag.bit2 = ahaszerotail;
    flag.bit3 = ahassingleprelen;
    flag.bit4 = ahasmultiprelen;
    flag.bit5 = ZERO;
    flag.bit6 = ZERO;
    flag.bit7 = ZERO;
    return flag;
}

uint8_t *get_head (Tadress_block_list list , uint8_t head_length , T_PointSerialized *pointers) {

    int cont;
    uint8_t *head = ( uint8_t * ) malloc (head_length * sizeof (uint8_t));
    pointers->pointer_head = head;
    if ( head == NULL) {

        printf ("GMAN_PACKET_C: unable to allocate memory for  HEAD array\n");
        return NULL;
    }

    for ( cont = 0 ; cont < head_length ; cont ++ ) {

        head[ cont ] = list.cidr[ 0 ].Address[ cont ];
    }
    return head;
}

uint8_t *get_tail (Tadress_block_list list , uint8_t tail_length , T_PointSerialized *pointers) {

    int cont;
    int cont1;
    cont1 = 0;
    uint8_t *tail = ( uint8_t * ) malloc (tail_length * sizeof (uint8_t));
    pointers->pointer_tail = tail;
    if ( tail == NULL) {

        printf ("GMAN_PACKET_C: unable to allocate memory for TAIL array\n");
        return NULL;
    }
    for ( cont = IPV4 - tail_length ; cont < IPV4 ; cont ++ ) {

        tail[ cont1 ] = list.cidr[ 0 ].Address[ cont ];
        cont1 ++;
    }
    return tail;
}
uint8_t *get_mid (Tadress_block_list list , uint8_t head_length , uint8_t mid_length , T_PointSerialized *pointers) {

    int cont;
    int cont1 , cont2;
    uint8_t *mid = ( uint8_t * ) malloc (( list.overall ) * ( mid_length ) * sizeof (uint8_t));
    pointers->pointer_mid = mid;
    if ( mid == NULL) {

        printf ("GMAN_PACKET_C: unable to allocate memory for  MID array\n");
        return NULL;
    }
    if ( list.overall == 1 ) {

        for ( cont = 0 ; cont < IPV4 ; cont ++ ) {

            mid[ cont ] = list.cidr[ 0 ].Address[ cont ];
        }
    }
    else {

        cont2 = 0;
        for ( cont = 0 ; cont < list.overall ; cont ++ ) {

            for ( cont1 = head_length ; cont1 < ( head_length + mid_length ) ; cont1 ++ ) {

                mid[ cont2 ] = list.cidr[ cont ].Address[ cont1 ];
                cont2 ++;
            }
        }
    }
    return mid;
}

uint8_t *get_prefix_length (Tadress_block_list list , struct flags addr_flags , T_PointSerialized *pointers) {

    if ( list.overall == 1 ) {

        uint8_t *prefix_length = ( uint8_t * ) malloc (sizeof (uint8_t));
        pointers->pointer_prefix_length = prefix_length;
        if ( prefix_length == NULL) {

            printf ("GMAN_PACKET_C: unable to allocate memory for  prefix_length array\n");
            return NULL;
        }
        prefix_length[ 0 ] = list.cidr[ 0 ].PrefixLen;
        return prefix_length;
    }
    if (( addr_flags.bit3 == 1 ) && ( addr_flags.bit4 == 0 )) {

        uint8_t *prefix_length = ( uint8_t * ) malloc (sizeof (uint8_t));
        pointers->pointer_prefix_length = prefix_length;
        if ( prefix_length == NULL) {

            printf ("GMAN_PACKET_C: unable to allocate memory for  prefix_length array\n");
            return NULL;
        }
        prefix_length[ 0 ] = list.cidr[ 0 ].PrefixLen;
        return prefix_length;
    }
    if (( addr_flags.bit3 == 0 ) && ( addr_flags.bit4 == 1 )) {

        uint8_t *prefix_length = ( uint8_t * ) malloc (list.overall * sizeof (uint8_t));
        pointers->pointer_prefix_length = prefix_length;
        if ( prefix_length == NULL) {

            printf ("GMAN_PACKET_C: unable to allocate memory for  prefix_length array\n");
            return NULL;
        }
        for ( int i = 0 ; i < list.overall ; i ++ ) {

            prefix_length[ i ] = list.cidr[ i ].PrefixLen;
        }
        return prefix_length;
    }
    return NULL;
}

void data_preparation_one (T_CIDR origprefix , T_CIDR targprefix , Tadress_block_list *list ,
                           uint8_t msg_type) {
    list->cidr[ 0 ] = origprefix;
    list->cidr[ 1 ] = targprefix;
    list->overall = 2;
    list->msg_type = msg_type;
}
void data_preparation_two (T_AddressListRERR addresslist_rerr , T_CIDR *pktsource , Tadress_block_list *list) {

    for ( int i = 0 ; i < addresslist_rerr.overall ; i ++ ) {

        list->cidr[ i ] = addresslist_rerr.Addresses[ i ];
        list->overall ++;
    }
    if ( pktsource != NULL) {
        list->cidr[ list->overall ] = *pktsource;
        list->overall ++;
    }
}

uint8_t compute_address_block_length (Pgman_address_block address_block) {

    uint8_t packet_length;
    uint8_t mid_length;
    mid_length = get_mid_length (address_block.head_length , address_block.tail_length);
    packet_length = 2;

    if ( address_block.addr_flags.bit0 == ONE ) {

        packet_length ++;
        packet_length = packet_length + address_block.head_length;
    }
    if (( address_block.addr_flags.bit1 == ONE ) && ( address_block.addr_flags.bit2 == ZERO )) {

        packet_length ++;
        packet_length = packet_length + address_block.tail_length;
    }
    if (( address_block.addr_flags.bit1 == ZERO ) && ( address_block.addr_flags.bit2 == ONE )) {

        packet_length ++;
    }
    if ( mid_length > 0 ) {

        packet_length = packet_length + ( address_block.num_addr * mid_length );
    }
    if (( address_block.addr_flags.bit3 == 1 ) && ( address_block.addr_flags.bit4 == 0 )) {

        packet_length ++;
    }
    if (( address_block.addr_flags.bit3 == 0 ) && ( address_block.addr_flags.bit4 == 1 )) {

        packet_length = packet_length + address_block.num_addr;
    }
    return packet_length;
}

/*----------TLV BLOCK ---------------*/
Pgman_tlv_block get_tlv_block_rreq (T_RREQ rreq , T_PointSerialized *pointers) {

    Pgman_tlv_block tlv_block;
    int cont;
    int size = 0;
    int A = 0;
    uint16_t length = 0;
    if ( rreq.TargSeqNum == 0 ) {

        A = 1;
    }
    size = 4 + A;
    tlv_block.tlvs = get_tlvs_rreq (size , rreq , pointers);
    tlv_block.tlvs_length = 0;
    for ( cont = 0 ; cont < size ; cont ++ ) {

        length = compute_tlvs_length (tlv_block.tlvs[ cont ].tlv_flags , tlv_block.tlvs[ cont ].length);
        tlv_block.tlvs_length = tlv_block.tlvs_length + length;
    }
    tlv_block.overall = ( uint8_t ) size;
    return tlv_block;
}
Pgman_tlv_block get_tlv_block_rrep (T_RREP rrep , T_PointSerialized *pointers) {

    Pgman_tlv_block tlv_block;
    int cont;
    uint16_t length = 0;
    int size = 4;// TOTAL TLV para un mensaje RREP
    tlv_block.tlvs = get_tlvs_rrep (size , rrep , pointers);
    tlv_block.tlvs_length = 0;
    for ( cont = 0 ; cont < size ; cont ++ ) {

        length = compute_tlvs_length (tlv_block.tlvs[ cont ].tlv_flags , tlv_block.tlvs[ cont ].length);
        tlv_block.tlvs_length = tlv_block.tlvs_length + length;
    }
    tlv_block.overall = ( uint8_t ) size;
    return tlv_block;
}

Pgman_tlv_block get_tlv_block_rerr (T_RERR rerr_container , T_PointSerialized *pointers) {

    Pgman_tlv_block tlv_block;
    int cont;
    int A = 0;
    int B = 0;
    int size = 0;
    uint16_t length = 0;
    if ( rerr_container.SeqNumList != NULL) {

        A = 1;
    }
    if ( rerr_container.PktSource != NULL) {

        B = 1;
    }
    size = 2 + A + B;
    tlv_block.tlvs = get_tlvs_rerr (size , rerr_container , A , B , pointers);
    tlv_block.tlvs_length = 0;
    for ( cont = 0 ; cont < size ; cont ++ ) {

        length = compute_tlvs_length (tlv_block.tlvs[ cont ].tlv_flags , tlv_block.tlvs[ cont ].length);
        tlv_block.tlvs_length = tlv_block.tlvs_length + length;
    }
    tlv_block.overall = ( uint8_t ) size;
    return tlv_block;
}

Pgman_tlv_block get_tlv_block_rrep_ack (uint8_t acknowledgement , T_PointSerialized *pointers) {

    Pgman_tlv_block tlv_block;
    tlv_block.tlvs_length = 0;
    uint16_t length = 0;
    if ( acknowledgement == ONE ) {

        tlv_block.tlvs = get_tlvs_rrep_ack (pointers);
        length = compute_tlvs_length (tlv_block.tlvs[ 0 ].tlv_flags , tlv_block.tlvs[ 0 ].length);
        tlv_block.tlvs_length = tlv_block.tlvs_length + length;
    }
    else {

        tlv_block.tlvs_length = 0;
    }
    return tlv_block;
}

Pgman_tlv *get_tlvs_rreq (int size , T_RREQ rreq , T_PointSerialized *pointers) {

    Pgman_tlv *tlvs = ( Pgman_tlv * ) malloc (size * sizeof (Pgman_tlv));
    pointers->pointer_tlvs = tlvs;
    uint8_t index_start = 0;
    if ( tlvs == NULL) {

        printf ("get_tlv_block: unable to allocate memory for  tlv array\n");
    }
    else {

        index_start = ZERO;
        tlvs[ 0 ] = get_tlv_ADDRESS_TYPE (ORIGPREFIX , index_start , index_start , ZERO);
        tlvs[ 1 ] = get_tlv_SEQ_NUM (NULL , &rreq.OrigSeqNum , index_start , index_start , pointers);
        tlvs[ 2 ] = get_tlv_PATH_METRIC (NULL , &rreq.OrigMetric , &rreq.MetricType , index_start ,
                                         index_start , pointers);
        index_start = ONE;
        tlvs[ 3 ] = get_tlv_ADDRESS_TYPE (TARGPREFIX , index_start , index_start , ZERO);
        if ( size == 5 ) {

            tlvs[ 4 ] = get_tlv_SEQ_NUM (NULL , &rreq.TargSeqNum , index_start , index_start , pointers);
        }
    }
    return tlvs;
}

Pgman_tlv *get_tlvs_rrep (int size , T_RREP rrep , T_PointSerialized *pointers) {

    uint8_t index_start;
    Pgman_tlv *tlvs = ( Pgman_tlv * ) malloc (size * sizeof (Pgman_tlv));
    pointers->pointer_tlvs = tlvs;
    if ( tlvs == NULL) {

        printf ("get_tlv_block: unable to allocate memory for  tlv array\n");
    }else {

        index_start = ZERO;
        tlvs[ 0 ] = get_tlv_ADDRESS_TYPE (ORIGPREFIX , index_start , index_start , ZERO);
        index_start = ONE;
        tlvs[ 1 ] = get_tlv_ADDRESS_TYPE (TARGPREFIX , index_start , index_start , ZERO);
        tlvs[ 2 ] = get_tlv_SEQ_NUM (NULL , &rrep.TargSeqNum , index_start , index_start , pointers);
        tlvs[ 3 ] = get_tlv_PATH_METRIC (NULL , &rrep.TargMetric , &rrep.MetricType , index_start ,
                                         index_start , pointers);
    }
    return tlvs;
}

Pgman_tlv *get_tlvs_rerr (int size , T_RERR rerr_container , int A , int B , T_PointSerialized *pointers) {

    Pgman_tlv *tlv = ( Pgman_tlv * ) malloc (size * sizeof (Pgman_tlv));
    pointers->pointer_tlvs = tlv;
    uint8_t index_start;
    uint8_t index_stop;
    if ( tlv == NULL) {

        printf ("get_tlv_block: unable to allocate memory for  tlv array\n");
    }else {

        index_start = 0;
        index_stop = ( uint8_t ) ( rerr_container.AddressListRERR.overall - 1 );
        tlv[ 0 ] = get_tlv_ADDRESS_TYPE (UNREACHABLE , index_start , index_stop , ONE);
        tlv[ 1 ] = get_tlv_PATH_METRIC (rerr_container.MetricTypeList , NULL , NULL , index_start , index_stop ,
                                        pointers);
        if ( A == 1 ) {

            tlv[ 2 ] = get_tlv_SEQ_NUM (rerr_container.SeqNumList , NULL , index_start , index_stop , pointers);
        }
        if ( B == 1 ) {

            index_start = ( uint8_t ) ( index_stop + 1 );
            index_stop = index_start;
            tlv[ 3 ] = get_tlv_ADDRESS_TYPE (PKTSOURCE , index_start , index_stop , ONE);
        }
    }
    return tlv;
}

Pgman_tlv *get_tlvs_rrep_ack (T_PointSerialized *pointers) {

    Pgman_tlv *tlvs = ( Pgman_tlv * ) malloc (sizeof (Pgman_tlv));
    pointers->pointer_tlvs = tlvs;
    tlvs[ 0 ] = get_tlv_ACK_REQ ();
    return tlvs;
}
uint16_t compute_tlvs_length (struct flags flag , uint8_t tlv_length) {

    uint16_t size = 2; //  TLV_type + TVL_flags =2
    if ( flag.bit0 == ONE ) size ++; // campo extension tlv
    if ( flag.bit1 == ONE && flag.bit2 == ZERO ) size ++; // campo index_start
    if ( flag.bit1 == ZERO && flag.bit2 == ONE ) size = ( uint16_t ) ( size + 2 ); // campo index_start y index_stop
    if ( flag.bit3 == ONE && flag.bit4 == ZERO ) size ++;    // campo tlv.length igual a 1 byte
    if ( flag.bit3 == ONE && flag.bit4 == ONE ) size = ( uint16_t ) ( size + 2 ); // campo tlv.length igual a 2 byte
    return size + tlv_length;
}

/*----------TLV----------------------*/
Pgman_tlv get_tlv_ADDRESS_TYPE (uint8_t value , uint8_t index_start , uint8_t index_stop , uint8_t multielement) {

    Pgman_tlv tlv;
    tlv.tlv_type = ADDRESS_TYPE;
    tlv.tlv_flags = compute_tlv_flags (ADDRESS_TYPE , multielement);
    if ( tlv.tlv_flags.bit1 == ONE ) {

        if ( tlv.tlv_flags.bit2 == ZERO ) {

            tlv.index_start = index_start;
        }
    }
    if ( tlv.tlv_flags.bit1 == ZERO && tlv.tlv_flags.bit2 == ONE ) {

        tlv.index_start = index_start;
        tlv.index_stop = index_stop;
    }
    tlv.length = 1;
    tlv.value = value;
    return tlv;
}

Pgman_tlv get_tlv_SEQ_NUM (T_SeqNumList *seqnumlist , uint16_t *value , uint8_t index_start , uint8_t index_stop ,
                           T_PointSerialized *pointers) {
    Pgman_tlv tlv;
    tlv.tlv_type = SEQ_NUM;
    if ( seqnumlist != NULL && value == NULL){//El TLV contendra una lista de numeros de secuencia

        tlv.tlv_flags = compute_tlv_flags (SEQ_NUM , ONE);
        tlv.index_start = index_start;
        tlv.index_stop = index_stop;
        tlv.length = ( uint8_t ) (( seqnumlist->overall ) * 2 );
        tlv.value_16bit = get_value_16bit (seqnumlist->overall , seqnumlist , NULL , pointers);
    }
    if ( seqnumlist == NULL && value != NULL) {//El TLV contendra un unico numero de secuencia

        tlv.tlv_flags = compute_tlv_flags (SEQ_NUM , ZERO);
        tlv.index_start = index_start;
        tlv.length = 2;
        tlv.value_16bit = get_value_16bit (1 , NULL , value , pointers);
    }
    return tlv;
}

Pgman_tlv get_tlv_PATH_METRIC (T_MetricTypeList *MetricTypeList , uint8_t *value , const uint8_t *MetricType ,
                               uint8_t index_start , uint8_t index_stop , T_PointSerialized *pointers) {
    Pgman_tlv tlv;

    tlv.tlv_type = PATH_METRIC;

    if ( MetricTypeList != NULL && value == NULL)//El TLV contendra una lista de prefijos
    {
        tlv.tlv_flags = compute_tlv_flags (PATH_METRIC , ONE);
        tlv.index_start = index_start;
        tlv.index_stop = index_stop;
        tlv.length = (uint8_t) ( MetricTypeList->overall );
        tlv.value_8bit = get_value_8bit (MetricTypeList->overall , MetricTypeList , NULL , pointers);
    }
    if ( MetricTypeList == NULL && value != NULL)  //El TLV contendra un unico numero de secuencia
    {
        tlv.tlv_type_ext = *MetricType;
        tlv.tlv_flags = compute_tlv_flags (PATH_METRIC , ZERO);
        tlv.index_start = index_start;
        tlv.length = 1;
        tlv.value_8bit = get_value_8bit (1 , NULL , value , pointers);
    }
    return tlv;
}

Pgman_tlv get_tlv_ACK_REQ () {
    Pgman_tlv tlv;
    tlv.tlv_type = ACK_REQ;
    tlv.tlv_flags = compute_tlv_flags (ACK_REQ , ZERO);
    tlv.length = 0;
    return tlv;
}

uint16_t *get_value_16bit (int size , T_SeqNumList *seqnumlist , const uint16_t *value , T_PointSerialized *pointers) {

    int cont;
    uint16_t *value_16bit = ( uint16_t * ) malloc (size * sizeof (uint16_t));
    pointers->pointer_value_16bit = value_16bit;
    if ( value_16bit == NULL) {

        printf ("get_tlv_block: unable to allocate memory for  tlv array\n");
    }else {

        if ( value != NULL) {

            value_16bit[ 0 ] = *value;
        }
        if ( seqnumlist != NULL) {

            for ( cont = 0 ; cont < size ; cont ++ ) {

                value_16bit[ cont ] = seqnumlist->SeqNumbers[ cont ];
            }
        }
    }
    return value_16bit;
}

uint8_t *get_value_8bit (int size , T_MetricTypeList *metrictypelist , const uint8_t *value , T_PointSerialized *pointers) {

    int cont;
    uint8_t *value_8bit = ( uint8_t * ) malloc (size * sizeof (uint8_t));
    pointers->pointer_value_8bit = value_8bit;

    if ( value_8bit == NULL) {

        printf ("get_tlv_block: unable to allocate memory for  tlv array\n");
    }else {

        if ( value != NULL) {

            value_8bit[ 0 ] = *value;
        }
        if ( metrictypelist != NULL) {
            for ( cont = 0 ; cont < size ; cont ++ ) {

                value_8bit[ cont ] = metrictypelist->MetricTypes[ cont ];
            }
        }
    }
    return value_8bit;
}

struct flags compute_tlv_flags (uint8_t tlv_type , uint8_t multielement) {

    struct flags tlv_flags;
    unsigned thastypeext = 0 , thassingleindex = 0 , thasmultiindex = 0 , thasvalue = 0 , thasextlen = 0 , tismultivalue = 0;
    if ( tlv_type == PATH_METRIC ) {

        if ( multielement == ONE ) {

            thastypeext = ZERO;
            thassingleindex = ZERO;
            thasmultiindex = ONE;
            tismultivalue = ONE;
        }else {

            thastypeext = ONE;
            thassingleindex = ONE;
            thasmultiindex = ZERO;
            tismultivalue = ZERO;

        }
        thasvalue = ONE;
        thasextlen = ZERO;
    }
    if ( tlv_type == SEQ_NUM ) {

        thastypeext = ZERO;

        if ( multielement == ONE ) {

            thassingleindex = ZERO;
            thasmultiindex = ONE;
            tismultivalue = ONE;
        }else {

            thassingleindex = ONE;
            thasmultiindex = ZERO;
            tismultivalue = ZERO;
        }
        thasvalue = ONE;
        thasextlen = ZERO;
    }
    if ( tlv_type == ADDRESS_TYPE ) {

        thastypeext = ZERO;
        if ( multielement == ONE ) {

            thassingleindex = ZERO;
            thasmultiindex = ONE;
        }else {

            thassingleindex = ONE;
            thasmultiindex = ZERO;
        }
        thasvalue = ONE;
        thasextlen = ZERO;
        tismultivalue = ZERO;
    }
    if ( tlv_type == ACK_REQ ) {

        thastypeext = ZERO;
        thassingleindex = ZERO;
        thasmultiindex = ZERO;
        thasvalue = ZERO;
        thasextlen = ZERO;
        tismultivalue = ZERO;
    }
    tlv_flags.bit0 = thastypeext;
    tlv_flags.bit1 = thassingleindex;
    tlv_flags.bit2 = thasmultiindex;
    tlv_flags.bit3 = thasvalue;
    tlv_flags.bit4 = thasextlen;
    tlv_flags.bit5 = tismultivalue;
    tlv_flags.bit6 = ZERO;
    tlv_flags.bit7 = ZERO;
    return tlv_flags;
}
/*
void printf_tlv (Pgman_tlv tlv) {

    uint16_t size = compute_tlvs_length (tlv.tlv_flags , tlv.length);
    int cont;
    printf ("\n\n");
    printf ("TLV_TYPE: %"PRIu8"" , tlv.tlv_type);
    printf ("(OCTETS:  %"PRIu16")\n" , size);
    printf ("TLV_FLAGS: %u %u %u %u %u %u RSV: %u %u \n" , tlv.tlv_flags.bit0 ,
            tlv.tlv_flags.bit1 ,
            tlv.tlv_flags.bit2 ,
            tlv.tlv_flags.bit3 ,
            tlv.tlv_flags.bit4 ,
            tlv.tlv_flags.bit5 ,
            tlv.tlv_flags.bit6 ,
            tlv.tlv_flags.bit7);
    if ( tlv.tlv_flags.bit0 == ONE ) {

        printf ("TYPE_EXT: %"PRIu8"\n" , tlv.tlv_type_ext);
    }
    if ( tlv.tlv_type == SEQ_NUM ) {

        if ( tlv.tlv_flags.bit1 == ONE && tlv.tlv_flags.bit2 == ZERO ) {

            printf ("INDEX_START %"PRIu8"\n" , tlv.index_start);
            printf ("LENGTH: %"PRIu8"\n" , tlv.length);
            printf ("VALUE:  %"PRIu16"\n" , tlv.value_16bit[ 0 ]);
        }
        if ( tlv.tlv_flags.bit1 == ZERO && tlv.tlv_flags.bit2 == ONE ) {

            printf ("INDEX_START %"PRIu8"\n" , tlv.index_start);
            printf ("INDEX_STOP %"PRIu8"\n" , tlv.index_stop);
            printf ("LENGTH: %"PRIu8"\n" , tlv.length);
            printf ("VALUE: ");
            for ( cont = 0 ; cont < ( tlv.length ) / 2 ; cont ++ ) {

                printf (" %"PRIu16"" , tlv.value_16bit[ cont ]);
            }
            printf ("\n\n");
        }
    }
    if ( tlv.tlv_type == PATH_METRIC ) {

        if ( tlv.tlv_flags.bit1 == ONE && tlv.tlv_flags.bit2 == ZERO ) {

            printf ("INDEX_START %"PRIu8"\n" , tlv.index_start);
            printf ("LENGTH: %"PRIu8"\n" , tlv.length);
            printf ("VALUE:  %"PRIu8"\n" , tlv.value_8bit[ 0 ]);
        }
        if ( tlv.tlv_flags.bit1 == ZERO && tlv.tlv_flags.bit2 == ONE ) {

            printf ("INDEX_START %"PRIu8"\n" , tlv.index_start);
            printf ("INDEX_STOP %"PRIu8"\n" , tlv.index_stop);
            printf ("LENGTH: %"PRIu8"\n" , tlv.length);
            printf ("VALUE: ");
            for ( cont = 0 ; cont < tlv.length ; cont ++ ) {

                printf (" %"PRIu8"" , tlv.value_8bit[ cont ]);
            }
            printf ("\n\n");
        }
    }
    if ( tlv.tlv_type == ADDRESS_TYPE ) {

        if ( tlv.tlv_flags.bit1 == ONE && tlv.tlv_flags.bit2 == ZERO ) {

            printf ("INDEX_START %"PRIu8"\n" , tlv.index_start);
        }
        if ( tlv.tlv_flags.bit1 == ZERO && tlv.tlv_flags.bit2 == ONE ) {

            printf ("INDEX_START %"PRIu8"\n" , tlv.index_start);
            printf ("INDEX_STOP %"PRIu8"\n" , tlv.index_stop);
        }
        printf ("LENGTH: %"PRIu8"\n" , tlv.length);
        printf ("VALUE:  %"PRIu8"\n" , tlv.value);
    }
}

void printf_packet (T_PgmanPacket packet) {

    uint8_t type = packet.message.msg_header.msg_type;
    int ack = 0;
    int cont;
    if ( packet.message.message_tlv_block.tlvs_length == 2 ) {
        ack = 1;
    }
    printf ("--------PACKET-------------------\n\n");

    printf_pkt_header (packet.pkt_header);
    if ( type == RREP_ACK && ack == 0 ) {
        printf_msg_header (packet.message.msg_header);
        printf ("MESSAGE_TLV_BLOCK_: %u\n" , packet.message.message_tlv_block.tlvs_length);
    }
    if ( type == RREP_ACK && ack == 1 ) {
        printf_msg_header (packet.message.msg_header);
        printf ("MESSAGE_TLV_BLOCK_: %u\n" , packet.message.message_tlv_block.tlvs_length);
        if ( packet.message.message_tlv_block.tlvs_length != 0 ) //RREP_ACK sin reconocimiento
        {
            printf_tlv (packet.message.message_tlv_block.tlvs[ 0 ]);
        }
    }
    if ( type == RREQ || type == RREP ) {
        printf_msg_header (packet.message.msg_header);
        printf ("MESSAGE_TLV_BLOCK_ LENGHT: %"PRIu16"\n" , packet.message.message_tlv_block.tlvs_length);
        printf_address_block (packet.message.address_block);
        printf ("TLV_BLOCK_LENGHT: %"PRIu16"\n" , packet.message.address_tlv_block.tlvs_length);

        for ( cont = 0 ; cont < packet.message.address_tlv_block.overall ; cont ++ ) {
            printf_tlv (packet.message.address_tlv_block.tlvs[ cont ]);
        }
    }
    if ( type == RERR ) {

        printf_msg_header (packet.message.msg_header);
        printf ("MESSAGE_TLV_BLOCK_ LENGHT: %"PRIu16"\n" , packet.message.message_tlv_block.tlvs_length);
        printf_address_block (packet.message.address_block);
        printf ("TLV_BLOCK_LENGHT: %"PRIu16"\n" , packet.message.address_tlv_block.tlvs_length);
        for ( cont = 0 ; cont < packet.message.address_tlv_block.overall ; cont ++ ) {
            printf_tlv (packet.message.address_tlv_block.tlvs[ cont ]);
        }
    }
    printf ("--------------------------------\n\n");
}
 */
/*
void printf_address_block (Pgman_address_block address_block) {

    int cont;
    int mid_length;
    printf ("IPAddress Block: ");
    printf ("(OCTETS: %"PRIu8")\n\n" , address_block.address_block_length);
    printf ("- NUM_ADDR: %"   PRIu8"\n" , address_block.num_addr);
    printf ("- ADDR_FLAGS: ");
    printf (" %u %u %u %u %u RSV: %u %u %u \n" , address_block.addr_flags.bit0 ,
            address_block.addr_flags.bit1 ,
            address_block.addr_flags.bit2 ,
            address_block.addr_flags.bit3 ,
            address_block.addr_flags.bit4 ,
            address_block.addr_flags.bit5 ,
            address_block.addr_flags.bit6 ,
            address_block.addr_flags.bit7);

    if ( address_block.addr_flags.bit0 == ONE ) {

        printf ("- HEAD_LENGTH: %"PRIu8"\n" , address_block.head_length);
        printf ("- HEAD: {");
        for ( cont = 0 ; cont < address_block.head_length ; cont ++ ) {

            printf (" %"PRIu8"" , address_block.head[ cont ]);
        }
        printf (" }\n");
    }
    if (( address_block.addr_flags.bit1 == ONE ) && ( address_block.addr_flags.bit2 == ZERO )) {

        printf ("- TAIL_LENGTH: %"PRIu8"\n" , address_block.tail_length);
        printf ("- TAIL: {");
        for ( cont = 0 ; cont < address_block.tail_length ; cont ++ ) {
            printf (" %"PRIu8"" , address_block.tail[ cont ]);
        }
        printf (" }\n");

    }
    if (( address_block.addr_flags.bit1 == ZERO ) && ( address_block.addr_flags.bit2 == ONE )) {

        printf ("- TAIL_LENGTH: %"PRIu8"\n" , address_block.tail_length);
    }

    mid_length = get_mid_length (address_block.head_length , address_block.tail_length);
    if ( mid_length > 0 ) {
        printf ("- MID: {");
        for ( cont = 0 ; cont < ( address_block.num_addr ) * ( mid_length ) ; cont ++ ) {
            printf (" %"PRIu8"" , address_block.mid[ cont ]);

        }
        printf (" }\n");
    }
    printf ("- PREFIX_LENGTH: {");
    if (( address_block.addr_flags.bit3 == 1 ) && ( address_block.addr_flags.bit4 == 0 )) {

        printf ("%"PRIu8"" , address_block.prefix_length[ 0 ]);
    }
    if (( address_block.addr_flags.bit3 == 0 ) && ( address_block.addr_flags.bit4 == 1 )) {

        for ( cont = 0 ; cont < address_block.num_addr ; cont ++ ) {

            printf (" %"PRIu8"" , address_block.prefix_length[ cont ]);
        }
    }
    printf ("}\n\n");
}
 */

/*
void printf_msg_header (Pgman_msg_header msg_header) {

    printf ("Msg-Header:  (OCTETS: %"PRIu8")\n\n" , msg_header.msg_header_length);
    printf ("- MSG_TYPE: %d\n" , msg_header.msg_type);
    printf ("- MSG_FLAGS: %u %u %u %u\n" , msg_header.msg_flags.bit0 ,
            msg_header.msg_flags.bit1 ,
            msg_header.msg_flags.bit2 ,
            msg_header.msg_flags.bit3);
    printf ("- MSG_ADDR_LENGTH: %"PRIu8"\n" , msg_header.msg_addr_length);
    printf ("- MSG_SIZE: %"PRIu16"\n\n" , msg_header.msg_size);
    if ( msg_header.msg_flags.bit1 == ONE ) {

        printf ("- MSG_HOP_LIMIT: %"PRIu8"\n\n" , msg_header.MsgHopLimit);
    }

}
*/

/*
void printf_pkt_header (Pgman_pkt_header pkt_header) {

    printf ("Pkt-Header: (OCTETS: %"PRIu8")\n\n" , pkt_header.pkt_header_length);
    printf ("- VERSION: %u\n" , pkt_header.version);
    printf ("- PKT_FLAGS: %u %u %u %u\n\n" , pkt_header.pkt_flags.bit0 ,
            pkt_header.pkt_flags.bit1 ,
            pkt_header.pkt_flags.bit2 ,
            pkt_header.pkt_flags.bit3);

}
*/