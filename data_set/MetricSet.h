#ifndef METRIC_SET_H
#define METRIC_SET_H

#include <stdint.h>
#include <stdbool.h>

typedef struct{

	uint8_t MetricType;
	uint8_t Cost_L;
	uint8_t MaxMetric;
}T_MetricSet;

void ini_MetricSet(T_MetricSet *MetricSet);
uint8_t get_MetricSet_MetricType(T_MetricSet * MetricSet);
uint8_t get_MetricSet_CostL(T_MetricSet *MetricSet);
uint8_t get_MetricSet_MaxMetric(T_MetricSet *MetricSet);
uint8_t calculate_incoming_link_cost(uint8_t metric_incoming, T_MetricSet *MetricSet);
int check_maximum_metric_value(uint8_t metric_advertised,T_MetricSet *MetricSet);
int compare_cost(uint8_t local_cost, uint8_t advrmsg_cost);
int check_MetricType(T_MetricSet *MetricSet,uint8_t MetricType);
bool LoopFree(int R1, int R2);
#endif