#include <stdint.h>
#include "SequenceNumberSet.h"

uint16_t get_SeqNumSet(T_SeqNumSet *SeqNum){

	return SeqNum->SeqNum;
}
void ini_SeqNumSet(T_SeqNumSet *SeqNumSet){
		
	SeqNumSet->SeqNum = 1;
}
int16_t compare_SeqNum(uint16_t SeqNumSet, uint16_t SeqNum){

	return (int16_t)(SeqNum - SeqNumSet);
}
void increment_SeqNumSet(T_SeqNumSet *SeqNumSet){

	if(SeqNumSet->SeqNum == 65535){

		SeqNumSet->SeqNum = 1;
	}else{

		SeqNumSet->SeqNum = (uint16_t) (SeqNumSet->SeqNum + 1);
	}

}

