
#include "MulticastRouteMessageSet.h"
#include <stdio.h>
#include "../event_definition.h"
#include "../constant_definition.h"
#include "../global_structs.h"

void ini_McMsgSet(T_McMsgSet *McMsgSet){

    McMsgSet->overall = 0;
}
int decrease_McMsgSet_Overall(T_McMsgSet * McMsgSet){

    if(get_McMsgSet_Overall(McMsgSet) == 0){

        return FAIL_OPERATION;
    }else {

        McMsgSet->overall--;
    }
}
int increase_McMsgSet_Overall(T_McMsgSet * McMsgSet){

    if(get_McMsgSet_Overall(McMsgSet) == MAXMULTICASTROUTE){

        return FAIL_OPERATION;
    }else {

        McMsgSet->overall++;
    }
}
int update_McMsgSet_McMsg_Position(T_McMsgSet *McMsgSet, T_McMsg *McMsg){

    if(0 < get_McMsg_Position(McMsg) < get_McMsgSet_Overall(McMsgSet)){

        McMsgSet->McMsg[get_McMsg_Position(McMsg)] = *McMsg;
        return CORRECT_OPERATION;
    } else {

        return FAIL_OPERATION;
    }
}
int get_McMsgSet_McMsg_Index(T_McMsgSet *McMsgSet, T_McMsg *McMsg, int index){

    if(index != McMsgSet->McMsg[index].Position){

        return FAIL_OPERATION;
    }
    *McMsg = McMsgSet->McMsg[index];
    return CORRECT_OPERATION;
}
int get_McMsgSet_Overall(T_McMsgSet *McMsgSet){

    return McMsgSet->overall;
}
int add_McMsgSet_McMsg(T_McMsgSet *McMsgSet, T_McMsg *McMsg){

    int overall = get_McMsgSet_Overall(McMsgSet);
    if(overall < MAXMULTICASTROUTE){

        add_McMsg_Position(McMsg,overall);
        McMsgSet->McMsg[overall] = *McMsg;
        increase_McMsgSet_Overall(McMsgSet);
        return CORRECT_OPERATION;
    }else {

        return FULL_ARRAY;
    }
}
int del_McMsgSet_McMsg(T_McMsgSet *McMsgSet, T_McMsg *McMsg){

    int i = 0;
    int a = 0;
    int err;
    T_McMsg update_McMsg;
    if(get_McMsgSet_Overall(McMsgSet) == MIN){

        return EMPTY_ARRAY;
    }
    while(i < get_McMsgSet_Overall(McMsgSet)){

        err = get_McMsgSet_McMsg_Index(McMsgSet,&update_McMsg,i);
        if(err != CORRECT_OPERATION){
            return err;
        }
        if(get_McMsg_OrigPrefix(&update_McMsg) == get_McMsg_OrigPrefix(McMsg)){

            a = i;
            while(a < get_McMsgSet_Overall(McMsgSet)) {

                McMsgSet->McMsg[a] =  McMsgSet->McMsg[a + 1];
                McMsgSet->McMsg[a].Position = a;
                a++;
            }
            decrease_McMsgSet_Overall(McMsgSet);
            return CORRECT_OPERATION;
        }else {

            i++;
        }
    }
    decrease_McMsgSet_Overall(McMsgSet);
    return NOTFOUND_ITEM;
}
void print_McMsgSet(T_McMsgSet *McMsgSet){

    T_McMsg McMsg;
    int err;

    printf(" ----------------------\n");
    printf("|       McMsgSet       |\n");
    printf("|----------------------|\n");
    for(int i = 0; i < get_McMsgSet_Overall(McMsgSet); i++){
        err = get_McMsgSet_McMsg_Index(McMsgSet,&McMsg,i);

        printf("|Entrada: %d           |\n",i);
        printf("|----------------------|\n");
        print_McMsg(&McMsg);
    }
}


