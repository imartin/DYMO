#ifndef SEQUENCE_NUMBER_SET_H
#define SEQUENCE_NUMBER_SET_H

#include <stdint.h>

typedef struct{

	uint16_t SeqNum;
}T_SeqNumSet;

uint16_t get_SeqNumSet(T_SeqNumSet *SeqNum);
void ini_SeqNumSet(T_SeqNumSet *SeqNumSet);
int16_t compare_SeqNum(uint16_t SeqNumSet, uint16_t SeqNum);
void increment_SeqNumSet(T_SeqNumSet *SeqNumSet);
#endif