#include "InterfaceSet.h"
#include <stdio.h>
#include <string.h>
#include <ifaddrs.h>
#include <curses.h>
#include "../event_definition.h"
#include "../global_structs.h"

int ini_InterfaceSet(T_InterfaceSet *InterfaceSet,bool FLAG_AUTOCONFIGURATION) {

    InterfaceSet->overall = 0;

    if (FLAG_AUTOCONFIGURATION == TRUE) {


        T_Interface update_Interface = {0};
        struct ifaddrs *ifaddr;
        struct ifaddrs *ifa;
        struct sockaddr_in *sa;
        char *protocol = NULL;
        if (getifaddrs(&ifaddr) == (-1)) {

            perror("getifaddrs");
            return FAIL_OPERATION;
        }
        for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {

            if (ifa->ifa_addr == NULL || ifa->ifa_addr->sa_family != AF_PACKET) continue;
            if ((strcmp(ifa->ifa_name, "lo") != 0) && (strcmp(ifa->ifa_name, "eth0") != 0)) {

                /* update_Interface.InterfaceId = ifa->ifa_name;
                 InterfaceSet->update_Interface[InterfaceSet->overall] = update_Interface;  ACtivar para redes cableadas
                 InterfaceSet->overall= InterfaceSet->overall +1;
                   */
                if (check_WirelessInterface(ifa->ifa_name, protocol)) {

                    sa = (struct sockaddr_in *) ifa->ifa_addr;
                    strcpy(update_Interface.InterfaceId, ifa->ifa_name);
                    InterfaceSet->Interface[InterfaceSet->overall] = update_Interface;
                    InterfaceSet->overall = InterfaceSet->overall + 1;
                } else {

                    strcpy(update_Interface.InterfaceId, ifa->ifa_name);
                    InterfaceSet->Interface[InterfaceSet->overall] = update_Interface;
                    InterfaceSet->overall = InterfaceSet->overall + 1;
                }
            }
        }
        freeifaddrs(ifaddr);
        return CORRECT_OPERATION;
    }
    return CORRECT_OPERATION;
}

void print_InterfaceSet(T_InterfaceSet *InterfaceSet){

    T_Interface Interface;
    int err;
  	printf(" --------------------------\n");
    printf("|        INTERFACE SET     |\n");
    printf(" --------------------------\n");
    for(int i = 0;i < get_InterfaceSet_Overall(InterfaceSet); i++){

        err = get_InterfaceSet_Interface_Index(InterfaceSet, &Interface,i);
        if( err != CORRECT_OPERATION ) {

            return;
        }
        printf("|Entrada: %d           |\n",i);
        printf("|----------------------|\n");
        print_Interface(&Interface);
    }
}


int add_InterfaceSet_Interface(T_InterfaceSet *InterfaceSet, T_Interface *Interface){

    int overall =  get_InterfaceSet_Overall(InterfaceSet);
    if(overall < MAXINTERFACE){

        return FAIL_OPERATION;
    }
    add_InterfaceSet_Overall(InterfaceSet,overall);
    InterfaceSet->Interface[overall] = *Interface;
    increase_InterfaceSet_Overall(InterfaceSet);
    return CORRECT_OPERATION;
}


int get_InterfaceSet_Overall(T_InterfaceSet *InterfaceSet){

    return  InterfaceSet->overall;
}
void add_InterfaceSet_Overall(T_InterfaceSet *InterfaceSet, int overall){

    InterfaceSet->overall = overall;
}
int get_InterfaceSet_Interface_Index(T_InterfaceSet *InterfaceSet, T_Interface *Interface, int index){

    if(index < 0  || index >= get_InterfaceSet_Overall(InterfaceSet)){

        return FAIL_OPERATION;
    }
    *Interface =  InterfaceSet->Interface[index];
    return CORRECT_OPERATION;
}
int del_InterfaceSet_Interface(T_InterfaceSet *InterfaceSet, T_Interface *Interface){

    int a=0,i=0;
    int err;
    T_Interface update_Interface;
    if(get_InterfaceSet_Overall(InterfaceSet) == MIN){

        return EMPTY_ARRAY;
    }
    while(i < get_InterfaceSet_Overall(InterfaceSet)){

        err = get_InterfaceSet_Interface_Index(InterfaceSet, &update_Interface, i);
        if(err != CORRECT_OPERATION){

            printf("get_RerrMsgSet_RerrMsg_Index FAIL\n");
        }
        if(strcmp(get_Interface_InterfaceId(&update_Interface), get_Interface_InterfaceId(Interface)) == 0){

            a = i;
            while(a < get_InterfaceSet_Overall(InterfaceSet)) {

                InterfaceSet->Interface[a] =  InterfaceSet->Interface[a + 1];
                InterfaceSet->Interface[a].Position = a;
                a++;
            }
            decrease_InterfaceSet_Overall(InterfaceSet);
            return CORRECT_OPERATION;
        }else {

            i++;
        }
    }
    return NOTFOUND_ITEM;
}




void decrease_InterfaceSet_Overall(T_InterfaceSet * InterfaceSet){

    add_InterfaceSet_Overall(InterfaceSet,get_InterfaceSet_Overall(InterfaceSet) - 1);
}
void increase_InterfaceSet_Overall(T_InterfaceSet * InterfaceSet){

    add_InterfaceSet_Overall(InterfaceSet,get_InterfaceSet_Overall(InterfaceSet) + 1);
}