#ifndef ROUTE_ERROR_SET_H
#define ROUTE_ERROR_SET_H



#include "../data/RouteError.h"


#define MAXROUTEERROR 20

typedef struct{

    T_RerrMsg RerrMsg[MAXROUTEERROR];
    int overall;
}T_RerrMsgSet;

void ini_RerrMsgSet(T_RerrMsgSet *RerrMsgSet);
void decrease_RerrMsgSet_Overall(T_RerrMsgSet *RerrMsgSet);
void increase_RerrMsgSet_Overall(T_RerrMsgSet *RerrMsgSet);
int add_RerrMsgSet_RerrMsg(T_RerrMsgSet *RerrMsgSet, T_RerrMsg *RerrMsg);
int del_RerrMsgSet_RerrMsg(T_RerrMsgSet *RerrMsgSet,T_RerrMsg *RerrMsg);
void add_RerrMsgSet_Overall(T_RerrMsgSet *RerrMsgSet, int overall);
int get_RerrMsgSet_Overall(T_RerrMsgSet *RerrMsgSet);
int get_RerrMsgSet_RerrMsg_Index(T_RerrMsgSet *RerrMsgSet, T_RerrMsg *RerrMsg, int Position);
int get_RerrMsgSet_RerrMsg_UnreachableAddress(T_RerrMsgSet *RerrMsgSet,T_RerrMsg *RerrMsg, uint32_t UnreachableAddress);

void print_RerrMsgSet(T_RerrMsgSet *RerrMsgSet);
#endif
