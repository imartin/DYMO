#include "RouteErrorSet.h"
#include <stdio.h>
#include <data/RouteError.h>
#include "../global_structs.h"
#include "../constant_definition.h"
#include "../event_definition.h"

void ini_RerrMsgSet(T_RerrMsgSet *RerrMsgSet){

	add_RerrMsgSet_Overall(RerrMsgSet,0);
}
void decrease_RerrMsgSet_Overall(T_RerrMsgSet *RerrMsgSet){

	add_RerrMsgSet_Overall(RerrMsgSet,get_RerrMsgSet_Overall(RerrMsgSet) - 1);
}
void increase_RerrMsgSet_Overall(T_RerrMsgSet *RerrMsgSet){

	add_RerrMsgSet_Overall(RerrMsgSet,get_RerrMsgSet_Overall(RerrMsgSet) + 1);
}
int del_RerrMsgSet_RerrMsg(T_RerrMsgSet *RerrMsgSet,T_RerrMsg *RerrMsg){

	int i = 0;
	int a = 0;
	int err;
	T_RerrMsg m_RerrMsg;
	if(get_RerrMsgSet_Overall(RerrMsgSet) == MIN){

		printf("get_RerrMsgSet_Overall EMPTY ARRAY\n");
		return FAIL_OPERATION;
	}
	while(i < get_RerrMsgSet_Overall(RerrMsgSet)){

		err = get_RerrMsgSet_RerrMsg_Index(RerrMsgSet, &m_RerrMsg, i);
		if(err != CORRECT_OPERATION){

			printf("get_RerrMsgSet_RerrMsg_Index FAIL\n");
			return FAIL_OPERATION;
		}
		if(get_RerrMsg_UnreachableAddress(&m_RerrMsg) == get_RerrMsg_UnreachableAddress(RerrMsg) &&
		(get_RerrMsg_PktSource(&m_RerrMsg) == get_RerrMsg_PktSource(RerrMsg))){

			a = i;
			while(a < get_RerrMsgSet_Overall(RerrMsgSet)) {

				err = get_RerrMsgSet_RerrMsg_Index(RerrMsgSet, &m_RerrMsg, a + 1);
				if(err != CORRECT_OPERATION){

					printf("get_RerrMsgSet_RerrMsg_Index FAIL\n");
					return FAIL_OPERATION;
				}
				add_RerrMsg_Position(&m_RerrMsg,a);
				RerrMsgSet->RerrMsg[a] = m_RerrMsg;
				a++;
                printf("Deleted RerrMsg\n");
			}
			decrease_RerrMsgSet_Overall(RerrMsgSet);
            return CORRECT_OPERATION;
        }else {

			i++;
		}
	}
    return NOTFOUND_ITEM;
}
int add_RerrMsgSet_RerrMsg(T_RerrMsgSet *RerrMsgSet, T_RerrMsg *RerrMsg){
	
	if(get_RerrMsgSet_Overall(RerrMsgSet) < MAXROUTEERROR) {

		add_RerrMsg_Position(RerrMsg, get_RerrMsgSet_Overall(RerrMsgSet));
		RerrMsgSet->RerrMsg[get_RerrMsgSet_Overall(RerrMsgSet)] = *RerrMsg;
		increase_RerrMsgSet_Overall(RerrMsgSet);
		return CORRECT_OPERATION;
	}
	return FULL_ARRAY;
}
int get_RerrMsgSet_RerrMsg_UnreachableAddress(T_RerrMsgSet *RerrMsgSet,T_RerrMsg *RerrMsg, uint32_t UnreachableAddress){

	int err;
	for(int i = 0; i < get_RerrMsgSet_Overall(RerrMsgSet); i++){

		err = get_RerrMsgSet_RerrMsg_Index(RerrMsgSet, RerrMsg, i);
		if(get_RerrMsg_UnreachableAddress(RerrMsg) == UnreachableAddress){

			return err == CORRECT_OPERATION ? CORRECT_OPERATION : err;
		}
	}
	return NOTFOUND_ITEM;
}
void add_RerrMsgSet_Overall(T_RerrMsgSet *RerrMsgSet, int overall){

	RerrMsgSet->overall = overall;
}
int get_RerrMsgSet_Overall(T_RerrMsgSet *RerrMsgSet){

	return RerrMsgSet->overall;
}
int get_RerrMsgSet_RerrMsg_Index(T_RerrMsgSet *RerrMsgSet, T_RerrMsg *RerrMsg, int Position){

	if(get_RerrMsgSet_Overall(RerrMsgSet) == 0){

		return EMPTY_ARRAY;
	}
	*RerrMsg = RerrMsgSet->RerrMsg[Position];
	return CORRECT_OPERATION;
}


void print_RerrMsgSet(T_RerrMsgSet *RerrMsgSet){

    T_RerrMsg RerrMsg;
    int err;
    printf(" ---------------------------\n");
    printf("|    ROUTER ERROR SET       |\n");
    printf(" ---------------------------\n");
    for(int i = 0; i < get_RerrMsgSet_Overall(RerrMsgSet); i++) {

        err = get_RerrMsgSet_RerrMsg_Index(RerrMsgSet,&RerrMsg,i);
        printf("|Entrada: %d           |\n",i);
        printf("|----------------------|\n");
        print_RerrMsg(&RerrMsg);
    }
}