#include "NeighborSet.h"
#include <stdio.h>
#include <data/Neighbor.h>
#include "../event_definition.h"
#include "../global_structs.h"




int get_NeighborSet_Overall(T_NeighborSet *NeighborSet){

    return NeighborSet->overall;
}
int get_NeighborSet_Neighbor_Index(T_NeighborSet *NeighborSet, T_Neighbor *Neighbor, int Position){

    if(get_NeighborSet_Overall(NeighborSet) == 0){

        return EMPTY_ARRAY;
    }
    *Neighbor = NeighborSet->Neighbor[Position];
    return CORRECT_OPERATION;
}
int get_NeighborSet_Neighbor_Address(T_NeighborSet * NeighborSet,T_Neighbor * Neighbor, uint32_t Address){

    int err;
    for(int i = 0; i < get_NeighborSet_Overall(NeighborSet); i++){

        err = get_NeighborSet_Neighbor_Index(NeighborSet, Neighbor, i);
        if(err != CORRECT_OPERATION) {
            return err;
        }
        if(get_Neighbor_IPAddress(Neighbor) == Address){

            return CORRECT_OPERATION;
        }
    }
    return NOTFOUND_ITEM;
}

void increase_NeihborSet_Overall(T_NeighborSet * NeighborSet){

    add_NeihborSet_Overall(NeighborSet,get_NeighborSet_Overall(NeighborSet) + 1);
}
void decrease_NeihborSet_Overall(T_NeighborSet * NeighborSet){

    add_NeihborSet_Overall(NeighborSet,get_NeighborSet_Overall(NeighborSet) - 1);
}
void ini_NeighborSet(T_NeighborSet *NeighborSet){

    add_NeihborSet_Overall(NeighborSet,0);
}

int add_NeighborSet_Neighbor(T_NeighborSet *NeighborSet,T_Neighbor new_Neighbor){

    if(get_NeighborSet_Overall(NeighborSet) < MAXNeighbor) {

        add_Neighbor_Position(&new_Neighbor,get_NeighborSet_Overall(NeighborSet));
        NeighborSet->Neighbor[get_NeighborSet_Overall(NeighborSet)] = new_Neighbor;
        increase_NeihborSet_Overall(NeighborSet);
        return CORRECT_OPERATION;
    }
    return FULL_ARRAY;
}
void add_NeihborSet_Overall(T_NeighborSet * NeighborSet, int overall){

    NeighborSet->overall = overall;
}

void update_NeighborSet_Neighbor(T_NeighborSet * NeighborSet, T_Neighbor * Neighbor){

    NeighborSet->Neighbor[get_Neighbor_Position(Neighbor)] = *Neighbor;
}

int del_NeighborSet_Neighbor(T_NeighborSet *NeighborSet, uint32_t Address){

    int i = 0;
    int a = 0;
    int err;
    T_Neighbor Neighbor;
    if(get_NeighborSet_Overall(NeighborSet) == MIN){

        return NOTFOUND_ITEM;
    } else{

        while(i < get_NeighborSet_Overall(NeighborSet)){

            err = get_NeighborSet_Neighbor_Index(NeighborSet, &Neighbor, i);
            if(err != CORRECT_OPERATION){
                return err;
            }
            if(get_Neighbor_IPAddress(&Neighbor) == Address){

                a = i;
                while(a < get_NeighborSet_Overall(NeighborSet)) {

                    NeighborSet->Neighbor[a]= NeighborSet->Neighbor[a + 1];
                    NeighborSet->Neighbor[a].Position = a;
                    a++;
                }
                decrease_NeihborSet_Overall(NeighborSet);
                return CORRECT_OPERATION;

            }else {

                i++;
            }
        }
    }
    return NOTFOUND_ITEM;
}

void print_NeighborSet(T_NeighborSet *NeighborSet){

    int err;
    T_Neighbor Neighbor;
    printf(" ----------------------\n");
    printf("|     Neighbor Set     |\n");
    printf(" ----------------------\n");
    for(int i = 0;i < get_NeighborSet_Overall(NeighborSet); i++){

        err = get_NeighborSet_Neighbor_Index(NeighborSet,&Neighbor,i);
        if(err != CORRECT_OPERATION){

            return;
        }
        printf("|Entrada: %d           |\n",i);
        printf("|----------------------|\n");
        print_Neighbor(&Neighbor);
    }
}


