#ifndef ROUTER_CLIENT_SET_H
#define ROUTER_CLIENT_SET_H

#include "../data/RouterClient.h"
#include "InterfaceSet.h"


#define MAXCLIENTROUTER 10

typedef struct{

	int overall;
	T_RouterClient RouterClient[MAXCLIENTROUTER];
}T_RouterClientSet;

void ini_RouterClientSet(T_RouterClientSet *RouterClientSet);
void decrease_RouterClientSet_Overall(T_RouterClientSet *RouterClientSet);
void increase_RouterClientSet_Overall(T_RouterClientSet *RouterClientSet);

void add_RouterClientSet_Overall(T_RouterClientSet *RouterClientSet, int overall);
int add_RouterClientSet_RouterClient(T_RouterClientSet *RouterClientSet,T_RouterClient *RouterClient);

int get_RouterClientSet_Overall(T_RouterClientSet *RouterClientSet);
int get_RouterClientSet_RouterClient_Index(T_RouterClientSet *RouterClientSet, T_RouterClient *RouterClient,
										   int Position);

int del_RouterClientSet_RouterClient(T_RouterClientSet *RouterClientSet, uint32_t Address);

int matching_address_RouterClientSet(T_RouterClient *RouterClient, T_RouterClientSet *pointer_RouterClientSet,uint32_t Address);
int configuracion_automatica_RouterClientSet(T_InterfaceSet *InterfaceSet,T_RouterClientSet *RouterClientSet);

void print_RouterClientSet(T_RouterClientSet *RouterClientSet);
#endif	