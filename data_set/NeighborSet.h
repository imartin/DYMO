#ifndef Neighbor_SET_H
#define Neighbor_SET_H

#include <stdint.h>
#include "../data/Neighbor.h"

#define MAXNeighbor 10

typedef struct{

	int overall;
	T_Neighbor Neighbor[MAXNeighbor];
}T_NeighborSet;

int get_NeighborSet_Overall(T_NeighborSet *NeighborSet);
int get_NeighborSet_Neighbor_Index(T_NeighborSet *NeighborSet, T_Neighbor *Neighbor, int Position);
int get_NeighborSet_Neighbor_Address(T_NeighborSet * NeighborSet,T_Neighbor * Neighbor, uint32_t Address);

void decrease_NeihborSet_Overall(T_NeighborSet * NeighborSet);
void increase_NeihborSet_Overall(T_NeighborSet * NeighborSet);
void ini_NeighborSet(T_NeighborSet *NeighborSet); //Inicializa neighor set a 0

void update_NeighborSet_Neighbor(T_NeighborSet * NeighborSet, T_Neighbor * Neighbor);

int  add_NeighborSet_Neighbor(T_NeighborSet *NeighborSet,T_Neighbor Neighbor);//Anade un router vecino a la NeighborSet
void add_NeihborSet_Overall(T_NeighborSet * NeighborSet, int overall);

int del_NeighborSet_Neighbor(T_NeighborSet *NeighborSet, uint32_t Address); //Elimina un router vecino a la NeighborSet

void print_NeighborSet(T_NeighborSet *NeighborSet);



#endif	