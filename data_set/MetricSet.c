#include "MetricSet.h"
#include <curses.h>
#include "../constant_definition.h"
#include "../event_definition.h"

void ini_MetricSet(T_MetricSet *MetricSet){

	MetricSet->MetricType = HOP_COUNT;
	MetricSet->Cost_L = 1;
	MetricSet->MaxMetric = MAX_METRIC_HopCount;
}
uint8_t get_MetricSet_MetricType(T_MetricSet * MetricSet){

	return MetricSet->MetricType;
}
uint8_t get_MetricSet_CostL(T_MetricSet *MetricSet){

	return MetricSet->Cost_L;
}
uint8_t get_MetricSet_MaxMetric(T_MetricSet *MetricSet){

	return MetricSet->MaxMetric;
}
uint8_t calculate_incoming_link_cost(uint8_t metric_incoming, T_MetricSet *MetricSet){

	return (metric_incoming + get_MetricSet_CostL(MetricSet));
}
int check_maximum_metric_value(uint8_t metric_advertised,T_MetricSet *MetricSet) {

	return metric_advertised <= (get_MetricSet_MaxMetric(MetricSet) - get_MetricSet_CostL(MetricSet)) ? TRUE : FALSE;
}
int compare_cost(uint8_t local_cost, uint8_t advrmsg_cost){

	if(local_cost > advrmsg_cost)
		return UPDATE;   //

	else if(local_cost < advrmsg_cost)
		return NOT_UPDATE;

	else
		return EQUAL;
}
int check_MetricType(T_MetricSet *MetricSet,const uint8_t MetricType){

	return MetricType == get_MetricSet_MetricType(MetricSet) ? TRUE : FALSE;
}
bool LoopFree(int R1, int R2){

	if ( R1 <= R2 ) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}