#include "LocalRouteSet.h"
#include <stdio.h>
#include <data/LocalRoute.h>
#include "../event_definition.h"
#include "../global_structs.h"


int get_LocalRouteSet_Overall(T_LocalRouteSet *LocalRouteSet){

    return LocalRouteSet->overall;
}
int get_LocalRouteSet_LocalRoute_Index(T_LocalRouteSet *LocalRouteSet, T_LocalRoute *LocalRoute, int position){
    if(get_LocalRouteSet_Overall(LocalRouteSet) == 0){

        return EMPTY_ARRAY;
    }
    if(get_LocalRoute_Position(&LocalRouteSet->LocalRoute[position]) != position){

        return FAIL_OPERATION;
    }
    *LocalRoute = LocalRouteSet->LocalRoute[position];
    return CORRECT_OPERATION;
}

void ini_LocalRouteSet(T_LocalRouteSet *LocalRouteSet){

    add_LocalRouteSet_Overall(LocalRouteSet,0);
}
void decrease_LocalRouteSet_Overall(T_LocalRouteSet * LocalRouteSet){

    add_LocalRouteSet_Overall(LocalRouteSet,get_LocalRouteSet_Overall(LocalRouteSet) - 1);
}
void increase_LocalRouteSet_Overall(T_LocalRouteSet * LocalRouteSet){

    add_LocalRouteSet_Overall(LocalRouteSet,get_LocalRouteSet_Overall(LocalRouteSet) + 1);
}

void update_LocalRouteSet_LocalRoute(T_LocalRouteSet *LocalRouteSet, T_LocalRoute * LocalRoute){

    LocalRouteSet->LocalRoute[get_LocalRoute_Position(LocalRoute)] = *LocalRoute;
}

void add_LocalRouteSet_Overall(T_LocalRouteSet * LocalRouteSet, int overall){

    LocalRouteSet->overall = overall;
}
int add_LocalRouteSet_LocalRoute(T_LocalRouteSet * LocalRouteSet,T_LocalRoute * LocalRoute){

    if(get_LocalRouteSet_Overall(LocalRouteSet) < MAXLOCALROUTESET) {

        add_LocalRoute_Position(LocalRoute,get_LocalRouteSet_Overall(LocalRouteSet));
        LocalRouteSet->LocalRoute[get_LocalRouteSet_Overall(LocalRouteSet)] = *LocalRoute;
        increase_LocalRouteSet_Overall(LocalRouteSet);
        return CORRECT_OPERATION;
    }
    return FULL_ARRAY;
}

int  del_LocalRouteSet_LocalRoute_Position(T_LocalRouteSet  * LocalRouteSet, T_LocalRoute * LocalRoute){

    int a=0,i=0;
    int err;
    T_LocalRoute update_LocalRoute;
    if(get_LocalRouteSet_Overall(LocalRouteSet) == MIN){
        printf ("LocalRouteSet Empty Array");
        return FAIL_OPERATION;
    }
    while(i < get_LocalRouteSet_Overall(LocalRouteSet)){

        err = get_LocalRouteSet_LocalRoute_Index(LocalRouteSet, &update_LocalRoute, i);
        if(err != CORRECT_OPERATION){

            printf("get_RerrMsgSet_RerrMsg_Index FAIL\n");
        }
        if(get_LocalRoute_Address(&update_LocalRoute) == get_LocalRoute_Address(LocalRoute) &&
          get_LocalRoute_Prefixlength(&update_LocalRoute) == get_LocalRoute_Prefixlength(LocalRoute) &&
          get_LocalRoute_Metrictype(&update_LocalRoute) == get_LocalRoute_Metrictype(LocalRoute) &&
          get_LocalRoute_Seqnum(&update_LocalRoute) == get_LocalRoute_Seqnum(LocalRoute) &&
          get_LocalRoute_Metric(&update_LocalRoute) == get_LocalRoute_Metric(LocalRoute)){

            a = i;
            while(a < get_LocalRouteSet_Overall(LocalRouteSet)) {

                LocalRouteSet->LocalRoute[a] =  LocalRouteSet->LocalRoute[a + 1];
                LocalRouteSet->LocalRoute[a].Position = a;
                a++;
            }
            decrease_LocalRouteSet_Overall(LocalRouteSet);
            return CORRECT_OPERATION;
        }else {

            i++;
        }
    }
    return NOTFOUND_ITEM;
}

int check_LocalRouteSet_LocalRoute_State(T_LocalRouteSet *LocalRouteSet, uint32_t OrigPrefix){

    int err;
    T_LocalRoute localRoute;
    for(int i = 0 ;i < get_LocalRouteSet_Overall(LocalRouteSet); i ++){

        err = get_LocalRouteSet_LocalRoute_Index(LocalRouteSet, &localRoute, i);
        if(err != CORRECT_OPERATION) {

            printf("check_LocalRouteSet_LocalRoute_State : get_LocalRouteSet_LocalRoute_Index FAIL\n");
            return FAIL_OPERATION;
        }
        printf("LOCALROUTE ADDRESS:%s\n",Int32_To_String (localRoute.Address));

        if(get_LocalRoute_Address(&localRoute) == OrigPrefix){


            if(get_LocalRoute_State (&localRoute) != INVALID){

                return FOUND_ITEM;
            }
        }
    }
    return NOTFOUND_ITEM;
}
int best_route_to_origprefix(T_LocalRouteSet *LocalRouteSet, uint32_t origprefix,T_LocalRoute *best_route){

    int p = 0;
    int found = 0;
    T_LocalRoute temp;
    for(int i = 0; i< LocalRouteSet->overall; i++){

        if((LocalRouteSet->LocalRoute[i].Address == origprefix)&&
           (LocalRouteSet->LocalRoute[i].State != INVALID)){

            found = 1;
            temp = LocalRouteSet->LocalRoute[i];
            if(p == 0){

                *best_route = temp;
                p = 1;
            }
            else{

                if(temp.Metric < best_route->Metric){

                    *best_route = temp;
                }
            }
        }
    }
    if(found == 0){
        return NOTFOUND_ITEM;
    }
    return  CORRECT_OPERATION;
}
void print_LocalRouteSet(T_LocalRouteSet *LocalRouteSet){

    int err;
    T_LocalRoute LocalRoute;
    printf(" ----------------------\n");
    printf("|     LocalRouteSet    |\n");
    printf(" ----------------------\n");
    for(int i = 0; i < get_LocalRouteSet_Overall(LocalRouteSet); i++) {

        err = get_LocalRouteSet_LocalRoute_Index(LocalRouteSet, &LocalRoute, i);
        printf("|Entrada: %d           |\n",i);
        printf("|----------------------|\n");
        print_LocalRoute(&LocalRoute);
    }
}

int get_LocalRouteSet_Index_LocalRoute(T_LocalRouteSet *LocalRouteSet,T_LocalRoute *LocalRoute){

    int err;
    T_LocalRoute matching_route;
    for(int i = 0; i < get_LocalRouteSet_Overall(LocalRouteSet); i++ ){

        err = get_LocalRouteSet_LocalRoute_Index(LocalRouteSet, &matching_route, i);
        if(get_LocalRoute_Address(&matching_route) == get_LocalRoute_Address(LocalRoute)){

            if(get_LocalRoute_Metrictype(&matching_route) == get_LocalRoute_Metrictype(LocalRoute)){

                if(get_LocalRoute_Seqnum(&matching_route) == get_LocalRoute_Seqnum(LocalRoute)){

                    if(get_LocalRoute_Nexthop(&matching_route) == get_LocalRoute_Nexthop(LocalRoute)){

                        if(get_LocalRoute_Prefixlength(&matching_route) == get_LocalRoute_Prefixlength(LocalRoute)){

                            if (err == CORRECT_OPERATION) {

                                return i;
                            }
                        }
                    }
                }
            }
        }
    }
    return NOTFOUND_ITEM;
}
int get_LocalRouteSet_LocalRoute_Address(T_LocalRouteSet *LocalRouteSet,T_LocalRoute *return_route,uint32_t address) {

    int err;
    for (int i = 0; i < get_LocalRouteSet_Overall(LocalRouteSet); i++) {

        err = get_LocalRouteSet_LocalRoute_Index(LocalRouteSet, return_route, i);
        if(err != CORRECT_OPERATION){

            return FAIL_OPERATION;
        }

        if (get_LocalRoute_Address(return_route) == address) {

            if (get_LocalRoute_State(return_route) == IDLE || get_LocalRoute_State(return_route) == ACTIVE) {

                return FOUND_ITEM;
            }
        }
    }
    return NOTFOUND_ITEM;
}