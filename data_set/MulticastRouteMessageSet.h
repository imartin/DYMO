#ifndef MULTICAST_ROUTE_MESSAGE_SET_H
#define MULTICAST_ROUTE_MESSAGE_SET_H

#include <stdint.h>
#include "../data/MulicastRouteMessage.h"

#define MAXMULTICASTROUTE 20

typedef struct{

	T_McMsg McMsg[MAXMULTICASTROUTE];
	int overall;
}T_McMsgSet;

void ini_McMsgSet(T_McMsgSet *McMsgSet);
int decrease_McMsgSet_Overall(T_McMsgSet * McMsgSet);
int increase_McMsgSet_Overall(T_McMsgSet * McMsgSet);
int update_McMsgSet_McMsg_Position(T_McMsgSet *McMsgSet, T_McMsg *McMsg);
int get_McMsgSet_McMsg_Index(T_McMsgSet *McMsgSet, T_McMsg *McMsg, int index);
int get_McMsgSet_Overall(T_McMsgSet *McMsgSet);
int add_McMsgSet_McMsg(T_McMsgSet *McMsgSet, T_McMsg *McMsg);
int del_McMsgSet_McMsg(T_McMsgSet *McMsgSet, T_McMsg *McMsg);
void print_McMsgSet(T_McMsgSet *McMsgSet);


#endif