#ifndef INTERFACE_SET_H
#define INTERFACE_SET_H

#include <stdint.h>
#include <stdbool.h>
#include "../data/Interface.h"
#include "../constant_definition.h"

#define MAXINTERFACE 5

typedef struct{

	T_Interface Interface[MAXINTERFACE];
    int overall;
}T_InterfaceSet;

int ini_InterfaceSet(T_InterfaceSet *InterfaceSet,bool FLAG_AUTOCONFIGURATION);
int add_InterfaceSet_Interface(T_InterfaceSet *InterfaceSet, T_Interface *Interface);
int get_InterfaceSet_Overall(T_InterfaceSet *InterfaceSet);
void add_InterfaceSet_Overall(T_InterfaceSet *InterfaceSet, int overall);
int get_InterfaceSet_Interface_Index(T_InterfaceSet *InterfaceSet, T_Interface *Interface, int index);
int del_InterfaceSet_Interface(T_InterfaceSet *InterfaceSet, T_Interface *Interface);
void decrease_InterfaceSet_Overall(T_InterfaceSet * InterfaceSet);
void increase_InterfaceSet_Overall(T_InterfaceSet * InterfaceSet);
void print_InterfaceSet(T_InterfaceSet *InterfaceSet);

#endif	
