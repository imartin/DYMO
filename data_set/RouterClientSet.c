
#include "RouterClientSet.h"
#include <stdio.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <data/RouterClient.h>
#include "../event_definition.h"
#include "../constant_definition.h"
#include "../global_structs.h"
#define MAXCLIENTROUTER 10

void ini_RouterClientSet(T_RouterClientSet *RouterClientSet){

	add_RouterClientSet_Overall(RouterClientSet,0);
}

void decrease_RouterClientSet_Overall(T_RouterClientSet *RouterClientSet){

	add_RouterClientSet_Overall(RouterClientSet,get_RouterClientSet_Overall(RouterClientSet) - 1);
}
void increase_RouterClientSet_Overall(T_RouterClientSet *RouterClientSet){

	add_RouterClientSet_Overall(RouterClientSet,get_RouterClientSet_Overall(RouterClientSet) + 1);
}
int get_RouterClientSet_Overall(T_RouterClientSet *RouterClientSet){

	return RouterClientSet->overall;
}
int get_RouterClientSet_RouterClient_Index(T_RouterClientSet *RouterClientSet, T_RouterClient *RouterClient,int Position){

	if(get_RouterClientSet_Overall(RouterClientSet) == 0){

		return EMPTY_ARRAY;
    }
	*RouterClient = RouterClientSet->RouterClient[Position];
	return CORRECT_OPERATION;
}

int get_RouterClientSet_RouterClient_Address(T_RouterClientSet *RouterClientSet,T_RouterClient *RouterClient, uint32_t address){

    int err;
    for(int i = 0; i < get_RouterClientSet_Overall(RouterClientSet); i++){

        err = get_RouterClientSet_RouterClient_Index(RouterClientSet, RouterClient, i);
        if(get_RouterClient_Address(RouterClient) == address){

            return err == CORRECT_OPERATION ? CORRECT_OPERATION : err;
        }
    }
    return NOTFOUND_ITEM;
}
void add_RouterClientSet_Overall(T_RouterClientSet *RouterClientSet, int overall){

	RouterClientSet->overall = overall;
}

int add_RouterClientSet_RouterClient(T_RouterClientSet *RouterClientSet,T_RouterClient *RouterClient){
		
	if(get_RouterClientSet_Overall(RouterClientSet) < MAXCLIENTROUTER) {

		add_RouterClient_Position(RouterClient, get_RouterClientSet_Overall(RouterClientSet));
		RouterClientSet->RouterClient[get_RouterClientSet_Overall(RouterClientSet)] = *RouterClient;
		increase_RouterClientSet_Overall(RouterClientSet);
		return CORRECT_OPERATION;
	}
	return FULL_ARRAY;
}

int del_RouterClientSet_RouterClient(T_RouterClientSet *RouterClientSet, uint32_t Address){

    int i = 0;
    int a = 0;
    int err;
    T_RouterClient RouterClient;
    if(get_RouterClientSet_Overall(RouterClientSet) == MIN){

        return EMPTY_ARRAY;
    }
    else{

        while(i < get_RouterClientSet_Overall(RouterClientSet)){

            err = get_RouterClientSet_RouterClient_Index(RouterClientSet, &RouterClient, i);
            if(err != CORRECT_OPERATION){

                return err;
            }
            if(get_RouterClient_Address(&RouterClient) == Address){

                a = i;
                while(a < get_RouterClientSet_Overall(RouterClientSet)) {

                    RouterClientSet->RouterClient[a] =  RouterClientSet->RouterClient[a + 1];
                    RouterClientSet->RouterClient[a].Position = a;
                    a++;
                }
                decrease_RouterClientSet_Overall(RouterClientSet);
                return CORRECT_OPERATION;
            }else {

                i++;
            }
        }
    }
    return NOTFOUND_ITEM;
}

void print_RouterClientSet(T_RouterClientSet *RouterClientSet){
	int err;
    T_RouterClient RouterClient;
        printf(" ---------------------------\n");
        printf("|    ROUTER CLIENT SET      |\n");
        printf("|---------------------------|\n");
    if(get_RouterClientSet_Overall(RouterClientSet) >= 0) {

        for (int i = 0; i < get_RouterClientSet_Overall(RouterClientSet); i++) {

            err = get_RouterClientSet_RouterClient_Index(RouterClientSet, &RouterClient, i);
            printf("|Entrada: %d                 |\n", i);
            printf("|---------------------------|\n");
            print_RouterClient(&RouterClient);
        }
    }else{
        printf("|Empty table                |\n");
        printf("|---------------------------|\n");
    }
}

int configuracion_automatica_RouterClientSet(T_InterfaceSet *InterfaceSet,T_RouterClientSet *RouterClientSet){

    int fd;
    int err;
    struct ifreq ifr;
    T_RouterClient RouterClient;
    struct in_addr ipaddress;
    struct in_addr masc;
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    for(int i = 0 ; i < InterfaceSet->overall; i++)  {

        strncpy(ifr.ifr_name,InterfaceSet->Interface[i].InterfaceId,IFNAMSIZ-1);
        ioctl(fd, SIOCGIFADDR, &ifr);
        ipaddress  = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;
        ioctl(fd, SIOCGIFNETMASK, &ifr);
        masc = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;
        add_RouterClient_PrefixLength(&RouterClient, Mask_To_Prefix(masc.s_addr));
        add_RouterClient_Cost(&RouterClient,1);
        add_RouterClient_Address(&RouterClient,ipaddress.s_addr );
        err = add_RouterClientSet_RouterClient(RouterClientSet,&RouterClient);
        if( err != CORRECT_OPERATION){
            return err;
        }
    }
    return get_RouterClientSet_Overall(RouterClientSet) <= 0 ? FAIL_OPERATION : CORRECT_OPERATION;
}

