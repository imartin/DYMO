#ifndef PROGRAMA_V4_LocalRouteSet_H
#define PROGRAMA_V4_LocalRouteSet_H

#include <stdint.h>
#include "../data/LocalRoute.h"

#define MAXLOCALROUTESET  10

typedef struct{

    int overall;
    T_LocalRoute LocalRoute[MAXLOCALROUTESET];
}T_LocalRouteSet;

int get_LocalRouteSet_Overall(T_LocalRouteSet *LocalRouteSet);
int get_LocalRouteSet_LocalRoute_Index(T_LocalRouteSet *LocalRouteSet, T_LocalRoute *LocalRoute, int position);
void ini_LocalRouteSet(T_LocalRouteSet *LocalRouteSet);
void decrease_LocalRouteSet_Overall(T_LocalRouteSet * LocalRouteSet);
void increase_LocalRouteSet_Overall(T_LocalRouteSet * LocalRouteSet);
void update_LocalRouteSet_LocalRoute(T_LocalRouteSet *LocalRouteSet, T_LocalRoute * LocalRoute);
void add_LocalRouteSet_Overall(T_LocalRouteSet * LocalRouteSet, int overall);
int add_LocalRouteSet_LocalRoute(T_LocalRouteSet *LocalRouteSet,T_LocalRoute *new_LocalRoute);
int del_LocalRouteSet_LocalRoute_Position(T_LocalRouteSet  * LocalRouteSet, T_LocalRoute * LocalRoute);
int check_LocalRouteSet_LocalRoute_State(T_LocalRouteSet *pointer_LocalRouteSet, uint32_t OrigPrefix);
int best_route_to_origprefix(T_LocalRouteSet *LocalRouteSet, uint32_t origprefix,T_LocalRoute *best_route);
int get_LocalRouteSet_Index_LocalRoute(T_LocalRouteSet *LocalRouteSet,T_LocalRoute *LocalRoute);
int get_LocalRouteSet_LocalRoute_Address(T_LocalRouteSet *LocalRouteSet,T_LocalRoute *return_route,uint32_t address);

void print_LocalRouteSet(T_LocalRouteSet *LocalRouteSet);

#endif //PROGRAMA_V4_LocalRouteSet_H
