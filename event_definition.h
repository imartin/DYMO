//
// Created by carlos on 18/03/18.
//

#ifndef PROGRAMA_V4_EVENT_DEFINITION_H
#define PROGRAMA_V4_EVENT_DEFINITION_H



#define NOTFOUND_ITEM     (-1)
#define ADDED_ITEM         2
#define DELETED_ITEM       3
#define FORWARDING_RREP    4
#define EMPTY_ARRAY        5
#define CORRECT_OPERATION  6
#define WRONG_IP           7
#define FOUND_ITEM         8
#define FAIL_OPERATION     9
#define NOENOUGHT_MEMORY   10
#define DROP_MESSAGE       11
#define UPDATE             12
#define REDUNDANT          13
#define NOT_REDUNDANT      14
#define NOT_UPDATE         15
#define EQUAL              16
#define IGNORE_MESSAGE     17
#define GENERATE_RREP      18
#define FORWARDING_RREQ    19
#define FULL_ARRAY         20
#define BLACKLIST          21
#define LIMIT_HOPE         22
#define GENERATE_RERR      23
#define ROUTE_DISCOVERY_FINISH 24
#define RREP_ACK_RESPONSE 25
#define UPDATE_LocalRoute 26
#define DELETED_LocalRoute 27
#define REGENERATE_RERR     28
#define NO_DEVICE_NAME  29
#define ROUTE_ACTIVE 30
#define ROUTE_ACTIVED 31
#define INVALID_STATE 31
#define Neighbor_UPDATE 32
#define Neighbor_NOTFOUND 33
#define RERR_ADDRESSLIST_INVALID_ADDRESS 35
#define RERR_METRICTYPE_INVALID 36
#define T_McMsg_FOUND_ITEM 37
#define T_McMsg_NOTFOUND_ITEM 38
#define Neighbor_UPDATE_BLACKLISTED 39
#define UPDATE_LocalRouteSet_STEP_2_PROBLEM 40
#define RREP_ACK_WAS_NOT_EXPECTED 41
#define ROUTE_ERROR_EXIST 42
#define NEIGHBOR_STATE_BLACKLISTED 43


#endif //PROGRAMA_V4_EVENT_DEFINITION_H
