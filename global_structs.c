
#include "global_structs.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libnfnetlink/libnfnetlink.h>
#include <linux/wireless.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <curses.h>



void ini_DataPoint(T_DataPoint *DataPoint){

    DataPoint->point_LocalRouteSet->overall = 0;
    DataPoint->point_NeighborSet->overall = 0;
    DataPoint->point_RerrMsgSet->overall = 0;
    DataPoint->point_McMsgSet->overall = 0;
}
char  *get_DataPoint_InterfaceId(T_DataPoint *DataPoint){

    return DataPoint->InterfaceId;
}
uint32_t get_DataPoint_InterfaceIp(T_DataPoint *DataPoint){

    return DataPoint->InterfaceIp;
}
uint32_t get_DataPoint_InterfaceMasc(T_DataPoint *DataPoint){

    return DataPoint->InterfaceMasc;
}
void add_DataPoint_InterfaceId(T_DataPoint *DataPoint,char InterfaceId[256]){

    strcpy(DataPoint->InterfaceId,InterfaceId);
}
void add_DataPoint_InterfaceIp(T_DataPoint *DataPoint,uint32_t InterfaceIp){
    DataPoint->InterfaceIp = InterfaceIp;
}
void add_DataPoint_InterfaceMasc(T_DataPoint *DataPoint, uint32_t InterfaceMasc){

    DataPoint->InterfaceMasc = InterfaceMasc;
}
T_InterfaceSet *get_DataPoint_InterfaceSet(T_DataPoint *DataPoint){

    return  DataPoint->point_InterfaceSet;
}
T_RerrMsgSet *get_DataPoint_RerrMsgSet(T_DataPoint *DataPoint){

    return DataPoint->point_RerrMsgSet;
}
T_McMsgSet *get_DataPoint_McMsgSet(T_DataPoint *DataPoint){

    return DataPoint->point_McMsgSet;
}
T_MetricSet *get_DataPoint_MetricSet(T_DataPoint *DataPoint){

    return DataPoint->point_MetricSet;
}
T_LocalRouteSet * get_DataPoint_LocalRouteSet(T_DataPoint *DataPoint){

    return DataPoint->point_LocalRouteSet;
}
T_SeqNumSet * get_DataPoint_SeqNumSet(T_DataPoint *DataPoint){

    return DataPoint->point_SeqNumSet;
}
T_NeighborSet * get_DataPoint_NeighborSet(T_DataPoint *DataPoint){

    return DataPoint->point_NeighborSet;
}
T_RouterClientSet *get_DataPointers_RouterClientSet(T_DataPoint * DataPoint){

    return DataPoint->point_RouterClientSet;
}
T_RerrMsg get_DataPoint_RerrMsg(T_DataPoint *DataPoint){

    return DataPoint->RerrMsg;
}

void add_DataPoint_RerrMsg(T_DataPoint *DataPoint,T_RerrMsg *RerrMsg){

    DataPoint->RerrMsg = *RerrMsg;
}
uint32_t get_DataPoint_NeighborBlacklist(T_DataPoint *DataPoint){

    return DataPoint->black_Address;
}
T_AddressList *get_DataPoint_AddressList(T_DataPoint *DataPoint){

    return DataPoint->AddressList;
}
void add_DataPoint_AddressList(T_DataPoint *DataPoint,T_AddressList *AddressList){

    DataPoint->AddressList = AddressList;
}
T_CIDR get_DataPoint_AddressList_OrigPrefix(T_DataPoint *DataPoint){

    return DataPoint->AddressList->OrigPrefix;
}
T_CIDR get_DataPoint_AddressList_TargPrefix(T_DataPoint *DataPoint){

    return DataPoint->AddressList->TargPrefix;
}

int get_DataPacket_IdPacket(T_DataPacket *DataPacket){

    return DataPacket->idPacket;
}
uint32_t get_DataPacket_sourceIP(T_DataPacket *DataPacket){

    return DataPacket->sourceIP;
}
char *get_DataPacket_IntDev(T_DataPacket *DataPacket){

    return DataPacket->IntDev;
}
char *get_DataPacket_OutDev(T_DataPacket *DataPacket){

    return DataPacket->OutDev;
}
uint8_t *get_DataPacket_Payload(T_DataPacket *DataPacket){

    return DataPacket->PayLoad;
}
uint32_t get_DataPacket_destIP(T_DataPacket *DataPacket){

    return DataPacket->destIP;
}
uint32_t get_DataPacket_deUIP(T_DataPacket *DataPacket){

    return DataPacket->deUIP;
}
uint32_t get_DataPacket_soUIP(T_DataPacket *DataPacket){

    return DataPacket->soUIP;
}
int get_DataPacket_codeICMP(T_DataPacket *DataPacket){

    return  DataPacket->codeICMP;
}

void print_MacAddress(uint8_t *cMacAddr){

    printf("HWaddr %02X:%02X:%02X:%02X:%02X:%02X\n",cMacAddr[0],
           cMacAddr[1],
           cMacAddr[2],
           cMacAddr[3],
           cMacAddr[4],
           cMacAddr[5]);
}
void insert_NetFilterRules(uint32_t Address, T_DataPoint *DataPoint){

    uint32_t IP_nodo2 = string2int("10.0.3.2");
    uint32_t IP_nodo3 = string2int("10.0.3.3");
    uint32_t IP_nodo4 = string2int("10.0.3.4");
    uint32_t IP_nodo5 = string2int("10.0.3.5");
    uint32_t IP_nodo6 = string2int("10.0.3.6");

    if(Address==IP_nodo2){

        printf("Configuracion nodo_2\n");
        system(" ./scripts_confnetfilter/confnetfilter_nodo2.sh"); //Archivo que contiene las reglas para NetFilter
    }
    if(Address==IP_nodo3){

        printf("Configuracion nodo_3\n");
        system("./scripts_confnetfilter/confnetfilter_nodo3.sh"); //Archivo que contiene las reglas para NetFilter
    }
    if(Address==IP_nodo4){

        printf("Configuracion nodo_4\n");
        system("./scripts_confnetfilter/confnetfilter_nodo4.sh"); //Archivo que contiene las reglas para NetFilter
    }
    if(Address==IP_nodo5){

        printf("Configuracion nodo_5\n");
        system("./scripts_confnetfilter/confnetfilter_nodo5.sh"); //Archivo que contiene las reglas para NetFilter
    }
    if(Address==IP_nodo6){

        printf("Configuracion nodo_6\n");
        system("./scripts_confnetfilter/confnetfilter_nodo6.sh"); //Archivo que contiene las reglas para NetFilter
    }

}
int check_WirelessInterface(const char *IfName, char *Protocol){

    int sock;
    struct iwreq pwrq;
    memset(&pwrq, 0, sizeof(pwrq));
    strncpy(pwrq.ifr_name, IfName, IFNAMSIZ);

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1){

        perror("socket");
        return 0;
    }
    if (ioctl(sock, SIOCGIWNAME, &pwrq) != -1){

        if (Protocol) strncpy(Protocol, pwrq.u.name, IFNAMSIZ);
        close(sock);
        return 1;
    }
    close(sock);
    return 0;
}
long long int get_CTime_ms(){

    long long int t;
    long long int micro=1000000;
    struct timeval now;
    gettimeofday(&now, NULL);
    t=(now.tv_sec*micro)+now.tv_usec;
    return t;

}

uint32_t string2int(char *Address){

    return  inet_addr(Address);
}
uint8_t *Int16_To_8(uint16_t uData){

    static uint8_t arrayData[2] = { 0x00, 0x00 };
    *(arrayData) = (uint8_t) ((uData >> 8) & 0x00FF);
    arrayData[1] = (uint8_t) (uData & 0x00FF);
    return arrayData;
}
uint16_t Int8_To_16(uint8_t oData, uint8_t uData){

    uint16_t dataBoth = 0x0000;
    dataBoth = oData;
    dataBoth = dataBoth << 8;
    dataBoth |= uData;
    return dataBoth;
}
uint32_t Int8_To_32(T_CIDR Cidr){

    uint32_t myInt1 = (Cidr.Address[3] << 24) +
                      (Cidr.Address[2] << 16) +
                      (Cidr.Address[1] << 8) +
                      Cidr.Address[0];
    return myInt1;
}

char *Int_To_StringN(int nState){

    char *res = NULL;
    if(nState == 0){res = "HEARD";}
    if(nState == 1){res = "CONFIRMED";}
    if(nState == 2){res = "BLACKLISTED";}
    return res;
}
char *Int_To_StringL(int lState){

    char *res = NULL;
    if(lState == 0){res = "UNCONFIRMED";}
    if(lState == 1){res= "IDLE";}
    if(lState == 2){res= "ACTIVE";}
    if(lState == 3){res= "INVALID";}
    return res;
}
char *Int_To_StringEvent(int Event){

    char *res=NULL;

    if(Event==(-1)){res="NOTFOUND_ITEM";}
    if(Event==1){res="FULL_ARRAY";}
    if(Event==2){res="ADDED_ITEM";}
    if(Event==3){res="DELETED_ITEM";}
    if(Event==4){res="FORWARDING_RREP";}
    if(Event==5){res="EMPTY_ARRAY";}
    if(Event==6){res="CORRECT_OPERATION";}
    if(Event==7){res="WRONG_IP";}
    if(Event==8){res="FOUND_ITEM";}
    if(Event==9){res="FAIL_OPERATION";}
    if(Event==10){res="NOENOUGHT_MEMORY";}
    if(Event==11){res="DROP_MESSAGE";}
    if(Event==12){res="UPDATE";}
    if(Event==13){res="REDUNDANT";}
    if(Event==14){res="NOT_REDUNDANT";}
    if(Event==15){res="NOT_UPDATE";}
    if(Event==16){res="EQUAL";}
    if(Event==17){res="IGNORE_MESSAGE";}
    if(Event==18){res="GENERATE_RREP";}
    if(Event==19){res="FORWARDING_RREQ";}
    if(Event==20){res="FULL_ARRAY";}
    if(Event==21){res="BLACKLIST";}
    if(Event==22){res="LIMIT_HOPE";}
    if(Event==23){res="GENERATE_RERR";}
    if(Event==24){res="ROUTE_DISCOVERY_FINISH";}
    if(Event==25){res="RREP_ACK_RESPONSE";}
    if(Event==26){res="UPDATE_LocalRoute";}
    if(Event==27){res="DELETED_LocalRoute";}
    if(Event==28){res="REGENERATE_RERR";}
    if(Event==29){res="NO_DEVICE_NAME";}
    if(Event==30){res="ROUTE_ACTIVE";}
    if(Event==31){res="ROUTE_ACTIVED";}
    if(Event==32){res="INVALID_STATE";}
    if(Event==33){res="Neighbor_UPDATE";}
    if(Event==34){res="Neighbor_NOTFOUND";}
    if(Event==35){res="RERR_ADDRESSLIST_INVALID_ADDRESS";}
    if(Event==36){res="RERR_METRICTYPE_INVALID";}
    if(Event==37){res="T_McMsg_FOUND_ITEM";}
    if(Event==38){res="T_McMsg_NOTFOUND_ITEM";}
    if(Event==39){res="Neighbor_UPDATE_BLACKLISTED";}
    if(Event==40){res="UPDATE_LocalRouteSet_STEP_2_PROBLEM";}
    if(Event==41){res="RREP_ACK_WAS_NOT_EXPECTED";}
    if(Event==43){res="NEIGHBOR_STATE_BLACKLISTED";}

    return res;
}
char *Int32_To_String(uint32_t Address){

    struct in_addr inr;
    inr.s_addr=Address;
    return inet_ntoa(inr);
}
T_CIDR get_CIDR(uint32_t Address, uint8_t Prefix){

    T_CIDR cidr;
    cidr.Address[3] = (uint8_t) ((Address & 0xff000000) >> 24);
    cidr.Address[2] = (uint8_t) ((Address & 0x00ff0000) >> 16);
    cidr.Address[1] = (uint8_t) ((Address & 0x0000ff00) >> 8);
    cidr.Address[0] = (uint8_t) (Address & 0x000000ff);
    cidr.PrefixLen= Prefix;
    return cidr;
}
int valid_Address(T_CIDR cidr){

    for(int i=0;i < IPV4 ;i++){

        if(cidr.Address[i] > 255){
            return FALSE;
        }
    }
    return TRUE;
}
in_addr_t get_NetAddress(in_addr_t Addr, int Prefix){

    return( Addr & Int_To_32(Prefix) );
}
uint32_t Int_To_32(int prefix){

    return htonl((uint32_t) (0x00 - (1 << (32 - prefix))));
}

int Mask_To_Prefix(u_int32_t mask){

    int i;
    int count = IPBITS;
    for (i = 0; i < IPBITS; i++)
    {
        if (!(ntohl(mask) & ((2 << i) - 1)))
        {
            count--;
        }
    }
    return count;
}
uint32_t Prefix_To_Mask(int prefix){

    return htonl((uint32_t) ~((1 << (32 - prefix)) - 1));
}

uint32_t get_DataPoint_OrigPrefixRREP(T_DataPoint * DataPoint){

    return DataPoint->OrigPrefixRREP;
}
void add_DataPoint_OrigPrefixRREP(T_DataPoint * DataPoint, uint32_t OrigPrefixRREP ){

    DataPoint->OrigPrefixRREP = OrigPrefixRREP;
}

T_LocalRoute get_DataPoint_LocalRoute(T_DataPoint * DataPoint){
    return DataPoint->LocalRoute;
}
void add_DataPoint_LocalRoute(T_DataPoint *DataPoint, T_LocalRoute *LocalRoute){

    DataPoint->LocalRoute = *LocalRoute;
}