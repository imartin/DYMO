#include "evaluating_route_information_process.h"
#include "../global_structs.h"
#include "../event_definition.h"


bool check_fields(T_LocalRoute * LocalRoute,T_ADVRTE *advrte);

int EvalRouteInf_Process(T_ADVRTE *advrte_container, T_LocalRouteSet *LocalRouteSet, T_LocalRouteSet *matching_routes){

	int err=0;
	T_LocalRoute matchroute;
	/*Search for LocalRoutes in the Local Route Set matching AdvRte’s
	address, prefix length, metric type, and SeqNoRtr (the AODVv2
	router address corresponding to the sequence number).*/
	for(int i = 0; i < get_LocalRouteSet_Overall(LocalRouteSet); i++){

		err = get_LocalRouteSet_LocalRoute_Index(LocalRouteSet, &matchroute, i);
		if( err != CORRECT_OPERATION){

			printf("get_LocalRouteSet_LocalRoute_Index : %s\n", Int_To_StringEvent(err));
			return err;
		}
		if(get_LocalRoute_Address(&matchroute) == get_ADVRTE_Address(advrte_container)){

			if((get_LocalRoute_Prefixlength(&matchroute) == get_ADVRTE_Prefixlength(advrte_container))){

				if((get_LocalRoute_Metrictype(&matchroute) == get_ADVRTE_Metrictype(advrte_container))){

					if(check_fields(&matchroute,advrte_container) == TRUE){

						err = add_LocalRouteSet_LocalRoute (matching_routes, &matchroute);
						if(err == FAIL_OPERATION ){

							return FAIL_OPERATION;
						}
					}else {

                        err = NOT_UPDATE;
					}
				}
			}
		}
	}
	if(matching_routes->overall > 0){

		return  UPDATE;
	}
	if((matching_routes->overall == 0) && (err != NOT_UPDATE)){

		return UPDATE;
	}
	if((matching_routes->overall == 0) && (err == NOT_UPDATE)){

		return NOT_UPDATE;
	}
	return FAIL_OPERATION;
}

bool check_fields(T_LocalRoute * LocalRoute,T_ADVRTE *advrte){
	/*If AdvRte is more recent than all matching LocalRoutes, AdvRte
	  MUST be used to update the Local Route Set and no further
	  checks are required.*/
	if(get_ADVRTE_Seqnum(advrte) > get_LocalRoute_Seqnum(LocalRoute)){

		return TRUE;
	}
	/*If AdvRte is stale, AdvRte MUST NOT be used to update the
	Local Route Set. Ignore AdvRte for further processing.*/
	else if(get_ADVRTE_Seqnum(advrte) < get_LocalRoute_Seqnum(LocalRoute)){

		return  FALSE;
	}
	else{

		if(LoopFree(get_ADVRTE_Cost(advrte),get_LocalRoute_Metric(LocalRoute)) == FALSE){
			return FALSE;
		}

		if(get_ADVRTE_Cost(advrte) < get_LocalRoute_Metric(LocalRoute)){

			return TRUE;
		}
		if((get_ADVRTE_Cost(advrte) == get_LocalRoute_Metric(LocalRoute)) &&
				((get_LocalRoute_State(LocalRoute) == IDLE)||
				 (get_LocalRoute_State(LocalRoute) == ACTIVE))){

			return FALSE;
		}
		if((get_ADVRTE_Cost(advrte) > get_LocalRoute_Metric(LocalRoute)) &&
				  ((get_LocalRoute_State(LocalRoute) == IDLE)||
				   (get_LocalRoute_State(LocalRoute) == ACTIVE))){

			return FALSE;
		}
		if((get_ADVRTE_Cost(advrte) >= get_LocalRoute_Metric(LocalRoute)) &&
		   (get_LocalRoute_State(LocalRoute) == INVALID)){

			return TRUE;
		}

	}
	return CORRECT_OPERATION;
}
