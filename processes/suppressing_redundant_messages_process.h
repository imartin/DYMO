#ifndef SUPPRESSING_REDUNDANT_MESSAGES_PROCESS_H
#define SUPPRESSING_REDUNDANT_MESSAGES_PROCESS_H
#include <stdint.h> /* int8_t*/
#include "data_set/MulticastRouteMessageSet.h"
#include "data_set/MetricSet.h"
#include "containers/RREQ_container.h"
#include "containers/RREP_container.h"
#include "../global_structs.h"
#include "../event_definition.h"


int SupRedMsg_Process(T_DataPoint *DataPoint,T_RREQ *rreq,char *interface);

#endif

//DataPacket.PayLoad,&pointers_parse,le32toh(DataPacket.sourceIP),DataPacket.IntDev