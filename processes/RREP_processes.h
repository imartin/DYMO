#ifndef RREP_PROCESSES_H
#define RREP_PROCESSES_H

#include <stdint.h> /* int8_t*/
#include "../containers/RREQ_container.h"
#include "../containers/RREP_container.h"
#include "../containers/ADVRTE_container.h"
#include "../data_set/MetricSet.h"
#include "../RFC5444/gman_packet_parse.h"
#include "../RFC5444/parse_container.h"
#include "../RFC5444/gman_packet_serialized.h"
#include "../global_structs.h"


int RREP_GenerationProcess(T_RREQ *rreq, T_DataPoint *DataPoint, T_RREP *rrep);
int RREP_ReceptionProcess(T_DataPoint *DataPoint, T_DataPacket *DataPacket, T_RREP *rrep, T_ADVRTE *advrte, T_PointParsed *pointers_parse);
int RREP_ForwardingProcess(T_RREP *rrep, T_DataPoint *DataPoint, T_LocalRoute *best_route);
int send_RREP_Packet(T_RREQ *rreq, T_DataPoint *DataPoint);
int forwarding_rrep_packet(T_RREP *rrep,T_DataPoint *DataPoint);
T_ADVRTE RREP_ProcessingReceivedRouteInformation(T_RREP *rrep, uint32_t SourceAddress, T_MetricSet *MetricSet);
int receipt_RREP_update_NeighborSet(T_DataPoint *DataPoint,T_DataPacket *DataPacket);


#endif
