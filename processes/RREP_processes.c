#include "RREP_processes.h"
#include <sys/time.h>
#include "evaluating_route_information_process.h"
#include "applying_route_updates_process.h"
#include "RREP_ACK_processes.h"
#include "../Timers/timers.h"
#include "../monitors_pthreads_v3/thread_message.h"
#include "../monitors_pthreads_v3/thread_queue_manager.h"
#include "../operative_system/operative_system_functions.h"
#include "../event_definition.h"



int RREP_GenerationProcess(T_RREQ *rreq, T_DataPoint *DataPoint, T_RREP *rrep){

    int err;
    T_RouterClient RouterClient;
    goto STEP_1;

    STEP_1:{

    add_RREP_MsgHopLimit(rrep, (uint8_t) (MAX_HOPCOUNT - get_RREQ_MsgHopLimit(rreq)));

        goto STEP_2;
    }
    STEP_2:{

        add_RREP_Origprefix(rrep,&rreq->AddressList.OrigPrefix);
        add_RREP_TargPrefix(rrep,&rreq->AddressList.TargPrefix);
        goto STEP_4;
    }
    STEP_4:{

        pthread_mutex_lock(DataPoint->Mutex);
    increment_SeqNumSet(get_DataPoint_SeqNumSet(DataPoint));
        pthread_mutex_unlock(DataPoint->Mutex);
        add_RREP_TargSeqNum(rrep, get_SeqNumSet(get_DataPoint_SeqNumSet(DataPoint)));
        goto STEP_5;
    }
    STEP_5:{

        add_RREP_MetricType(rrep,get_MetricSet_MetricType(get_DataPoint_MetricSet(DataPoint)));
        goto STEP_6;
    }
    STEP_6:{

        err = get_RouterClientSet_RouterClient_Index(get_DataPointers_RouterClientSet(DataPoint), &RouterClient, 0);
        if(err != CORRECT_OPERATION){

            printf("RREP_GenerationProcess: get_RouterClientSet_RouterClient_Index FAIL\n");
            return FAIL_OPERATION;
        }
        add_RREP_TargMetric(rrep,get_RouterClient_Cost(&RouterClient));
        return CORRECT_OPERATION;
    }
}
int RREP_ReceptionProcess(T_DataPoint *DataPoint, T_DataPacket *DataPacket, T_RREP *rrep, T_ADVRTE *advrte, T_PointParsed *pointers_parse){

    int err;
    T_McMsg McMsg;
    int FLAG_RREQ_GEN;
    time_t t;
    T_RouterClient RouterClient;
    T_LocalRouteSet matching_routes;
    ini_LocalRouteSet(&matching_routes);

    bool FLAG_MCMSG;

    T_PgmanPacket packet = get_parse_array(DataPacket->PayLoad,pointers_parse);
    *rrep = get_parse_container_rrep(packet,pointers_parse);

    uint32_t origprefix = get_RREP_OrigPrefix(rrep);
    uint32_t ipsource_adrress = get_DataPacket_sourceIP(DataPacket);
    err = get_RouterClientSet_RouterClient_Index(get_DataPointers_RouterClientSet(DataPoint), &RouterClient, 0);
    if(err !=CORRECT_OPERATION){

        printf("RREQ_ReceptionProcess : get_RouterClientSet_RouterClient_Index FAIL\n");
        return FAIL_OPERATION;
    }
    if (get_RouterClient_Address(&RouterClient) == get_RREP_OrigPrefix(rrep)) {

        FLAG_RREQ_GEN = ONE;
    } else {

        FLAG_RREQ_GEN = ZERO;
    }

    goto STEP_1_1;

    STEP_1_1:{
        if(check_RREP(rrep) == FALSE){

            printf("CHECK RREP CONTAINER : FALSE\n");
            return IGNORE_MESSAGE;
        }
    goto STEP_1_2;
    }

    // STEP 2 [pag.45]
    STEP_1_2:{
        if(check_MetricType(get_DataPoint_MetricSet(DataPoint),get_RREP_MetricType(rrep)) == FALSE){

            printf("CHECK METRICTYPE : FALSE\n");
            return IGNORE_MESSAGE;
        }
        goto STEP_1_3;
    }

    STEP_1_3:{

        FLAG_MCMSG =  FALSE;
        for(int i = 0; i < get_McMsgSet_Overall(get_DataPoint_McMsgSet(DataPoint)); i++){

            err = get_McMsgSet_McMsg_Index(get_DataPoint_McMsgSet(DataPoint),&McMsg,i);
            if(err != CORRECT_OPERATION){

                printf("get_McMsgSet_McMsg_Index FAIL\n");
                return FAIL_OPERATION;
            }
            if (FLAG_RREQ_GEN == ONE) {

                t = get_CTime_ms() - (DataPoint->RREQ_GEN_WAIT_TIME * 1000000);
            } else {

                t = get_CTime_ms() - (RREQ_WAIT_TIME * 1000000);
            }
            if(get_McMsg_OrigPrefix(&McMsg) == get_RREP_OrigPrefix(rrep)){

                if(get_McMsg_OrigPrefixLen(&McMsg) == get_RREP_OrigPrefixLen(rrep)){

                    if(get_McMsg_TargPrefix(&McMsg) == get_RREP_TargPrefix(rrep)){

                      if(get_McMsg_MetricType(&McMsg) == get_RREP_MetricType(rrep)){

                            if(get_McMsg_TimeStamp(&McMsg) > t){

                                if(strcmp(get_McMsg_Interface(&McMsg), get_DataPacket_IntDev(DataPacket)) == 0){
                                    FLAG_MCMSG = TRUE;
                                }
                            }
                        }
                    }
                }
            }
        }
        if(FLAG_MCMSG == FALSE){

            printf("RREP message not correspond to an RREQ\n");
            return IGNORE_MESSAGE;
        }else{

            goto STEP_1_4;
        }
    }
    STEP_1_4:{

        goto STEP_2_1;
    }
    STEP_2_1:{

        if(receipt_RREP_update_NeighborSet(DataPoint,DataPacket) != CORRECT_OPERATION){

            return FAIL_OPERATION;
        }
        goto STEP_2_2;
    }
    STEP_2_2:{

        if(check_maximum_metric_value(get_RREP_TargMetric(rrep), get_DataPoint_MetricSet(DataPoint)) == FALSE){

            printf("EXTERNAL: check_maximum_metric_value\n");
            return IGNORE_MESSAGE;
        }
        goto STEP_2_3;
    }
    STEP_2_3:{

        *advrte = RREP_ProcessingReceivedRouteInformation(rrep, ipsource_adrress, get_DataPoint_MetricSet(DataPoint));
        err = EvalRouteInf_Process(advrte, get_DataPoint_LocalRouteSet(DataPoint), &matching_routes);
        if(err == FAIL_OPERATION){

            printf("EvalRouteInf_Process: FAIL\n");
            return FAIL_OPERATION;
        }
        if(err == NOT_UPDATE){

            return IGNORE_MESSAGE;
        }

        if(AppRouteUpdate_Process(DataPoint, get_DataPacket_IntDev(DataPacket), advrte, &matching_routes) == FAIL_OPERATION){

            printf("UPDATE ROUTE PROCESS: fail operation\n");
            return FAIL_OPERATION;
        }
        goto STEP_2_4;
    }
    STEP_2_4:{

        /*
        int value = SupRedMsg_Process(DataPoint,NULL,&rrep,DataPacket->IntDev);
        if(value == REDUNDANT){
            printf("EXTERNAL: REDUNDANT RREP\n");
            return IGNORE_MESSAGE;
        }
        if(value == FULL_ARRAY){
            printf("SUPRESSING REDUNDATN MESSAGES PROCESS: full array FAIL\n");
            return FAIL_OPERATION;
        }*/
        goto STEP_2_5;
    }
    STEP_2_5:{

        if(FLAG_RREQ_GEN == ONE){

            return ROUTE_DISCOVERY_FINISH;
        }
        goto STEP_2_6;
    }
    STEP_2_6:{


        err = check_LocalRouteSet_LocalRoute_State(get_DataPoint_LocalRouteSet(DataPoint), origprefix);
        if(err == FOUND_ITEM){

            return FORWARDING_RREP;
        }else if(err == FAIL_OPERATION){

            return FAIL_OPERATION;
        }else{

            return GENERATE_RERR;
        }
    }
}
int RREP_ForwardingProcess(T_RREP *rrep, T_DataPoint *DataPoint, T_LocalRoute *best_route){

    int err;
    int exit=0,rest=0;
    bool FLAG_LOCALROUTE;
    struct timeval now;
    T_Neighbor Neighbor;
    T_LocalRoute LocalRoute;
    T_DataPacket *DataPacket = NULL;
    pthread_t RREP_Ack_SENT_TIMEOUT_id;
    goto STEP_1;

    STEP_1:{

        add_RREP_MsgHopLimit(rrep, (uint8_t) (get_RREP_MsgHopLimit(rrep) - 1));
        //printf("RREP_MSHOPLIMIT = %u\n",rrep->MsgHopLimit);
            if(get_RREP_MsgHopLimit(rrep) == 0){

                printf("RREP FORWARDING PROCESS: limit hope\n");
                return FAIL_OPERATION;
            }
            goto STEP_2;
        }
    STEP_2:{

        err = best_route_to_origprefix(get_DataPoint_LocalRouteSet(DataPoint),get_RREP_OrigPrefix(rrep),best_route);
        if(err == NOTFOUND_ITEM){

            printf("RREP_ForwardingProcess : best_route_to_origprefix :  NOTFOUNDITeM\n");
            return GENERATE_RERR;
        }
        if(get_LocalRoute_State(best_route) == UNCONFIRMED){

            if(send_RREP_Ack_AckReq(best_route,DataPoint) == FAIL_OPERATION){

                printf ("EXTERNAL: send_RREP_Ack_AckReq FAIL\n");
                return FAIL_OPERATION;
            }
            printf ("EXTERNAL: SEND ACK_AckReq\n");
            err = pthread_create(&RREP_Ack_SENT_TIMEOUT_id, NULL,timer_RREP_ACK_SENT_TIMEOUT,(void*)DataPoint);
            if (err != 0){

                printf("EXTERNAL: timer_RREP_GEN_ack FAIL[%s]\n", strerror(err));
                return	FAIL_OPERATION;
            }
            while(!exit) {

                ThreadMessage * message = get_message(MESSAGE_QUEUE_ACK_to_EXTERNAL);
                if(message->message_code == EXIT_RREP_ACK_RECEIVED){
                    DataPacket = (T_DataPacket *) getData(message);
                    exit = message->message_code;
                }
                if(message->message_code == EXIT_RREP_ACK_SENT_FINISH){

                    exit = message->message_code;
                }
            }
            if ( exit == EXIT_RREP_ACK_RECEIVED) {

                err = get_NeighborSet_Neighbor_Address(get_DataPoint_NeighborSet(DataPoint),&Neighbor,
                                                       get_DataPacket_sourceIP(DataPacket));
                if( err != CORRECT_OPERATION){

                    printf("get_NeighborSet_Neighbor_Address : NOT FOUND ITEM\n");
                    return err;
                }else{

                    if(get_Neighbor_State(&Neighbor) != HEARD ){

                        if(get_Neighbor_Timeout(&Neighbor) > get_CTime_ms()){

                            if(strcmp(get_Neighbor_Interface(&Neighbor), get_DataPacket_IntDev(DataPacket)) != 0){

                                return RREP_ACK_WAS_NOT_EXPECTED;
                            }

                        }
                    }
                }
                err = pthread_cancel (RREP_Ack_SENT_TIMEOUT_id);
                if ( err != 0 ) {

                    printf ("EXTERNAL: pthread_cancel RREP_Ack_SENT_TIMEOUT_id  FAIL\n");
                    return  FAIL_OPERATION;
                }
                printf("EXTERNAL: timer_RREP_ACK CANCELED\n");
                add_Neighbor_State(&Neighbor,CONFIRMED);
                add_Neighbor_Timeout(&Neighbor,INFINITY_TIME);
                update_NeighborSet_Neighbor(get_DataPoint_NeighborSet(DataPoint),&Neighbor);
                printf("EXTERNAL: Neighbor  %s CONFIRMED\n", Int32_To_String(get_Neighbor_IPAddress(&Neighbor)));
                /*When the value of Neighbor.State is set to Confirmed, the Unconfirmed routes in the Local Route Set
                 * using that neighbor as a next hop MUST have LocalRoute.State set to Idle*/
                for(int i = 0;i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++){

                    err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &LocalRoute, i);
                    if(err != CORRECT_OPERATION){

                        printf("send_RREP_Packet: get_LocalRouteSet_LocalRoute_Index FAIL \n");
                    }
                    if(get_LocalRoute_Nexthop(&LocalRoute) == get_Neighbor_IPAddress(&Neighbor)){

                        add_LocalRoute_State(&LocalRoute,IDLE);
                        update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&LocalRoute);
                        if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_ONGOING || get_status (
                                get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_RESTARTED ){

                            abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute));
                        }
                        if (get_LocalRoute_Address(&LocalRoute) == get_LocalRoute_Nexthop(&LocalRoute)) {

                            add_RIB_Host(&LocalRoute,DataPoint);
                        } else {

                            add_RIB_Route(&LocalRoute,DataPoint);
                        }
                        /*Any other matching LocalRoutes with metric values worse than LocalRoute.Metric MUST be expunged
                         *from the Local Route Set.*/
                        T_LocalRoute expunged_route;
                        for(int o = 0;o < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); o++){

                            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &expunged_route, o);
                            if(err != CORRECT_OPERATION){

                                printf("send_RREP_Packet: get_LocalRouteSet_LocalRoute_Index FAIL \n");
                            }
                            if(get_LocalRoute_Address(&LocalRoute) == get_LocalRoute_Address(&expunged_route)){

                                if(get_LocalRoute_Metric (&LocalRoute) < get_LocalRoute_Metric (&expunged_route)){

                                    del_LocalRouteSet_LocalRoute_Position(get_DataPoint_LocalRouteSet(DataPoint), &expunged_route);
                                    if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&expunged_route)) == TIMEOUT_ONGOING || get_status (
                                            get_LocalRoute_TimerMaxSeqNumLifeTime (&expunged_route)) == TIMEOUT_RESTARTED ){

                                        abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&expunged_route));
                                    }

                                    if(get_LocalRoute_State (&expunged_route) == IDLE || get_LocalRoute_State (&expunged_route) == ACTIVE){

                                        (get_LocalRoute_Address(&expunged_route) == get_LocalRoute_Nexthop(&expunged_route)
                                         ? del_RIB_Host : del_RIB_Route) (&expunged_route, DataPoint);
                                    }
                                }
                            }
                        }
                        rest = 1;
                    }
                }
                for(int i = 0;i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++){

                    err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &LocalRoute, i);
                    if(err != CORRECT_OPERATION){

                        printf("send_RREP_Packet: get_LocalRouteSet_LocalRoute_Index FAIL \n");
                    }
                    if(get_LocalRoute_Nexthop(&LocalRoute) == get_Neighbor_IPAddress(&Neighbor)){

                        add_LocalRoute_State(&LocalRoute,IDLE);
                        update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&LocalRoute);
                        if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_ONGOING || get_status (
                                get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_RESTARTED ){

                            abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute));
                        }
                        if (get_LocalRoute_Address(&LocalRoute) == get_LocalRoute_Nexthop(&LocalRoute)) {

                            add_RIB_Host(&LocalRoute,DataPoint);
                        } else {

                            add_RIB_Route(&LocalRoute,DataPoint);
                        }
                        rest = 1;
                    }
                }
            }
            if ( exit == EXIT_RREP_ACK_SENT_FINISH ) {

                err = get_NeighborSet_Neighbor_Address(get_DataPoint_NeighborSet(DataPoint),&Neighbor,get_LocalRoute_Nexthop(best_route));
                if( err != CORRECT_OPERATION){

                    printf("get_NeighborSet_Neighbor_Address : NOT FOUND ITEM\n");
                    return err;
                }
                if(get_Neighbor_State (&Neighbor) == BLACKLISTED){

                    return NEIGHBOR_STATE_BLACKLISTED;
                }
                add_Neighbor_State(&Neighbor,BLACKLISTED);
                gettimeofday(&now, NULL);
                add_Neighbor_Timeout(&Neighbor,(((now.tv_sec + MAX_BLACKLIST_TIME)*1000000) + now.tv_usec));
                update_NeighborSet_Neighbor(get_DataPoint_NeighborSet(DataPoint),&Neighbor);
                for(int i = 0;i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++){

                    err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &LocalRoute, i);
                    if(err !=CORRECT_OPERATION){
                        printf("RREP_ForwardingProcess: get_LocalRouteSet_LocalRoute_Index\n");
                        return FAIL_OPERATION;
                    }
                    if(get_LocalRoute_Nexthop(&LocalRoute) == get_Neighbor_IPAddress(&Neighbor)){

                        if(get_LocalRoute_State(&LocalRoute) == IDLE || get_LocalRoute_State(&LocalRoute) == ACTIVE ){

                            if (get_LocalRoute_Address(&LocalRoute) == get_LocalRoute_Nexthop(&LocalRoute)) {

                                del_RIB_Host(&LocalRoute,DataPoint);
                            } else {

                                del_RIB_Route(&LocalRoute,DataPoint);
                            }
                        }
                        add_LocalRoute_State(&LocalRoute,INVALID);
                        update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&LocalRoute);
                        if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_ONGOING || get_status (
                                get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_RESTARTED ){

                            abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute));
                        }
                        rest = 1;
                    }
                }
                if(rest != 1){

                    printf("RREP_ForwardingProcess : get_LocalRouteSet_Overall FAIL\n");
                    return FAIL_OPERATION;
                }
                DataPoint->black_Address = get_Neighbor_IPAddress(&Neighbor);
                pthread_t BLACKLIST_TIMER_ID = get_LocalRoute_Nexthop(best_route);
                err = pthread_create(&BLACKLIST_TIMER_ID,NULL,timer_MAX_BLACKLIST_TIME,(void*)DataPoint);
                if(err != 0){

                    printf("EXTERNAL: pthread_create BLACKLIST_TIMER_ID  FAIL\n");
                    return FAIL_OPERATION;
                }
                return Neighbor_UPDATE_BLACKLISTED;
            }
        }
        goto STEP_3;
    }
    STEP_3:{

        T_LocalRoute targ_route;
        FLAG_LOCALROUTE = FALSE;
        for(int i = 0;i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++){

            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &targ_route, i);
            if (err != CORRECT_OPERATION){
                printf("RREP_ForwardingProcess : get_LocalRouteSet_LocalRoute_Index FAIL\n");
            }
            if(get_RREP_TargPrefix(rrep) == get_LocalRoute_Address(&targ_route)){
                add_RREP_TargMetric(rrep,get_LocalRoute_Metric(&targ_route));
                FLAG_LOCALROUTE = TRUE;
            }
        }
        return FLAG_LOCALROUTE == FALSE ? FAIL_OPERATION : CORRECT_OPERATION;
    }
}

int send_RREP_Packet(T_RREQ *rreq, T_DataPoint *DataPoint){

	int err;
    int exit = 0;
    int rest = 0;
	uint8_t size = 0;
    struct timeval now;
    T_Neighbor Neighbor;
    T_PgmanPacket packet;
    uint8_t *buffer = NULL;
    T_LocalRoute best_route;
    T_LocalRoute LocalRoute;
    T_PointSerialized pointers;
    T_RREP rrep;
    T_DataPacket *DataPacket = NULL;
    pthread_t RREP_Ack_SENT_TIMEOUT_id;

	err = best_route_to_origprefix(get_DataPoint_LocalRouteSet(DataPoint), Int8_To_32(rreq->AddressList.OrigPrefix),&best_route);
	if(err == NOTFOUND_ITEM){

		printf("send_RREP_Packet :  best_route_to_origprefix;NOT FOUND ITEM\n");
		return FAIL_OPERATION;
	}

	printf("EXTERNAL: BEST ROUTE TO SEND RREP: %s\n", Int32_To_String(best_route.NextHop));
	if(best_route.State == UNCONFIRMED){

		if(send_RREP_Ack_AckReq(&best_route,DataPoint) == FAIL_OPERATION){

            printf ("send_RREP_Packet : send_RREP_Ack_AckReq FAIL\n");
			return FAIL_OPERATION;
		}else {

            printf ("EXTERNAL: SEND ACK_AckReq\n");
            err = pthread_create(&RREP_Ack_SENT_TIMEOUT_id, NULL,timer_RREP_ACK_SENT_TIMEOUT,(void*)DataPoint);
            if (err != 0){

                printf("send_RREP_Packet : pthread_create: [%s]\n", strerror(err));
                return	FAIL_OPERATION;
            }

            while(!exit) {

                ThreadMessage * message = get_message(MESSAGE_QUEUE_ACK_to_EXTERNAL);

                if(message->message_code == EXIT_RREP_ACK_RECEIVED){

                    DataPacket = (T_DataPacket *) getData(message);
                    exit = message->message_code;
                }
                if(message->message_code == EXIT_RREP_ACK_SENT_FINISH){

                    exit = message->message_code;
                }
            }
            if ( exit == EXIT_RREP_ACK_RECEIVED) {

                err = get_NeighborSet_Neighbor_Address(get_DataPoint_NeighborSet(DataPoint),&Neighbor,
                                                       get_DataPacket_sourceIP(DataPacket));
                if( err != CORRECT_OPERATION){

                    printf("send_RREP_Packet : get_NeighborSet_Neighbor_Address : NOT FOUND ITEM\n");
                    return err;
                }else{

                    if(get_Neighbor_State(&Neighbor) != HEARD ){

                        if(get_Neighbor_Timeout(&Neighbor) > get_CTime_ms()){

                            if(strcmp(get_Neighbor_Interface(&Neighbor), get_DataPacket_IntDev(DataPacket)) != 0){

                                return RREP_ACK_WAS_NOT_EXPECTED;
                            }

                        }
                    }
                }
                err = pthread_cancel (RREP_Ack_SENT_TIMEOUT_id);
                if ( err != 0 ) {

                    printf ("send_RREP_Packet : pthread_cancel  FAIL\n");
                    return  FAIL_OPERATION;
                }
                printf("EXTERNAL: timer_RREP_ACK CANCELED\n");
                add_Neighbor_State(&Neighbor,CONFIRMED);
                add_Neighbor_Timeout(&Neighbor,INFINITY_TIME);
                update_NeighborSet_Neighbor(get_DataPoint_NeighborSet(DataPoint),&Neighbor);
                printf("EXTERNAL: Neighbor  %s CONFIRMED\n", Int32_To_String(get_Neighbor_IPAddress(&Neighbor)));
                /*When the value of Neighbor.State is set to Confirmed, the Unconfirmed routes in the Local Route Set
                 * using that neighbor as a next hop MUST have LocalRoute.State set to Idle*/
               for(int i = 0;i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++){

                    err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &LocalRoute, i);
                    if(err != CORRECT_OPERATION){

                        printf("send_RREP_Packet: get_LocalRouteSet_LocalRoute_Index FAIL \n");
                    }
                    if(get_LocalRoute_Nexthop(&LocalRoute) == get_Neighbor_IPAddress(&Neighbor)){

                        add_LocalRoute_State(&LocalRoute,IDLE);
                        update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&LocalRoute);
                        if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_ONGOING || get_status (
                                get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_RESTARTED ){

                            abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute));
                        }
                        if (get_LocalRoute_Address(&LocalRoute) == get_LocalRoute_Nexthop(&LocalRoute)) {

                            add_RIB_Host(&LocalRoute,DataPoint);
                        } else {

                            add_RIB_Route(&LocalRoute,DataPoint);
                        }
                        /*Any other matching LocalRoutes with metric values worse than LocalRoute.Metric MUST be expunged
                         *from the Local Route Set.*/
                        T_LocalRoute expunged_route;
                        for(int o = 0;o < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); o++){

                            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &expunged_route, o);
                            if(err != CORRECT_OPERATION){

                                printf("send_RREP_Packet: get_LocalRouteSet_LocalRoute_Index FAIL \n");
                            }
                            if(get_LocalRoute_Address(&LocalRoute) == get_LocalRoute_Address(&expunged_route)){

                                if(get_LocalRoute_Metric (&LocalRoute) < get_LocalRoute_Metric (&expunged_route)){

                                    del_LocalRouteSet_LocalRoute_Position(get_DataPoint_LocalRouteSet(DataPoint), &expunged_route);

                                    if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&expunged_route)) == TIMEOUT_ONGOING ||
                                     get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&expunged_route)) == TIMEOUT_RESTARTED ){

                                        abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&expunged_route));
                                    }
                                    if(get_LocalRoute_State (&expunged_route) == IDLE || get_LocalRoute_State (&expunged_route) == ACTIVE){

                                        (get_LocalRoute_Address(&expunged_route) == get_LocalRoute_Nexthop(&expunged_route)
                                         ? del_RIB_Host : del_RIB_Route) (&expunged_route, DataPoint);
                                    }
                                }
                            }
                        }
                        rest = 1;
                    }
                }
                if(rest != 1){

                    printf ("EXTERNAL: FAIL FIND LOCAL ROUTE SET\n");
                    return FAIL_OPERATION;
                }
                err = RREP_GenerationProcess(rreq, DataPoint, &rrep);
                if ( err == CORRECT_OPERATION ) {

                    nullPointSerialized(&pointers);
                    packet = get_RREP_Packet(rrep, &pointers);
                    buffer = get_Packet_Array(packet, &size, &pointers);
                    err = send_PacketUdpUnicast(buffer, size, best_route.NextHop, best_route.NextHopInterface);
                    {
                        freePointSerialized(&pointers);
                        nullPointSerialized(&pointers);
                        return err == FAIL_OPERATION ? FAIL_OPERATION : CORRECT_OPERATION;
                    }
                }
                else {
                    printf ("send_RREP_Packet : RREP_GenerationProcess FAIL\n");
                    return  FAIL_OPERATION;
                }
            }
            if ( exit == EXIT_RREP_ACK_SENT_FINISH ) {

                err = get_NeighborSet_Neighbor_Address(get_DataPoint_NeighborSet(DataPoint),&Neighbor,get_LocalRoute_Nexthop(&best_route));
                if( err != CORRECT_OPERATION){

                    printf("send_RREP_Packet : get_NeighborSet_Neighbor_Address : NOT FOUND ITEM\n");
                    return err;
                }
                if(get_Neighbor_State (&Neighbor) == BLACKLISTED){

                    return NEIGHBOR_STATE_BLACKLISTED;
                }
                add_Neighbor_State(&Neighbor,BLACKLISTED);
                gettimeofday(&now, NULL);
                add_Neighbor_Timeout(&Neighbor,(((now.tv_sec + MAX_BLACKLIST_TIME)*1000000) + now.tv_usec));
                update_NeighborSet_Neighbor(get_DataPoint_NeighborSet(DataPoint),&Neighbor);


                for(int i = 0;i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++){

                    err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &LocalRoute, i);
                    if(err != CORRECT_OPERATION){

                        printf("send_RREP_Packet: get_LocalRouteSet_LocalRoute_Index FAIL \n");
                    }

                    if(get_LocalRoute_Nexthop(&LocalRoute) == get_Neighbor_IPAddress(&Neighbor)){

                            if (get_LocalRoute_Address(&LocalRoute) == get_LocalRoute_Nexthop(&LocalRoute)) {

                                del_RIB_Host(&LocalRoute,DataPoint);
                            } else {

                                del_RIB_Route(&LocalRoute,DataPoint);
                            }

                        add_LocalRoute_State(&LocalRoute,INVALID);
                        update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&LocalRoute);
                        if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_ONGOING || get_status (
                                get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute)) == TIMEOUT_RESTARTED ){

                            abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&LocalRoute));
                        }
                        rest = 1;
                    }
                }
                if(rest != 1){

                    printf ("EXTERNAL: FAIL FIND LOCAL ROUTE SET\n");
                    return FAIL_OPERATION;
                }
                DataPoint->black_Address = Neighbor.IPAddress;
                pthread_t BLACKLIST_TIMER_ID = get_LocalRoute_Nexthop(&best_route);
                err = pthread_create(&BLACKLIST_TIMER_ID,NULL,timer_MAX_BLACKLIST_TIME,(void*)DataPoint);
                if(err != 0){

                    printf("send_RREP_Packet: pthread_create BLACKLIST_TIMER_ID  FAIL\n");
                    return FAIL_OPERATION;
                }
                return Neighbor_UPDATE_BLACKLISTED;
            }
		}
	}
    if(get_LocalRoute_State(&best_route) == IDLE || get_LocalRoute_State(&best_route) == ACTIVE){

        err = RREP_GenerationProcess(rreq, DataPoint, &rrep);
        if ( err == CORRECT_OPERATION ) {

            nullPointSerialized(&pointers);
            packet = get_RREP_Packet(rrep, &pointers);
            buffer = get_Packet_Array(packet, &size, &pointers);
            err = send_PacketUdpUnicast(buffer, size, get_LocalRoute_Nexthop(&best_route),
                                        get_LocalRoute_NextHopInterface(&best_route));
            {
                freePointSerialized(&pointers);
                nullPointSerialized(&pointers);
                return err == FAIL_OPERATION ? FAIL_OPERATION : CORRECT_OPERATION;
            }
        }
        else {
            printf ("send_RREP_Packet : RREP_GenerationProcess: FAIL\n");
            return  FAIL_OPERATION;
        }
    }
}
T_ADVRTE RREP_ProcessingReceivedRouteInformation(T_RREP *rrep, uint32_t SourceAddress, T_MetricSet *MetricSet){

    T_ADVRTE advrte;
    advrte.Address = get_RREP_TargPrefix(rrep);
    advrte.PrefixLen = get_RREP_TargPrefixLen(rrep);
    advrte.SeqNum = get_RREP_TargSeqNum(rrep);
    advrte.NextHop = SourceAddress;
    advrte.MetricType = get_RREP_MetricType(rrep);
    advrte.Metric = get_RREP_TargMetric(rrep);
    advrte.Cost = calculate_incoming_link_cost(get_RREP_TargMetric(rrep),MetricSet);
    return advrte;
}
int receipt_RREP_update_NeighborSet(T_DataPoint *DataPoint, T_DataPacket *DataPacket){

    int err;
	T_Neighbor Neighbor = {0};
    err = get_NeighborSet_Neighbor_Address(get_DataPoint_NeighborSet(DataPoint),&Neighbor,
                                           get_DataPacket_sourceIP(DataPacket));
    if(err == NOTFOUND_ITEM ){

        add_Neighbor_IPaddress(&Neighbor, get_DataPacket_sourceIP(DataPacket));
        add_Neighbor_State(&Neighbor,CONFIRMED);
        add_Neighbor_Timeout(&Neighbor,INFINITY_TIME);
        add_Neighbor_Interface(&Neighbor, get_DataPacket_IntDev(DataPacket));
        err = add_NeighborSet_Neighbor(DataPoint->point_NeighborSet,Neighbor);
        if( err != CORRECT_OPERATION){

            return err;
        }
        return CORRECT_OPERATION;
	}
    add_Neighbor_State(&Neighbor,CONFIRMED);
    add_Neighbor_Timeout(&Neighbor,INFINITY_TIME);
    update_NeighborSet_Neighbor(get_DataPoint_NeighborSet(DataPoint),&Neighbor);
	return CORRECT_OPERATION;
}
int forwarding_rrep_packet(T_RREP *rrep,T_DataPoint *DataPoint){

    int err;
    uint8_t size = 0;
    T_PgmanPacket packet;
    uint8_t *buffer = NULL;
    T_LocalRoute best_route;
    T_PointSerialized pointers;

    err = RREP_ForwardingProcess(rrep, DataPoint, &best_route);
    if (err != CORRECT_OPERATION) {

        printf("forwarding_rrep_packet : RREP_ForwardingProcess: FAIL\n");
        return err;
    }

    nullPointSerialized(&pointers);
    packet = get_RREP_Packet(*rrep, &pointers);
    buffer = get_Packet_Array(packet, &size, &pointers);
    err = send_PacketUdpUnicast(buffer, size, get_LocalRoute_Nexthop(&best_route),
                                get_LocalRoute_NextHopInterface(&best_route));
    {
        freePointSerialized(&pointers);
        nullPointSerialized(&pointers);
        return err == FAIL_OPERATION ? FAIL_OPERATION : CORRECT_OPERATION;
    }
}
