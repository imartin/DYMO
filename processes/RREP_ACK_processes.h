#ifndef RREP_ACK_PROCESSES_H
#define RREP_ACK_PROCESSES_H

#include <stdint.h>
#include "../data_set/NeighborSet.h"
#include "../data_set/LocalRouteSet.h"
#include "../RFC5444/gman_packet_parse.h"
#include "../RFC5444/gman_packet_serialized.h"
#include "../RFC5444/parse_container.h"
#include "../global_structs.h"

int send_RREP_Ack_AckReq(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint);
int send_RREP_Ack(uint32_t ipaddress,char interface[256]);
int rrep_ack_reception_process(T_DataPacket *DataPacket,T_PointParsed *pointers_parse);
#endif
