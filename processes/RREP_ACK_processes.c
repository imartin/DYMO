#include "RREP_ACK_processes.h"
#include <sys/time.h>
#include "../event_definition.h"
#include "../operative_system/operative_system_functions.h"
#include "../monitors_pthreads_v3/thread_message.h"
#include "../monitors_pthreads_v3/thread_queue_manager.h"

int send_RREP_Ack(uint32_t ipaddress,char interface[256]){

    int rest;
    T_PgmanPacket packet;
    uint8_t *buffer = NULL;
    uint8_t size = 0;
    T_PointSerialized pointers;
    nullPointSerialized(&pointers);
    packet = get_RREPACK_Packet(ZERO, &pointers);
    buffer= get_Packet_Array(packet, &size, &pointers);
    rest = send_PacketUdpUnicast(buffer, size, ipaddress, interface);
    if(rest!=CORRECT_OPERATION){
        printf("EXTERNAL: send_PacketUdpUnicast FAIL\n");
    }
	freePointSerialized(&pointers);
    nullPointSerialized(&pointers);
    return rest;
}
int send_RREP_Ack_AckReq(T_LocalRoute *LocalRoute,T_DataPoint *DataPoint){

	int rest;
	int err;
    uint8_t size=0;
    struct timeval now;
	T_PgmanPacket packet;
	uint8_t *buffer = NULL;
	T_PointSerialized pointers;
    T_Neighbor Neighbor;

    nullPointSerialized(&pointers);
	packet = get_RREPACK_Packet(ONE, &pointers);
	buffer = get_Packet_Array(packet, &size, &pointers);
	rest = send_PacketUdpUnicast(buffer, size, LocalRoute->NextHop, LocalRoute->NextHopInterface);
	if(rest == CORRECT_OPERATION){

		err = get_NeighborSet_Neighbor_Address(get_DataPoint_NeighborSet(DataPoint),&Neighbor,LocalRoute->NextHop);
		if(err != CORRECT_OPERATION){
			return err;
		}
		gettimeofday(&now, NULL);
		add_Neighbor_Timeout(&Neighbor,(((now.tv_sec + RREP_ACK_SENT_TIMEOUT)*1000000) + now.tv_usec));
		update_NeighborSet_Neighbor(get_DataPoint_NeighborSet(DataPoint),&Neighbor);
		return err;

	}
	else{
		printf("EXTERNAL: send_PacketUdpUnicast FAIL\n");
	}
	freePointSerialized(&pointers);
    nullPointSerialized(&pointers);

	return rest;
}
int rrep_ack_reception_process(T_DataPacket *DataPacket,T_PointParsed *pointers_parse){

	T_PgmanPacket packet;
	uint8_t acknowledgement;
	packet = get_parse_array(DataPacket->PayLoad,pointers_parse);
	acknowledgement = get_parse_container_rrep_ack(packet);
	if(acknowledgement==ONE){

        printf("ACK_ACTIVITY:RREP ACK ACREQ RECIVED %s\n", Int32_To_String(DataPacket->sourceIP));
        return RREP_ACK_RESPONSE;
    }
	printf("ACK_ACTIVITY:RREP ACK RECIVED%s\n", Int32_To_String(DataPacket->sourceIP));
    ThreadMessage *message = create_message(0,EXIT_RREP_ACK_RECEIVED,DataPacket);
    put_message(message,MESSAGE_QUEUE_ACK_to_EXTERNAL);
	return CORRECT_OPERATION;
}