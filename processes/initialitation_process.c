#include "initialitation_process.h"

#include <stdio.h>//ssize_t
#include <stdint.h> /* int8_t*/
#include <unistd.h>//sleep()
#include <pthread.h>

#include "../constant_definition.h"

void  *initialitation_process(void *arg){
	uint16_t *sequence_number = (uint16_t *) arg;
	*sequence_number = 1;
	printf("T_Timer MAX_SEQNUM_LIFETIME on\n");
	sleep(MAX_SEQNUM_LIFETIME);
	printf("T_Timer MAX_SEQNUM_LIFETIME off\n");
	pthread_exit(0);

}

