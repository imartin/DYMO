
#include "Suppressing_Redundant_RouteError_Messages_Process.h"
#include <stdio.h>
#include <curses.h>
#include "../event_definition.h"
#include "../Timers/timers.h"

int SupRedRouteErrorMsgProcesss(T_RerrMsgSet *RerrMsgSet,uint32_t UnreachableAddress,T_CIDR *PktSource,T_DataPoint *DataPoint){

    T_RerrMsg RerrMsg;
    int err;
    uint32_t pktsource = 0;

/*-To determine if a RERR should be created:-*/
    goto STEP_1;

    STEP_1: {

        for(int i = 0; i < get_RerrMsgSet_Overall(RerrMsgSet); i++) {

            err = get_RerrMsgSet_RerrMsg_Index(RerrMsgSet, &RerrMsg, i);
            if(err != CORRECT_OPERATION) {

                printf("SupRedRerrMsgMsgProcesss : get_RerrMsgSet_RerrMsg_Index FAIL\n");
                return FAIL_OPERATION;
            }

/*-Search for an entry in the Route Error Set-*/
            if(PktSource != NULL) {

                pktsource = Int8_To_32(*PktSource);
                if(pktsource == get_RerrMsg_PktSource(&RerrMsg) &&
                    (UnreachableAddress == get_RerrMsg_UnreachableAddress(&RerrMsg))) {

/*-If a matching entry is found, no further processing is required and the RERR SHOULD NOT be sent.-*/
                    return FOUND_ITEM;
                }
            }else{

/*-If a matching entry is found, no further processing is required and the RERR SHOULD NOT be sent.-*/
                if(UnreachableAddress == get_RerrMsg_UnreachableAddress(&RerrMsg)) {

                    return FOUND_ITEM;
                }
            }
        }
        goto STEP_2;
    }

    STEP_2:{

/*-If no matching entry is found, a new entry with the following properties is created-*/
        add_RerrMsg_TimeOut(&RerrMsg, get_CTime_ms() + (RERR_TIMEOUT * MICRO));
        add_RerrMsg_UnreachableAddress(&RerrMsg,UnreachableAddress);
    if (PktSource != NULL) {
        add_RerrMsg_PktSource(&RerrMsg, pktsource);
    } else {
        add_RerrMsg_PktSource(&RerrMsg, 0);
    }
        err = add_RerrMsgSet_RerrMsg(RerrMsgSet,&RerrMsg);
        if(err != CORRECT_OPERATION){

            printf("SupRedRouteErrorMsgProcesss :add_RerrMsgSet_RerrMsg FAIL\n");
            return FAIL_OPERATION;
        }else {

            add_DataPoint_RerrMsg(DataPoint,&RerrMsg);
            pthread_t RERR_TIMEOUT_ID = UnreachableAddress;
            err = pthread_create(&RERR_TIMEOUT_ID, NULL,timer_RERR_TIMEOUT,(void*)DataPoint);
            if (err != 0){

                printf("send_RREP_Packet : pthread_create: [%s]\n", strerror(err));
                return	FAIL_OPERATION;
            }
            return NOTFOUND_ITEM;
        }
    }
}