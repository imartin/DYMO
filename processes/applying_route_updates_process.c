
#include "applying_route_updates_process.h"
#include <curses.h>
#include "../Timers/timers.h"
#include "../event_definition.h"
#include "../operative_system/operative_system_functions.h"

int AppRouteUpdate_Process(T_DataPoint *DataPoint, char *indev, T_ADVRTE *advrte,T_LocalRouteSet *match_Routes){

    int err;
    int index;
    T_LocalRoute match_route={0};
    T_LocalRoute step4_route={0};
    T_LocalRoute step5_route;
    T_LocalRoute step5_1_route;
    T_LocalRoute step6_route;
    T_LocalRoute unconfirmed_route={0};
    T_LocalRoute valid_route={0};
    T_Neighbor Neighbor;
    pthread_t MAX_SEQNUM_LIFETIME_id;
    bool FLAG_ADD_ROUTE = FALSE;
    bool FLAG_UPDATE_ROUTE = FALSE;

    if(match_Routes->overall == 0){

        goto STEP_4;
    }
    if(match_Routes->overall == 2) {

        err = get_NeighborSet_Neighbor_Address (get_DataPoint_NeighborSet(DataPoint),&Neighbor,advrte->NextHop);
        if(err != CORRECT_OPERATION){

            return err;
        }
        if ((match_Routes->LocalRoute[0].State == UNCONFIRMED) &&
        ((match_Routes->LocalRoute[1].State == ACTIVE) ||
        (match_Routes->LocalRoute[1].State == IDLE))) {

            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &valid_route, 1);
            if(err != CORRECT_OPERATION){

                return err;
            }
            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &unconfirmed_route, 0);
            if(err != CORRECT_OPERATION){

                return err;
            }
            if (get_Neighbor_State(&Neighbor) == HEARD) {

                match_route = unconfirmed_route;
                FLAG_UPDATE_ROUTE = TRUE;
                step5_route = match_route;
                goto STEP_5;

            }
            if (get_Neighbor_State(&Neighbor) == CONFIRMED) {

                match_route = valid_route;
                FLAG_UPDATE_ROUTE = TRUE;
                step5_route = match_route;
                goto STEP_5;
            }
        }else if((match_Routes->LocalRoute[1].State == UNCONFIRMED) &&
                ((match_Routes->LocalRoute[0].State == ACTIVE) ||
                (match_Routes->LocalRoute[0].State == IDLE))) {

            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &unconfirmed_route, 1);
            if(err != CORRECT_OPERATION){

                return err;
            }
            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &valid_route, 0);
            if(err != CORRECT_OPERATION){

                return err;
            }
            if (get_Neighbor_State(&Neighbor) == HEARD) {

                match_route = unconfirmed_route;
                FLAG_UPDATE_ROUTE = TRUE;
                step5_route = match_route;
                goto STEP_5;
            }
            if (get_Neighbor_State(&Neighbor) == CONFIRMED) {

                match_route = valid_route;
                FLAG_UPDATE_ROUTE = TRUE;
                step5_route = match_route;
                goto STEP_5;
            }
        }else{

            goto STEP_EXTRA;
        }
    }
    if(match_Routes->overall == 1) {

        err = get_LocalRouteSet_LocalRoute_Index(match_Routes, &match_route, 0);
        if(err != CORRECT_OPERATION){

            return err;
        }
        err = get_NeighborSet_Neighbor_Address (get_DataPoint_NeighborSet(DataPoint),&Neighbor,get_ADVRTE_Nexthop(advrte));
        if(err != CORRECT_OPERATION){

            return err;
        }
        if(get_Neighbor_State(&Neighbor) == CONFIRMED) {

            step5_route = match_route;
            FLAG_UPDATE_ROUTE = TRUE;
            goto STEP_5;
        }
        if(get_LocalRoute_State(&match_route) == UNCONFIRMED){

            step5_route = match_route;
            FLAG_UPDATE_ROUTE = TRUE;
            goto STEP_5;
        }
        if(get_LocalRoute_State(&match_route) == INVALID) {

            step5_route = match_route;
            FLAG_UPDATE_ROUTE = TRUE;
            goto STEP_5;
        }
        if(get_LocalRoute_State(&match_route) == ACTIVE ||  get_LocalRoute_State(&match_route) == IDLE){

            step4_route.State = UNCONFIRMED;
            goto STEP_4;
        }
    }

    STEP_4:{

        add_LocalRoute_Address(&step4_route,get_ADVRTE_Address(advrte));
        add_LocalRoute_Prefixlength(&step4_route,get_ADVRTE_Prefixlength(advrte));
        add_LocalRoute_Metrictype(&step4_route,get_ADVRTE_Metrictype(advrte));

        err = add_LocalRouteSet_LocalRoute (get_DataPoint_LocalRouteSet(DataPoint),&step4_route);
        if(err != CORRECT_OPERATION){

            printf ("UPDATE LRS: add_LocalRouteSet_LocalRoute FULL ARRAY\n");
            return FAIL_OPERATION;
        }
        step5_route = step4_route;
        FLAG_ADD_ROUTE = TRUE;
        goto STEP_5;
    }

    STEP_5:{

        step5_1_route = step5_route;
        add_LocalRoute_Seqnum(&step5_route,get_ADVRTE_Seqnum(advrte));
        add_LocalRoute_Nexthop(&step5_route,get_ADVRTE_Nexthop(advrte));
        add_LocalRoute_NextHopInterface(&step5_route,indev);
        add_LocalRoute_Metric(&step5_route,get_ADVRTE_Cost(advrte));
        add_LocalRoute_Lastused(&step5_route, get_CTime_ms());
        add_LocalRoute_Lastseqnumupdate(&step5_route, get_CTime_ms());
        index = get_LocalRouteSet_Index_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&step5_1_route);
        if(index == NOTFOUND_ITEM){

            printf("STEP 5 : get_LocalRouteSet_Index_LocalRoute Fail\n");
            return FAIL_OPERATION;
        }
        add_LocalRoute_Position(&step5_route,index);
        update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint), &step5_route);


        step6_route = step5_route;
        goto STEP_6;
    }

    STEP_6:{

        if(((get_LocalRoute_State(&match_route) == INVALID) || (get_LocalRoute_State(&match_route) == UNCONFIRMED)) ||
           (FLAG_ADD_ROUTE == TRUE)){

            err = get_NeighborSet_Neighbor_Address (get_DataPoint_NeighborSet(DataPoint),&Neighbor,get_LocalRoute_Nexthop(&step5_route));
            if(err != CORRECT_OPERATION){

                return err;
            }
            if (get_Neighbor_State(&Neighbor) == HEARD) {

                add_LocalRoute_State(&step6_route,UNCONFIRMED);
                update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint), &step6_route);
                if(get_status(get_LocalRoute_TimerMaxSeqNumLifeTime (&step6_route)) == TIMEOUT_UNDEFINED) {

                    add_DataPoint_LocalRoute (DataPoint,&step6_route);
                    err = pthread_create(&MAX_SEQNUM_LIFETIME_id, NULL,timer_MAX_SEQNUM_LIFETIME,(void*)DataPoint);
                    if (err != 0){

                        printf("send_RREP_Packet : pthread_create: [%s]\n", strerror(err));
                        return	FAIL_OPERATION;
                    }
                }
                if(get_status(get_LocalRoute_TimerMaxSeqNumLifeTime (&step6_route)) == TIMEOUT_ONGOING ||
                   get_status(get_LocalRoute_TimerMaxSeqNumLifeTime (&step6_route)) == TIMEOUT_RESTARTED) {

                    restart_timer(get_LocalRoute_TimerMaxSeqNumLifeTime (&step6_route),get_Timer_Timeout (
                            get_LocalRoute_TimerMaxSeqNumLifeTime (&step6_route)));
                }
            }
            if (get_Neighbor_State(&Neighbor) == CONFIRMED) {

                add_LocalRoute_State(&step6_route,IDLE);
                if(get_status(get_LocalRoute_TimerMaxSeqNumLifeTime (&step6_route)) == TIMEOUT_ONGOING ||
                   get_status(get_LocalRoute_TimerMaxSeqNumLifeTime (&step6_route)) == TIMEOUT_RESTARTED) {

                    abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&step6_route));
                }
                if (get_LocalRoute_Address(&step6_route) == get_LocalRoute_Nexthop(&step6_route)) {

                    add_RIB_Host(&step6_route,DataPoint);
                } else {

                    add_RIB_Route(&step6_route,DataPoint);
                }
                update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint), &step6_route);
            }
        }
        //return CORRECT_OPERATION;
        goto STEP_7;
    }

    STEP_7:{

        T_LocalRoute route;
        if(((get_LocalRoute_State(&step5_route) == INVALID)||(get_LocalRoute_State(&step5_route) == UNCONFIRMED)) &&
            (get_LocalRoute_State(&step6_route) == IDLE)){
            for(int i = 0; i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++ ){

                err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &route, i);
                if(get_LocalRoute_Address(&route) == get_LocalRoute_Address(&step6_route) &&
                   (get_LocalRoute_Metrictype(&route) == get_LocalRoute_Metrictype(&step6_route)) &&
                   (get_LocalRoute_Prefixlength(&route) == get_LocalRoute_Prefixlength(&step6_route)) &&
                   (get_LocalRoute_Prefixlength(&route) == UNCONFIRMED) &&
                   (get_LocalRoute_Metric(&route) > get_LocalRoute_Metric(&step6_route))){

                    if(err == CORRECT_OPERATION){

                        del_LocalRouteSet_LocalRoute_Position(get_DataPoint_LocalRouteSet(DataPoint),&route);
                        if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&route)) == TIMEOUT_ONGOING || get_status (
                                get_LocalRoute_TimerMaxSeqNumLifeTime (&route)) == TIMEOUT_RESTARTED ){

                            abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&route));
                        }

                        if (get_LocalRoute_Address(&route) == get_LocalRoute_Nexthop(&route)) {

                            add_RIB_Host(&step6_route,DataPoint);
                        } else {

                            add_RIB_Route(&step6_route,DataPoint);
                        }
                    } else{

                        printf("STEP 7 : get_LocalRouteSet_LocalRoute_Index FAIL\n");
                        return NOTFOUND_ITEM;
                    }
                }
            }
        }
        goto STEP_8;
    }
    STEP_8:{

        T_LocalRoute route;
        if(FLAG_UPDATE_ROUTE == TRUE){

            if(get_LocalRoute_Metric(&match_route) > get_LocalRoute_Metric(&step6_route)){

                for(int i = 0; i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++ ){

                    err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &route, i);
                    if(get_LocalRoute_Address(&route) == get_LocalRoute_Address(&step6_route) &&
                       (get_LocalRoute_Metrictype(&route) == get_LocalRoute_Metrictype(&step6_route)) &&
                       (get_LocalRoute_Prefixlength(&route) == get_LocalRoute_Prefixlength(&step6_route)) &&
                       (get_LocalRoute_Prefixlength(&route) == UNCONFIRMED) &&
                       (get_LocalRoute_Metric(&route) > get_LocalRoute_Metric(&step6_route))){

                        if(err == CORRECT_OPERATION){

                            del_LocalRouteSet_LocalRoute_Position(get_DataPoint_LocalRouteSet(DataPoint),&route);
                            if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&route)) == TIMEOUT_ONGOING || get_status (
                                    get_LocalRoute_TimerMaxSeqNumLifeTime (&route)) == TIMEOUT_RESTARTED ){

                                abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&route));
                            }
                            if (get_LocalRoute_Address(&route) == get_LocalRoute_Nexthop(&route)) {

                                add_RIB_Host(&step6_route,DataPoint);
                            } else {

                                add_RIB_Route(&step6_route,DataPoint);
                            }
                        } else{

                            printf("STEP 8: get_LocalRouteSet_LocalRoute_Index FAIL\n");
                            return NOTFOUND_ITEM;
                        }
                    }
                }
            }
        }
       goto STEP_EXTRA;
    }
    STEP_EXTRA:
    {

        T_LocalRoute route;
        int a = 0;
        T_LocalRouteSet result_routes={0};
        ini_LocalRouteSet(&result_routes);
        for(int i = 0; i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++){

            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &route, i);
            if( err != CORRECT_OPERATION){

                printf("STEP EXTRA : get_LocalRouteSet_LocalRoute_Index FAIL\n");
                return err;
            }
            if(get_LocalRoute_Address(&route) == get_ADVRTE_Address(advrte)){

                add_LocalRouteSet_LocalRoute (&result_routes,&route);
                result_routes.LocalRoute[a].Position = i;
                a++;
            }
        }
        if(get_LocalRouteSet_Overall(&result_routes) == 2){

            if(result_routes.LocalRoute[0].Metric > result_routes.LocalRoute[1].Metric){

                result_routes.LocalRoute[1].State = UNCONFIRMED;
                update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&result_routes.LocalRoute[1]);
                if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&result_routes.LocalRoute[ 1 ])) == TIMEOUT_UNDEFINED){

                    add_DataPoint_LocalRoute (DataPoint,&result_routes.LocalRoute[1]);
                    update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&result_routes.LocalRoute[1]);
                    err = pthread_create(&MAX_SEQNUM_LIFETIME_id, NULL,timer_MAX_SEQNUM_LIFETIME,(void*)DataPoint);
                    if (err != 0){

                        printf("send_RREP_Packet : pthread_create: [%s]\n", strerror(err));
                        return	FAIL_OPERATION;
                    }
                }
                return CORRECT_OPERATION;
            }
            if(result_routes.LocalRoute[0].Metric < result_routes.LocalRoute[1].Metric){

                result_routes.LocalRoute[0].State = UNCONFIRMED;
                update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&result_routes.LocalRoute[0]);
                if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&result_routes.LocalRoute[ 0 ])) == TIMEOUT_UNDEFINED){

                    add_DataPoint_LocalRoute (DataPoint,&result_routes.LocalRoute[0]);
                    update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&result_routes.LocalRoute[0]);
                    err = pthread_create(&MAX_SEQNUM_LIFETIME_id, NULL,timer_MAX_SEQNUM_LIFETIME,(void*)DataPoint);
                    if (err != 0){

                        printf("send_RREP_Packet : pthread_create: [%s]\n", strerror(err));
                        return	FAIL_OPERATION;
                    }
                }
                return CORRECT_OPERATION;
            }
            return CORRECT_OPERATION;
        }
    }
    return CORRECT_OPERATION;
}

/*if ((matching_routes->LocalRoute[0].State == UNCONFIRMED) &&
        ((matching_routes->LocalRoute[1].State == ACTIVE) ||
        (matching_routes->LocalRoute[1].State == IDLE))) {

            //printf ("UPDATE LRS: STEP 2.1\n");
            err = get_LocalRouteSet_LocalRoute_Position(get_DataPointers_LocalRouteSet(DataPoint),&valid_route,1);
            if(err != CORRECT_OPERATION){

                return err;
            }
            err = get_LocalRouteSet_LocalRoute_Position(get_DataPointers_LocalRouteSet(DataPoint),&unconfirmed_route,0);
            if(err != CORRECT_OPERATION){

                return err;
            }
        }else if((matching_routes->LocalRoute[1].State == UNCONFIRMED) &&
                ((matching_routes->LocalRoute[0].State == ACTIVE) ||
                (matching_routes->LocalRoute[0].State == IDLE))) {

            //printf ("UPDATE LRS: STEP 2.2\n");
            err = get_LocalRouteSet_LocalRoute_Position(get_DataPointers_LocalRouteSet(DataPoint),&unconfirmed_route,1);
            if(err != CORRECT_OPERATION){

                return err;
            }
            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint),&valid_route,0);
            if(err != CORRECT_OPERATION){

                return err;
            }
        }*/