#ifndef RREQ_PROCESSES_H
#define RREQ_PROCESSES_H

#include <stdint.h> /* int8_t*/
#include "data/RouterClient.h"
#include "containers/RREQ_container.h"
#include "containers/ADVRTE_container.h"
#include "../global_structs.h"
#include "data_set/MetricSet.h"
#include "../RFC5444/gman_packet_parse.h"



int forwar_RREQ_Packet(T_RREQ *rreq, T_DataPoint *DataPoint);
int send_RREQ_Packet(T_RouterClient *RouterClient, T_DataPacket *DataPacket, T_DataPoint *DataPoint);

int RREQ_ReceptionProcess(T_DataPoint *DataPoint, T_ADVRTE *pointer_advrte, T_DataPacket *DataPacket, T_RREQ *rreq,T_PointParsed *pointers_parse);
int RREQ_GenerationProcess(T_RREQ *rreq, T_DataPacket *DataPacket, T_RouterClient *RouterClient, T_DataPoint *DataPoint);
int RREQ_ForwardingProcess(T_RREQ *rreq, T_DataPoint *DataPoint);
int RREQ_NeighborSetUpdateProcess(T_DataPoint *DataPoint, T_DataPacket *DataPacket);
T_ADVRTE RREQ_ProcessingReceivedRouteInformation(T_RREQ *rreq, uint32_t SourceAddress, T_MetricSet *MetricSet);

#endif
