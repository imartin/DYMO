#include "suppressing_redundant_messages_process.h"
#include <stdio.h>

void get_McMsg(T_RREQ *rreq,char interface[256],T_McMsg *McMsg);


int SupRedMsg_Process(T_DataPoint *DataPoint, T_RREQ *rreq, char *interface) {

    int err;
    T_McMsg McMsg;

    goto STEP_1;

    STEP_1:
    {
        for (int i = 0; i < get_McMsgSet_Overall(get_DataPoint_McMsgSet(DataPoint)); i++) {

            err = get_McMsgSet_McMsg_Index(get_DataPoint_McMsgSet(DataPoint), &McMsg, i);
            if (err != CORRECT_OPERATION) {

                printf("get_McMsgSet_McMsg_Index FAIL\n");
                return FAIL_OPERATION;
            }
            if (get_McMsg_OrigPrefix(&McMsg) == get_RREQ_OrigPrefix(rreq)){

                if (get_McMsg_OrigPrefixLen(&McMsg) == get_RREQ_OrigPrefixLen(rreq)){

                    if(get_McMsg_TargPrefix(&McMsg) == get_RREQ_TargPrefix(rreq)){

                        if(strcmp(get_McMsg_Interface(&McMsg),interface) == 0){

                            if(get_McMsg_MetricType(&McMsg) == get_RREQ_MetricType(rreq)){

                                goto STEP_2;
                            }
                        }
                    }
                }
            }
        }
        goto NEW_MESSAGE_MCMSG;
    }
    STEP_2:
    {

        if(McMsg.OrigSeqNum < get_RREQ_OrigSeqNum(rreq)){

            goto MESSAGE_NOT_REDUNDANT;
        }else if(McMsg.OrigSeqNum > get_RREQ_OrigSeqNum(rreq)){

            goto MESSAGE_REDUNDANT;
        }else{

            goto STEP_3;
        }
    }
    STEP_3:{

        if(McMsg.Metric >= get_RREQ_OrigMetric(rreq)){

            goto MESSAGE_REDUNDANT;
        }else{

            goto MESSAGE_NOT_REDUNDANT;
        }
    }
    MESSAGE_REDUNDANT:{

        long long int t = get_CTime_ms();
        add_McMsg_TimeStamp(&McMsg, t);
        add_McMsg_RemovalTime(&McMsg,t + ( MAX_SEQNUM_LIFETIME * MICRO));
        update_McMsgSet_McMsg_Position(get_DataPoint_McMsgSet(DataPoint),&McMsg);
        return REDUNDANT;
    }
    MESSAGE_NOT_REDUNDANT:{

        get_McMsg(rreq,interface,&McMsg);
        update_McMsgSet_McMsg_Position(get_DataPoint_McMsgSet(DataPoint),&McMsg);
        return NOT_REDUNDANT;
}
    NEW_MESSAGE_MCMSG:{

        T_McMsg newMcMsg;
        get_McMsg(rreq,interface,&newMcMsg);
        pthread_mutex_lock(DataPoint->Mutex);
        err = add_McMsgSet_McMsg(DataPoint->point_McMsgSet, &newMcMsg);
        pthread_mutex_unlock(DataPoint->Mutex);
        if (err != CORRECT_OPERATION) {

            return FAIL_OPERATION;
        } else {

            return NOT_REDUNDANT;
        }
    }
}

void get_McMsg(T_RREQ *rreq,char interface[256],T_McMsg *McMsg){

    McMsg->OrigPrefix = get_RREQ_OrigPrefix(rreq);
    McMsg->OrigPrefixLen = get_RREQ_OrigPrefixLen(rreq);
    McMsg->TargPrefix = get_RREQ_TargPrefix(rreq);
    McMsg->OrigSeqNum = get_RREQ_OrigSeqNum(rreq);
    McMsg->TargSeqNum = get_RREQ_TargSeqNum(rreq);
    McMsg->MetricType = get_RREQ_MetricType(rreq);
    McMsg->Metric = get_RREQ_OrigMetric(rreq);
    strcpy(McMsg->Interface,interface);
    long long int t = get_CTime_ms();
    McMsg->TimeStamp = t;
    McMsg->RemovalTime = t + ( MAX_SEQNUM_LIFETIME * MICRO);
}