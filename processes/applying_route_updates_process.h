#ifndef APPLYING_ROUTE_UPDATES_PROCESS_H
#define APPLYING_ROUTE_UPDATES_PROCESS_H

#include "../global_structs.h"
#include "../data_set/LocalRouteSet.h"
#include "containers/ADVRTE_container.h"

int AppRouteUpdate_Process(T_DataPoint *DataPoint, char *indev, T_ADVRTE *advrte,
                           T_LocalRouteSet *match_Routes);

#endif
