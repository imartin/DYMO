

#include "RERR_processes.h"
#include <curses.h>
#include "Suppressing_Redundant_RouteError_Messages_Process.h"
#include "../event_definition.h"
#include "../RFC5444/gman_packet_serialized.h"
#include "../operative_system/operative_system_functions.h"

int RERR_GenerationProcess(T_DataPoint *DataPoint, T_DataPacket *DataPacket,T_RerrPoint *RERRpoint,int Event){

    int err;
    uint8_t size = 0;
    uint32_t broadaddr;
    uint32_t pktsource;
    T_CIDR  temp;
    bool FLAG_ROUTE_REPORTED;
    T_AddressListRERR list;
    T_LocalRoute invRoute;
    T_RERR rerr;
    T_LocalRouteSet invRoutes;
    T_PointSerialized pointSerialized;

    T_CIDR *PktSource = (T_CIDR *) malloc(sizeof(T_CIDR));
    T_SeqNumList *SeqNumList = (T_SeqNumList *) malloc(sizeof(T_SeqNumList));
    T_MetricTypeList *MetricTypeList = (T_MetricTypeList *) malloc(sizeof(T_MetricTypeList));
    T_PrefixLenList *PrefixLenList =(T_PrefixLenList *) malloc(sizeof(T_PrefixLenList));
    if(PrefixLenList == NULL || SeqNumList == NULL|| MetricTypeList == NULL || PktSource == NULL){

        free(PktSource);
        free(SeqNumList);
        free(MetricTypeList);
        free(PrefixLenList);
        return NOENOUGHT_MEMORY;
    }
    RERRpoint->SeqNumList = SeqNumList;
    RERRpoint->MetricTypeList = MetricTypeList;
    RERRpoint->PrefixLenList = PrefixLenList;
    RERRpoint->PktSource = PktSource;

    pktsource = get_DataPacket_soUIP(DataPacket);
    ini_PrefixLenList(PrefixLenList);
    ini_SeqNumList(SeqNumList);
    ini_MetricTypeList(MetricTypeList);
    ini_AddressListRERR(&list);
    FLAG_ROUTE_REPORTED = FALSE;

    switch(Event){

        /*-When an IP packet that has been forwarded from another router, but there is no valid route in the Routing Information Base for its destination-*/
        case 1:{
            printf("RERRGENERA_EVENT_1\n");
            err = SupRedRouteErrorMsgProcesss(get_DataPoint_RerrMsgSet(DataPoint), get_DataPacket_deUIP(DataPacket),NULL,DataPoint);
            if( err == FAIL_OPERATION) {

                printf("SupRedRouteErrorMsgProcesss : FAIL\n");
                return FAIL_OPERATION;
            }
            if(err == FOUND_ITEM){

                printf("RERR existe en el SET\n");
                return CORRECT_OPERATION;
            }

            /*The RERR generated MUST include PktSource set to the source Address of the IP packet*/
            *PktSource = get_CIDR(get_DataPacket_soUIP(DataPacket), 32);
            temp = get_CIDR(get_DataPacket_deUIP(DataPacket), 32);
            if(add_AddressListRERR_Address(temp, &list) != ADDED_ITEM){

                return FAIL_OPERATION;
            }

             /*The prefix length, sequence number and Metric type SHOULD be included if known from an existing Invalid LocalRoute to the unreachable Address.*/
            for(int i = 0; i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++ ){

                err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &invRoute, i);
                if( err != CORRECT_OPERATION){

                    printf("RERR_GenerationProcess : get_LocalRouteSet_LocalRoute_Index\n");

                    return FAIL_OPERATION;
                }
                if (get_LocalRoute_Address(&invRoute) == get_DataPacket_deUIP(DataPacket) &&  (get_LocalRoute_State(&invRoute) == INVALID)){

                    if((add_PrefixLenList_PrefixLen(invRoutes.LocalRoute[i].PrefixLength, PrefixLenList)!= ADDED_ITEM) ||
                       (add_SeqNumList_SeqNum(invRoutes.LocalRoute[i].SeqNum, SeqNumList) != ADDED_ITEM) ||
                       (add_MetricTypeList_MetricTye(invRoutes.LocalRoute[i].MetricType, MetricTypeList) != ADDED_ITEM)){

                        printf("add_Item RERR FAIL\n");
                        return FAIL_OPERATION;
                    }else{

                        goto SEND_MESSAGE;
                    }
                }
            }
            goto SEND_MESSAGE;
        }

/*-When an RREP message cannot be forwarded because the LocalRoute to OrigPrefix has been lost or is Invalid-*/
        case 2: {
            printf("RERRGENERA_EVENT_2\n");
            err = SupRedRouteErrorMsgProcesss(get_DataPoint_RerrMsgSet(DataPoint), get_DataPoint_OrigPrefixRREP(DataPoint),NULL,DataPoint);
            if( err == FAIL_OPERATION) {

                printf("SupRedRouteErrorMsgProcesss : FAIL\n");
                return FAIL_OPERATION;
            }
            if(err == FOUND_ITEM){

                printf("RERR existe en el SET\n");
                return CORRECT_OPERATION;
            }

            /*-The RERR generated MUST include PktSource set to the TargPrefix of the RREP-*/
            *PktSource = get_DataPoint_AddressList_TargPrefix(DataPoint);
            temp = get_DataPoint_AddressList_OrigPrefix(DataPoint);
            if(add_AddressListRERR_Address(temp, &list) != ADDED_ITEM){

                return FAIL_OPERATION;
            }

            /*The prefix length, sequence number and Metric type SHOULD be included if known from an
             * existing Invalid LocalRoute to the unreachable Address.*/
            for(int i = 0; i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++ ){

                err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &invRoute, i);
                if( err != CORRECT_OPERATION){

                    printf("RERR_GenerationProcess : get_LocalRouteSet_LocalRoute_Index\n");
                    return FAIL_OPERATION;
                }
                if (get_LocalRoute_Address(&invRoute) == Int8_To_32(get_DataPoint_AddressList_OrigPrefix(DataPoint)) &&  (get_LocalRoute_State(&invRoute) == INVALID)){

                    if((add_PrefixLenList_PrefixLen(invRoutes.LocalRoute[i].PrefixLength, PrefixLenList)!= ADDED_ITEM) ||
                       (add_SeqNumList_SeqNum(invRoutes.LocalRoute[i].SeqNum, SeqNumList) != ADDED_ITEM) ||
                       (add_MetricTypeList_MetricTye(invRoutes.LocalRoute[i].MetricType, MetricTypeList) != ADDED_ITEM)){

                        printf("add_Item RERR FAIL\n");
                        return FAIL_OPERATION;
                    }
                    goto SEND_MESSAGE;
                }
            }
            goto SEND_MESSAGE;
        }
        case 3: {

/*-If the RERR is sent in response to a broken link-*/
           
            for(int i = 0; i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++ ) {

                err = get_LocalRouteSet_LocalRoute_Index (get_DataPoint_LocalRouteSet (DataPoint) , &invRoute , i);
                if ( err != CORRECT_OPERATION ) {

                    printf ("RERR_GenerationProcess : get_LocalRouteSet_LocalRoute_Index\n");
                    return FAIL_OPERATION;
                }
                if(get_LocalRoute_Address (&invRoute) == get_DataPacket_deUIP (DataPacket)) {

                    if(SupRedRouteErrorMsgProcesss(get_DataPoint_RerrMsgSet(DataPoint), get_LocalRoute_Address(&invRoute),NULL,DataPoint) == NOTFOUND_ITEM) {

                        err = del_NeighborSet_Neighbor(get_DataPoint_NeighborSet (DataPoint),invRoute.NextHop);
                        if(err == NOTFOUND_ITEM){

                            printf ("Not Found Neighbor\n");
                            return CORRECT_OPERATION;
                        }
                        printf ("Neighbor %s deleted\n",Int32_To_String (invRoute.NextHop));
                        uint8_t state = get_LocalRoute_State(&invRoute);
                        add_LocalRoute_State(&invRoute, INVALID);
                        update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint), &invRoute);
                        if(get_status(get_LocalRoute_TimerMaxSeqNumLifeTime (&invRoute)) == TIMEOUT_ONGOING ||
                           get_status(get_LocalRoute_TimerMaxSeqNumLifeTime (&invRoute)) == TIMEOUT_RESTARTED) {

                            abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&invRoute));
                        }
                        if (state == IDLE || state == ACTIVE) {

                            if (get_LocalRoute_Nexthop(&invRoute) == get_LocalRoute_Address(&invRoute)) {

                                del_RIB_Host(&invRoute,DataPoint);
                            } else {

                                del_RIB_Route(&invRoute,DataPoint);
                            }
                        }
/*-For each LocalRoute that needs to be reported-*/
                        temp = get_CIDR(get_LocalRoute_Address (&invRoute), get_LocalRoute_Prefixlength(&invRoute));
                        if ((add_AddressListRERR_Address(temp, &list) != ADDED_ITEM) ||
                            (add_PrefixLenList_PrefixLen(invRoute.PrefixLength, PrefixLenList)!= ADDED_ITEM) ||
                            (add_SeqNumList_SeqNum(invRoute.SeqNum, SeqNumList) != ADDED_ITEM) ||
                            (add_MetricTypeList_MetricTye(invRoute.MetricType, MetricTypeList)!= ADDED_ITEM)) {

                            return FAIL_OPERATION;
                        } else {

                            FLAG_ROUTE_REPORTED = TRUE;
                        }
                    }
                }
            }

            if( FLAG_ROUTE_REPORTED == FALSE){

                return CORRECT_OPERATION;
            }else{

                goto SEND_MESSAGE;
            }
        }
        default:{
            printf("Invalid case\n");
            return FAIL_OPERATION;
        }
    }
    SEND_MESSAGE:{

        rerr = create_RERR(RERRpoint->PktSource, list, PrefixLenList, SeqNumList, MetricTypeList);

        nullPointSerialized(&pointSerialized);
        print_RERR (&rerr);
/*-The message is constructed with the RFC5444 format from the RERR container-*/
        T_PgmanPacket packet = get_RERR_Packet(rerr,&pointSerialized);

/*-The package is serialized-*/
        uint8_t *buffer = get_Packet_Array(packet, &size, &pointSerialized);
        if (Event == 3) {

            for (int i = 0; i < get_InterfaceSet_Overall(get_DataPoint_InterfaceSet(DataPoint)); i++) {

/*-If the RERR is sent in response to a broken link, i.e., PktSource is
       no included, the RERR is, by default, multicast to LL-MANET-Routers-*/
                broadaddr = get_BroadcastAddress(get_DataPoint_InterfaceId(DataPoint));
                err = send_PacketUdpBroadcast(buffer, size, broadaddr);
                if (err != CORRECT_OPERATION) {

                    printf("send_PacketUdpBroadcast: FAIL\n");
                    freePointSerialized(&pointSerialized);
                    nullPointSerialized(&pointSerialized);
                    return err;
                }else{

                    printf("RERR_MULTICAST_SEND\n");
                }
            }
            freePointSerialized(&pointSerialized);
            nullPointSerialized(&pointSerialized);
            return CORRECT_OPERATION;
        }else {

/*If the RERR is sent in response to an undeliverable IP packet or RREP message (i.e., if PktSource is included), the RERR SHOULD be sent
unicast to the next hop on the route to PktSource*/
            err = get_LocalRouteSet_LocalRoute_Address(get_DataPoint_LocalRouteSet(DataPoint),&invRoute,pktsource);
            if (err != FOUND_ITEM) {

                freePointSerialized(&pointSerialized);
                nullPointSerialized(&pointSerialized);
                return NOTFOUND_ITEM;
            }
            err = send_PacketUdpUnicast(buffer, size, get_LocalRoute_Nexthop(&invRoute),get_LocalRoute_NextHopInterface(&invRoute));
            if (err != CORRECT_OPERATION) {

                freePointSerialized(&pointSerialized);
                nullPointSerialized(&pointSerialized);
                printf("send_PacketUdpUnicast  FAIL\n");
                return err;
            }else{

                printf("RERR SEND\n");
            }
            freePointSerialized(&pointSerialized);
            nullPointSerialized(&pointSerialized);
            return CORRECT_OPERATION;
        }
    }

}
int RERR_ReceptionProcess(T_RERR *rerr, T_DataPoint *DataPoint, T_DataPacket *DataPacket,
                          T_LocalRouteSet *invRoutes){

	int err;
	uint16_t seqnum;
	uint32_t address;
	T_LocalRoute invalid_route;
    uint8_t metrictype;
    uint8_t prefixlength;
    T_RouterClient RouterClient;

    ini_LocalRouteSet(invRoutes);
    err = get_RouterClientSet_RouterClient_Index(get_DataPointers_RouterClientSet(DataPoint), &RouterClient, 0);
    if( err != CORRECT_OPERATION){

        printf("RERR_ReceptionProcess : get_RouterClientSet_RouterClient_Index: FAIL\n");
        return FAIL_OPERATION;
    }
	goto STEP_1;

	STEP_1:{

		if(rerr->AddressListRERR.overall <= 0){

			printf("AddresList doRerrMsges not have any IPAddress\n");
			return IGNORE_MESSAGE;
		}

		goto STEP_2;
	}
	STEP_2:{

        for (int i = 0; i < get_RERR_AdressList_Overall(rerr); i++) {

            metrictype = get_RERR_MetricTypeList_Index(rerr, i);
            prefixlength = get_RERR_PrefixLengthList_Index(rerr,i);
            seqnum = get_RERR_SeqnumList_Index(rerr,i);
            address = get_RERR_AdressList_Index(rerr, i);
            /* The IPAddress is valid (routable and unicast). */
            err = valid_Address(rerr->AddressListRERR.Addresses[i]);
            if (err != TRUE) {

                printf("valid_Address FAIL\n");
                return FAIL_OPERATION;
            }
            /*The MetricType is supported and configured for use.*/
            err = check_MetricType(get_DataPoint_MetricSet(DataPoint), metrictype);
            if (err != TRUE) {

                printf("check_MetricType FAIL\n");
                return FAIL_OPERATION;
            }

            for(int a = 0;a < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); a++){

                err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &invalid_route, a);
                if(err != CORRECT_OPERATION){

                    printf("get_LocalRouteSet_LocalRoute_Index FAIL\n");
                    return FAIL_OPERATION;
                }

                /*There is a LocalRoute with the same MetricType matching the IPAddress using longest PrefixLength matching.*/
                if((get_LocalRoute_Address(&invalid_route) == address) || (get_LocalRoute_Metrictype(&invalid_route) == metrictype)){

                    if(((get_LocalRoute_Nexthop(&invalid_route) == get_DataPacket_sourceIP(DataPacket)) &&
                   (strcmp(get_LocalRoute_NextHopInterface(&invalid_route), get_DataPacket_IntDev(DataPacket)) == 0)) ||
                   ((rerr->PktSource != NULL) && (get_RERR_PktSource_Address(rerr) == get_RouterClient_Address(&RouterClient))))
                    {

                        if(seqnum == 0 || seqnum >= get_LocalRoute_Seqnum(&invalid_route)){

                            uint8_t state = get_LocalRoute_State(&invalid_route);
                            if(get_LocalRoute_Prefixlength(&invalid_route) == prefixlength){

                                add_LocalRoute_State(&invalid_route,INVALID);
                                update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&invalid_route);
                                if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&invalid_route)) == TIMEOUT_ONGOING || get_status (
                                        get_LocalRoute_TimerMaxSeqNumLifeTime (&invalid_route)) == TIMEOUT_RESTARTED ){

                                    abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&invalid_route));
                                }
                            }else if(get_LocalRoute_Prefixlength(&invalid_route) < prefixlength) {

                                del_LocalRouteSet_LocalRoute_Position(get_DataPoint_LocalRouteSet(DataPoint),&invalid_route);
                                if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&invalid_route)) == TIMEOUT_ONGOING || get_status (
                                        get_LocalRoute_TimerMaxSeqNumLifeTime (&invalid_route)) == TIMEOUT_RESTARTED ){

                                    abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&invalid_route));
                                }

                            }else {

                                T_LocalRoute newlocalroute;
                                add_LocalRoute_Address(&newlocalroute,address);
                                add_LocalRoute_Prefixlength(&newlocalroute,prefixlength);
                                add_LocalRoute_Seqnum(&newlocalroute,seqnum);
                                add_LocalRoute_State(&newlocalroute,INVALID);
                                err = add_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&newlocalroute);
                                if(err != CORRECT_OPERATION){

                                    printf("RERR_ReceptionProcess : add_LocalRouteSet_LocalRoute FAIL\n");
                                    return FAIL_OPERATION;
                                }
                            }
                            if(get_LocalRoute_Seqnum(&invalid_route) < seqnum){

                                add_LocalRoute_Seqnum(&invalid_route,seqnum);
                                update_LocalRouteSet_LocalRoute(get_DataPoint_LocalRouteSet(DataPoint),&invalid_route);
                                if(get_status (get_LocalRoute_TimerMaxSeqNumLifeTime (&invalid_route)) == TIMEOUT_ONGOING || get_status (
                                        get_LocalRoute_TimerMaxSeqNumLifeTime (&invalid_route)) == TIMEOUT_RESTARTED ){

                                    abort_timer (get_LocalRoute_TimerMaxSeqNumLifeTime (&invalid_route));
                                }
                            }
                            if(state == IDLE || state == ACTIVE){

                                err = add_LocalRouteSet_LocalRoute(invRoutes,&invalid_route);
                                if( err != CORRECT_OPERATION){

                                    printf("RERR_ReceptionProcess : add_LocalRouteSet_LocalRoute : FAIL\n");
                                    return FAIL_OPERATION;
                                }
                                if (get_LocalRoute_Nexthop(&invalid_route) == get_LocalRoute_Address(&invalid_route)) {

                                    del_RIB_Host(&invalid_route,DataPoint);
                                } else {

                                    del_RIB_Route(&invalid_route,DataPoint);
                                }
                            }
                        }
                    }
                }
            }
        }
        goto STEP_3;
    }
    STEP_3:{

        if (get_LocalRouteSet_Overall(invRoutes) > 0) {
            return REGENERATE_RERR;
        } else {
            return CORRECT_OPERATION;
        }
    }
}
int rcv_RERR_Packet(T_RERR *rerr, T_DataPoint *DataPoint, T_DataPacket *DataPacket,
                    T_LocalRouteSet *invRoutes, T_PointParsed *pointers_parse){

    int err;
    /*By parsing the received message, the packet is obtained in the format defined by RFC5444*/
    pointer_parsed_NULL(pointers_parse);
    T_PgmanPacket packet = get_parse_array(DataPacket->PayLoad,pointers_parse);
    /*From a package in the RFC5444 format you get the RERR container*/
    *rerr = get_parse_container_rerr(packet,pointers_parse);
    /*The RERR Reception process defined in the draft is applied [Page 51]*/
    print_RERR (rerr);
    err = RERR_ReceptionProcess(rerr, DataPoint, DataPacket, invRoutes);
    return err;
}
int RERR_ReGenerationProcess(T_DataPoint *DataPoint, T_RERR *rerr, T_LocalRouteSet *invRoutes) {

    int err;
    uint8_t size = 0;
    uint32_t broadaddr;
    T_CIDR temp;
    T_AddressListRERR list;
    T_LocalRoute res_Route;
    T_RERR regen_RERR;
    T_RerrPoint RerrPoint;
    T_RouterClient RouterClient;
    T_PointSerialized PointSerialized;

    err = get_RouterClientSet_RouterClient_Index(get_DataPointers_RouterClientSet(DataPoint), &RouterClient, 0);
    if (err != CORRECT_OPERATION) {

        printf("RERR_ReceptionProcess : get_RouterClientSet_RouterClient_Index: FAIL\n");
        return FAIL_OPERATION;
    }
    T_CIDR *pktsource = (T_CIDR *) malloc(sizeof(T_CIDR));
    T_PrefixLenList *prefixlengthlist = (T_PrefixLenList *) malloc(sizeof(T_PrefixLenList));
    T_SeqNumList *seqnumlist = (T_SeqNumList *) malloc(sizeof(T_SeqNumList));
    T_MetricTypeList *metrictypelist = (T_MetricTypeList *) malloc(sizeof(T_MetricTypeList));
    if (prefixlengthlist == NULL || seqnumlist == NULL || metrictypelist == NULL || pktsource == NULL) {

        free(pktsource);
        free(prefixlengthlist);
        free(seqnumlist);
        free(metrictypelist);
        return NOENOUGHT_MEMORY;
    }
    null_RERR(&RerrPoint);
    goto STEP_1;

    STEP_1:
    {
        /*If PktSource was included in the original RERR, and PktSource is not a Router Client*/
        if (get_RERR_PktSource(rerr) != NULL) {

            if (get_RouterClient_Address(&RouterClient) != get_RERR_PktSource_Address(rerr)) {

                add_RERR_PktSource(&regen_RERR, get_RERR_PktSource(rerr));
            }
        }
        goto STEP_2;
    }
    STEP_2:
    {

        RerrPoint.PrefixLenList = prefixlengthlist;get_RerrPoint_PrefixLenList(&RerrPoint);
        RerrPoint.SeqNumList = seqnumlist;
        RerrPoint.MetricTypeList = metrictypelist;
        ini_AddressListRERR(&list);
        ini_PrefixLenList(prefixlengthlist);
        ini_SeqNumList(seqnumlist);
        ini_MetricTypeList(metrictypelist);
        for (int i = 0; i < invRoutes->overall; i++) {

            temp = get_CIDR(invRoutes->LocalRoute[i].Address,invRoutes->LocalRoute[i].PrefixLength);
            if ((add_AddressListRERR_Address(temp, &list) != ADDED_ITEM) ||
            (add_PrefixLenList_PrefixLen(invRoutes->LocalRoute[i].PrefixLength, prefixlengthlist) != ADDED_ITEM) ||
            (add_SeqNumList_SeqNum(invRoutes->LocalRoute[i].SeqNum, seqnumlist) != ADDED_ITEM) ||
            (add_MetricTypeList_MetricTye(invRoutes->LocalRoute[i].MetricType, metrictypelist) != ADDED_ITEM)) {

                free_RERR(&RerrPoint);
                null_RERR(&RerrPoint);
                return FULL_ARRAY;
            }
        }
        goto SEND_RERR;
    }

    SEND_RERR:{

        regen_RERR = create_RERR(get_RerrPoint_PktSource(&RerrPoint), list, prefixlengthlist, seqnumlist, metrictypelist);
        /*The message is constructed with the RFC5444 format from the RERR container*/
        nullPointSerialized(&PointSerialized);
        T_PgmanPacket packet = get_RERR_Packet(regen_RERR, &PointSerialized);
        /*The package is serialized*/
        uint8_t *buffer = get_Packet_Array(packet, &size, &PointSerialized);
        /*If the RERR is sent in response to a broken link, i.e., PktSource is
        not included, the RERR is, by default, multicast to LL-MANET-Routers.*/
        if (get_RerrPoint_PktSource(&RerrPoint) == NULL) {

            for (int i = 0; i < get_InterfaceSet_Overall(get_DataPoint_InterfaceSet(DataPoint)); i++) {

                broadaddr = get_BroadcastAddress(DataPoint->point_InterfaceSet->Interface[i].InterfaceId);
                err = send_PacketUdpBroadcast(buffer, size, broadaddr);
                if (err != CORRECT_OPERATION) {

                    printf("send_PacketUdpBroadcast: FAIL\n");
                    freePointSerialized(&PointSerialized);
                    nullPointSerialized(&PointSerialized);
                    free_RERR(&RerrPoint);
                    null_RERR(&RerrPoint);
                    return err;
                }
            }
        } else {

            err = get_LocalRouteSet_LocalRoute_Address(get_DataPoint_LocalRouteSet(DataPoint),&res_Route , get_RERR_PktSource_Address(&regen_RERR));
            if (err != FOUND_ITEM) {

                freePointSerialized(&PointSerialized);
                nullPointSerialized(&PointSerialized);
                free_RERR(&RerrPoint);
                null_RERR(&RerrPoint);
                return NOTFOUND_ITEM;
            }
            err = send_PacketUdpUnicast(buffer, size, get_LocalRoute_Nexthop(&res_Route),get_LocalRoute_NextHopInterface(&res_Route));
            if (err != CORRECT_OPERATION) {

                freePointSerialized(&PointSerialized);
                nullPointSerialized(&PointSerialized);
                free_RERR(&RerrPoint);
                null_RERR(&RerrPoint);
                printf("send_PacketUdpUnicast  FAIL\n");
                return err;
            }
            freePointSerialized(&PointSerialized);
            nullPointSerialized(&PointSerialized);
            free_RERR(&RerrPoint);
            null_RERR(&RerrPoint);
            return CORRECT_OPERATION;
        }

    }
}