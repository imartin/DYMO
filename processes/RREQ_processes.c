#include <global_structs.h>
#include "RREQ_processes.h"
#include "../event_definition.h"
#include "../RFC5444/parse_container.h"
#include "../RFC5444/gman_packet_serialized.h"
#include "../processes/suppressing_redundant_messages_process.h"
#include "../processes/evaluating_route_information_process.h"
#include "../processes/applying_route_updates_process.h"
#include "../operative_system/operative_system_functions.h"



int send_RREQ_Packet(T_RouterClient *RouterClient, T_DataPacket *DataPacket, T_DataPoint *DataPoint){

	int err;
	uint8_t size = 0;
	uint8_t *buffer = NULL;
	T_PgmanPacket packet;
	T_RREQ rreq;
	T_PointSerialized pointers;

    nullPointSerialized(&pointers);
	err = RREQ_GenerationProcess(&rreq, DataPacket, RouterClient, DataPoint);
	if(err != CORRECT_OPERATION) {

		printf("RREQ_GenerationProcess : %s\n", Int_To_StringEvent(err));
		return err;
	}
	packet = get_RREQ_Packet(rreq, &pointers);
	buffer = get_Packet_Array(packet, &size, &pointers);
    //print_packet (buffer,size);
	for(int i = 0; i < DataPoint->point_InterfaceSet->overall; i++){

		if(send_PacketUdpBroadcast(buffer, size, get_BroadcastAddress(get_DataPoint_InterfaceId(DataPoint))) == FAIL_OPERATION){

			printf("send_PacketUdpBroadcast: FAIL\n");
            freePointSerialized(&pointers);
            nullPointSerialized(&pointers);
			return FAIL_OPERATION;
		}
		if(SupRedMsg_Process(DataPoint, &rreq, get_DataPoint_InterfaceId(DataPoint)) == FAIL_OPERATION){

			printf("SupRedMsg_Process:  FAIL\n");
            freePointSerialized(&pointers);
            nullPointSerialized(&pointers);
			return FAIL_OPERATION;
		}
	}
    freePointSerialized(&pointers);
    nullPointSerialized(&pointers);
	return CORRECT_OPERATION;
}
int RREQ_ReceptionProcess(T_DataPoint *DataPoint, T_ADVRTE *advrte, T_DataPacket *DataPacket, T_RREQ *rreq,T_PointParsed *pointers_parse){

	int err;
	T_LocalRouteSet  matching_routes;
	T_RouterClient RouterClient;

    T_PgmanPacket packet = get_parse_array(DataPacket->PayLoad,pointers_parse);
    //print_packet(DataPacket->PayLoad,DataPacket->size_payload);
    *rreq = get_parse_container_rreq(packet,pointers_parse);


	uint32_t  origprefix = get_RREQ_OrigPrefix(rreq);
	uint32_t  targprefix = get_RREQ_TargPrefix(rreq);
    err = get_RouterClientSet_RouterClient_Index(get_DataPointers_RouterClientSet(DataPoint), &RouterClient, 0);
    if(err != CORRECT_OPERATION){

        printf("RREQ_ReceptionProcess : get_RouterClientSet_RouterClient_Index FAIL\n");
    }
	ini_LocalRouteSet(&matching_routes);
	if(origprefix == get_RouterClient_Address(&RouterClient)){

		printf ("RREQ CON ORIGPREFIX IGUAL A ROUTER CLIENT\n");
		return IGNORE_MESSAGE;
	}
	goto STEP_1;

    STEP_1:{

        err = RREQ_NeighborSetUpdateProcess(DataPoint, DataPacket);
        if(err != CORRECT_OPERATION){

            printf("RREQ_NeighborSetUpdateProcess:  FAIL %s\n", Int_To_StringEvent(err));
            return IGNORE_MESSAGE;
        }
        goto STEP_2;
    }

	STEP_2:{

        if(check_RREQ(rreq) == FALSE){

            printf("check_RREQ  ignore rreq\n");
            return IGNORE_MESSAGE;
        }
        goto STEP_3;
    }

	STEP_3:{

        if(check_MetricType(get_DataPoint_MetricSet(DataPoint),get_RREQ_MetricType(rreq)) == FALSE){

            printf("check_MetricType: ignore rreq\n");
            return IGNORE_MESSAGE;
        }
        goto STEP_4;
    }

	STEP_4:{

        if(check_maximum_metric_value(get_RREQ_OrigMetric(rreq), get_DataPoint_MetricSet(DataPoint)) == FALSE){

            printf("check_maximum_metric_value:  ignore rreq\n");
            return IGNORE_MESSAGE;
        }
        goto STEP_5;
    }

	STEP_5:{

        *advrte = RREQ_ProcessingReceivedRouteInformation(rreq, get_DataPacket_sourceIP(DataPacket),
                                                          get_DataPoint_MetricSet(DataPoint));
        err = EvalRouteInf_Process(advrte, get_DataPoint_LocalRouteSet(DataPoint),&matching_routes);
        if(err == FAIL_OPERATION){

            printf("EvalRouteInf_Process: FAIL\n");
            return FAIL_OPERATION;
        }
        if(err == NOT_UPDATE){

            return IGNORE_MESSAGE;
        }
        if(AppRouteUpdate_Process(DataPoint, get_DataPacket_IntDev(DataPacket),advrte,&matching_routes) == FAIL_OPERATION){

            printf("AppRouteUpdate_Process: fail operation\n");
            return FAIL_OPERATION;
        }
        goto STEP_6;
    }

    STEP_6:{

        err = SupRedMsg_Process(DataPoint,rreq, get_DataPacket_IntDev(DataPacket));
        if(err == REDUNDANT){

            return IGNORE_MESSAGE;
        }
        if(err == FULL_ARRAY){

            return FAIL_OPERATION;
        }
        goto STEP_7;
    }

	STEP_7:{

    if (get_RouterClient_Address(&RouterClient) == targprefix) {
        return GENERATE_RREP;
    } else {
        return FORWARDING_RREQ;
    }
    }
}
int RREQ_GenerationProcess(T_RREQ *rreq,T_DataPacket *DataPacket,T_RouterClient *RouterClient,T_DataPoint *DataPoint) {

    int err;
    goto STEP_1;

    STEP_1:{

	    add_RREQ_MsgHopLimit(rreq,MAX_HOPCOUNT - 1);
        //printf("RREQ_MSHOPLIMIT = %u\n",rreq->MsgHopLimit);
        goto STEP_2;
    }
    STEP_2:{

        add_RREQ_OrigPrefix(rreq,get_RouterClient_Address(RouterClient),get_RouterClient_PrefixLen(RouterClient));
        add_RREQ_TargPrefix(rreq, get_DataPacket_destIP(DataPacket),32);
        goto STEP_3;
    }
    STEP_3:{
        /*No se ha utilizado PrefixLenList para los mensajes RREQ*/
        goto STEP_4;
    }
    STEP_4:{

        pthread_mutex_lock(DataPoint->Mutex);
        increment_SeqNumSet(get_DataPoint_SeqNumSet(DataPoint));
        add_RREQ_OrigSeqNum(rreq, get_SeqNumSet(get_DataPoint_SeqNumSet(DataPoint)));
        pthread_mutex_unlock(DataPoint->Mutex);
		goto STEP_5;
    }
    STEP_5:{

        T_LocalRoute match_route;
        for (int i = 0; i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++) {

            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &match_route, i);
            if (err == CORRECT_OPERATION) {

                if (get_LocalRoute_State(&match_route) == INVALID) {

                    if (get_LocalRoute_Address(&match_route) == get_DataPacket_destIP(DataPacket)) {

                        add_RREQ_TargSeqNum(rreq,get_LocalRoute_Seqnum(&match_route));
                    } else {

                        add_RREQ_TargSeqNum(rreq, 0);// TarseqNum omitido;
                    }
                }
            } else {

                printf("get_LocalRouteSet_LocalRoute_Index: %s\n", Int_To_StringEvent(err));
                return FAIL_OPERATION;
            }
        }
        goto STEP_6;
    }
    STEP_6:{

        add_RREQ_MetricType(rreq,get_MetricSet_MetricType(get_DataPoint_MetricSet(DataPoint)));
        goto STEP_7;
    }
    STEP_7:{

        add_RREQ_OrigMetric(rreq,get_RouterClient_Cost(RouterClient));
        return CORRECT_OPERATION;
    }
}

int RREQ_ForwardingProcess(T_RREQ *rreq,T_DataPoint *DataPoint){

	int err;
    bool FLAG_LOCALROUTE;
    T_LocalRoute orig_route;
	goto STEP_1;

	STEP_1:{

        add_RREQ_MsgHopLimit(rreq,(uint8_t)(get_RREQ_MsgHopLimit(rreq) - 1));

        goto STEP_2;
    }
	STEP_2:{

        if(get_RREQ_MsgHopLimit(rreq) == 0){

            printf("RREQ FORWARDING PROCESS: limit hope\n");
            return FAIL_OPERATION;
        }
        goto STEP_3;
    }
	STEP_3:{

        FLAG_LOCALROUTE = FALSE;
        for(int i = 0;i < get_LocalRouteSet_Overall(get_DataPoint_LocalRouteSet(DataPoint)); i++){

            err = get_LocalRouteSet_LocalRoute_Index(get_DataPoint_LocalRouteSet(DataPoint), &orig_route, i);
            if (err != CORRECT_OPERATION){

                printf("RREQ_ForwardingProcess : get_LocalRouteSet_LocalRoute_Index\n");
                return FAIL_OPERATION;
            }
            if(get_RREQ_OrigPrefix(rreq) == get_LocalRoute_Address(&orig_route)){

                add_RREQ_OrigMetric(rreq,get_LocalRoute_Metric(&orig_route));
                FLAG_LOCALROUTE = TRUE;
            }
        }
    if (FLAG_LOCALROUTE == FALSE) {
        return FAIL_OPERATION;
    } else {
        return CORRECT_OPERATION;
    }
    }
}
int RREQ_NeighborSetUpdateProcess(T_DataPoint *DataPoint,T_DataPacket *DataPacket){

	T_Neighbor Neighbor;
	int err = get_NeighborSet_Neighbor_Address(get_DataPoint_NeighborSet(DataPoint),&Neighbor,
                                               get_DataPacket_sourceIP(DataPacket));
	if(err == NOTFOUND_ITEM){

		add_Neighbor_IPaddress(&Neighbor, get_DataPacket_sourceIP(DataPacket));
		add_Neighbor_State(&Neighbor,HEARD);
		add_Neighbor_Timeout(&Neighbor,INFINITY_TIME);
		add_Neighbor_Interface(&Neighbor, get_DataPacket_IntDev(DataPacket));
		err = add_NeighborSet_Neighbor(DataPoint->point_NeighborSet,Neighbor);
		if( err != CORRECT_OPERATION){

			return err;
		}
		return CORRECT_OPERATION;
	}
	if(get_Neighbor_State(&Neighbor) == BLACKLISTED){

		return BLACKLIST;
	}
	return CORRECT_OPERATION;
}
T_ADVRTE RREQ_ProcessingReceivedRouteInformation(T_RREQ *rreq,uint32_t SourceAddress,T_MetricSet *MetricSet){

    return create_ADVRTE(get_RREQ_OrigPrefix(rreq),get_RREQ_OrigPrefixLen(rreq),get_RREQ_OrigSeqNum(rreq),SourceAddress,get_RREQ_MetricType(rreq),get_RREQ_OrigMetric(rreq),
           calculate_incoming_link_cost(get_RREQ_OrigMetric(rreq),MetricSet));
}
int forwar_RREQ_Packet(T_RREQ *rreq,T_DataPoint *DataPoint){

    T_PgmanPacket packet;
    uint8_t *buffer = NULL;
    uint8_t size = 0;
    T_PointSerialized pointers;
    nullPointSerialized(&pointers);
    if(RREQ_ForwardingProcess(rreq, DataPoint) == FAIL_OPERATION){

        freePointSerialized(&pointers);
        nullPointSerialized(&pointers);
        printf("RREQ_ForwardingProcess: FAIL\n");
        return FAIL_OPERATION;
    }
    packet = get_RREQ_Packet(*rreq, &pointers);
    buffer = get_Packet_Array(packet, &size, &pointers);
    for(int i = 0;i < DataPoint->point_InterfaceSet->overall; i++){

        if(send_PacketUdpBroadcast(buffer, size, get_BroadcastAddress(get_DataPoint_InterfaceId(DataPoint))) == FAIL_OPERATION){

            printf("send_PacketUdpBroadcast : Interface %d FAIL\n",i);
            freePointSerialized(&pointers);
            nullPointSerialized(&pointers);
            return FAIL_OPERATION;
        }
    }
    freePointSerialized(&pointers);
    nullPointSerialized(&pointers);
    return CORRECT_OPERATION;
}