#ifndef RERR_PROCESSES_H
#define RERR_PROCESSES_H


#include "../data_set/LocalRouteSet.h"
#include "../RFC5444/parse_container.h"
#include "../global_structs.h"

int RERR_ReceptionProcess(T_RERR *rerr, T_DataPoint *DataPoint, T_DataPacket *DataPacket,T_LocalRouteSet *invRoutes);
int rcv_RERR_Packet(T_RERR *rerr, T_DataPoint *DataPoint, T_DataPacket *DataPacket,T_LocalRouteSet *invRoutes, T_PointParsed *pointers_parse);
int RERR_ReGenerationProcess(T_DataPoint *DataPoint, T_RERR *rerr, T_LocalRouteSet *invRoutes);
int RERR_GenerationProcess(T_DataPoint *DataPoint, T_DataPacket *DataPacket,T_RerrPoint *RERRpoint,int Event);
#endif