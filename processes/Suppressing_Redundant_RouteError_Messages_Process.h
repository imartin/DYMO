//
// Created by root on 5/04/18.
//

#ifndef PROGRAMA_V4_SUPPRESSING_REDUNDANT_ROUTEERROR_MESSAGES_PROCESS_H
#define PROGRAMA_V4_SUPPRESSING_REDUNDANT_ROUTEERROR_MESSAGES_PROCESS_H

#include "../data_set/RouteErrorSet.h"
#include "../global_structs.h"

int SupRedRouteErrorMsgProcesss(T_RerrMsgSet *RerrMsgSet, uint32_t UnreachableAddress, T_CIDR *PktSource,T_DataPoint *DataPoint);
#endif //PROGRAMA_V4_SUPPRESSING_REDUNDANT_ROUTEERROR_MESSAGES_PROCESS_H
