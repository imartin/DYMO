#ifndef EVALUATING_ROUTE_INFORMATION_PROCESS_H
#define EVALUATING_ROUTE_INFORMATION_PROCESS_H

#include <curses.h>
#include "containers/ADVRTE_container.h"
#include "../data_set/LocalRouteSet.h"


int EvalRouteInf_Process(T_ADVRTE *advrte_container, T_LocalRouteSet *LocalRouteSet, T_LocalRouteSet *matching_routes);


#endif